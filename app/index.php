<?php

$uri = System::request('uri');

$options = explode("/", $uri);

$action = "";
$value = "";

if(isset($options[0])){
  $action = $options[0];
}
if(isset($options[1])){
  $value = $options[1];
}

if($action){
  switch($action){
    case 'financeiro':
      header('Content-type: text/html; charset=utf-8');

      System::desire('c', 'principal', 'Financeiro', 'src');
      System::desire('c', 'principal', 'Atleta', 'src');

      $financeiroCtrl = new FinanceiroCtrl(PATH_APP);
      $financeiros = $financeiroCtrl->getFinanceiroCtrl("MD5(fnc_codigo) = '".$value."'", "", "");

      $atletaCtrl = new AtletaCtrl(PATH_APP);

      if(is_array($financeiros)){

        $financeiro = $financeiros[0];

        $atletas = $atletaCtrl->getAtletaCtrl("atl_codigo = '".$financeiro->get_fnc_value('fnc_cod_ATLETA')."'", "", "");

        if(is_array($atletas)){

          $atleta = $atletas[0];

          $dadosboleto["nosso_numero"] = $financeiro->get_fnc_value('fnc_codigo');
          $dadosboleto["numero_documento"] = $financeiro->get_fnc_value('fnc_codigo');
          $dadosboleto["data_vencimento"] = $financeiro->get_fnc_value('fnc_data_vencimento');
          $dadosboleto["data_documento"] = date("d/m/Y");
          $dadosboleto["data_processamento"] = date("d/m/Y");
          $dadosboleto["valor_boleto"] = Number::format($financeiro->get_fnc_value('fnc_valor_total'));

          $dadosboleto["sacado"] = htmlentities($atleta->get_atl_value('atl_nome'));
          $dadosboleto["endereco1"] = htmlentities($atleta->get_atl_value('atl_endereco'));
          //"Cidade - Estado -  CEP: 00000-000"
          $endereco = $atleta->get_atl_value('atl_cep');
          $dadosboleto["endereco2"] = htmlentities($endereco);

          $dadosboleto["demonstrativo1"] = htmlentities("");
          $dadosboleto["demonstrativo2"] = htmlentities("");
          $dadosboleto["demonstrativo3"] = htmlentities("");

          $dadosboleto["instrucoes1"] = htmlentities("- Sr. Caixa, cobrar multa de 2% ap�s o vencimento");
          $dadosboleto["instrucoes2"] = htmlentities("- Receber at� 10 dias ap�s o vencimento");
          $dadosboleto["instrucoes3"] = htmlentities("- Em caso de d�vidas entre em contato conosco: ".EMAIL_REMETENTE);
          $dadosboleto["instrucoes4"] = htmlentities("");

          $dadosboleto["quantidade"] = "10";
          $dadosboleto["valor_unitario"] = "10";
          $dadosboleto["aceite"] = "N";
          $dadosboleto["especie"] = "R$";
          $dadosboleto["especie_doc"] = "DM";

          $dadosboleto["agencia"] = "1534";
          $dadosboleto["conta"] = "24614";

          $dadosboleto["convenio"] = "0001534";
          $dadosboleto["contrato"] = "024614";
          $dadosboleto["carteira"] = "18";
          $dadosboleto["variacao_carteira"] = "-019";

          $dadosboleto["formatacao_convenio"] = "7";
          $dadosboleto["formatacao_nosso_numero"] = "2";

          $dadosboleto["identificacao"] = htmlentities(TITLE);
          $dadosboleto["cpf_cnpj"] = COMPANY_CNPJ;
          $dadosboleto["endereco"] = htmlentities(COMPANY_ADDRESS);
          $dadosboleto["cidade_uf"] = htmlentities(COMPANY_STATE." / ".COMPANY_CITY);
          $dadosboleto["cedente"] = htmlentities(COMPANY);

          define('IMAGES', PAGE_APP."lib/boletophp/imagens/");

          include("../lib/boletophp/include/funcoes_bb.php");
          include("../lib/boletophp/include/layout_bb.php");

        }

      }else{

        System::showMessage("T&iacute;tulo n&atilde;o encontrado!");

      }

      break;

    default:
      break;
  }
}

?>
