<?php

class WebService {

  /**
   * 
   * @param string $server
   * @return array
   */
  public final function getFunctions($server) {

    $functions = array();

    $reflection = new ReflectionClass($this);

    $methods = $reflection->getMethods();

    foreach ($methods as $method) {
      if ($method->isPublic() && !$method->isFinal()) {

        $annotations = $this->getAnnotations($method->getDocComment());

        $doc = "";
        if (isset($annotations['doc'])) {
          $doc = $annotations['doc'];
        }

        $parameters = $method->getParameters();
        $params = array();
        if (is_array($parameters)) {

          foreach ($parameters as $parameter) {
            $type = "mixed";
            if (isset($annotations['param']['$' . $parameter->getName()])) {
              $type = $annotations['param']['$' . $parameter->getName()];
            }
            $params[] = array("name" => $parameter->getName(), "type" => $type);
          }
        }

        $returnType = 'void';
        if (isset($annotations['return'])) {
          $returnType = $annotations['return'];
        }

        $functions[] = array(
          "procedure" => $method->getName(),
          "doc" => $doc,
          "params" => $params,
          "output" => array(array("name" => $method->getName() . "ResponseType", "type" => $returnType)),
          "uri" => $server
        );
      }
    }

    return $functions;
  }

  /**
   * 
   * @param type $documentation
   * @return array
   */
  private function getAnnotations($documentation) {

    $tags = array(
      "doc" => "single",
      "param" => "multiple",
      "return" => "single"
    );
    $annotations = array();

    $doc = explode("*", $documentation);
    foreach ($tags as $tag => $type) {
      $id = "@" . $tag;
      foreach ($doc as $line) {
        if (strpos($line, $id)) {
          $line = trim(preg_replace("/[[:blank:]]+/", " ", String::replace($line, $id, "")));
          if ($type === "single") {
            $annotations[$tag] = System::encode($line, 'encode');
          } else {
            $values = explode(" ", $line);
            if (isset($values[1])) {
              $annotations[$tag][$values[1]] = System::encode($values[0]);
            }
          }
        }
      }
    }
    return $annotations;
  }

  /**
   * 
   * @param string $id
   * @param string $function
   * @param boolean $register
   * @param boolean $register
   * 
   * @return boolean
   */
  private function login($id, $function = "", $register = false, $content = "") {

    $ip = "";
    if (isset($_SERVER['REMOTE_ADDR'])) {
      $ip = $_SERVER['REMOTE_ADDR'];
    }

    $logged = false;

    $modulo = System::import('c', 'organizacao', 'Instituicao', 'src', false, '');
    if (file_exists($modulo)) {

      System::import('c', 'organizacao', 'Instituicao', 'src', true, '');

      $instituicaoCtrl = new InstituicaoCtrl(PATH_APP);
      $ist_identificador = $instituicaoCtrl->getColumnInstituicaoCtrl("ist_identificador", "ist_chave = '" . $id . "'");
      if (!is_null($ist_identificador)) {
        if (!defined('DEFAULT_DATABASE_IDENTIFICATOR')) {
          define('DEFAULT_DATABASE_IDENTIFICATOR', $ist_identificador);
        }
        $logged = true;
      }
    }

    if ($register) {
      $filename = "access.txt";
      $fp = fopen($filename, "w+");
      $time = date('d/m/Y H:i:s');
      fwrite($fp, " - " . $time . " :: " . $ip . "[" . $id . "/" . String::booleanText($logged) . "/" . $function . "]" . "(" . $content . ")" . "\r\n");
    }

    return $logged;
  }

  /**
   * @doc Salva remotamente um arquivo
   * 
   * @param string $id
   * @param string $filename
   * @param string $contents
   * 
   * @return boolean
   */
  public function saveFile($id, $filename, $contents) {

    $save = 'false';

    $logged = $this->login($id, "saveFile");
    if ($logged) {

      if (file_exists($filename) && strpos($filename, PATH_APP)) {
        unlink($filename);
      }

      $save = File::saveFile($filename, base64_decode($contents));
      if ($save) {
        chmod($filename, 0755);
      }

    }

    return $save;
  }

  /**
   * @doc Sincroniza os dados entre os servidores da institui��o
   * 
   * @param string $cliente
   * @param string $operacao
   * @param string $modulo
   * @param string $entidade
   * @param string $requisicao
   * 
   * @return string
   */
  public function synchronize($cliente, $operacao, $modulo, $entidade, $requisicao) {

    ob_start();

    System::import('c', 'cadastro', 'Sincronizacao', 'src', true, '');

    $sincronizacaoCtrl = new SincronizacaoCtrl(PATH_APP);

    $synchronize = '';
    $logged = $this->login($cliente, "synchronize", true, $requisicao);

    if (!$logged) {

      $synchronize = json_encode(array("status"=>'Fail', "message"=>base64_encode('Acesso Incorreto'), "log"=>array()));

    } else {
      
      $module = System::import('m', $modulo, $entidade, 'src', false, '');

      if (file_exists($module)) {
      
      	System::import('m', $modulo, $entidade, 'src', true, '');
        System::import('c', $modulo, $entidade, 'src', true, '');

        $sincronizacaoCtrl->createSincronizacaoCtrl($cliente, $operacao, $modulo, $entidade, $requisicao);

        if ($operacao === 'enviar') {

          $registros = json_decode('{"rows":' . $requisicao . '}');
          $rows = is_object($registros) ? $registros->rows : null;
          $synchronize = $this->receiveSynchronize($rows, $entidade);

        } else if ($operacao === 'receber') {

          $synchronize = $this->sendSynchronize($entidade);

        }

      } else {

        $synchronize = json_encode(array("status"=>'Fail', "message"=>base64_encode('Classe não encontrada'), "log"=>array()));

      }

    }
      
    ob_end_clean();

    return $synchronize;
  }
  
  /**
   * @doc Sincroniza os dados entre os servidores da institui��o
   * 
   * @param string $cliente
   * @param string $modulo
   * @param string $entidade
   * 
   * @return string
   */
  public function last($cliente, $modulo, $entidade) {

    ob_start();

    System::import('c', 'cadastro', 'Sincronizacao', 'src', true, '');

    $sincronizacaoCtrl = new SincronizacaoCtrl(PATH_APP);

    $logged = $this->login($cliente, "synchronize", true, $requisicao);

    $retorno = '';
    $status = 'Fail';
    $last = 'Cliente n�o encontrado em nossa base de dados';
    if ($logged) {

      $status = 'Sucess';
      $last = $sincronizacaoCtrl->getUltimaSincronizacaoCtrl($cliente, $modulo, $entidade);

    }
    
    $retorno = json_encode(array("status"=>$status, "message"=>base64_encode($last), "log"=>array()));

    ob_end_clean();
    
    return $retorno;
  }

  /**
   * 
   * @param type $class
   * @return string
   */
  private function getPrefix($class) {
    $prefix = "";

    $reflection = new ReflectionClass($class);

    $methods = $reflection->getMethods();

    foreach ($methods as $method) {
      $name = $method->getName();
      if (strpos($name, "properties")) {
        $prefix = String::replace(String::replace($name, "_properties", ""), "get_", "");
      }
    }

    return $prefix;
  }

  /**
   * 
   * @param type $rows
   * @param type $target
   * 
   * @return string
   */
  private function receiveSynchronize($rows, $target) {

    $message = "";
    $status = "";
    $log = array();

    if (is_array($rows)) {

      $total = 0;
      $processado = 0;

      $prefix = $this->getPrefix($target);
      $class = $target;

      $classCtrl = $class . "Ctrl";
      $getClassCtrl = "get" . $classCtrl;
      $addClassCtrl = "add" . $classCtrl;
      $setClassCtrl = "set" . $classCtrl;
      $getReference = "get_" . $prefix . "_reference";
      $getItems = "get_" . $prefix . "_items";
      $setValue = "set_" . $prefix . "_value";

      $model = new $class();
      $modelCtrl = new $classCtrl(PATH_APP);

      $reference = $model->$getReference();
      $items = $model->$getItems();
      $id = $reference . "_externo";

      foreach ($rows as $row) {

        $ok = false;
        $update = false;
        $object = new $class();

        if (isset($row->$id)) {

          $objects = $modelCtrl->$getClassCtrl($id . " = '" . base64_decode($row->$id) . "'", "", "", "0", "1");
          $object = new $class();

          $update = is_array($objects);
          if ($update) {
            $object = $objects[0];
          }

        }

        foreach ($items as $item) {

          $key = $item['id'];
          if (!$item['pk']) {
            $value = null;
            if (isset($row->$key)) {
              $value = base64_decode($row->$key);
            }
            $object->$setValue($key, $value);
          }

        }

        if ($update) {

          $ok = $modelCtrl->$setClassCtrl($object);

        } else {

          $object->$setValue($id, base64_decode($row->$id));
          $object->$setValue($reference, null);

          $ok = $modelCtrl->$addClassCtrl($object);
        }

        if ($ok) {
          $processado++;
        } else {
          $log[] = base64_encode("N�o foi poss�vel salvar o registro '" . base64_decode($row->$id) . "'.");
        }

        $total++;
      }

      $status = ($processado === $total) ? "Sucess" : "Warning";

      $message = 'Foram atualizados ' . $processado . ' registros com sucesso dos ' . $total . ' recebidos e processados.';

    } else {

      $status = "Fail";
      $message = 'Nenhum registro a ser processado.';

    }

    return json_encode(array("status"=>$status, "message"=>base64_encode($message), "log"=>$log));
  }

  /**
   *
   * @param string $text
   * @param string $message
   */
  private function log($id, $message) {

    $filename = "log.txt";
    $fp = fopen($filename, "w+");
    $time = date('d/m/Y H:i:s');
    fwrite($fp, " - " . $time . " :: " . $id . "[" . $message. "]" . "\r\n");

    fclose($fp);
  }

  /**
   * 
   * @param type $target
   * @return type
   */
  private function sendSynchronize($target) {
    return " -> XML de " . $target;
  }

}