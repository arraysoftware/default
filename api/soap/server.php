<?php

if (isset($_SERVER)) {

  System::import('class', 'soap', 'WebService', 'api', true, '');

  $protocol = "http";
  $uri = $protocol . "://" . $_SERVER["HTTP_HOST"] . dirname($_SERVER['PHP_SELF']);
  $wsdl = null;
  if (isset($_GET['wsdl'])) {
    $wsdl = "wsdl.xml";
  }
  
  $server = new SoapServer($wsdl, array('uri' => $uri));
  $server->setClass('WebService');
  $server->handle();

  /**
   *
   * @param <type> $errno
   * @param <type> $errstr
   * @param <type> $errfile
   * @param <type> $errline
   * @param <type> $errcontext
   */
  function error($errno, $errstr, $errfile = "", $errline = "", $errcontext = "") {

    $filename = "error.txt";
    $fp = fopen($filename, "w");
    $time = date('d/m/Y H:m:s');
    fwrite($fp, "[($time) $errfile :: $errline] $errno - $errstr\r\n" . "-\r\n");

    fclose($fp);
  }

  function error_fatal() {
    if (@is_array($e = @error_get_last())) {
      $code = isset($e['type']) ? $e['type'] : 0;
      $msg = isset($e['message']) ? $e['message'] : '';
      $file = isset($e['file']) ? $e['file'] : '';
      $line = isset($e['line']) ? $e['line'] : '';
      if ($code > 0) {
        error("Fatal Error", $msg, $file, $line, null);
      }
    }
  }

  set_error_handler('error');
  register_shutdown_function('error_fatal');
  error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));

}