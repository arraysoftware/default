<?php

if (isset($_SERVER)) {

  $protocol = "http";

  if (MODO_TESTE === "") {
    if (empty($_SERVER["HTTPS"]) || $_SERVER["HTTPS"] !== "on") {
      $protocol = "https";
    }
  }

  $URI = $protocol . "://" . $_SERVER["HTTP_HOST"] . dirname($_SERVER['PHP_SELF']) . "/server.php";

  $serviceName = 'WebService';

  System::import('class', 'soap', 'WebService', 'api', true, '');
  
  $webService = new WebService();

  $functions = $webService->getFunctions($URI);

  if (System::request("show") === "text") {

    // Page accessed normally - output documentation
    echo '<!-- Attention: To access via a SOAP client use ' . $URI . '?WSDL -->';
    echo '<html>';
    echo '<head><title>' . $serviceName . '</title></head>';
    echo '<body>';
    echo '<h1>' . $serviceName . '</h1>';
    echo '<p style="margin-left:20px;">To access via a SOAP client use <code>' . $URI . '</code></p>';

    // Document each function
    echo '<h2>Available Services:</h2>';
    echo '<div style="margin-left:20px;">';
    for ($i = 0; $i < count($functions); $i++) {
      echo '<h3>Function: ' . $functions[$i]['procedure'] . '</h3>';
      echo '<div style="margin-left:20px;">';
      echo '<p>';
      echo $functions[$i]['doc'];
      echo '<ul>';
      if (array_key_exists("params", $functions[$i])) {
        echo '<li>Input Parameters:<ul>';
        for ($j = 0; $j < count($functions[$i]['params']); $j++) {
          echo '<li>' . $functions[$i]['params'][$j]['name'];
          echo ' (' . $functions[$i]['params'][$j]['type'];
          echo ')</li>';
        }
        echo '</ul></li>';
      }
      if (array_key_exists("output", $functions[$i])) {
        echo '<li>Output Parameters:<ul>';
        for ($j = 0; $j < count($functions[$i]['output']); $j++) {
          echo '<li>' . $functions[$i]['output'][$j]['name'];
          echo ' (' . $functions[$i]['output'][$j]['type'];
          echo ')</li>';
        }
        echo '</ul></li>';
      }
      echo '</ul>';
      echo '</p>';
      echo '</div>';
    }
    echo '</div>';
    echo '<h2>WSDL output:</h2>';
    echo '<pre style="margin-left:20px;width:800px;overflow-x:scroll;border:1px solid black;padding:10px;background-color:#D3D3D3;">';
    echo DisplayXML($serviceName, $functions, false);
    echo '</pre>';
    echo '</body></html>';

  } else {

    // WSDL request - output raw XML
    header("Content-Type: application/soap+xml; charset=utf-8");
    echo DisplayXML($serviceName, $functions);

  }

}

/**
 * 
 * @global $functions
 * @global $serviceName
 * 
 * @param type $xmlformat
 * @return type
 */
function DisplayXML($serviceName, $functions, $xmlformat = true) {

  $i = 0;                    // For traversing functions array
  $j = 0;                    // For traversing parameters arrays
  $str = '';                 // XML String to output
  // Tab spacings

  $t1 = '    ';
  if (!$xmlformat) {
    $t1 = '&nbsp;&nbsp;&nbsp;&nbsp;';
  }

  $t2 = $t1 . $t1;
  $t3 = $t2 . $t1;
  $t4 = $t3 . $t1;
  $t5 = $t4 . $t1;

  $serviceID = str_replace(" ", "", $serviceName);

  // Declare XML format
  $str .= '<?xml version="1.0" encoding="UTF-8" standalone="no"?>' . "\n\n";

  // Declare definitions / namespaces
  $str .= '<wsdl:definitions ' . "\n";
  $str .= $t1 . 'xmlns:soap="http://schemas.xmlsoap.org/wsdl/soap/" ' . "\n";
  $str .= $t1 . 'xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/" ' . "\n";
  $str .= $t1 . 'xmlns:s="http://www.w3.org/2001/XMLSchema" ' . "\n";
  $str .= $t1 . 'targetNamespace="http://www.darkerwhite.com/" ' . "\n";
  $str .= $t1 . 'xmlns:tns="http://www.darkerwhite.com/" ' . "\n";
  $str .= $t1 . 'name="' . $serviceID . '" ' . "\n";
  $str .= '>' . "\n\n";

  // Declare Types / Schema
  $str .= '<wsdl:types>' . "\n";
  $str .= $t1 . '<s:schema elementFormDefault="qualified" targetNamespace="http://www.darkerwhite.com/">' . "\n";
  for ($i = 0; $i < count($functions); $i++) {

    // Define Request Types
    if (array_key_exists("params", $functions[$i])) {
      $str .= $t2 . '<s:element name="' . $functions[$i]['procedure'] . 'Request">' . "\n";
      $str .= $t3 . '<s:complexType><s:sequence>' . "\n";
      for ($j = 0; $j < count($functions[$i]['params']); $j++) {
        $str .= $t4 . '<s:element minOccurs="1" maxOccurs="1" ';
        $str .= 'name="' . $functions[$i]['params'][$j]['name'] . '" ';
        $str .= 'type="s:' . $functions[$i]['params'][$j]['type'] . '" />' . "\n";
      }
      $str .= $t3 . '</s:sequence></s:complexType>' . "\n";
      $str .= $t2 . '</s:element>' . "\n";
    }

    // Define Response Types
    if (array_key_exists("output", $functions[$i])) {
      $str .= $t2 . '<s:element name="' . $functions[$i]['procedure'] . 'Response">' . "\n";
      $str .= $t3 . '<s:complexType><s:sequence>' . "\n";
      for ($j = 0; $j < count($functions[$i]['output']); $j++) {
        $str .= $t4 . '<s:element minOccurs="1" maxOccurs="1" ';
        $str .= 'name="' . $functions[$i]['output'][$j]['name'] . '" ';
        $str .= 'type="s:' . $functions[$i]['output'][$j]['type'] . '" />' . "\n";
      }
      $str .= $t3 . '</s:sequence></s:complexType>' . "\n";
      $str .= $t2 . '</s:element>' . "\n";
    }
  }

  $str .= $t1 . '</s:schema>' . "\n";
  $str .= '</wsdl:types>' . "\n\n";

  // Declare Messages
  for ($i = 0; $i < count($functions); $i++) {

    // Define Request Messages
    if (array_key_exists("params", $functions[$i])) {
      $str .= '<wsdl:message name="' . $functions[$i]['procedure'] . 'Request">' . "\n";
      $str .= $t1 . '<wsdl:part name="parameters" element="tns:' . $functions[$i]['procedure'] . 'Request" />' . "\n";
      $str .= '</wsdl:message>' . "\n";
    }

    // Define Response Messages
    if (array_key_exists("output", $functions[$i])) {
      $str .= '<wsdl:message name="' . $functions[$i]['procedure'] . 'Response">' . "\n";
      $str .= $t1 . '<wsdl:part name="parameters" element="tns:' . $functions[$i]['procedure'] . 'Response" />' . "\n";
      $str .= '</wsdl:message>' . "\n\n";
    }
  }

  // Declare Port Types
  for ($i = 0; $i < count($functions); $i++) {
    $str .= '<wsdl:portType name="' . $functions[$i]['procedure'] . 'PortType">' . "\n";
    $str .= $t1 . '<wsdl:operation name="' . $functions[$i]['procedure'] . '">' . "\n";
    if (array_key_exists("params", $functions[$i])) {
      $str .= $t2 . '<wsdl:input message="tns:' . $functions[$i]['procedure'] . 'Request" />' . "\n";
    }
    if (array_key_exists("output", $functions[$i])) {
      $str .= $t2 . '<wsdl:output message="tns:' . $functions[$i]['procedure'] . 'Response" />' . "\n";
    }
    $str .= $t1 . '</wsdl:operation>' . "\n";
    $str .= '</wsdl:portType>' . "\n\n";
  }

  // Declare Bindings
  for ($i = 0; $i < count($functions); $i++) {
    $str .= '<wsdl:binding name="' . $functions[$i]['procedure'] . 'Binding" type="tns:' . $functions[$i]['procedure'] . 'PortType">' . "\n";
    $str .= $t1 . '<soap:binding style="document" transport="http://schemas.xmlsoap.org/soap/http" />' . "\n";
    $str .= $t1 . '<wsdl:operation name="' . $functions[$i]['procedure'] . '">' . "\n";
    $str .= $t2 . '<soap:operation soapAction="' . $functions[$i]['uri'] . '#' . $functions[$i]['procedure'] . '" style="document" />' . "\n";
    if (array_key_exists("params", $functions[$i])) {
      $str .= $t2 . '<wsdl:input><soap:body use="literal" /></wsdl:input>' . "\n";
    }
    if (array_key_exists("output", $functions[$i])) {
      $str .= $t2 . '<wsdl:output><soap:body use="literal" /></wsdl:output>' . "\n";
    }
    $str .= $t2 . '<wsdl:documentation>' . $functions[$i]['doc'] . '</wsdl:documentation>' . "\n";
    $str .= $t1 . '</wsdl:operation>' . "\n";
    $str .= '</wsdl:binding>' . "\n\n";
  }

  // Declare Service
  $str .= '<wsdl:service name="' . $serviceID . '">' . "\n";
  for ($i = 0; $i < count($functions); $i++) {
    $str .= $t1 . '<wsdl:port name="' . $functions[$i]['procedure'] . 'Port" binding="tns:' . $functions[$i]['procedure'] . 'Binding">' . "\n";
    $str .= $t2 . '<soap:address location="' . $functions[$i]['uri'] . '" />' . "\n";
    $str .= $t1 . '</wsdl:port>' . "\n";
  }
  $str .= '</wsdl:service>' . "\n\n";

  // End Document
  $str .= '</wsdl:definitions>' . "\n";
  if (!$xmlformat) {
    $str = str_replace("<", "&lt;", $str);
  }
  if (!$xmlformat) {
    $str = str_replace(">", "&gt;", $str);
  }
  if (!$xmlformat) {
    $str = str_replace("\n", "<br />", $str);
  }

  return $str;
}