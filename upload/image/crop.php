<?php

$id = "";
if(isset($_GET['i'])){
  $id = $_GET['i'];
}

$target = "";
if(isset($_GET['t'])){
  $target = $_GET['t'];
}

$field = "";
if(isset($_GET['f'])){
  $field = $_GET['f'];
}

$img = "";
if(isset($_GET['d'])){
  $img = $_GET['d'];
}

$width = "";
if(isset($_GET['w'])){
  $width = $_GET['w'];
}

$height = "";
if(isset($_GET['h'])){
  $height = $_GET['h'];
}

$lib = "system";
$version = unserialize(VERSION_LIBRARY);
$v = $version[$lib];
$conected = true;

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link rel="stylesheet" type="text/css" href="css/imgareaselect-default.css" />
    <link rel="stylesheet" type="text/css" href="css/styles.css" />

    <link rel="stylesheet" type="text/css" media="all" href="<?php print PAGE_LIBRARY;?>resources/css/?s=<?php print $conected;?>&v=<?php print $v;?>&file=system.css" />

    <style>
      body{
        overflow: hidden;
      }
      .imgareaselect-outer{
        z-index: 1 !important;
      }
    </style>
    <script type="text/javascript">
      var screen = {
        /**
         *
         * @type type
         */
        crop: {
          width: 95,
          height: 110,
          x1: 0,
          y1: 0,
          x2: 95,
          y2: 110,

          minHeight: 100,
          minWidth: 85,

          aspectRatio: '1:1.15',
          handles: true,
          fadeSpeed: 200,
          resizeable: true,
          /**
           *
           * @param {type} form
           * @param {type} id
           * @param {type} dir
           * @param {type} target
           * @param {type} field
           * @param {type} img
           * @param {type} width
           * @param {type} height
           * @returns {undefined}
           */
          save: function(value){
            parent.system.upload.save('<?php print $id;?>', '<?php print $target;?>', '<?php print $field;?>', '<?php print $img;?>', '<?php print $width;?>', '<?php print $height;?>', '<?php print DIR_IMAGE_APP;?>'+value);
          }
        }
      };

    </script>

    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/jquery.imgareaselect.min.js"></script>
    <script type="text/javascript" src="js/custom.js"></script>

  </head>

  <body>
    <div id="body" style="background: #fff;">
      <div id="wrap">
        <br>
        <div id="uploader">

          <div id="big_uploader">
            <h3>Passo 1: Selecione uma imagem</h3>
            <form name="upload_big" id="upload_big"  method="post" enctype="multipart/form-data" action="upload.php?act=upload&fit-w=760&fit-h=380" target="upload_target">
              <input name="photo" id="file" accept="image/*" size="27" type="file" onchange="$('#upload_big').submit();"/>
            </form>
          </div><!-- big_uploader -->

          <div style="height: 30px;">
            <div id="notice">Processando..</div>
          </div>

          <div id="content">
            <div id="uploaded">
              <h3>Passo 2: Posicione o recorte</h3>
              <div id="div_upload_big"></div>

              <form name="upload_thumb" id="upload_thumb" method="post" action="upload.php?act=thumb" target="upload_target">

                <input type="hidden" name="img_src" id="img_src" class="img_src" />
                <input type="hidden" id="crop-w" name="crop-w" value="100"/>
                <input type="hidden" id="crop-h" name="crop-h" value="133"/>

                <input type="hidden" id="w" name="w" value="300"/>
                <input type="hidden" id="h" name="h" value="400"/>

                <input type="hidden" id="y1" class="y1" name="y" />
                <input type="hidden" id="x1" class="x1" name="x" />
                <input type="hidden" id="y2" class="y2" name="y1" />
                <input type="hidden" id="x2" class="x2" name="x1" />

                <input type="hidden" id="value" name="value" />

                <h3>Passo 3: Clique em recortar quando a Pr&eacute;via da imagem estiver adequada</h3>
                <!--<input type="button" class="form-buttom" value="Recortar" onclick="screen.crop.save('upload_thumb','<?php print $id;?>', '<?php print DIR_IMAGE_APP;?>', '<?php print $target;?>', '<?php print $field;?>', '<?php print $img;?>', '<?php print $width;?>', '<?php print $height;?>');"/>-->
                <input type="submit" class="form-buttom" value="Recortar"/>
              </form>


            </div><!-- uploaded-->

            <div id="thumbnail">
              <h3>Pr&eacute;via</h3>
              <div id="preview"></div>

              <!--<h3>Recorte</h3>-->
              <div id="div_upload_thumb" style="display: none;"></div>

              <div id="details" style="display: none;">
                <table width="200">
                  <tr>
                    <td colspan="2">Image Source<br /><input type="text" id="img_src" name="img_src" class="img_src" size="35" /></td>
                  </tr>
                  <tr>
                    <td>Height<br /><input type="text" name="height" class="height" size="5" /></td>
                    <td>Width<br /><input type="text" name="width" class="width" size="5"/></td>
                  </tr>
                  <tr>
                    <td>Y1<br /><input type="text" class="y1"  size="5"/></td>
                    <td>X1<br /><input type="text" class="x1" size="5" /></td>
                  </tr>
                  <tr>
                    <td>Y2<br /><input type="text" class="y2" size="5" /></td>
                    <td>X2<br /><input type="text" class="x2" size="5" /></td>
                  </tr>
                </table>
              </div>
            </div>
          </div>

          <iframe id="upload_target" name="upload_target" src="" style="display:none"></iframe>
        </div>
      </div>

    </div>
  </body>
</html>