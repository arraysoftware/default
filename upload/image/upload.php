<?php

include('function.php');

if ($_GET['act'] == 'upload') {

  $big_arr = array(
      'uploaddir' => 'files/big/',
      'tempdir' => 'files/temp/',
      'width' => System::request('fit-w'),
      'height' => System::request('fit-h'),
      'x' => 0,
      'y' => 0
  );

  resizeImg($big_arr);

} else if ($_GET['act'] == 'thumb') {

  $arr = array(
      'uploaddir' => 'files/thumb/',
      'tempdir' => 'files/temp/',
      'x' => System::request('x'),
      'y' => System::request('y'),
      'x1' => System::request('x1'),
      'y1' => System::request('y1'),
      'width' => System::request('crop-w'),
      'height' => System::request('crop-h'),
      'w' => System::request('w'),
      'h' => System::request('h'),
      'img_src' => System::request('img_src'),
      'thumb' => true
  );
  resizeThumb($arr);

} else {
  //
}
?>