<?php

ini_set('post_max_size', '1000M');
ini_set('upload_max_filesize', '1000M');

/**
 * Handle file uploads via XMLHttpRequest
 */
class qqUploadedFileXhr {

  /**
   * Save the file to the specified path
   * @return boolean TRUE on success
   */
  function save($path) {
    $input = fopen("php://input", "r");
    $temp = tmpfile();
    $realSize = stream_copy_to_stream($input, $temp);
    fclose($input);

    if ($realSize != $this->getSize()) {
      return false;
    }

    $target = fopen($path, "w");
    fseek($temp, 0, SEEK_SET);
    stream_copy_to_stream($temp, $target);
    fclose($target);

    return true;
  }

  function getName() {
    return $_GET['qqfile'];
  }

  function getSize() {
    if (isset($_SERVER["CONTENT_LENGTH"])) {
      return (int) $_SERVER["CONTENT_LENGTH"];
    } else {
      throw new Exception('Getting content length is not supported.');
    }
  }

}

/**
 * Handle file uploads via regular form post (uses the $_FILES array)
 */
class qqUploadedFileForm {

  /**
   * Save the file to the specified path
   * @return boolean TRUE on success
   */
  function save($path) {
    if (!move_uploaded_file($_FILES['qqfile']['tmp_name'], $path)) {
      return false;
    }
    return true;
  }

  function getName() {
    return $_FILES['qqfile']['name'];
  }

  function getSize() {
    return $_FILES['qqfile']['size'];
  }

}

/**
 * load file
 */
class qqFileUploader {

  private $allowedExtensions = array();
  private $sizeLimit = 104857600;
  private $file;

  function __construct(array $allowedExtensions = array(), $sizeLimit = 104857600) {
    $allowedExtensions = array_map("strtolower", $allowedExtensions);

    $this->allowedExtensions = $allowedExtensions;
    $this->sizeLimit = $sizeLimit;

    $this->checkServerSettings();

    if (isset($_GET['qqfile'])) {
      $this->file = new qqUploadedFileXhr();
    } elseif (isset($_FILES['qqfile'])) {
      $this->file = new qqUploadedFileForm();
    } else {
      $this->file = false;
    }
  }

  private function checkServerSettings() {
    $postSize = $this->toBytes(ini_get('post_max_size'));
    $uploadSize = $this->toBytes(ini_get('upload_max_filesize'));

    if ($postSize < $this->sizeLimit || $uploadSize < $this->sizeLimit) {
      $size = max(1, $this->sizeLimit / 1024 / 1024).'M';
      die("{'error':'increase post_max_size and upload_max_filesize to $size. In server configuration upload_max_filesize is ".($uploadSize / 1024 / 1024)."M and post_max_size is ".($postSize / 1024 / 1024)."M'}");
    }
  }

  private function toBytes($str) {
    $val = trim($str);
    $last = strtolower($str[strlen($str) - 1]);
    switch ($last) {
      case 'g': $val *= 1024;
      case 'm': $val *= 1024;
      case 'k': $val *= 1024;
    }
    return $val;
  }

  /**
   *
   * Returns array('success'=>true) or array('error'=>'error message')
   */
  function handleUpload($dir, $name, $replaceOldFile = false) {

    $error = 'N&atilde;o foi possivel realizar o upload. ';

    if(!is_dir($dir)){
      return array('error' => $error.'Diret&oacute;rio de destino n&atildeo encontrado: ['.$dir.'].');
    }

    if(!is_writable($dir)){
      return array('error' => $error.'Imposs&iacute;vel escrever no diret&oacute;rio do upload: ['.$dir.'].');
    }

    if(!$this->file){
      return array('error' => $error.'Nenhum arquivo foi recebido.');
    }

    $size = $this->file->getSize();

    if($size == 0){
      return array('error' => $error.'Arquivo est&aacute; vazio.');
    }

    if($size > $this->sizeLimit){
      return array('error' => $error.'Arquivo tem tamanho '.$size.' e o limite de tamanho &eacute; '.$this->sizeLimit.'.');
    }

    $pathinfo = pathinfo($this->file->getName());

    if($name == ""){
      $filename = $pathinfo['filename'];
    }else{
      $filename = $name;
    }

    $ext = "no-extension";
    if(isset($pathinfo['extension'])){
      $ext = $pathinfo['extension'];
    }

    if($this->allowedExtensions && !in_array(strtolower($ext), $this->allowedExtensions)) {
      $these = implode(', ', $this->allowedExtensions);
      return array('error' => $error.'Arquivo com extens&atilde;o inv&aacute;lida: '.$these.'.');
    }

    if(!$replaceOldFile) {
      if (file_exists($dir.$filename.'.'.$ext)) {
        return array('error' => $error.'J&aacute; existe um arquivo com este nome no servidor.');
      }
    }

    if($this->file->save($dir.$filename.'.'.$ext)) {
      return array('success' => true);
    }else{
      return array('error' => $error.'O upload foi cancelado, ou ocorreu algum erro no servidor');
    }

  }

}

$allowedExtensions = array();

$sizeLimit = 100 * 1024 * 1024;

$uploader = new qqFileUploader($allowedExtensions, $sizeLimit);

$path = "upload/temp/";

if (isset($_REQUEST['path'])) {
  $path = $_REQUEST['path'];
}

$name = "";
if (isset($_REQUEST['name'])) {
  $name = $_REQUEST['name'];
}

$replaceOldFile = true;
if (isset($_REQUEST['replaceOldFile'])) {
  $replaceOldFile = $_REQUEST['replaceOldFile'];
}

$result = $uploader->handleUpload($path, $name, $replaceOldFile);

print json_encode($result);
