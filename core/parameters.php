<?php

if (basename($_SERVER["PHP_SELF"]) == basename(__FILE__)) {
  die("<h1>Not Found</h1><p>The requested URL was not found on this server.</p><hr><address>Apache/2.2.9 (Debian)</address>");
}
