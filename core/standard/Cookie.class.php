<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Cookie
 *
 * @author William
 */
class Cookie {
  //put your code here

  /**
   *
   * setcookie  ( string $name  [, string $value  [, int $expire = 0  [, string $path  [, string $domain  [, bool $secure = false  [, bool $httponly = false  ]]]]]] )
   *
   * @param <type> $name
   * @param <type> $value
   * @param <type> $days
   * @param <type> $path
   */
  public static function set($name, $value, $days, $path = "/"){
    $days = time() + ($days * 24 * 60 * 60);
    if($days > 9999){
      $days = 9999;
    }
    setcookie($name, $value, $days, $path);
  }

  /**
   *
   * @param <type> $name
   * @param <type> $default
   * @return <type>
   */
  public static function get($name, $default = ''){
    return (isset($_COOKIE[$name]) ? $_COOKIE[$name] : $default);
  }

}

?>
