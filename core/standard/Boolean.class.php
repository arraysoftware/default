<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Boolean
 *
 * @author William
 */
class Boolean {
  //put your code here

  /**
   *
   * @param type $value
   */
  public static function parse($value) {
    if (gettype($value) === 'boolean') {
      $value = (bool) $value;
    } else {
      if (strtolower($value) === 'true') {
        $value = true;
      } else {
        if (Number::parseInteger($value) > 0) {
          $value = true;
        } else {
          $value = false;
        }
      }
    }
    return $value;
  }

}
