<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Util
 *
 * @author William
 */
class Util{

  /**
   *
   * @param type $str
   * @return type
   */
  public static function isUTF8($str){
    return (utf8_encode(utf8_decode($str)) == $str);
  }

  /**
   *
   * @param <type> $color
   * @return <type>
   */
  public static function hexadecimalRcb($color){
    return array(
      'red'=>hexdec(substr($color, 0, 2)),
      'green'=>hexdec(substr($color, 2, 2)),
      'blue'=>hexdec(substr($color, 4, 2))
    );
  }

  /**
   * $date = "28/02/2009"
   * @param <type> $date
   * @return <type>
   */
  public static function isDate($date){
    $char = strpos($date, "/") !== false ? "/" : "-";
    $date_array = explode($char, $date);
    if(count($date_array) != 3)
      return false;
    return checkdate($date_array[1], $date_array[0], $date_array[2]) ? ($date_array[2]."-".$date_array[1]."-".$date_array[0]) : false;
  }

}

?>
