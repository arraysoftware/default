<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of File
 *
 * @author William
 */
class File{
  //put your code here
  
  /**
   *
   * Modo Acesso
   * 'r' - Abre somente para leitura; coloca o ponteiro no comeoo do arquivo.
   * 'r+' - Abre para leitura e gravaooo; coloca o ponteiro no comeoo do arquivo.
   * 'w' - Abre somente para gravaooo; coloca o ponteiro no comeoo do arquivo e apaga o conteodo que jo foi escrito. Se o arquivo noo existir, tentar crio-lo.
   * 'w+' - Abre para leitura e escrita; coloca o ponteiro no inocio do arquivo e apaga o conteodo que jo foi escrito. Se o arquivo noo existir, tentar crio-lo.
   * 'a' - Abre o arquivo somente para escrita; coloca o ponteiro no fim do arquivo. Se o arquivo noo existir, tentar crio-lo.
   * 'a+' - Abre o arquivo para leitura e gravaooo; coloca o ponteiro no fim do arquivo. Se o arquivo noo existir, tentar crio-lo.
   *
   * @param <type> $temp_path
   * @param <type> $temp_url
   * @param <type> $nome_arq
   * @param <type> $txt
   * @param <type> $modoAcesso
   */
  public static function saveLinkFile($temp_path, $temp_url, $nome_arq, $txt, $modoAcesso){
    $filepath = $temp_path.$nome_arq;
    try{
      if(file_exists($filepath)){
        unlink($filepath);
      }
    }catch(Excpetion $exception){
      //print $exception;
    }
    $file = fopen($filepath, $modoAcesso); // abre o arquivo
    fwrite($file, $txt); // escreve no arquivo
    fclose($file); // fecha o arquivo
    $url = $temp_path.$nome_arq;
    ?>
    &nbsp;&nbsp;<a href="<?php print PAGE_APP; ?>/download/?file=<?php print $url; ?>" target="_blank"><b><i>Download - <?php print $nome_arq; ?></i></b></a>
    <?
  }

  /**
   *
   * @param type $filename
   * @param type $contents
   * @param type $mode
   * @param type $encode
   *
   * @return type
   */
  public static function saveFile($filename, $contents, $mode = 'w', $encode = true){
    $saved = false;

    if (!file_exists(dirname($filename))) {
      mkdir(dirname($filename), 0777, true);
    }
    
    if (file_exists(dirname($filename))) {

      if (is_writable(dirname($filename))) {
          /**
           *
           * UTF8
           * fwrite($handle, pack("CCC",0xEF,0xBB,0xBF));
           * UTF8
           * fprintf($handle, chr(0xEF).chr(0xBB).chr(0xBF));
           * BIG ENDIAN
           * fprintf($handle, chr(0xFE).chr(0xFF).chr(0xEF).chr(0xBB).chr(0xBF));
           */
        $handle = fopen($filename, $mode);
        if($handle !== false){
          if($encode){
            $contents = utf8_encode($contents);
          }
          $saved = fwrite($handle, $contents);
          fclose($handle);
        }
        
      }

    }

    return $saved;
  }

  /**
   * 
   * @param type $id
   * @param type $server
   * @param type $file_local
   * @param type $file_remote
   */
  public static function saveRemoteFile($id, $server, $file_local, $file_remote){
    
    $uri = $server . "/api/soap/";
    $location = $uri . "server.php";
    $trace = 1;

    $client = new SoapClient(null, array('location' => $location, 'uri' => $uri, 'trace' => $trace));
    $file_content = file_get_contents($file_local);
    $result = $client->saveFile($id, $file_remote, base64_encode($file_content));
    if (is_soap_fault($result)) {
      $result = $result->faultcode." - ".$result->faultstring;
    }
    
    return $result;
  }
  
  /**
   *
   * @param type $dir
   * @return type
   */
  public static function delTree($dir){
    $scan = array();
    if(is_dir($dir)){
      $scan = scandir($dir);
    }

    $files = array_diff($scan, array('.', '..'));
    foreach($files as $file){
      (is_dir("$dir/$file")) ? self::delTree("$dir/$file") : unlink("$dir/$file");
    }

    // Se o diret?rio existir, remove
    if($scan){
      return rmdir($dir);
    }
    return false;
  }

  /**
   *
   * @param type $type_content
   * @return type
   */
  public static function infoContent($type_content){
    $f = explode("|", $type_content);
    $file = array();
    $file['path'] = isset($f[0]) ? $f[0] : PATH_TEMP;
    $file['name'] = isset($f[1]) ? $f[1] : '';
    $file['extensions'] = isset($f[2]) ? $f[2] : '[]';
    $file['width'] = isset($f[3]) ? $f[3] : '100';
    $file['height'] = isset($f[4]) ? $f[4] : '100';
    $file['size'] = isset($f[5]) ? $f[5] : 'null';

    return $file;
  }

  /**
   *
   * @param type $file
   * @return type
   */
  public static function getExtension($file){
    return pathinfo($file, PATHINFO_EXTENSION);
  }

  /**
   * 
   * @param type $plataform
   * @return string
   */
  public static function getSeparator($plataform = null) {
    
    if ($plataform === null) {
      $plataform = PHP_OS;
    }
    
    $separator = ($plataform == "Windows" || $plataform == "WINNT") ? "\\" : "/"; 

    return $separator;
  }

  /**
   * 
   * @param type $filename
   * @return type
   */
  public static function getParent($filename) {
    return dirname($filename);
  }
  
  /**
   * 
   * @param type $filename
   */
  public static function openFile($filename) {
    return file_get_contents($filename);
  }

  /**
   * 
   * @param type $filename
   * @return type
   */
  public static function getName($filename) {
    return pathinfo($filename, PATHINFO_FILENAME);
  }

  /**
   * 
   * @param string $filename
   * @return boolean
   */
  public static function exists($filename) {
    $exists = false;
    if (file_exists($filename)) {
      if (!self::isDirectory($filename)) {
        $exists = true;
      }
    }
    return $exists;
  }

  /**
   * 
   * @param string $filename
   * @return boolean
   */
  public static function isDirectory($filename) {
    return is_dir($filename);
  }

}
