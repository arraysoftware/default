<?php

/**
 *
 * ArraySoftware.Net
 * SOMMAR
 * Modulo..........: Geral
 * Funcionalidade..: Registrar constantes para o sistema
 * Pre-requisitos..: Nao possui
 */
class System {

  const config = 'config';

  /**
   *
   * @param type $errno
   * @param type $errstr
   * @param string $errfile
   * @param string $errline
   * @param string $errcontext
   */
  public static function err($errno, $errstr, $errfile = null, $errline = null, $errcontext = null) {
    core_err($errno, $errstr, $errfile, $errline, $errcontext);
  }

  /**
   * Fun��o recursiva abstrata para utiliza��o em arrays ou strings
   * @param array|string $input
   * @param string $procedure
   * @return array|string
   * @example recursiveCallback($_POST, 'utf8_decode')
   */
  public static function recursiveCallback($input, $procedure) {
    if (!is_array($input)) {
      $output = $procedure($input);
    } else {
      foreach ($input as $key => $value) {
        $output[$key] = $procedure($input[$key]);
      }
    }
    return $output;
  }

  /**
   *
   * @param type $id
   * @param type $module
   * @param type $entity
   * @param type $path
   * @param type $require
   * @param type $rsc
   */
  public static function desire($id, $module, $entity, $path = "", $require = true, $rsc = null) {

    return self::import($id, $module, $entity, $path, $require, $rsc);
  }

  /**
   *
   * @param type $id
   * @param type $module
   * @param type $entity
   * @param type $path
   * @param type $require
   * @param type $rsc
   */
  public static function import($id, $module, $entity, $path = "", $require = true, $rsc = null) {

    $rsc = ($rsc === null or $rsc === '{project.rsc}') ? defined("DEFAULT_EXTENSION") ? DEFAULT_EXTENSION : ""  : $rsc;

    $p = str_replace("../../../../src", "", $path);
    $path = (strpos($path, PATH_APP) === false) ? PATH_APP . $p : $p;

    $layers['c'] = array("name" => "controller/", "sufix" => ".ctrl", "resource" => $rsc);
    $layers['d'] = array("name" => "dao/", "sufix" => ".dao", "resource" => $rsc);
    $layers['m'] = array("name" => "model/", "sufix" => ".class", "resource" => $rsc);
    $layers['p'] = array("name" => "post/", "sufix" => ".post", "resource" => $rsc);
    $layers['v'] = array("name" => "view/", "sufix" => ".view", "resource" => $rsc);
    $layers['j'] = array("name" => "json/", "sufix" => ".json", "resource" => $rsc);
    $layers['s'] = array("name" => "screen/", "sufix" => ".screen", "resource" => $rsc);

    $layers['r'] = array("name" => "report/", "sufix" => ".rep", "resource" => $rsc);
    $layers['f'] = array("name" => "file/", "sufix" => ".fil", "resource" => $rsc);
    $layers['file'] = array("name" => "", "sufix" => "", "resource" => "");
    $layers['class'] = array("name" => "", "sufix" => ".class", "resource" => "");

    $sufix = $layers[$id]['sufix'];
    $layer = $layers[$id]['name'];
    $resource = $layers[$id]['resource'];

    $file = $path . "/" . $module . "/" . $layer . $entity . $resource . $sufix . ".php";
    $file = str_replace("//", "/", $file);
    
    $include = true;
    
    if (in_array($id, array("c", "d", "m", "s"))) {
      
      $class = $entity;
      if ($id === "c") {
        $class = $entity . "Ctrl";
      } else if ($id === "d") {
        $class = $entity . "DAO";
      } else if ($id === "s") {
        $class = $entity . "Screen";
      }

      if (class_exists($class)) {
        $include = false;
      }
      
    }

    if ($include) {
      if ($require) {
        if (file_exists($file)) {
          require_once $file;
        } else {
          core_err("ImportError", "N&atilde;o foi poss&iacute;vel fazer o require para $file", "", "", "core.standard.System.import ($file)", "PHP");
        }
      }
    }

    return $file;
  }

  /**
   * 
   * @param type $index
   * @param type $value
   * @param type $encode
   * @return type
   */
  public static function request($index, $value = "", $encode = "") {

    if (isset($_GET[$index])) {
      $value = $_GET[$index];
    } else if (isset($_POST[$index])) {
      $value = $_POST[$index];
    }

    if (!is_null($value) and !empty($value)) {

      //$value = self::recursiveCallback($value, 'trim');

      if ($encode) {
        $value = self::recursiveCallback($value, $encode);
      } else {
        if (UTF8_REQUEST == 'encode') {
          $value = self::recursiveCallback($value, 'utf8_encode');
        } else if (UTF8_REQUEST == 'decode') {
          $value = self::recursiveCallback($value, 'utf8_decode');
        }
      }

      if (HTML_ENTITIES_REQUEST) {
        $value = self::recursiveCallback($value, 'htmlentities');
      }

      if (ADD_SLASHES_REQUEST) {
        $value = self::recursiveCallback($value, 'addslashes');
      }
    }

    return $value;
  }

  /**
   * 
   * @param type $item
   * @param type $default
   * @return type
   */
  public static function get($item, $default = null) {

    if ($item['type'] === 'select-multi') {

      $entity = $item['select-multi']['entity'];
      $value = self::request('source-' . $entity, $default);

    } else if($item['type_behavior'] === 'parent') {

      if (isset($item['parent'])) {

        $class = $item['parent']['entity'];
        self::import('m', $item['parent']['modulo'], $class, 'src', true);
        $object = new $class();
        $set_value = "set_".$item['parent']['prefix']."_value";
        $get_items = "get_".$item['parent']['prefix']."_items";

        $items = $object->$get_items();
        foreach ($items as $iten) {
          $k = $iten['id'];
          $v = self::request($k, null);
          $object->$set_value($k, $v);
        }
        $value = $object;

      }

    } else {

      $value = self::request($item['id'], $default);

    }
      
    return $value;
  }

  /**
   * 
   * @param type $operations
   * @param type $index
   * 
   * @param type $result
   */
  public static function status($operation, $result) {
    $status = 'fail';
    $type = gettype($result);
    if (!$operation->parse) {
      if ($type !== 'array') {
        $status = 'success';
      }
    }

    return $status;
  }

  /**
   *
   * @param type $text
   * @param type $print
   * @param type $addslaches
   * @param type $breakline
   * @param type $htmlentities
   *
   * @return type
   */
  public static function printText($text, $print = false, $addslaches = false, $breakline = false, $htmlentities = false) {

    $text = $addslaches ? addslashes($text) : $text;
    $text = $breakline ? preg_replace('/\s/', ' ', $text) : $text;
    $text = $htmlentities ? htmlentities($text) : $text;

    $text = UTF8_PRINT === 'encode' ? utf8_encode($text) : $text;
    $text = UTF8_PRINT === 'decode' ? utf8_decode($text) : $text;

    if ($print) {
      print $text;
    }

    return $text;
  }

  /**
   * 
   * @param type $id
   * @param type $die
   * 
   * @return string
   */
  public static function getUser($id = 'name', $die = false) {
    
    if ($id === false) {
      $die = false;
      $id = 'name';
    }

    $value = "";

    if (MODO_LEGADO === "true") {
      
      $options = array("code" => "siga_ppid_1", "name" => "siga_ppid_2", "user" => "siga_ppid_4");

      if (isset($options[$id])) {
        $id = $options[$id];

        $value = Cookie::get($id, null);
        if ($value) {
          $value = Encode::decrypt($value);
        }
      }
      
    } else {

      $session = Cookie::get(Application::$session, null);
      if ($session) {
        $value = self::getSessionValue($session, $id);
      }

    }

    if (!$value) {
      if (PHP_SAPI == 'cli') {
        $value = DEFAULT_USER_SYSTEM;
      } else {
        if ($die) {
          die("Usu&aacute;rio n&atilde;o encontrado.");
        }
      }
    }

    return $value;
  }

  /**
   * 
   * @param type $session
   * @param type $index
   * 
   * @return type
   */
  public static function getSessionValue($session, $index) {
    
    $value = "";
    
    $options = array(
      "code" => new Object(array("property" => "usu_codigo", "decrypt" => false)),
      "name" => new Object(array("property" => "usu_nome", "decrypt" => true)),
      "type" => new Object(array("property" => "usu_tipo", "decrypt" => true)),
      "admin" => new Object(array("property" => "usu_admin", "decrypt" => true)),
      "email" => new Object(array("property" => "usu_email", "decrypt" => true)),
      "login" => new Object(array("property" => "usu_login", "decrypt" => true))
    );

    if (isset($options[$index])) {

      $option = $options[$index];

      $user = $session;
      if (!is_object($session)) {
        $json = stripslashes($session);
        $user = json_decode($json);
      }

      if ($user) {

        $property = $option->property;
        if (property_exists($user, $property)) {
          $value = $user->$property;
          if ($option->decrypt) {
            $value = Encode::decrypt($value);
          }
        }

      }

    }

    return $value;
  }


  /**
   *
   * @param type $property
   * @param type $default
   *
   * @return type
   */
  public static function getConfig($property, $default = "") {
    $value = $default;
    $configuration = Cookie::get(self::config, 'undefined');
    if ($configuration !== 'undefined') {
      $configuration = utf8_decode($configuration);
      $config = json_decode($configuration, true);
      if ($config) {
        if (isset($config[$property])) {
          $value = $config[$property];
        }
      } else {
        print "Error: $configuration";
        switch (json_last_error()) {
          case JSON_ERROR_NONE:
            echo ' - No errors. ';
            break;
          case JSON_ERROR_DEPTH:
            echo ' - Maximum stack depth exceeded. ';
            break;
          case JSON_ERROR_STATE_MISMATCH:
            echo ' - Underflow or the modes mismatch. ';
            break;
          case JSON_ERROR_CTRL_CHAR:
            echo ' - Unexpected control character found. ';
            break;
          case JSON_ERROR_SYNTAX:
            echo ' - Syntax error, malformed JSON. ';
            break;
          case JSON_ERROR_UTF8:
            echo ' - Malformed UTF-8 characters, possibly incorrectly encoded. ';
            break;
          default:
            echo ' - Unknown error. ';
            break;
        }
      }
    }
    return $value;
  }

  /**
   *
   * @param type $var
   */
  public static function debug($var) {
    print "<pre>" . print_r($var, true) . "</pre>";
  }

  /**
   *
   * @param type $items
   * @param type $where
   * @param type $group
   * @param type $order
   * @param type $string
   * @return array
   */
  public static function recover($items, $where, $group, $order, $string = '') {

    $recovered = array();

    if ($where) {

      $where = "";
      $where_description = "";
      $conector = " AND ";
      $separator = " <i>&</i> ";

      $labels = new Object(
        '{' . 
          '"lbl_01":"com c&oacute;digo igual a"' . 
          ', "lbl_02":"com c&oacute;digo entre"' . 
          ', "lbl_03":"e"' . 
          ', "lbl_04":"igual a"' . 
          ', "lbl_05":"cont&eacute;m"' . 
          ', "lbl_06":"com valor entre"' . 
          ', "lbl_07":"semelhante a"' . 
          ', "lbl_08":"est&aacute; marcado com"' . 
          ', "lbl_09":"entre"' . 
          ', "lbl_10":"incluso na pesquisa"' . 
          ', "lbl_11":"Filtro:"' . 
          ', "lbl_12":"ou"' . 
        '}'
      );

      foreach ($items as $item) {

        if (!isset($item['fast'])) {
          $item['fast'] = "";
        }
        $complete = true;
        $value = '';
        $start = '';
        $end = '';

        if ($string) {

          $value = $string;
          $conector = " OR ";
          $separator = " <br><i>ou</i><br>";
          $complete = false;

        } else {

          $subquery = array();
          if ($item['type'] == 'subquery') {
            $subquery = self::request($item['id']);
          } else {
            $value = self::request($item['id'], '');
            $start = self::request($item['id'] . '_start', '');
            $end = self::request($item['id'] . '_end', '');
          }

        }

        $ignore = array('', '(__) ____-____', '_____-___', '__.___.___/____-__', '___.___.___-__', '__/__/____', '0.00', '0,00');

        $valid = !in_array($value, $ignore, true) || (!in_array($start, $ignore, true) and !in_array($end, $ignore, true)) || (count($subquery));

        $search = !($item['fast'] == 3);
        if (!$search) {
          //$search = isset($item['search']) ? $item['search'] : false;
        }
        
        if ($valid and $search and ($complete or $item['fast'])) {

          $type = $item['type'];

          if ($type === 'pk') {

            if (is_numeric($value)) {
              $where .= $conector . $item['id'] . "='" . $value . "'";
              $where_description .= $separator . "" . $item['description'] . " <b>" . $labels->lbl_01 ."</b> " . $value . "";
            } else if (is_numeric($start) and is_numeric($end)) {
              $where .= $conector . "" . $item['id'] . " BETWEEN '" . $start . "' AND '" . $end . "'";
              $where_description .= $separator . "" . $item['description'] . " <b>" . $labels->lbl_02 ."</b> " . $start . " <b>" . $labels->lbl_03 ."</b> " . $end . "";
            }

          } else if ($type === 'fk') {

            if (isset($item['foreign'])) {

              $id = $item['id'];
              
              $foreign = $item['foreign'];
              if (is_numeric($value)) {
                $where .= $conector . $id . "='" . $value . "'";
              } else {

                $module = $foreign['modulo'];
                $entity = $foreign['entity'];
                $prefix = $foreign['prefix'];
                $key = $foreign['description'];
                
                System::import('m', $module, $entity, 'src', true, '{project.rsc}');

                $class = new $entity();
                $getItems = "get_" . $prefix . "_items";
                $elements = $class->$getItems();
                $element = $elements[$key];
                
                $doit = true;
                if ($element['type'] === 'calculated') {
                  $doit = false;
                  if ($element['update'] || $element['insert']) {
                    $doit = true;
                  }
                }
                
                if ($doit) {
                  
                  $where .= $conector . "(" . String::searchFormat($key, $value) . ")";

                  $description = System::request('wd-' . $item['id'], "", "trim");
                  if ($description) {
                    $where_description .= $separator . "" . $item['description'] . " <b>" . $labels->lbl_04 . "</b> " . $description . "";
                  } else {
                    $where_description .= $separator . "" . $item['description'] . " <b>" . $labels->lbl_05  . "</b> " . $value . "";
                  }

                }
                
              }

            }

          } else if ($type == 'cpf' || $type == 'cnpj' || $type == 'cep' || $type == 'telefone') {

            $value = String::mask($value, $type);

            $where .= $conector . $item['id'] . " LIKE '%" . $value . "%'";
            $where_description .= $separator . "" . $item['description'] . " <b>" . $labels->lbl_04 . "</b> " . $value . "";

          } else if ($type == 'int' || $type == 'double' || $type == 'money' || $type == 'percent' || $type == 'min/max') {

            if (is_numeric($value)) {

              $where .= $conector . $item['id'] . "='" . $value . "'";
              $where_description .= $separator . "" . $item['description'] . " <b>" . $labels->lbl_04 . "</b> " . $value . "";
            } else if (is_numeric($start) and is_numeric($end)) {

              $where .= $conector . "" . $item['id'] . " BETWEEN '" . $start . "' AND '" . $end . "'";
              $where_description .= $separator . "" . $item['description'] . " <b>" . $labels->lbl_06  . "</b> " . $start . " e " . $end . "";
            }

          } else if ($type == 'fk - string') {

            $where .= $conector . $item['id'] . "='" . $value . "'";
            $description = System::request('wd-' . $item['id'], "", "trim");

            if ($description) {
              $where_description .= $separator . "" . $item['description'] . " <b>" . $labels->lbl_04 . "</b> " . $description . "";
            } else {
              $where_description .= $separator . "" . $item['description'] . " <b>" . $labels->lbl_01 . "</b> " . $value . "";
            }

          } else if ($type == 'html') {
            
            $doit = false;
            if (isset($item['select'])) {
              if ($item['select']) {
                $doit = true;
              }
            } else if ($item['fast'] === 0) {//apenas pesquisa avancada
              $doit = true;
            }
            
            if ($doit) {
              
              $where .= $conector . $item['id'] . "='" . $value . "'";
              $description = System::request('wd-' . $item['id'], "", "trim");

              if ($description) {
                $where_description .= $separator . "" . $item['description'] . " <b>" . $labels->lbl_04 . "</b> " . $description . "";
              } else {
                $where_description .= $separator . "" . $item['description'] . " <b>" . $labels->lbl_01 . "</b> " . $value . "";
              }

            }            
            
          } else if ($type == 'text' || $type == 'longtext' || $type == 'source-code' || $type == 'rich') {

            $where .= $conector . String::searchFormat($item['id'], $value) . "";
            $where_description .= $separator . "" . $item['description'] . " <b>" . $labels->lbl_07  . "</b> " . $value . "";

          } else if ($type == 'string' || $type == 'upper' || $type == 'user') {

            $value = $type == 'upper' ? strtoupper($value) : $value;
            $where .= $conector . "(" . String::searchFormat($item['id'], $value) . ")";
            $where_description .= $separator . "" . $item['description'] . " <b>" . $labels->lbl_05  . "</b> " . $value . "";

          } else if ($type == 'option' || $type == 'list') {

            if ($value) {

              $t = $item['type_content'];
              $type_content = explode("|", $t);

              $label = $value;
              $id = $value;
              for ($i = 0; $i < count($type_content); $i++) {
                $data = explode(",", $type_content[$i]);

                if (isset($data[1])) {
                  $_value = $data[1];
                  if ($value == $_value) {
                    $id = $data[0];
                  }
                }
              }

              $where .= $conector . $item['id'] . "='" . $id . "'";
              $where_description .= $separator . "" . $item['description'] . " <b>" . $labels->lbl_04 . "</b> " . $label . "";
            }

          } else if ($type == 'boolean' || $type == 'yes/no') {

            if (is_numeric($value)) {

              $where .= $conector . $item['id'] . "='" . $value . "'";
              $boolean = ($value) ? 'Sim' : 'N&atilde;o';
              $where_description .= $separator . "" . $item['description'] . " <b>" . $labels->lbl_08 ."</b> " . $boolean . "";
            }

          } else if ($type == 'between' || $type == 'date' || $type == 'datetime') {

            $between = "BETWEEN";
            $type_content = $item['type_content'];
            if ($type_content === 'NOT') {
              $between = "NOT BETWEEN";
            }
            $where .= $conector . "DATE(" . $item['id'] . ") " . $between . " '" . Date::brmysql($start) . "' AND '" . Date::brmysql($end) . "'";
            $where_description .= $separator . "" . $item['description'] . " <b>" . $labels->lbl_09 ."</b> " . $start . " " . $labels->lbl_03 ." " . $end . "";

          } else if ($type == 'subquery') {

            $type_content = $item['type_content'];
            $filter = explode('|', $type_content);
            $where_ = '';
            foreach ($filter as $key => $value) {
              if (!isset($subquery[$key]) || !$subquery[$key]) {
                continue;
              }

              $filter[$key] = explode(';', $value);

              if ($key) {
                $separator = " <br><i>ou</i><br> ";
              }

              if (!isset($filter[$key][1])) {
                $filter[$key][1] = $item['description'];
              }

              $where_ .= " OR (" . $filter[$key][0] . ")";
              $where_description .= $separator . "" . $labels->lbl_11 .": <b>" . $filter[$key][1] . "</b> " . $labels->lbl_10 ."";
            }

            if ($where_) {
              $where .= $conector . "(0 " . $where_ . ")";
            }

          } else if ($type == 'subquery2') {

            $id = $item['id'];
            $type_content = $item['type_content'];
            $fields = explode('|', $type_content);
            $filter = explode(';', $fields[$value]);

            $logic = System::request($id . "_type");
            $input = System::request($id . "_input");

            $where .= $conector . "(" . $filter[0] . ") " . $logic . " " . $input . "";
            $where_description .= $separator . "" . $labels->lbl_11 ." <b>" . $filter[1] . " " . $logic . " " . $input . "</b> " . $labels->lbl_10 ."";

          } else if ($type == 'subquery-radio') {

            $type_content = $item['type_content'];
            $filters = explode('|', $type_content);
            
            $filter = explode(";", $filters[$value]);
            
            $where .= $conector . "(" . $filter[0] . ")";
            $where_description .= $separator . "" . $labels->lbl_11 ." <b>" . $filter[1] . "</b> " . $labels->lbl_10 ."";

          }

        }

      }
      
      $recovered['w'] = substr($where, strlen($conector));
      $recovered['wd'] = substr(str_replace(array(" OR ", " or ", " Or ", " oR "," OU ", " ou ", " Ou ", " oU "), " <b>" . $labels->lbl_12 ."</b> ", $where_description), strlen($separator));
    }

    if ($group) {
      $group = "";
      for ($var = 0; $var < count($items); $var++) {
        
      }
      $recovered['g'] = $group;
      $recovered['gd'] = '';
    }

    if ($order) {
      $order = "";
      for ($var = 0; $var < count($items); $var++) {
        
      }
      $recovered['o'] = $order;
      $recovered['od'] = '';
    }

    return $recovered;
  }

  /**
   *
   * @param type $item
   * @param type $value
   *
   * @return string
   */
  public static function validate($item, $value) {

    $validate = "";

    if ($item['validate']) {
      if ($item['readonly'] == 0) {
        if ($value !== null) {
          $invalids = array("", '', '(__) ____-____', '_____-___', '__.___.___/____-__', '___.___.___-__', '__/__/____', '0,00', '0.00', '(  )     -    ', '     -   ', '  .   .   /    -  ', '   .   .   -  ', '  /  /    ');

          switch ($item['validate']) {
            case '[E]':
              $invalid = in_array($value, $invalids, true);
              if ($invalid) {
                $validate = array("id" => $item['id'], "description" => $item['description'], "type" => $item['type'], "value" => $item['value'], "message" => "Favor informar um valor v&aacute;lido");
              }
              break;

            case '[P]':
              $invalid = in_array($value, $invalids, true);
              if ($invalid) {
                $validate = array("id" => $item['id'], "description" => $item['description'], "type" => $item['type'], "value" => $item['value'], "message" => "Favor informar um valor v&aacute;lido");
              }
              break;

            case '[F]':
              $invalid = in_array($value, $invalids, true);
              if ($invalid) {
                $validate = array("id" => $item['id'], "description" => $item['description'], "type" => $item['type'], "value" => $item['value'], "message" => "Favor informar um valor v&aacute;lido");
              }
              break;

            case '[T]':
              $invalid = in_array($value, $invalids, true);
              if ($invalid) {
                $validate = array("id" => $item['id'], "description" => $item['description'], "type" => $item['type'], "value" => $item['value'], "message" => "Favor informar um valor v&aacute;lido");
              }
              break;

            case '[R]':
              $invalid = in_array($value, $invalids, true);
              if ($invalid) {
                $validate = array("id" => $item['id'], "description" => $item['description'], "type" => $item['type'], "value" => $item['value'], "message" => "Favor informar um valor v&aacute;lido");
              }
              break;

            case '[C]':
              $invalid = in_array($value, $invalids, true);
              if ($invalid) {
                $validate = array("id" => $item['id'], "description" => $item['description'], "type" => $item['type'], "value" => $item['value'], "message" => "Favor informar um valor v&aacute;lido");
              }
              break;

            case '[D]':
              $invalid = in_array($value, $invalids, true);
              if ($invalid) {
                $validate = array("id" => $item['id'], "description" => $item['description'], "type" => $item['type'], "value" => $item['value'], "message" => "Favor informar um valor v&aacute;lido");
              }
              break;

            case '[S]':
              $invalid = in_array($value, $invalids, true);
              if ($invalid) {
                $validate = array("id" => $item['id'], "description" => $item['description'], "type" => $item['type'], "value" => $item['value'], "message" => "Favor informar um valor v&aacute;lido");
              }
              break;

            case '[CPF]':
              $invalid = in_array($value, $invalids, true);
              if ($invalid) {
                $validate = array("id" => $item['id'], "description" => $item['description'], "type" => $item['type'], "value" => $item['value'], "message" => "Favor informar um valor v&aacute;lido");
              }
              break;

            default:
              break;
          }
        }
      }
    }

    return $validate;
  }

  /**
   *
   * @param type $text
   * @param type $encode
   * @param type $addslaches
   * @param type $breakline
   * @param type $htmlentities
   *
   */
  public static function encode($text, $encode = false, $addslaches = false, $breakline = false, $htmlentities = false) {
    if ($encode) {
      $text = $encode === 'encode' ? utf8_encode($text) : $text;
      $text = $encode === 'decode' ? utf8_decode($text) : $text;
    }
    return $text;
  }

  /**
   * captura o ip do cliente
   * @return <type>
   */
  public static function address($all = true) {
    $HTTP_CLIENT_IP = isset($_SERVER['HTTP_CLIENT_IP']) ? $_SERVER['HTTP_CLIENT_IP'] : "";
    $HTTP_X_FORWARDED_FOR = isset($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : "";
    $REMOTE_ADDR = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : "";

    if (PHP_SAPI == 'cli') {

      $address = "localhost";
    } else {

      //check ip from share internet
      if (!empty($HTTP_CLIENT_IP)) {
        $ip = $HTTP_CLIENT_IP;
        //to check ip is pass from proxy
      } else if (!empty($HTTP_X_FORWARDED_FOR)) {
        $ip = $HTTP_X_FORWARDED_FOR;
      } else {
        $ip = $REMOTE_ADDR;
      }
      $address = $ip;
      if ($all) {
        $address = $REMOTE_ADDR . ":" . $ip;
      }
    }

    return $address;
  }

  /**
   * Retorna o n�mero da linha do relat�rio
   * @param PHPReports $report
   * @param int $header = 3 N�mero de linhas do cabe�alho
   * @param int $lines = 70 N�mero de resultados por linha
   * @return int
   */
  public static function reportLine($report, $header = 3, $lines = 70) {
    return ($report->getRowNum()) + (($report->getPageNum() - 1) * $lines) - ($report->getPageNum() * $header);
  }

  /**
   *
   * @param type $message
   * @param type $type
   */
  public static function showMessage($message, $type = 1) {
    System::import('class', 'resource', 'Screen', 'core', true);

    $screen = new Screen();

    if ($type === 1) {
      $screen->message->printMessageError($message);
    } else if ($type === 2) {
      $screen->message->printMessageSucess($message);
    }
  }

  /**
   *
   * @param type $param
   * @param type $properties
   * @param type $object
   * @param type $condicional
   * @param type $replace
   * @param type $copia
   * @param type $exibir
   */
  public static function notification($param, $properties, $object, $condicional = '', $replace = array(), $copia = null, $exibir = false) {

    System::import('class', 'base', 'Email', 'core', true, '');

    System::import('c', 'manager', 'EmailNotificacao', 'src');
    System::import('m', 'manager', 'EmailNotificacao', 'src');

    $email = new Email(PATH_APP);

    $emailNotificacaoCtrl = new EmailNotificacaoCtrl(PATH_APP);
    $emailNotificacaos = $emailNotificacaoCtrl->getEmailNotificacaoCtrl("(emn_action = '" . $param . "' OR emn_action = 'all') AND emn_tag = '" . $properties['tag'] . "' AND emn_condicional = '" . $condicional . "'", "", "");
    if (is_array($emailNotificacaos)) {

      $rotule = isset($properties['rotule']) ? $properties['rotule'] : '';
      $title = $param == 'add' ? "Novo " . $rotule . " registrado" : $param == 'set' ? "Altera&ccedil;&atilde;o feita em " . $rotule : $param == 'remove' ? "Registro removido de " . $rotule : "";

      System::import('c', $properties['module'], $properties['entity'], 'src');

      $entityCtrl = $properties['entity'] . "Ctrl";
      $getColumnCtrl = "getColumn" . $properties['entity'] . "Ctrl";
      $get_items = "get_" . $properties['prefix'] . "_items";
      $get_value = "get_" . $properties['prefix'] . "_value";

      $objectCtrl = new $entityCtrl(PATH_APP);
      $items = $object->$get_items();

      $exclude = array("alteracao", "registro", "responsavel", "criador");
      $changed = "";
      foreach ($items as $item) {
        if (!in_array($item['type'], $exclude)) {
          $key = $item['id'];

          $value = $item['value'];

          if (isset($item['foreign'])) {
            $description = $objectCtrl->$getColumnCtrl($item['foreign']['description'], $item['foreign']['key'] . " = '" . $item['value'] . "'", $item['foreign']['table'], true, false);
            $value = Json::encodeValue($description);
          } else if ($item['type'] == 'calculated') {
            $description = $objectCtrl->$getColumnCtrl($item['type_content'], $properties['reference'] . " = '" . $object->$get_value($properties['reference']) . "'");
            $value = Json::encodeValue($description);
          } else {
            $value = Json::render($object, $key, $properties['prefix']);
          }

          $changed .= '<tr><td align="right"><b>' . $item['description'] . '</b>: </td><td>' . System::encode($value, UTF8_EMAIL) . '</td></tr>';
        }
      }

      $replace['rotule'] = $rotule;
      $replace['title'] = $title;
      $replace['changed'] = "<table>" . $changed . "</table>";

      foreach ($emailNotificacaos as $emailNotificacao) {
        $email->sendTemplate($emailNotificacao->get_emn_value('emn_email'), $emailNotificacao->get_emn_value('emn_nome'), $copia, $emailNotificacao->get_emn_value('emn_cod_EMAIL_MODELO'), $replace, $exibir);
        $copia = null;
      }
    }
  }

  /**
   * 
   * @param string $string
   * @param array $replaces
   * 
   * @return string
   */
  public static function replaceMarkup($string, $replaces = array()) {
   
    if (is_array($replaces)) {
      
      $replaces[] = array("key"=>'sys_nome', "value"=>COMPANY, "type"=>"string");
      $replaces[] = array("key"=>'sys_nome_curto', "value"=>COMPANY_SHORT, "type"=>"string");
      $replaces[] = array("key"=>'sys_razao_social', "value"=>COMPANY_RAZAO_SOCIAL, "type"=>"string");

      $replaces[] = array("key"=>'sys_cnpj', "value"=>COMPANY_CNPJ, "type"=>"string");
      $replaces[] = array("key"=>'sys_telefone', "value"=>COMPANY_TELEFONE, "type"=>"string");
      $replaces[] = array("key"=>'sys_caixa_postal', "value"=>COMPANY_CX, "type"=>"string");
      $replaces[] = array("key"=>'sys_site', "value"=>COMPANY_SITE, "type"=>"string");
      $replaces[] = array("key"=>'sys_email', "value"=>COMPANY_EMAIL, "type"=>"string");

      $replaces[] = array("key"=>'sys_endereco', "value"=>COMPANY_ADDRESS, "type"=>"string");
      $replaces[] = array("key"=>'sys_bairro', "value"=>COMPANY_DISTRICT, "type"=>"string");
      $replaces[] = array("key"=>'sys_cidade', "value"=>COMPANY_CITY, "type"=>"string");
      $replaces[] = array("key"=>'sys_estado', "value"=>COMPANY_STATE, "type"=>"string");
      $replaces[] = array("key"=>'sys_uf', "value"=>COMPANY_UF, "type"=>"string");
      $replaces[] = array("key"=>'sys_cep', "value"=>COMPANY_CEP, "type"=>"string");

      $replaces[] = array("key"=>'sys_dados', "value"=>COMPANY_Z, "type"=>"string");
      
      $replaces[] = array("key"=>'sys_short', "value"=>SHORT, "type"=>"string");
      $replaces[] = array("key"=>'sys_title', "value"=>TITLE, "type"=>"string");
      $replaces[] = array("key"=>'sys_copy_right', "value"=>COPY_RIGHT, "type"=>"string");
      
      $replaces[] = array("key"=>'sys_page_app', "value"=>PAGE_APP, "type"=>"string");
      $replaces[] = array("key"=>'sys_page_photo', "value"=>PAGE_PHOTO, "type"=>"string");

      $replaces[] = array("key"=>'sys_user', "value"=>System::getUser(false), "type"=>"string");
      $replaces[] = array("key"=>'sys_now', "value"=>date('d/m/Y H:i:s'), "type"=>"datetime");
      $replaces[] = array("key"=>'sys_date', "value"=>date('d/m/Y'), "type"=>"date");
      
      $replaces[] = array("key"=>'sys_uniqid', "value"=>uniqid(), "type"=>"string");

      foreach ($replaces as $replace) {

        $key = $replace['key'];
        $value = $replace['value'];
        $extra = "";

        if ($replace['type'] === "date") {
          $value = String::substring($value, 0, 10);
          $extra = String::dateText($value, false, false);
        } else if ($replace['type'] === "money") {
          $extra = String::moneyText($value, true);
          $value = Number::format($value);
        } else if ($replace['type'] === "int") {
          $extra = String::moneyText($value, true, "number");
        }

        $extenso = trim($extra);

        $s = String::replace($string, '[{' . $key . '}]', $extenso);
        $st = String::replace($s, '({' . $key . '})', $value . ' (' . $extenso . ')');
        $string = String::replace($st, '{' . $key . '}', $value);

      }

    }
    
    return $string;
  }
  
}