<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Json
 *
 * @author William
 */
class Json {

  //put your code here

  const actionView = 'icon-view';
  const actionEdit = 'icon-edit';
  const actionRemove = 'icon-trash';

  /**
   * 
   * @param type $resultset
   * @param type $sigle
   * @return type
   */
  public static function parseQuery($resultset, $sigle = true) {

    $json_str = ""; //Init the JSON string.

    if (!$sigle)
      $json_str .= "[\n";

    if ($total = mysql_num_rows($resultset)) { //See if there is anything in the query
      $row_count = 0;
      while ($data = mysql_fetch_assoc($resultset)) {
        if (count($data) > 1)
          $json_str .= "{\n";

        $count = 0;
        foreach ($data as $key => $value) {
          //If it is an associative array we want it in the format of "key":"value"
          if (count($data) > 1)
            $json_str .= "\"$key\":\"$value\"";
          else
            $json_str .= "\"$value\"";

          //Make sure that the last item don't have a ',' (comma)
          $count++;
          if ($count < count($data))
            $json_str .= ",\n";
        }
        $row_count++;
        if (count($data) > 1)
          $json_str .= "}\n";

        //Make sure that the last item don't have a ',' (comma)
        if ($row_count < $total)
          $json_str .= ",\n";
      }
    }

    if (!$sigle)
      $json_str .= "]\n";

    //Replace the '\n's - make it faster - but at the price of bad redability.
    $json = str_replace("\n", "", $json_str); //Comment this out when you are debugging the script
    //Finally, output the data
    return $json;
  }

  /**
   * 
   * @param type $jsonurl
   * @return type
   */
  function parse($jsonurl) {
    //Retorna o conteudo do arquivo em formato de string
    $json = file_get_contents($jsonurl, 0, null, null);

    //Decodificando a string e criando o json
    $json_object = json_decode($json);

    return $json_object;
  }

  /**
   * 
   * @param type $acesso_controle
   * @param type $type
   * @param type $items
   * @param type $operations
   * @param type $reference
   * @param type $referenced
   * @param type $readonly
   * @param type $saveonly
   * @return type
   */
  public static function parseAction($acesso_controle, $type, $items, $operations, $reference, $referenced, $readonly, $saveonly) {

    $actions = array();
    foreach ($operations as $op) {

      if ($op->position == 'grid') {

        if ($acesso_controle >= $op->level) {

          $add = 1;

          $settings = $op->settings;          
          $conditions = null;
          if ($settings) {
            $conditions = $settings->conditions;
          }

          if (is_array($conditions)) {
            foreach ($conditions as $id => $value) {
              $add = ($items[$id]['value'] === $value) ? $add : $add - 1;
            }
          }
            

          if ($add >= 1) {
            $actions[] = '{"reference":"' . $reference . "=" . $referenced .'","class":"' . $op->class . '","type":"' . $op->type . '","layout":"' . $op->layout . '","operation":"' . $op->action . '","label":"' . $op->label . '","confirm":"' . $op->confirm . '"}';
          }

        }

      }

    }

    return self::encodeAction("[" . implode(",", $actions) . "]");
  }
  
  /**
   * 
   * @param type $item
   * @param type $encode
   * @return type
   */
  public static function parseValue($item, $encode = true) {

    $type = $item['type'];
    $value = $item['value'];
    $reference = '';

    if ($type == 'option' || $type == 'list') {

      $t = $item['type_content'];
      $type_content = explode("|", $t);

      for ($i = 0; $i < count($type_content); $i++) {

        $data = explode(",", $type_content[$i]);

        $_value = $data[0];

        if (isset($data[1])) {
          if ($value == $_value) {
            $value = $data[1];
          }
        }

      }

    } else if ($item['fk']) {

      if (isset($item['reference'])) {
        $reference = $item['reference'];
      }

    }

    if ($encode) {
      if (UTF8_SYSTEM === 'encode') {
        $value = utf8_encode($value);
        $reference = utf8_encode($reference);
      } else if (UTF8_SYSTEM === 'decode') {
        $value = utf8_decode($value);
        $reference = utf8_decode($reference);
      } else {
        $value = utf8_encode($value);
        $reference = utf8_encode($reference);
      }
    }

    $item['value'] = $value;

    if ($reference) {
      $item['reference'] = $reference;
    }

    return $item;
  }

  /**
   *
   * @param type $encoded
   * @return type
   */
  public static function decodeValue($encoded) {
    $decoded = utf8_decode($encoded);
    return $decoded;
  }

  /**
   *
   * @param type $decoded
   * @return type
   */
  public static function encodeValue($decoded) {
    $v = strip_tags($decoded);
    $encoded = utf8_encode($v);
    return $encoded;
  }

  /**
   *
   * @param type $action
   * @return type
   */
  public static function encodeAction($action) {
    $v = str_replace("###", "&nbsp;", $action);
    $value = utf8_encode($v);
    return $value;
  }

  /**
   * 
   * @param type $email
   * @return type
   */
  public static function validateEmail($email) {
    $validate = false;

    $username = EMAIL_VALIDATE_LOGIN;//coordti@fagoc.br
    $password = EMAIL_VALIDATE_PASSWORD;//F4g0k1

    $api_url = EMAIL_VALIDATE_URL . '?';//api.verify-email.org/api.php
    $url = 'http' . '://' . $api_url . 'usr=' . $username . '&pwd=' . $password . '&check=' . $email;

    if (function_exists('curl_init')) {
      $validate = self::getContentsCurl($url);
    } else {
      $validate = file_get_contents($url);
    }

    return $validate;
  }

  /**
   * 
   * @param type $url
   * @return type
   */
  private static function getContentsCurl($url) {
    // Initiate the curl session
    $ch = curl_init();

    // Set the URL
    curl_setopt($ch, CURLOPT_URL, $url);

    // Removes the headers from the output
    curl_setopt($ch, CURLOPT_HEADER, 0);

    // Return the output instead of displaying it directly
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    // Execute the curl session
    $output = curl_exec($ch);

    // Close the curl session
    curl_close($ch);

    // Return the output as a variable
    return $output;
  }
    
  /**
   * 
   * @param type $actions
   * @return type
   */
  public static function parseProperties($actions) {
    $properties = "{}";
    
    if (is_array($actions)) {
      $properties = json_encode($actions);
    }
    
    return $properties;
  }

  /**
   * 
   * @param type $value
   * 
   * @return type
   */
  public static function encode($value) {
    return json_encode($value);
  }
  
  /**
   * 
   * @param type $json
   * @param type $array
   * 
   * @return type
   */
  public static function decode($json, $array = false) {
    return json_decode($json, $array);
  }

}
