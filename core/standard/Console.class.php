<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Util
 *
 * @author William
 */
class Console{

  private static $location = "log";
  private static $activated = false;
  
  private static $messages = array();
  
  public static $STATUS_ERROR = "E";
  public static $STATUS_WARNING = "W";
  public static $STATUS_SUCESS = "S";
  
  /**
   * 
   * @return type
   */
  private static function getLocation() {
    return PATH_APP . self::$location;
  }
  
  /**
   * 
   * @param type $message
   * @param type $file
   * @param type $clear
   * 
   * @return type
   */
  public static function log($message, $file = "file.txt", $clear = null) {
    
    $log = false;
    $dir = self::getLocation();
    
    $exist = is_dir($dir);
    if (!$exist) {
      $exist = mkdir($dir, 0777, true);
    }
    
    if ($exist) {
      
      $filename = $dir . PATH_FILE_SEPARATOR . $file;
      
      if (!self::$activated) {
        $clear = $clear === null ? true : $clear;
        self::$activated = true;
      }
      
      if ($clear) {
        self::clear($filename);
      }
      
      $time = date('d/m/Y H:i:s');
      $contents = "[($time) " .  $message . "\r\n"."-\r\n";

      $log = File::saveFile($filename, $contents, "a+");
    }

    return $log;
  }
  
  /**
   * 
   * @param type $filename
   * 
   * @return type
   */
  public static function clear($filename = "") {
    
    $dir = self::getLocation();
    $clear = false;
    
    $exist = is_dir($dir);
    if ($exist) {
      
      if ($filename === "") {
        $clear = File::delTree($dir);
      } else {
        if (file_exists($filename)) {
          unlink($filename);
        }
      }
      
    }
    
    return $clear;
  }

  /**
   * 
   * @param type $message
   * @param type $status
   * 
   * @return type
   */
  public static function add($message, $status = null) {

    $status = ($status === null) ? Console::$STATUS_ERROR : $status;

    return self::$messages[] = array("m"=>$message, "s"=>$status);
  }

  /**
   * 
   * @param type $clear
   * @param type $translates
   */
  public static function json($clear, $translates) {
    return json_decode(self::get($clear, $translates));
  }

  /**
   * 
   * @param type $clear
   * @param type $translates
   */
  public static function get($clear, $translates) {
    
    $messages = array();
    foreach (self::$messages as $message) {
      $messages[] = array(
        "m"=>System::replaceMarkup($message["m"], $translates), 
        "s"=>$message["s"]
      );
    }

    if ($clear) {
      self::$messages = array();
    }
    
    return $messages;
  }

}
