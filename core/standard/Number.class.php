<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Number
 *
 * @author William
 */
class Number{
  //put your code here

  /**
   *
   * $value = 1230,00
   * @param <type> $value
   * @return double
   */
  public static function parseDouble($value){
    //limpa formtacacao da virgula
    $number = (double) str_replace(",", ".", $value);
    return $number;
  }

  /**
   *
   * @param <type> $value
   * @return int
   */
  public static function parseInteger($value){
    //limpa formtacacao da virgula
    $number = ((int) $value) * 1;
    return $number;
  }

  /**
   *
   * @param <type> $valor
   * @param <type> $casas LEFT
   * @param <type> $completar acrescenta 00 no final
   * @return <type> STRING
   */
  public static function clear($valor, $casas, $completar = ""){
    //Sequencial = "" aplica formatacao financeira acrescentando 00 no final do numero
    if($completar == ""){
      $valor = str_replace(",", ".", $valor);
      $valor = number_format($valor, 2, ',', '');
      $valor = str_replace(",", "", $valor);
      $valor = str_replace(".", "", $valor);
    }
    $valor = str_pad($valor, $casas, "0", STR_PAD_LEFT);
    return $valor;
  }

  /**
   *
   * @param <type> $value
   * @param <type> $default
   * @param <type> $current
   * @param <type> $precision
   * @return <type>
   */
  public static function format($value, $default = true, $current = "", $precision = 2){
    $value = ($value) ? $value : 0;

    $precision = ($precision) ? ((int) $precision) : 2;
    if($default){
      $value = number_format($value, $precision, ',', '.');
    }else{
      $value = number_format($value, $precision, '.', '');
    }

    if($current != ""){
      $value = "<span style='float: left;'>".$current."</span><span style='float: right;'>".$value."</span>";
    }

    return $value;
  }

}

?>
