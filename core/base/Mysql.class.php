<?php

/**
 *
 * Faculdade Ubaense Ozanam Coelho - Fagoc
 * Projeto SIGA - 2010
 * Modulo..........: Geral
 * Funcionalidade..: Classe que prove recursos para a conexao com o banco
 * Pre-requisitos..: dao/Configuracao.class.php
 *
 * http://php.net/manual/en/book.mysql.php
 *
 */
System::import('class', 'pattern', 'Database', 'core');

class Mysql extends Database {

  /**
   * 
   * @param type $database
   * 
   * @return type
   */
  public function connect() {

    $this->restore();

    if (!$this->getTransaction()) {
      $this->connection = mysql_connect($this->server, $this->user, $this->password, true);
    }

    if ($this->debug) {
      print 'connect: ' . print_r($this->connection, true) . ' - ' . $this->db . '<br>';
    }

    if (is_resource($this->connection)) {

      $select = mysql_select_db($this->db, $this->connection);
      if (!$select) {
        core_err("Mysql Conection", "Erro na conex&atilde;o com o banco de dados. Verifique se o banco informado est&aacute; dispon&iacute;vel.");
      }

      $this->connected = true;
    } else {
      core_err("Mysql Conection", "Erro na conex&atilde;o com o banco de dados. Verifique as configura&ccedil;&otilde;es de conex&atilde;o.");
    }

    return $this->connection;
  }

  /**
   *
   */
  public function disconnect() {

    if (!$this->getTransaction()) {

      if ($this->connected) {

        if (is_resource($this->connection)) {

          if ($this->debug) {
            print 'disconnect: ' . print_r($this->connection, true) . '<br>';
          }

          mysql_close($this->connection);
          $this->connected = false;
        }
      }
    }
  }

  /**
   * 
   * @param type $sql
   * 
   * @return type
   */
  protected function query($sql) {

    if ($this->debug) {
      print $sql . "<br>";
    }

    $u = System::getUser(false);
    $user = $u ? $u : "ANONIMO";

    $trace = "";
    //$dt = debug_backtrace(false);
    //if (is_array($dt)) {
    //  foreach ($dt as $k=> $v) {
    //    if (isset($v['file'])) {
    //      $trace = "[" . $v['file'] . "->" . $v['function'] . "]" . $trace;
    //    }
    //  }
    //}

    $query = mysql_query('/*' . $user . '*//*' . $trace . '*/ ' . $sql, $this->connection);

    return $query;
  }

  /**
   * 
   * @param type $sql
   * @param type $save
   * @param type $validate
   * @param type $deactivate
   * 
   * @return int (auto increment id)
   */
  public function insert($sql, $save = true, $validate = true, $deactivate = false) {

    if ($validate) {
      $this->validate($sql);
    }

    $this->connect();
    if ($deactivate) {
      $this->query('SET FOREIGN_KEY_CHECKS = 0');
    }
    $this->query($sql);

    $inserted = (mysql_insert_id($this->connection)) ? mysql_insert_id($this->connection) : mysql_affected_rows($this->connection);
    $affected_rows = ($inserted) ? "Rows inserted: 1" : false;
    $backup = "NO BACKUP (NEW ID $inserted)";

    if ($deactivate) {
      $this->query('SET FOREIGN_KEY_CHECKS = 1');
    }

    $this->error($sql, true);
    $this->disconnect();

    if ($save) {
      $this->history($sql, $backup, $affected_rows);
    }
    return $inserted;
  }

  /**
   * 
   * @param type $sql
   * @param type $history
   * @param type $validate
   * @param type $id
   * @param type $transaction
   * 
   * @return type
   */
  public function execute($sql, $history = true, $validate = true, $id = "", $transaction = null) {

    if ($validate) {
      $this->validate($sql);
    }

    $backup = "";
    if ($history) {
      $backup = $this->generateBackup($sql);
    }

    $this->connect($id);

    if ($transaction !== null) {
      $this->setTransaction($transaction);
    }

    $affected_rows = $this->query($sql, $this->connection);
    $info = mysql_info($this->connection);
    if ($affected_rows) {
      $affected_rows = mysql_affected_rows($this->connection) ? mysql_affected_rows($this->connection) : $affected_rows;
    }

    $this->error($sql, true);

    $this->disconnect();

    if ($history) {
      $this->history($sql, $backup, $info);
    }
    return $affected_rows;
  }

  /**
   * 
   * @param resource $result
   * 
   * @return array
   */
  public function fetchRow($result) {
    $fetched = mysql_fetch_array($result);

    return $fetched;
  }

  /**
   * 
   * @param type $sql
   * 
   * @return string
   */
  protected function generateBackup($sql) {
    return "";

    $backup = "";
    $acsh_tipo = $this->getOperationType($sql);

    if ($acsh_tipo === 'INSERT' or $acsh_tipo === 'DELETE' or $acsh_tipo === 'UPDATE') {

      $s = trim($sql);
      $sq = preg_replace('/\s+/', ' ', $s);
      $sql = str_replace("(", "(", $sq);

      $table = $this->getOperationTable($sql, $acsh_tipo);
      $value = "";
      $f = "";

      $select = "DESC " . $table;
      $resource = $this->select($select);
      if ($resource != NULL) {
        while ($row = mysql_fetch_array($resource)) {
          $fields[] = $row['Field'];
          $f .= "," . $row['Field'];
        }
      }
      $field = substr($f, 1);
      $start = strrpos($sql, "WHERE");
      $where = substr($sql, ($start + 5));

      $query = "SELECT " . $field . " FROM " . $table . " WHERE " . $where;
      $resultset = $this->select($query);

      if (gettype($resultset) === 'resource') {

        if (mysql_num_rows($resultset) == 1) {

          if ($acsh_tipo == "UPDATE") {
            if ($resultset != NULL) {
              while ($row = mysql_fetch_array($resultset)) {
                for ($a = 0; $a < count($fields); $a++) {
                  $value .= "," . $fields[$a] . " = '" . addslashes($row[$fields[$a]]) . "'";
                }
              }
            }
            $value = substr($value, 1);
            $backup = "UPDATE " . $table . " SET " . $value . " WHERE " . $where;
          } else if ($acsh_tipo == "DELETE") {
            if ($resultset != NULL) {
              while ($row = mysql_fetch_array($resultset)) {
                for ($a = 0; $a < count($fields); $a++) {
                  $value .= ",'" . addslashes($row[$fields[$a]]) . "'";
                }
              }
            }
            $value = substr($value, 1);
            $backup = "INSERT INTO " . $table . " " . $field . " VALUES (" . $value . ")";
          }
        } else {
          $backup = "Max data size allocated"; //Json::parseQuery($resultset)
        }
      }
    }

    return $backup;
  }

  /**
   * 
   * @param type $sql
   * @param type $save
   * 
   * @return boolean
   */
  protected function error($sql, $save = true) {
    $err = false;

    if ($sql) {

      $err_number = mysql_errno($this->connection);
      $err_description = mysql_error($this->connection);

      if (isset($err_number)) {
        $err = true;

        if ($err_number > 0) {

          if ($err_number == 1451) {
            print '<span class="error_message">';
            print '  &nbsp;N&atilde;o &eacute; poss&iacute;vel excluir este registro. Remova primeiro todas as refer&ecirc;ncias feitas a ele.&nbsp;';
            print '</span>';
          } else {
            core_err($err_number, $err_description, null, null, $sql, "MYSQL");
          }
        }
      }
    }
    return $err;
  }

  /**
   *
   * @param type $item
   * @param type $source
   *
   * @return boolean
   */
  public function isValid($item, $source) {

    $return = true;

    $type = $item['type'];

    $invalid_types = array("", "html", "sub-item", "subquery", "subquery-radio", "select-multi");
    $invalid_type_behaviors = array("child", "popup", "parent");

    $system_types = array("criador", "registro", "responsavel", "alteracao");

    if (in_array($type, $invalid_types) || in_array($item['type_content'], $invalid_type_behaviors)) {

      $return = false;
    } else if ($source === 'SELECT') {

      if (isset($item['select'])) {
        if (!$item['select']) {
          $return = false;
        }
      }
    } else if ($source === 'INSERT') {

      if (!$item['insert']) {
        $return = false;
      } else if ($item['value'] === null && !in_array($type, $system_types)) {
        $return = false;
      }
    } else if ($source === 'UPDATE') {

      if ($type === 'criador' || $type === 'registro') {
        $return = false;
      } else if (!$item['update']) {
        $return = false;
      } else if ($item['value'] === null && !($type === 'responsavel' || $type === 'alteracao')) {
        $return = false;
      }
    }

    return $return;
  }

  /**
   * 
   * @param array $item
   * @param string $source
   * 
   * @return array
   */
  public function prepare($item, $source) {

    $item['field'] = $item['id'];

    if ($source === 'SELECT') {

      if ($item['type'] === 'calculated') {
        $item['field'] = "(" . $item['type_content'] . ")";
      } else if ($item['fk']) {
        if (isset($item['foreign'])) {
          $item['reference'] = $item['foreign']['description'];
        }
      }
    }

    return $item;
  }

  /**
   *
   * @param type $item
   * @param type $value
   * @param type $source
   * 
   * @return string
   */
  public function renderer($item, $value, $source) {

    $value = String::mask($value, $item['type']);
    $user = System::getUser(false) ? System::getUser(false) : 'VISITANTE';

    if ($source === 'SELECT') {
      if ($item['type'] === 'alteracao') {
        $value = Date::mysqlbr($value, true, false);
      } else if ($item['type'] === 'registro') {
        $value = Date::mysqlbr($value, true, false);
      } else if ($item['type'] === 'datetime') {
        #$value = Date::mysqlbr($value, true, false); //Old
        $value = Util::isDate(Date::mysqlbr($value, false, false)) ? $value : "";
      } else if ($item['type'] === 'date') {
        $value = Date::mysqlbr($value, false, false);
      } else if ($item['type'] === 'base64') {
        $value = stripcslashes(base64_decode($value));
      } else if ($item['type'] === 'date') {
        $value = "'" . Date::mysqlbr($value) . "'";
      } else if ($item['type'] === 'source-code') {
        $value = "" . ($value) . "";
      } else if ($item['type'] === 'rich-text') {
        $value = stripcslashes($value);
      } else if ($item['type'] === 'fk') {
        $value = ((int) $value) !== 0 ? $value : '';
      } else if ($item['type'] === 'second') {
        $value = stripcslashes(String::substring($value, 3, 5));
      } else if (in_array($item['type'], array('cep', 'cpf', 'cnpj', 'telefone'), true)) {
        $value = String::mask($value, $item['type']);
      } else {
        $value = stripcslashes($value);
      }
    } else if ($source === 'INSERT') {

      if ($item['type'] === 'alteracao') {
        $value = "NOW()";
      } else if ($item['type'] === 'registro') {
        $value = "NOW()";
      } else if ($item['type'] === 'datetime') {
        $value = "'" . $value . "'";
      } else if ($item['type'] === 'date') {
        $value = "'" . Date::brmysql($value) . "'";
      } else if ($item['type'] === 'upper') {
        $value = "UPPER('" . addslashes($value) . "')";
      } else if ($item['type'] === 'base64') {
        $value = "'" . base64_encode($value) . "'";
      } else if ($item['type'] === 'source-code') {
        if (ADD_SLASHES_DAO) {
          $value = "'" . addslashes($value) . "'";
        } else {
          $value = "'" . $value . "'";
        }
      } else if ($item['type'] === 'rich-text') {
        $value = "'" . addslashes($value) . "'";
      } else if ($item['type'] === 'time') {
        $value = "'" . $value . "00'";
      } else if ($item['type'] === 'timestamp') {
        $value = "'" . $value . "'";
      } else if ($item['type'] === 'second') {
        $value = "'00" . $value . "'";
      } else if ($item['type'] === 'hash') {
        if (MODO_LEGADO === 'true') {
          $value = "PASSWORD('" . addslashes($value) . "')";
        } else {
          $value = "MD5('" . addslashes($value) . "')";
        }
      } else if ($item['type'] === 'user') {
        if ($value) {
          $value = "'" . addslashes($value) . "'";
        } else {
          $value = "'" . $user . "'";
        }
      } else if ($item['type'] === 'boolean' or $item['type'] === 'yes/no') {
        if ($value) {
          if ($value === 'false') {
            $value = "0";
          } else {
            $value = "1";
          }
        } else {
          $value = "0";
        }
      } else if ($item['type'] === 'criador') {
        $value = "'" . $user . "'";
      } else if ($item['type'] === 'responsavel') {
        $value = "'" . $user . "'";
      } else if (in_array($item['type'], array('cep', 'cpf', 'cnpj', 'telefone'), true)) {
        $value = "'" . String::mask($value, $item['type']) . "'";
      } else {
        $value = "'" . addslashes($value) . "'";
      }
    } else if ($source === 'UPDATE') {

      if ($item['type'] === 'alteracao') {
        $value = "NOW()";
      } else if ($item['type'] === 'datetime') {
        $value = "'" . $value . "'";
      } else if ($item['type'] === 'date') {
        $value = "'" . Date::brmysql($value) . "'";
      } else if ($item['type'] === 'upper') {
        $value = "UPPER('" . addslashes($value) . "')";
      } else if ($item['type'] === 'base64') {
        $value = "'" . base64_encode($value) . "'";
      } else if ($item['type'] === 'source-code') {
        if (ADD_SLASHES_DAO) {
          $value = "'" . addslashes($value) . "'";
        } else {
          $value = "'" . $value . "'";
        }
      } else if ($item['type'] === 'rich-text') {
        $value = "'" . addslashes($value) . "'";
      } else if ($item['type'] === 'time') {
        $value = "'" . $value . "00'";
      } else if ($item['type'] === 'timestamp') {
        $value = "'" . $value . "'";
      } else if ($item['type'] === 'second') {
        $value = "'00" . $value . "'";
      } else if ($item['type'] === 'hash') {
        if (MODO_LEGADO === 'true') {
          $value = "PASSWORD('" . addslashes($value) . "')";
        } else {
          $value = "MD5('" . addslashes($value) . "')";
        }
      } else if ($item['type'] === 'execute') {
        $value = $value;
      } else if ($item['type'] === 'user') {
        if ($value) {
          $value = "'" . addslashes($value) . "'";
        } else {
          $value = "'" . $user . "'";
        }
      } else if ($item['type'] === 'boolean' or $item['type'] === 'yes/no') {
        if ($value) {
          if ($value === 'false') {
            $value = "0";
          } else {
            $value = "1";
          }
        } else {
          $value = "0";
        }
      } else if ($item['type'] === 'criador') {
        $value = "'" . $user . "'";
      } else if ($item['type'] === 'responsavel') {
        $value = "'" . $user . "'";
      } else if (in_array($item['type'], array('cep', 'cpf', 'cnpj', 'telefone'), true)) {
        $value = "'" . String::mask($value, $item['type']) . "'";
      } else {
        $value = "'" . addslashes($value) . "'";
      }
    }

    return $value;
  }

  /**
   * 
   * @param array $items
   * @param string $table
   * @param string $where
   * @param string $group
   * @param string $order
   * @param string $start
   * @param string $end
   * @param array $fields
   * @param boolean $validate
   */
  public function buildSelect($items, $table, $where, $group = null, $order = null, $start = null, $end = null, $fields = null) {

    if ($where) {
      $where = " WHERE " . $where;
    }

    if ($group) {
      $group = " GROUP BY " . $group;
    }

    if ($order) {
      $order = " ORDER BY " . $order;
    }

    if ($start) {
      $start = 0;
    }

    $limit = "";
    if ($end) {
      $limit = " LIMIT " . $start . "," . $end;
    }

    $arr_fields = array();
    if (!is_array($fields)) {
      $fields = array();
      foreach ($items as $item) {
        $fields[] = $item['id'];
      }
    }

    foreach ($items as $item) {
      if ($this->isValid($item, 'SELECT')) {
        $field = $item['id'];

        $item = $this->prepare($item, 'SELECT');

        if (in_array($item['id'], $fields)) {
          if ($item['type'] == 'calculated') {
            $field = "(" . $item['type_content'] . ") AS " . $item['id'];
          }

          $arr_fields[$item['id']] = $field;

          if (isset($item['reference'])) {
            $field = $item['reference'] . " AS " . $item['reference'];
            $arr_fields[$item['reference']] = $field;
          }
        }
      }
    }
    $fields = implode(", ", $arr_fields);

    $sql = "SELECT " . $fields . " FROM " . $table . $where . $group . $order . $limit;

    return $sql;
  }

  /**
   * 
   * @param array $items
   * @param string $table
   * 
   * @return string
   */
  public function buildInsert($items, $table) {
    $arr_fields = array();
    $arr_values = array();

    foreach ($items as $item) {
      if ($this->isValid($item, 'INSERT')) {
        $key = $item['id'];
        $value = $this->renderer($item, $item['value'], 'INSERT');
        $arr_fields[] = $key;
        $arr_values[] = $value;
      }
    }

    $fields = implode(', ', $arr_fields);
    $values = implode(', ', $arr_values);

    $sql = "INSERT INTO " . $table . " (" . $fields . ") VALUES (" . $values . ")";

    return $sql;
  }

  /**
   * 
   * @param type $table
   * @param type $items
   * @param type $where
   * 
   * @return string
   */
  public function buildUpdate($table, $items, $where) {
    $arr_update = array();

    foreach ($items as $item) {
      if ($this->isValid($item, 'UPDATE')) {
        $key = $item['id'];
        $value = $this->renderer($item, $item['value'], 'UPDATE');
        $arr_update[] = $key . " = " . $value;
      }
    }
    $update = implode(', ', $arr_update);

    $sql = "UPDATE " . $table . " SET " . $update . " WHERE " . $where;

    return $sql;
  }

  /**
   * 
   * @param string $table
   * @param string $where
   * 
   * @return string
   */
  public function buildDelete($table, $where) {
    $sql = "DELETE FROM " . $table . " WHERE " . $where;

    return $sql;
  }

  /**
   * 
   * @param array $items
   * @param resource $result
   * 
   * @return array
   */
  public function populateItems($items, $result) {
    $rows = null;

    if ($result != null) {

      $rows = array();

      while ($row = $this->fetchRow($result)) {
        foreach ($items as $key => $item) {
          $item = $this->prepare($item, 'SELECT');

          $value = $this->renderer($item, $row[$key], 'SELECT');

          $items[$key]['value'] = $value;

          $reference = "";
          if (isset($item['reference'])) {
            $reference = $row[$item['reference']];
            
            $items[$key]['reference'] = $reference;
          }
        }
        $rows[] = $items;
      }
    }

    return $rows;
  }

}
