<?php

/**
 *
 * Modulo..........: Geral
 * Criacao.........: 14/09/2011
 * Formato.........: ANSI
 * Funcionalidade..: Controle envio e atividades de email
 * Pre-requisitos..:
 */

class Email {

  private $path;
  private $email;

  /**
   *
   * @param type $base_path
   */
  public function Email($path) {
    if (is_dir($path)) {
      $this->path = $path;
    } else {
      die("Erro ao criar o objeto: caminho incorreto");
    }
  }

  /**
   *
   * @param type $assunto
   * @param type $corpo
   * @param type $endereco
   * @param type $nome
   * @param type $email_cc
   * @param type $resposta
   * @param type $modelo
   * @param type $replace
   * @param type $exibir
   */
  public function sendTemplate($endereco, $nome = "", $email_cc = null, $modelo = "", $replace = array(), $exibir = true) {

    System::desire('m', 'manager', 'EmailModelo', 'src', true, '');
    System::desire('c', 'manager', 'EmailModelo', 'src', true, '');

    $emailModeloCtrl = new EmailModeloCtrl(PATH_APP);
    $emailModelos = $emailModeloCtrl->getEmailModeloCtrl("emo_codigo = '" . $modelo . "' OR emo_id = '" . $modelo . "'", "", "", "0", "1");
    if(is_array($emailModelos)){
      $emailModelo = $emailModelos[0];

      $assunto = $emailModelo->get_emo_value('emo_assunto');
      $resposta = $emailModelo->get_emo_value('emo_resposta') ? $emailModelo->get_emo_value('emo_resposta') : EMAIL_REMETENTE;

      $corpo = $emailModelo->get_emo_value('emo_corpo');

      $replace['user'] = System::getUser(false) ? System::getUser(false) : 'VISITANTE';
      $replace['date'] = date('d/m/Y');

      if(is_array($replace)){
        foreach($replace as $key=> $item){
          $corpo = str_replace('{'.$key.'}', $item, $corpo);
        }
      }

      //array(array("email"=>$email,"nome"=>$nome),array("email"=>$email,"nome"=>$nome),...);
      $cc = array(array("email"=>$resposta, "nome"=>COMPANY));
      if($resposta != EMAIL_REMETENTE){
        if($email_cc === null){
          $email_cc = $cc;
        }else{
          $email_cc[] = $cc;
        }
      }

      $this->sendDefault($assunto, $corpo, $endereco, $nome, $email_cc, $resposta, $modelo, $exibir);
    }

  }

  /**
   * 
   * @param type $assunto
   * @param type $corpo
   * @param type $endereco
   * @param type $nome
   * @param type $email_cc
   * @param type $resposta
   * @param type $modelo
   * @param type $exibir
   * @param type $id
   * 
   * @return type
   */
  public function sendDefault($assunto, $corpo, $endereco, $nome = "", $email_cc = null, $resposta = "", $modelo = "", $exibir = true, $id = null) {

    $id = $id === null ? uniqid() : $id;

    $enviado = $this->send($assunto, $corpo, $endereco, $nome, "", "", "", $email_cc, null, $exibir, true, true, $resposta, "", false, $email_smtp_dominio = "", "", "", $id);

    System::desire('c', 'manager', 'EmailEnvio', 'src', true, '');
    System::desire('m', 'manager', 'EmailEnvio', 'src', true, '');

    $emailEnvo = new EmailEnvio();
    $emailEnvo->set_eme_value('eme_id', $id);
    $emailEnvo->set_eme_value('eme_email', $endereco);
    $emailEnvo->set_eme_value('eme_assunto', $assunto);
    $emailEnvo->set_eme_value('eme_corpo', $corpo);
    $emailEnvo->set_eme_value('eme_enviado', $enviado);
    $emailEnvo->set_eme_value('eme_modelo', $modelo);

    $emailEnvioCtrl = new EmailEnvioCtrl(PATH_APP);
    $emailEnvioCtrl->addEmailEnvioCtrl($emailEnvo);

    return $enviado;
  }

  /**
   *
   * @param string $email_assunto
   * @param string $email_mensagem
   * @param string $email_destinatario
   * @param string $email_destinatario_nome
   * @param string $email_remetente
   * @param string $email_remetente_nome
   * @param string $email_remetente_senha
   * @param array $email_cc
   * @param array $email_co
   * @param boolean $exibir_mensagem_confirmacao
   * @param boolean $mensagem_formato_padrao
   * @param boolean $mensagem_cabecalho_formato_padrao
   * @param string $email_remetente_resposta
   * @param array $email_anexo
   * @param boolean $nossl
   * @param string $email_smtp_dominio
   * @param string $email_smtp_porta
   * @param string $email_smtp_timeout
   *
   * @return boolean
   */
  public function send($email_assunto, $email_mensagem, $email_destinatario, $email_destinatario_nome = "", $email_remetente = "", $email_remetente_nome = "", $email_remetente_senha = "", $email_cc = null, $email_co = null, $exibir_mensagem_confirmacao = true, $mensagem_formato_padrao = false, $mensagem_cabecalho_formato_padrao = true, $email_remetente_resposta = "", $email_anexo = "", $nossl = false, $email_smtp_dominio = "", $email_smtp_porta = "", $email_smtp_timeout = "", $key = "") {

    $debug = MODO_TESTE === "true";

    if ($debug) {
      $destinatario = EMAIL_DEBUG;
    } else {
      $destinatario = $email_destinatario;
    }

    $email_assunto = EMAIL_ASSUNTO . " " . $email_assunto;

    $email_remetente = $email_remetente ? $email_remetente : EMAIL_REMETENTE;
    $email_remetente_nome = $email_remetente_nome ? $email_remetente_nome : COMPANY;
    $email_remetente_senha = $email_remetente_senha ? $email_remetente_senha : EMAIL_SENHA;

    $email_remetente_resposta = $email_remetente_resposta ? $email_remetente_resposta : $email_remetente;

    $email_smtp_dominio = $email_smtp_dominio ? $email_smtp_dominio : EMAIL_SMTP_DOMINIO;
    $email_smtp_porta = $email_smtp_porta ? $email_smtp_porta : EMAIL_SMTP_PORTA;
    $email_smtp_timeout = $email_smtp_timeout ? $email_smtp_timeout : EMAIL_SMTP_TIMEOUT;

    $cc = "";

    if ($mensagem_formato_padrao) {
      $body = '';
      $body .= '<div id="start">';
      $body .= '  <br>';
      if ($mensagem_cabecalho_formato_padrao) {
        $body .= '  <table width="*" border="0" cellspacing="2" cellpadding="2">';
        $body .= '    <tr>';
        $body .= '      <td valign="middle" align="center">';
        $body .= '        <a href="http' . '://' . COMPANY_SITE . '" target="_blank"><img src="' . PAGE_IMAGE . 'logo/logo.png" width="150" border="0"><a>';
        $body .= '      </td>';
        $body .= '      <td valign="middle" align="center">';
        $body .= '        <label align="left"><b>' . TITLE . '</b></label><br>';
        $body .= '      </td>';
        $body .= '    </tr>';
        $body .= '  </table>';
        $body .= '  <hr>';
        $body .= '  <br>';
      }
      $body .= '  <table>';
      $body .= '    <tr>';
      $body .= '      <td align="left">';
      $body .= $email_mensagem;
      $body .= '      </td>';
      $body .= '    </tr>';
      $body .= '  </table>';
      $body .= '  <br>';
      $body .= '  <hr>';
      $body .= '  <b>Mensagem autom&aacute;tica. Favor n&atilde;o reponder a essa mensagem.</b><br>';
      $body .= '  <br>';
      $body .= '  <div style="color: #fff; font-size: 10px; background-color: ' . EMAIL_COLOR . '; padding: 3px">&nbsp;<a href="http://' . COMPANY_SITE . '" target="_blank"  style="color: #FFF; text-decoration: none;">' . COMPANY_SITE . '</a> | <a href="' . PAGE_PRIVACITY . '" style="color: #FFF; text-decoration: none;">Pol&iacute;tica de Privacidade</a> | <a href="' . PAGE_TERM . '" style="color: #FFF; text-decoration: none;">Termos de Servi&ccedil;o</a> | <a href="' . PAGE_REMEMBER . '" style="color: #FFF; text-decoration: none;"><b>Esqueci minha senha</b></a> | <a href="' . PAGE_APP . '" style="color: #FFF; text-decoration: none;"><b>Acessar a sua conta</b></a></div>';
      $body .= '  <br>';
      $body .= '  <label style="font-size: 10px;">' . COPY_RIGHT . ' - ' . COMPANY . '</label> :: <label style="font-size: 9px;">id - ' . $key . '</label>';
      $body .= '</div>';
    } else {
      $body = $email_mensagem;
    }

    System::import('class', 'phpmailer', 'PHPMailer', 'lib/mail', true, '');

    $this->email = new PHPMailer();

    $this->email->CharSet = 'ISO-8859-1';

    $this->email->IsHTML(true);

      $this->email->IsSMTP();

      $this->email->Host = $email_smtp_dominio;
    $this->email->Port = $email_smtp_porta;
    if (!$nossl) {
      $this->email->SMTPSecure = 'tls';
    }
      $this->email->SMTPAuth = true;
      $this->email->Username = $email_remetente;
      $this->email->Password = $email_remetente_senha;

    $this->email->setFrom($email_remetente, $email_remetente_nome);

      $this->email->AddReplyTo($email_remetente_resposta, $email_remetente_nome);

      $this->email->AddAddress($destinatario, $email_destinatario_nome);

      //array(array("email"=>$email,"nome"=>$nome),array("email"=>$email,"nome"=>$nome),...);
      if ($email_cc != "" && $email_cc != null) {
        for ($a = 0; $a < count($email_cc); $a++) {
          if (isset($email_cc[$a]['email'])) {
            $cc_email = $email_cc[$a]['email'];
            $cc_nome = $email_cc[$a]['nome'];

            $e = ($debug) ? EMAIL_DEBUG : $cc_email;
            $n = ($cc_nome) ? $cc_nome : "";
            $c = ($cc_nome) ? $cc_nome : $cc_email;

            $this->email->AddCC($e, $n); // Copia
            $cc .= ", " . $c;
          }
        }
      }

      if ($email_co != "" && $email_co != null) {
        for ($a = 0; $a < count($email_co); $a++) {
          if (isset($email_co[$a]['email'])) {
            $co_email = $email_co[$a]['email'];
            $co_nome = $email_co[$a]['nome'];
            if ($debug) {
              $co_email = EMAIL_DEBUG;
            }
            $this->email->AddBCC($co_email, $co_nome); // Copia Oculta
          }
        }
      }

      if (isset($email_anexo['path'])) {
      if (file_exists($email_anexo['path'])) {
        $this->email->AddAttachment($email_anexo['path'], $email_anexo['name']);
      }
    }

      $this->email->Subject = $email_assunto;
      $this->email->Body = $body;

    $enviado = $this->email->send();

      $this->email->ClearAllRecipients();
      $this->email->ClearAttachments();

    if ($cc != "") {
      $cc = substr($cc, 1);
      $cc = ", com c&oacute;pia(s) para " . $cc;
    }
    $destinatario = $email_destinatario_nome ? $email_destinatario_nome . $cc : $email_destinatario . $cc;

    if ($exibir_mensagem_confirmacao) {
      if ($enviado) {
        $msg = MESSAGE_SEND_SUCCESS;
        $type = SHOW_MESSAGE_SUCCESS;
      } else {
        $msg = $debug ? MESSAGE_SEND_ERROR . "[" . $this->email->ErrorInfo . "]" : MESSAGE_SEND_ERROR;
        $type = SHOW_MESSAGE_ERROR;
      }
      System::showMessage($msg . " " . $email_destinatario, $type);
    }

    if ($debug) {
      print "<script>system.window.alert('Modo de depura&ccedil;&atilde;o ativado: mensagens redirecionadas para " . EMAIL_DEBUG . "');</script>";
    }

    return $enviado;
  }

  /**
   *
   * @param string $informacao
   * @param int $protocolo
   * @param string $mensagem
   * @param string $detalhes = ""
   *
   * @return string
   */
  public function getMessageFormated($informacao, $protocolo, $mensagem, $detalhes = "") {
    
    if ($detalhes != "") {
      $detalhes =
        "<tr>" .
          "<td>" . 
            "Detalhes:<br>" . $detalhes .
          "</td>" .
        "</tr>";
    }

    $mensagem =
     "<table>" .
       "<tr>" .
         "<td><h2>" . $informacao . "</h2></td>" .
       "</tr>" .
       "<tr>" .
         "<td><h3>Protocolo: " . str_pad($protocolo, 10, "0", STR_PAD_LEFT) . "</h3></td>" .
       "</tr>" .
       "<tr>" .
         "<td>"
          . $mensagem . 
         "</td>" .
       "</tr>"
          . $detalhes .
      "<table>";

    return $mensagem;
  }

}
