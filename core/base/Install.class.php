<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Install
 *
 * @author Analista
 */
class Install {

  private static $steps;
  
  /**
   * 
   * @param type $weblog_page
   * @param type $weblog_path
   * @param type $weblog_uri
   * @return Object
   */
  public static function load($weblog_page, $weblog_path, $weblog_uri) {
    
    self::$steps = array();
    
    self::$steps["1"] = array();
    self::$steps["1"]["weblog_title"] = new Object('{"id":"weblog_title", "name":"weblog_title", "type":"text", "label":"Nome do Aplicativo", "value":"", "placeholder":"My App", "disabled":0, "validate":1}');
    self::$steps["1"]["weblog_short"] = new Object('{"id":"weblog_short", "name":"weblog_short", "type":"text", "label":"Nome Abreviado", "value":"", "placeholder":"myapp", "disabled":0, "validate":1}');
    self::$steps["1"]["weblog_page"] = new Object('{"id":"weblog_page", "name":"weblog_page", "type":"text", "label":"P&aacute;gina inicial", "value":"' . $weblog_page . '", "placeholder":"myapp", "disabled":1, "validate":0}');
    self::$steps["1"]["weblog_path"] = new Object('{"id":"weblog_path", "name":"weblog_path", "type":"text", "label":"Caminho inicial", "value":"' . $weblog_path . '", "placeholder":"", "disabled":1, "validate":0}');
    self::$steps["1"]["weblog_uri"] = new Object('{"id":"weblog_uri", "name":"weblog_uri", "type":"text", "label":"Localiza&ccedil;&atilde;o", "value":"' . utf8_encode($weblog_uri) . '", "placeholder":"", "disabled":0, "validate":1, "label_info":"' . addslashes('P&aacute;gina inicial (ou Caminho Inicial) + "/site" significa "www.dominio.com.br/site"') . '"}');
    self::$steps["1"]["user_login"] = new Object('{"id":"user_login", "name":"user_name", "type":"text", "label":"Nome de usu&aacute;rio", "value":"admin", "placeholder":"", "disabled":0, "value_info":"Os nomes de usu&aacute;rios devem possuir apenas caracteres alfanum&eacute;ricos, espa&ccedil;os, sublinhados, h&iacute;fens, per&iacute;odos e o s&iacute;mbolo @."}');
    self::$steps["1"]["pass1"] = new Object('{"id":"pass1", "name":"admin_password", "type":"password", "label":"Senha", "value":"", "placeholder":"", "disabled":0, "validate":0, "label_info":"Uma senha ser&aacute; gerada automaticamente para voc&ecirc; se deixar isto em branco.", "value_info":"' . addslashes('Dica: Sua senha deve ter pelo menos sete caracteres. Para torn&aacute;-la mais segura, use letras mai&uacute;sculas e min&uacute;sculas, n&uacute;meros e s&iacute;mbolos como ! " ? $ % ^ &amp; ).') . '", "html":"' . addslashes('<p><input name="admin_password2" type="password" id="pass2" size="25" value=""/></p><div id="pass-strength-result">Indicador de for&ccedil;a</div>') . '"}');
    self::$steps["1"]["admin_email"] = new Object('{"id":"admin_email", "name":"admin_email", "type":"text", "label":"Endere&ccedil;o de e-mail", "value":"", "placeholder":"", "disabled":0, "validate":1, "label_info":"", "value_info":"Confira se o endere&ccedil;o de e-mail est&aacute; correto antes de prosseguir."}');
    self::$steps["1"]["admin_ini"] = new Object('{"id":"admin_ini", "name":"admin_ini", "type":"text", "label":"Arquivo de configura&ccedil;&atilde;o", "value":".user.ini", "placeholder":"", "disabled":0, "validate":1, "label_info":"", "value_info":"Nome do arquivo .ini criado pela instala&ccedil;&atilde;o. Verifique nas configura&ccedil;&otilde;es do seu servidor qual o padr&atilde;o utilizado por eles."}');
    self::$steps["1"]["admin_encode"] = new Object('{"id":"admin_encode", "name":"admin_encode", "type":"text", "label":"Charset padr&atilde;o", "value":"UTF-8", "placeholder":"", "disabled":0, "label_info":"", "validate":1, "value_info":"Conjunto de caracteres usado pelo projeto."}');
    
    
    self::$steps["2"] = array();
    self::$steps["2"]["dbhost"] = new Object('{"id":"dbhost", "name":"dbhost", "type":"text", "label":"Servidor", "value":"localhost", "placeholder":"", "disabled":0, "validate":1, "label_info":"", "value_info":"Voc&ecirc; deve obter esta informa&ccedil;&atilde;o do seu servidor de hospedagem, se <code>localhost</code> n&atilde;o funcionar."}');
    self::$steps["2"]["dbname"] = new Object('{"id":"dbname", "name":"dbname", "type":"text", "label":"Banco de Dados", "value":"", "placeholder":"", "disabled":0, "validate":1, "label_info":"", "value_info":"Nome do banco de dados onde voc&ecirc; quer instalar o aplicativo."}');
    self::$steps["2"]["uname"] = new Object('{"id":"uname", "name":"uname", "type":"text", "label":"Nome de Usu&aacute;rio", "value":"", "placeholder":"", "disabled":0, "validate":1, "label_info":"", "value_info":"Usu&aacute;rio de MySQL com acesso ao banco informado."}');
    self::$steps["2"]["pwd"] = new Object('{"id":"pwd", "name":"pwd", "type":"text", "label":"Senha", "value":"", "placeholder":"", "disabled":0, "validate":1, "label_info":"", "value_info":"E sua respectiva senha : )"}');
    //self::$steps["2"]["prefix"] = new Object('{"id":"prefix", "name":"prefix", "type":"text", "label":"Prefixo das Tabelas", "value":"", "placeholder":"", "disabled":0, "validate":0, "label_info":"", "value_info":"Se quiser rodar v&aacute;rias instala&ccedil;&otilde;es aplicativos em um &uacute;nico banco de dados, informe isto."}');

  }
  
  /**
   * 
   * @param type $step
   * @param type $message
   */
  public static function process($step, $message) {

    $function = "step" . $step;
    self::$function($message);
    
  }
  
  /**
   * 
   * @param type $step
   * @return string
   */
  public static function validate($step) {

    $message = "";
    
    $rows = self::$steps[$step];
    $warning = array();
    if (is_array($rows)) {
      foreach ($rows as $row) {
        if ($row->validate) {
          $value = System::request($row->name, null);
          if (!$value) {
            $warning[] = "Informe corretamente " . $row->label;
          } else {
            $row->value = $value;
          }
        }
      }
    }
    
    if ($step === "2") {
      if (count($warning) == 0) {
        $connected = self::connect($rows["dbhost"]->value, $rows["dbname"]->value, $rows["uname"]->value, $rows["pwd"]->value);
        if ($connected === false) {
          $warning[] = "N&atilde;o foi poss&iacute;vel conectar ao banco de dados com os dados informados";
        }
      }
    }
    
    if (count($warning) > 0) {
      $message = "Problemas encontrados: <br> - " . join("<br> - ", $warning);
    }
    
    return $message;
    
  }

  /**
   * 
   * @param type $rows
   * @param type $message
   */
  public static function form($step, $message = "") {
    ?>
      <div style="color: red">
          <?php print $message; ?>
      </div>

      <table class="form-table">
        <?php 
        
          $rows = self::$steps[$step];
          foreach ($rows as $row) {
            $disabled = $row->disabled ? "disabled" : "";
            $value = System::request($row->name, $row->value);
            ?>
              <tr>
                <th scope="row">
                  <label for="<?php print $row->id;?>"><?php print $row->label;?></label>
                  <?php
                    if ($row->label_info) {
                      ?>
                        <p><?php print $row->label_info; ?></p>
                      <?php
                    }
                  ?>
                </th>
                <td>
                  <input name="<?php print $row->name;?>" id="<?php print $row->id;?>" type="<?php print $row->type;?>" value="<?php print $value;?>" placeholder="<?php print $row->placeholder;?>" <?php print $disabled; ?> size="25" autocomplete="off"/>
                  <?php
                    if ($row->value_info) {
                      ?>
                        <p><?php print $row->value_info; ?></p>
                      <?php
                    }
                    if ($row->html) {
                      ?>
                        <?php print $row->html; ?>
                      <?php
                    }
                  ?>
                </td>
              </tr>
            <?php
          }
        ?>
      </table>
    <?php
    
    $hiddens = self::$steps[$step - 1];
    if (is_array($hiddens)) {
      foreach ($hiddens as $row) {
        $value = System::request($row->name, null);
        if (!is_null($value)) {
          ?>
            <input type="hidden" name="<?php print $row->name; ?>" id="<?php print $row->id; ?>" value="<?php print $value; ?>"/>
          <?
        }
      }
    }

  }
  
  /**
   * 
   * @param type $message
   */
  public static function step1($message = "") {

    self::form("1", $message);

    ?>
      
      <p class="step"><input name="submit" type="submit" value="Avan&ccedil;ar" class="button" /></p>

      <script type="text/javascript" src="setup/js/jquery.js"></script>
      <script type="text/javascript">
        /* <![CDATA[ */
        var pwsL10n = {"empty": "Indicador de for\u00e7a", "short": "Muito fraca", "bad": "Fraca", "good": "M\u00e9dio", "strong": "Forte", "mismatch": "Incompatibilidade"};
        /* ]]> */
      </script>
      <script type="text/javascript" src="setup/js/password-strength-meter.js"></script>
      <script type="text/javascript" src="setup/js/user-profile.js"></script>

    <?php

  }

  /**
   *
   * @param type $message
   */
  public static function step2($message = "") {

    ?>

      <p>Abaixo, voc&ecirc; deve digitar seus dados de conex&atilde;o de banco de dados. Se voc&ecirc; n&atilde;o tem certeza sobre isso, entre em contato com seu servi&ccedil;o de hospedagem.</p>

    <?php
    
    self::form("2", $message);

    ?>

      <p class="step">
        <input type="button" name="Back" value="Voltar" class="button" onclick="window.history.back();"/> 
        <input type="submit" name="Submit" value="Finalizar" class="button" />
      </p>

    <?php
    
  }

  /**
   * 
   * @param type $message
   */
  public static function step3($message = "") {

    $host = $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];
    if (empty($_SERVER["HTTPS"]) || $_SERVER["HTTPS"] !== "on") {
      $host = "http" . "://" . $host;
    } else {
      $host = "https" . "://" . $host;
    }

    $db = System::request('dbname', "");
    $username = System::request('uname', "");
    $password = System::request('pwd', "");
    $server = System::request('dbhost', "");
    //$prefix = isset($_POST['prefix']) ? $_POST['prefix'] : "";

    $weblog_title = System::request('weblog_title', "");
    $weblog_short = System::request('weblog_short', $weblog_title);
    $path = System::request('weblog_path', getcwd());
    $weblog_path = substr($path, -1) === PATH_FILE_SEPARATOR ? $path : $path . PATH_FILE_SEPARATOR;
    $weblog_page = System::request('weblog_page', $host);
    $weblog_uri = System::request('weblog_uri',  "/");
    $admin_ini = System::request('admin_ini', ".user.ini");
    
    $user_name = System::request('user_name', "admin");
    $admin_password = System::request('admin_password') ? System::request('admin_password') : String::passwordRandom(8);
    $admin_email = System::request('admin_email', "");

    /**
     * replaces
     */
    $replaces = array(
      new Object(array("key"=>'path', "value"=>$weblog_path))
      , new Object(array("key"=>'path_app', "value"=>$weblog_path))
      , new Object(array("key"=>'page', "value"=>$weblog_page))
      , new Object(array("key"=>'page_app', "value"=>$weblog_page))
      , new Object(array("key"=>'short', "value"=>$weblog_short))
      , new Object(array("key"=>'title', "value"=>$weblog_title))
      , new Object(array("key"=>'database', "value"=>$db))
      , new Object(array("key"=>'host', "value"=>$host))
      , new Object(array("key"=>'year', "value"=>date('Y')))
    );
    
    $database = "";
    /**
     * config dir
     */
    $config = $weblog_path . PATH_FILE_SEPARATOR . "config";
    $configuracao = '<?php ' . chr(13) . chr(10) 
                  . '$configuracao = array();' . chr(13) . chr(10) 
                  . '$configuracao["' . $db . '"] = new Object(' 
                  . 'array("server"=>\'' . $server . '\',"port"=>\'3306\',"db"=>\'' . $db . '\',"user"=>\'' . $username . '\',"password"=>\'' . $password . '\')' 
                  . ');' . chr(13) . chr(10) 
                  . '?>';
    /**
     * setup files dir
     */
    $files = $weblog_path . PATH_FILE_SEPARATOR . "setup" . PATH_FILE_SEPARATOR . "files";
    
    $database = $config . PATH_FILE_SEPARATOR . "database.php";
    if (is_writable($config)) {
      unlink($database);
      /**
       * save configuracao
       */
      $saved = file_put_contents($database, $configuracao);
      if ($saved) {

        define('DEFAULT_DATABASE', $db);
    
        System::import('class', 'base', 'DAO', 'core');
        $dao = new DAO();
        
        $schema = $files . PATH_FILE_SEPARATOR . "default-schema.sql";

        $sql = file_get_contents($schema);
        $schemas = explode(';', $sql);
        foreach ($schemas as $statement) {
          $dao->executeSQL($statement, false, false);
        }

        $data = $files . PATH_FILE_SEPARATOR . "default-data.sql";

        $query = file_get_contents($data);
        foreach ($replaces as $replace) {
          $query = String::replace($query, '{' . $replace->key . '}', $replace->value);
        }
        $inserts = explode(';', $query);
        foreach ($inserts as $insert) {
          $dao->executeSQL($insert, false, false);
        }
        $dao->executeSQL("UPDATE TBL_USUARIO SET usu_login = '" . $user_name . "', usu_password = MD5('" . $admin_password . "'), usu_email = '" . $admin_email . "' WHERE usu_codigo = 1", false, false);
        
        /**
         * PHP.INI
         * #{base} = PATH_APP|core|base.php
         * #{tmp} = PATH_APP|tmp
         */
        $i = file_get_contents($files . PATH_FILE_SEPARATOR . "default-php.ini");
        $in = str_replace("{base}", $weblog_path . "core/base.php", $i);
        $ini = str_replace("{tmp}", $weblog_path . "temp", $in);
        file_put_contents($weblog_path . PATH_FILE_SEPARATOR . $admin_ini, $ini);

        /**
         * HTACCESS
         * #{base} = PATH_APP|core|base.php
         * #{page_app} = PAGE_APP
         */
        $h = file_get_contents($files . PATH_FILE_SEPARATOR . "default-.htaccess");
        $ht = str_replace("{base}", $weblog_path . "core/base.php", $h);
        $htaccess = str_replace("{page_uri}", $weblog_uri, $ht);
        file_put_contents($weblog_path . PATH_FILE_SEPARATOR . ".htaccess", $htaccess);
        

        $end = file_put_contents($config . PATH_FILE_SEPARATOR . "installed", "instalation at " . gmdate('D, d M Y H:i ', time()) . ' GMT');

        if ($end) {

          System::import('c', 'manager', 'Parametros', 'src');
          $parametrosCtrl = new ParametrosCtrl(PATH_APP);
          $parametrosCtrl->updateVersionParametrosCtrl(4);
          
          $text = 'Nova instala&ccedil;&atilde;o conclu&iacute;da com sucesso.'
                . '<br>Acesse agora o aplicativo em <a href="' . $weblog_page . '">' . $weblog_page . '</a> para finalizar as configura&ccedil;&otilde;es.';

          print $text;
          
          $from = "sac@arraysoftware.net";
          $subject = "[" . $weblog_short . "] Processo Finalizado";
          
          $headers = "From: " . $from . "\r\n";
          $headers .= "Reply-To: ". $from . "\r\n";
          $headers .= "MIME-Version: 1.0\r\n";
          $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
          
          $message = '<html><body>';
          $message .= '<img src="http' . '://' . 'arraysoftware.net/site/custom/images/logo.png' . '" />';
          $message .= $text;
          $message .= '<table rules="all" style="border-color: #666;" cellpadding="10">';
          $message .= "<tr><td><strong>Usu&aacute;:</strong> </td><td>" . $user_name . "</td></tr>";
          $message .= "<tr><td><strong>Senha:</strong> </td><td>" . $admin_password . "</td></tr>";
          $message .= "</table>";
          $message .= "</body></html>";

          mail($admin_email, $subject, $message, $headers);
          
        } else {

          print 'A instala&ccedil;&atilde;o n&atilde;o foi finalizada corretamente';

        }

      } else {
        $database = "N&atilde;o foi poss&iacute;vel escrever o arquivo de conex&atilde;o com o banco de dados em: " . $database;
      }
    } else {
      $database = "N&atilde;o foi poss&iacute;vel escrever no diretório: " . $dir;
    }
  }

  /**
   * 
   * @param type $server
   * @param type $database
   * @param type $username
   * @param type $password
   * @param type $protocol
   * 
   * @return boolean
   */
  public static function connect($server, $database, $username, $password, $protocol = "mysql") {
    if (defined('PDO::ATTR_DRIVER_NAME')) {
      try {
        $link = new PDO($protocol . ':host=' . $server . ';dbname=' . $database, $username, $password);
      } catch (PDOException $e) {
        $connect = false;
      }
    } else {
      $connect = false;
      $link = mysql_connect($server, $username, $password);
      if (is_resource($link)) {
        if (mysql_select_db($database, $link)) {
          $connect = $link;
        }
      }
    }
    
    return $connect;
  }
  
}
