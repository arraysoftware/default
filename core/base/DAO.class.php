<?php

/**
 * DAO
 * 
 * @author Gennesis.net.br <contato@arraysoftware.net>
 */
class DAO {

  /**
   *
   * @var \DAO
   */
  private static $dao = array();

  /**
   *
   * @var \Database
   */
  protected $database;

  /**
   * 
   * 
   * @param string $path
   * @param string $database
   * @param boolean $auto_increment
   * @param string $plataform
   * 
   * @return \DAO
   */
  public function DAO($path = null, $database = null, $auto_increment = null, $plataform = null) {
    $params = array(
        array('param' => 'plataform', 'value' => 'Mysql'),
        array('param' => 'path', 'value' => PATH_APP),
        array('param' => 'database', 'value' => DEFAULT_DATABASE),
        array('param' => 'auto_increment', 'value' => true),
    );
    foreach ($params as $p) {
      $var = $p['param'];
      $val = $p['value'];
      if (is_null($$var)) {
        $$var = $val;
      }
    }

    $this->database = Connection::getDatabase($plataform, $path, $database, $auto_increment);

    return $this;
  }

  /**
   * Responsável por recuperar uma instância do DAO
   * 
   * @param string $path
   * @param string $database
   * @param string $plataform
   * @param boolean $auto_increment
   * @param boolean $singleton
   * 
   * @return \DAO
   */
  public static function getDAO($path = null, $database = null, $plataform = null, $auto_increment = null, $singleton = null) {
    $dao = null;

    if (is_null($singleton)) {
      $singleton = false;
    }

    if ($singleton) {
      if (isset(self::$dao[$plataform])) {
        $dao = self::$dao[$plataform];
      }
    }

    if (is_null($dao)) {
      $dao = new DAO($path, $database, $auto_increment, $plataform);
    }

    self::$dao[$plataform] = $dao;

    return $dao;
  }

  /**
   *
   * @return <type>
   */
  public function getConnection() {
    return $this->database;
  }

  /**
   *
   * @param type $item
   * @param type $source
   *
   * @return boolean
   */
  public function isValid($item, $source) {
    return $this->database->isValid($item, $source);
  }

  /**
   * 
   * @param array $item
   * @param string $source
   * 
   * @return array
   */
  public  function prepare($item, $source) {
    return $this->database->prepare($item, $source);
  }

  /**
   *
   * @param type $item
   * @param type $value
   * @param type $source
   * 
   * @return string
   */
  public function renderer($item, $value, $source) {
    return $this->database->renderer($item, $value, $source);
  }

  /**
   * 
   * @param array $items
   * @param string $table
   * @param string $where
   * @param string $group
   * @param string $order
   * @param string $start
   * @param string $end
   * @param array $fields
   * @param boolean $validate
   * 
   * @return string
   */
  public function buildSelect($items, $table, $where, $group = null, $order = null, $start = null, $end = null, $fields = null) {
    return $this->database->buildSelect($items, $table, $where, $group, $order, $start, $end, $fields);
  }

  /**
   * 
   * @param array $items
   * @param string $table
   * 
   * @return string
   */
  public function buildInsert($items, $table) {
    return $this->database->buildInsert($items, $table);
  }

  /**
   * 
   * @param type $table
   * @param type $items
   * @param type $where
   * 
   * @return string
   */
  public function buildUpdate($table, $items, $where) {
    return $this->database->buildUpdate($table, $items, $where);
  }

  /**
   * 
   * @param string $table
   * @param string $where
   * 
   * @return string
   */
  public function buildDelete($table, $where) {
    return $this->database->buildDelete($table, $where);
  }

  /**
   * 
   * @param array $items
   * @param resource $result
   * 
   * @return array
   */
  public function populateItems($items, $result) {
    return $this->database->populateItems($items, $result);
  }

  /**
   *
   * @param type $pSql
   * @param type $validate
   * 
   * @return type
   */
  public function selectSQL($pSql, $validate = true) {
    return $this->database->select($pSql, $validate);
  }

  /**
   *
   * @param type $pSql
   * @param type $save
   * @param type $validate
   * @param type $deactivate
   * 
   * @return type
   */
  public function insertSQL($pSql, $save = true, $validate = true, $deactivate = false) {
    return $this->database->insert($pSql, $save, $validate, $deactivate);
  }

  /**
   *
   * @param type $pSql
   * @param type $save
   * @param type $validate
   * 
   * @return type
   */
  public function executeSQL($pSql, $save = true, $validate = true) {
    return $this->database->execute($pSql, $save, $validate);
  }

  /**
   * 
   * @param resource $result
   * 
   * @return array
   */
  public function fetchRow($result) {
    return $this->database->fetchRow($result);
  }

}
