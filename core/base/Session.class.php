<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Session
 *
 * @author William
 */
class Session {
  //put your code here

  /**
   * 
   * Inicia a sessao do sistema
   *
   * @param type $user
   * @param type $override
   * @param type $remember
   * @param type $path
   * @param type $host
   */
  public function start($user, $override, $remember, $path, $host) {

    if (MODO_LEGADO === "true") {

      $expire = null;
      if ($override) {
        if ($remember == "on") {
          $expire = time() + (3600 * 24 * 30); //30 dias
        }
        setcookie(Application::$token, $user->token, $expire, $path, $host);
      }

      setcookie("siga_ppid_1", base64_encode($user->usu_codigo), null, $path, $host); //codigo do acesso controle
      setcookie("siga_ppid_2", base64_encode($user->usu_nome), null, $path, $host); //nome registrado para o usuario
      setcookie("siga_ppid_3", base64_encode($user->usu_tipo), null, $path, $host); //tipo de acesso dele
      setcookie("siga_ppid_4", base64_encode($user->usu_cod_USUARIO), null, $path, $host); //referencia da tabela do usuario
      setcookie("siga_ppid_5", base64_encode($user->usu_limitado), null, $path, $host); // nivel de acesso restrito ou nao
      setcookie("siga_ppid_6", base64_encode($user->usu_cod_EMPRESA), null, $path, $host);
      setcookie("siga_ppid_7", base64_encode($user->usu_cursos), null, $path, $host);
      setcookie("siga_ppid_8", base64_encode($user->usu_cod_ACESSO_MODULO), null, $path, $host);
      setcookie("siga_ppid_9", base64_encode($user->usu_administrador), null, $path, $host);

      $date = date("Y-n-j H:i:s");
      setcookie("last", base64_encode($date), null, $path, $host);

      $usuario = new stdClass();
      $usuario->usu_codigo = md5($user->usu_codigo);
      $usuario->usu_login = utf8_decode($user->usu_login);
      $usuario->usu_tipo = utf8_decode($user->usu_tipo);
      $usuario->usu_tratamento = utf8_decode($user->usu_tratamento);
      $usuario->usu_nome = utf8_decode($user->usu_nome);
      $usuario->usu_email = utf8_decode($user->usu_email);
      $usuario->usu_foto = $user->usu_foto;
      $usuario->usu_adicional = $user->usu_adicional;

      setcookie(Application::$session, json_encode($usuario), $expire, $path, $host);

      setcookie(Application::$remember, base64_encode($remember), null, $path, $host);

    } else {

      $token = uniqid();
      $expire = null;
      if ($override) {
        $expire = null;
        if ($remember == "on") {
          $expire = time() + (3600 * 24 * 30); //30 dias
        }
        setcookie(Application::$token, $token, $expire, $path, $host);
      }

      setcookie(Application::$session, json_encode($user), $expire, $path, $host);
      setcookie(Application::$remember, base64_encode($remember), null, $path, $host);
    }

  }

  /**
   * 
   * @param type $token
   * @param type $path
   * @param type $host
   */
  public function destroy($token, $path, $host) {

    if (MODO_LEGADO === "true") {

      setcookie("siga_ppid_1", "", time(), $path, $host);
      setcookie("siga_ppid_2", "", time(), $path, $host);
      setcookie("siga_ppid_3", "", time(), $path, $host);
      setcookie("siga_ppid_3", "", time(), $path, $host);
      setcookie("siga_ppid_4", "", time(), $path, $host);
      setcookie("siga_ppid_5", "", time(), $path, $host);
      setcookie("siga_ppid_6", "", time(), $path, $host);
      setcookie("siga_ppid_7", "", time(), $path, $host);
      setcookie("siga_ppid_8", "", time(), $path, $host);
      setcookie("siga_ppid_9", "", time(), $path, $host);

      setcookie("last", "", time(), $path, $host);
      setcookie(Application::$session, "", time(), $path, $host);
      setcookie(Application::$remember, "", time(), $path, $host);

      setcookie($token, "", time(), $path, $host);

    } else {

      setcookie($token, "", time(), $path, $host);

      setcookie(Application::$session, "", time(), $path, $host);
      setcookie(Application::$remember, "", time(), $path, $host);

    }
  }

  /**
   *
   */
  public function clear() {

    if (isset($_SERVER['HTTP_COOKIE'])) {
      $cookies = explode(';', $_SERVER['HTTP_COOKIE']);
      foreach ($cookies as $cookie) {
        $parts = explode('=', $cookie);
        $name = trim($parts[0]);
        setcookie($name, '', time() - 1000);
        setcookie($name, '', time() - 1000, '/');
      }
    }
  }

  /**
   * 
   * @param type $token
   * @param type $user
   */
  public function initSession($token, $user) {
    
  }

}
