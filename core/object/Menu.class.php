<?php
/**
 *
 * Faculdade Ubaense Ozanam Coelho - Fagoc
 * Projeto SIGA - 2010
 * Modulo..........: Geral
 * Funcionalidade..: Instanciar um Menu no sistema para o Modulo Network
 * Pre-requisitos..: Nao possui
 */

/**
 * Description of Menu
 *
 * @author WILLIAM
 */
class Menu {

  private $mnu_imagem;
  private $mnu_descricao;
  private $mnu_acao;

  /**
   *
   * @param <type> $mnu_imagem
   * @param <type> $mnu_descricao
   * @param <type> $mnu_acao
   */
  public function Menu($mnu_imagem, $mnu_descricao, $mnu_acao) {
    $this->mnu_imagem = $mnu_imagem;
    $this->mnu_descricao = $mnu_descricao;
    $this->mnu_acao = $mnu_acao;
  }
  /**
   *
   * @return <type>
   */
  public function get_mnu_imagem() {
    return $this->mnu_imagem;
  }
  /**
   *
   * @param <type> $mnu_imagem
   */
  public function set_mnu_imagem($mnu_imagem) {
    $this->mnu_imagem = $mnu_imagem;
  }
  /**
   *
   * @return <type>
   */
  public function get_mnu_descricao() {
    return $this->mnu_descricao;
  }
  /**
   *
   * @param <type> $mnu_descricao
   */
  public function set_mnu_descricao($mnu_descricao) {
    $this->mnu_descricao = $mnu_descricao;
  }
  /**
   *
   * @return <type>
   */
  public function get_mnu_acao() {
    return $this->mnu_acao;
  }
  /**
   *
   * @param <type> $mnu_acao
   */
  public function set_mnu_acao($mnu_acao) {
    $this->mnu_acao = $mnu_acao;
  }

}
?>
