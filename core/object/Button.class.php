<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Button
 *
 * @author William
 */
class Button {

  public $id;
  public $text;
  public $handler;
  public $cls;
  public $width;
  public $hidden;
  public $menu;

  /**
   * 
   * @param type $id
   * @param type $text
   * @param type $handler
   * @param type $cls
   * @param type $width
   * @param type $hidden
   * @param type $menu
   * 
   */
  public function Button($id, $text, $handler, $cls = "form-buttom", $width = "95", $hidden = false, $menu = null) {
    $this->id = $id;
    $this->text = $text;
    $this->handler = $handler;
    $this->cls = $cls;
    $this->width = $width;
    $this->hidden = $hidden;
    $this->menu = $menu;
  }

}

?>
