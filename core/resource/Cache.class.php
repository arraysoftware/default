<?php

/**
 * Sistema de cache
 *
 * @author Thiago Belem <contato@thiagobelem.net>
 * @link http://blog.thiagobelem.net/
 */
class Cache {

  /**
   * Tempo padr�o de cache
   *
   * @var string
   */
  private static $time = '5 minutes';

  /**
   * Local onde o cache ser� salvo
   *
   * Definido pelo construtor
   *
   * @var string
   */
  private $folder;

  /**
   * Construtor
   *
   * Inicializa a classe e permite a defini��o de onde os arquivos
   * ser�o salvos. Se o par�metro $folder for ignorado o local dos
   * arquivos tempor�rios do sistema operacional ser� usado
   *
   * @uses Cache::setFolder() Para definir o local dos arquivos de cache
   *
   * @param string $folder Local para salvar os arquivos de cache (opcional)
   *
   * @return void
   */
  public function __construct($folder = null) {
    $this->setFolder(!is_null($folder) ? $folder : sys_get_temp_dir());
  }

  /**
   * Define onde os arquivos de cache ser�o salvos
   *
   * Ir� verificar se a pasta existe e pode ser escrita, caso contr�rio
   * uma mensagem de erro ser� exibida
   *
   * @param string $folder Local para salvar os arquivos de cache (opcional)
   *
   * @return void
   */
  protected function setFolder($folder) {
    // Se a pasta existir, for uma pasta e puder ser escrita
    if (file_exists($folder) && is_dir($folder) && is_writable($folder)) {
      $this->folder = $folder;
    } else {
      trigger_error('N�o foi poss�vel acessar a pasta de cache', E_USER_ERROR);
    }
  }

  /**
   * Gera o local do arquivo de cache baseado na chave passada
   *
   * @param string $key Uma chave para identificar o arquivo
   *
   * @return string Local do arquivo de cache
   */
  protected function generateFileLocation($key) {
    return $this->folder . DIRECTORY_SEPARATOR . sha1($key) . '.tmp';
  }

  /**
   * Cria um arquivo de cache
   *
   * @uses Cache::generateFileLocation() para gerar o local do arquivo de cache
   *
   * @param string $key Uma chave para identificar o arquivo
   * @param string $content Conte�do do arquivo de cache
   *
   * @return boolean Se o arquivo foi criado
   */
  protected function createCacheFile($key, $content) {
    // Gera o nome do arquivo
    $filename = $this->generateFileLocation($key);

    // Cria o arquivo com o conte�do
    return file_put_contents($filename, $content) OR trigger_error('N�o foi poss�vel criar o arquivo de cache', E_USER_ERROR);
  }

  /**
   * 
   * @param type $key
   * 
   * @return boolean
   */
  public function clear($key = "") {
    if ($key) {
      $file = $this->generateFileLocation($key);
      if (file_exists($file) && is_readable($file)) {
        unlink($file);
        return true;
      }
    } else {
      $handle = opendir($this->folder);
      $removed = 0;
      $total = 0;
      while ($file = readdir($handle)) {
        $filename = $this->folder . '/' . $file;
        if (is_file($filename)) {
          $total++;
          print 'Apagando ' . $file . '...';
          $remove = unlink($filename);
          if ($remove) {
            $removed++;
            print ' Pronto.';
          } else {
            print ' Erro.';
          }
          print '<br>';
        }
      }
      closedir($handle);
      return ($removed == $total);
    }
    return false;
  }

  /**
   * 
   * @param type $key
   * @param type $content
   * @param type $time
   * 
   * @return type
   */
  public function save($key, $content, $time = null) {
    $time = strtotime(!is_null($time) ? $time : self::$time);

    $content = serialize(array(
      'expires' => $time,
      'content' => $content));

    return $this->createCacheFile($key, $content);
  }

  /**
   * 
   * @param type $key
   * 
   * @return null
   */
  public function read($key) {
    $filename = $this->generateFileLocation($key);
    if (file_exists($filename) && is_readable($filename)) {
      $cache = unserialize(file_get_contents($filename));
      if ($cache['expires'] > time()) {
        return $cache['content'];
      } else {
        unlink($filename);
      }
    }
    return null;
  }

  /**
   *
   *
   */
  public function browserCache() {
    header("Pragma: no-cache");
    header("Cache: no-cache");
    header("Cache-Control: no-cache, must-revalidate");
    header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
  }

  /**
   * 
   * @param type $version
   * @param type $clear
   * 
   * @return boolean
   */
  public function headerCache($version, $clear = false) {

    $v_system = explode('.', VERSION);
    $v_cookie = explode('.', Cookie::get('version'));

    if (!$clear and count($v_system) > 2 and count($v_cookie) > 2) {
      $clear = $v_system[0] != $v_cookie[0] || $v_system[1] != $v_cookie[1];
    } else {
      $clear = true;
    }

    $cache = 't';
    $time = time();
    $max = (60 * 60 * 24 * 365);

    if (Cookie::get($cache)) {
      $time = Cookie::get($cache);
    } else {
      Cookie::set($cache, $time, $max);
    }

    if ($clear) {
      $time = time();
      Cookie::set($cache, $time, $max);
    }

    $now = time();
    $age = $now - $time;
    $expire = $time + $max;

    header('Age: ' . $age, true);
    header('Cache-Control: max-age=' . $max . ', public', true);
    header('Date: ' . gmdate('D, d M Y H:i:s', $now) . ' GMT', true);
    header('Last-Modified: ' . gmdate('D, d M Y H:i:s', $time) . ' GMT', true);
    header('Expires: ' . gmdate('D, d M Y H:i:s', $expire) . ' GMT', true);
    header('X-Content-Type-Options: nosniff', true);
    header("Cache-Control: store, cache, no-revalidate", true);
    header("Cache-Control: post-check=1, pre-check=1", false);
    header("Pragma: cache", true);
    header('ETag: "' . base64_encode($version) . '-' . $time . '"', true);

    return $clear;
  }

  /**
   * 
   * @param type $type
   * @param type $source
   * @param type $compact
   * @param type $dir
   * @param type $rm
   */
  public function addCache($type, $source, $compact, $dir = '', $rm = false) {

    $output = $source;
    $file = '';
    if ($type == 'js') {
      if ($compact) {
        $output = $dir . '/' . $source;
        if ($rm) {
          if (file_exists($output)) {
            unlink($output);
          }
        }
        if (!file_exists($output)) {
          if (file_exists($source)) {
            $file = $this->javascript($source, $output);
          }
        }
      }
    }
    if (file_exists($output)) {
      require_once $output;
    }
  }

  /**
   * 
   * @param type $source
   * @param type $output
   * 
   * @return string
   */
  public function javascript($source, $output) {

    System::desire('class', 'resource', 'Packer', 'core');

    $script = file_get_contents($source);
    $packer = new Packer($script, 'Normal', true, false);
    $packed = $packer->pack() . ";";

    file_put_contents($output, $packed);

    return $packed;
  }

}