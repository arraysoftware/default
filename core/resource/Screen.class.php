<?php

class Screen {

  public $extension = '';
  public $encode = UTF8_SYSTEM;

  /**
   *
   * @var ScreenLine
   */
  public $line;
  /**
   *
   * @var ScreenList
   */
  public $list;
  /**
   *
   * @var ScreenManager
   */
  public $manager;
  /**
   *
   * @var ScreenCustom
   */
  public $custom;
  /**
   *
   * @var ScreenUtilities
   */
  public $utilities;
  /**
   *
   * @var ScreenMessage
   */
  public $message;
  /**
   *
   * @var type 
   */
  private $types = array();

  /**
   *
   * @param type $ext
   * @param type $encode
   * @return \Screen
   */
  public function Screen($ext = "", $encode = null) {

    $ext = $ext === '{project.rsc}' ? DEFAULT_EXTENSION : $ext;
    $this->extension = $ext;
    $this->encode = $encode !== null ? $encode : $this->encode;

    System::import('class', 'resource', 'ScreenLine', 'core');
    System::import('class', 'resource', 'ScreenList', 'core');
    System::import('class', 'resource', 'ScreenCustom', 'core');

    System::import('class', 'resource', 'ScreenUtilities', 'core');
    System::import('class', 'resource', 'ScreenMessage', 'core');

    $this->types = array(
      'pk'=>new Object(array("type"=>"numeric","component"=>"input[number]","formatter"=>false,"htmlentities"=>false)),
      'fk'=>new Object(array("type"=>"string","component"=>"select","formatter"=>false,"htmlentities"=>false)),

      'int'=>new Object(array("type"=>"numeric","component"=>"input[number]","formatter"=>false,"htmlentities"=>false)),
      'double'=>new Object(array("type"=>"numeric","component"=>"input[number]","formatter"=>true,"htmlentities"=>false)),
      'money'=>new Object(array("type"=>"numeric","component"=>"input[number]","formatter"=>true,"htmlentities"=>false)),
      'percent'=>new Object(array("type"=>"numeric","component"=>"input[number]","formatter"=>true,"htmlentities"=>false)),
      'min/max'=>new Object(array("type"=>"numeric","component"=>"input[number]","formatter"=>false,"htmlentities"=>false)),

      'boolean'=>new Object(array("type"=>"numeric","component"=>"input[checkbox]","formatter"=>true,"htmlentities"=>false)),

      'string'=>new Object(array("type"=>"string","component"=>"input[text]","formatter"=>false,"htmlentities"=>true)),
      'upper'=>new Object(array("type"=>"string","component"=>"input[text]","formatter"=>false,"htmlentities"=>true)),
      'hash'=>new Object(array("type"=>"string","component"=>"input[text]","formatter"=>false,"htmlentities"=>true)),
      'source'=>new Object(array("type"=>"string","component"=>"input[text]","formatter"=>false,"htmlentities"=>true)),
      'cpf'=>new Object(array("type"=>"string","component"=>"input[text]","formatter"=>false,"htmlentities"=>true)),
      'cnpj'=>new Object(array("type"=>"string","component"=>"input[text]","formatter"=>false,"htmlentities"=>true)),
      'telefone'=>new Object(array("type"=>"string","component"=>"input[text]","formatter"=>false,"htmlentities"=>true)),
      'cep'=>new Object(array("type"=>"string","component"=>"input[text]","formatter"=>false,"htmlentities"=>true)),
      'base64'=>new Object(array("type"=>"string","component"=>"input[text]","formatter"=>false,"htmlentities"=>true)),
      'email'=>new Object(array("type"=>"string","component"=>"input[text]","formatter"=>false,"htmlentities"=>true)),

      'text'=>new Object(array("type"=>"string","component"=>"textarea","formatter"=>false,"htmlentities"=>false)),
      'rich'=>new Object(array("type"=>"string","component"=>"textarea","formatter"=>false,"htmlentities"=>false)),
      'longtext'=>new Object(array("type"=>"string","component"=>"textarea","formatter"=>false,"htmlentities"=>false)),

      'datetime'=>new Object(array("type"=>"date","component"=>"datetimepicker","formatter"=>false,"htmlentities"=>false)),
      'date'=>new Object(array("type"=>"date","component"=>"datetimepicker","formatter"=>true,"htmlentities"=>false)),
      'second'=>new Object(array("type"=>"date","component"=>"datetimepicker","formatter"=>false,"htmlentities"=>false)),
      'timestamp'=>new Object(array("type"=>"date","component"=>"datetimepicker","formatter"=>false,"htmlentities"=>false)),
      'time'=>new Object(array("type"=>"date","component"=>"datetimepicker","formatter"=>false,"htmlentities"=>false)),
      'dateshort'=>new Object(array("type"=>"date","component"=>"datetimepicker","formatter"=>false,"htmlentities"=>false)),

      'yes/no'=>new Object(array("type"=>"string","component"=>"input[radio]","formatter"=>true,"htmlentities"=>false)),
      'option'=>new Object(array("type"=>"string","component"=>"input[radio]","formatter"=>false,"htmlentities"=>false)),

      'select'=>new Object(array("type"=>"string","component"=>"select","formatter"=>false,"htmlentities"=>false)),
      'select-multi'=>new Object(array("type"=>"string","component"=>"select","formatter"=>false,"htmlentities"=>false)),
      'list'=>new Object(array("type"=>"string","component"=>"select","formatter"=>false,"htmlentities"=>false)),
      'array'=>new Object(array("type"=>"string","component"=>"select","formatter"=>false,"htmlentities"=>false)),

      'image'=>new Object(array("type"=>"string","component"=>"input[file]","formatter"=>false,"htmlentities"=>false)),
      'file'=>new Object(array("type"=>"string","component"=>"input[file]","formatter"=>true,"htmlentities"=>false)),

      'html'=>new Object(array("type"=>"string","component"=>"html","formatter"=>false,"htmlentities"=>false)),
      'calculated'=>new Object(array("type"=>"string","component"=>"html","formatter"=>false,"htmlentities"=>false)),

      'between'=>new Object(array("type"=>"date","component"=>"datetimepicker","formatter"=>false,"htmlentities"=>false)),
      'range'=>new Object(array("type"=>"numeric","component"=>"input[number]","formatter"=>false,"htmlentities"=>false)),

      'subquery'=>new Object(array("type"=>"string","component"=>"html","formatter"=>false,"htmlentities"=>false)),
      'subquery-radio'=>new Object(array("type"=>"string","component"=>"html","formatter"=>false,"htmlentities"=>false)),
      'sub-item'=>new Object(array("type"=>"string","component"=>"html","formatter"=>false,"htmlentities"=>false)),
      'mixed'=>new Object(array("type"=>"string","component"=>"html","formatter"=>false,"htmlentities"=>false)),

      'object'=>new Object(array("type"=>"string","component"=>"html","formatter"=>false,"htmlentities"=>false))
    );

    $this->line = new ScreenLine($this->extension, $this->encode, $this->types);
    $this->utilities = new ScreenUtilities($this->extension, $this->encode, $this->types);
    $this->message = new ScreenMessage($this->extension, $this->encode, $this->types);
    
    return $this;
  }
  
  /**
   * 
   * @param type $user
   * @param type $admin
   */
  public function prinScreenHeader($user, $admin) {
    $path = System::import('file', 'header', 'index', Application::getThemeLocation(), false);
    require_once $path;
  }
  
  /**
   * 
   * @param type $user
   * @param type $admin
   */
  public function prinScreenBottom($user, $admin) {
    $path = System::import('file', 'bottom', 'index', Application::getThemeLocation(), false);
    require_once $path;
  }

  /**
   * 
   * @param type $message
   */
  public function printScreenSite($message) {
    $path = System::import('file', 'site', 'index', Application::getThemeLocation(), false);
    require_once $path;
  }

  /**
   * 
   * @param type $user
   */
  public function printScreenAdmin($user) {
    $path = System::import('file', 'admin', 'index', Application::getThemeLocation(), false);
    require_once $path;
  }

  /**
   * 
   * @param type $redirect
   */
  public function printScreenRedirect($redirect) {
    $path = System::import('file', 'redirect', 'index', Application::getThemeLocation(), false);
    require_once $path;
  }

  /**
   * 
   * @param type $target
   * @param type $layout
   * @param type $operation
   * @param type $level
   * @param type $filter
   * @param type $items
   * @param type $recovereds
   * @param type $interfaces
   * @param type $properties
   * @param type $custom
   * @param type $history
   * @param type $execute
   */
  public function printContent ($target, $layout, $operation, $level, $filter, $items, $recovereds, $interfaces, $properties, $custom, $history = "", $execute = "") {

    if ($layout === "list") {
      $this->printListContent($target, $operation, $level, $filter, $items, $recovereds, $interfaces, $properties, $custom);
    } else {
      $this->printManagerContent($target, $layout, $operation, $level, $filter, $items, $recovereds, $interfaces, $properties, $custom, $history, $execute);
    }

  }
  
  /**
   * 
   * @param type $target
   * @param type $layout
   * @param type $operation
   * @param type $level
   * @param type $filter
   * @param type $items
   * @param type $recovereds
   * @param type $interfaces
   * @param type $properties
   * @param type $custom
   * @param type $history
   * @param type $execute
   */
  private function printManagerContent($target, $layout, $operation, $level, $filter, $items, $recovereds, $interfaces, $properties, $custom, $history, $execute) {

    $form = isset($interfaces['form']) ? ($interfaces['form'] ? $interfaces['form'] : 'form-' . $target) : "";
    
    $lines = $properties["lines"];
    $module = $properties["module"];
    $entity = $properties["entity"];
    $tag = $properties["tag"];
    $operations = $properties["operations"];
    $reference = $properties["reference"];

    $width = isset($interfaces['width']) ? $interfaces['width'] : WINDOW_WIDTH;
    $height = isset($interfaces['height']) ? $interfaces['height'] : WINDOW_HEIGHT;
    $modal = isset($interfaces['modal']) ? $interfaces['modal'] : false;

    $registro = isset($history['registro']) ? $history['registro'] : "";
    $criador = isset($history['criador']) ? $history['criador'] : "";
    $alteracao = isset($history['alteracao']) ? $history['alteracao'] : "";
    $responsavel = isset($history['responsavel']) ? $history['responsavel'] : "";

    $primary = array();
    $foreign = array();
    $hidden = array();
    $popups = array();
    $children = array();

    foreach ($items as $key => $item) {
      if ($item['pk']) {
        $primary[] = $item;
      }
      if ($item['fk']) {
        $foreign[] = $item;
      }
      if (isset($item['hidden'])) {
        if ($item['hidden']) {
          $item['form'] = 0;
          $hidden[] = array("id" => $item['id'], "name" => $item['id'], "value" => $item['value']);
        }
      }
      $item['target'] = $target;
      $items[$key] = $item;
    }

    $action = "";
    $label = "";
    $popup = true;
    $child = true;
    $history = true;
    $op = isset($operations[$operation]) ? $operations[$operation] : "";
    if ($op) {
      $action = $operation;

      $label = $op->label;
      $popup = $op->popup;
      $child = $op->child;
      $history = $op->history;
    }

    if ($popup){
      $popups = $this->utilities->getPopups($items, $module, $entity, $tag, $target, $level, $width, $height);
    }

    if ($child) {
      $children = $this->utilities->getChildren($items, $filter, $layout, $lines, $modal, $target, $level, $width, $height);
    }


    if ($lines) {
      $items = $this->utilities->orderItems($items, $lines);
    }

    if ($layout === 'manager' or $layout === '') {
      
      $this->printFormHeader($operation, $items, $operations, $form, $target, $level, $module, $entity, $tag);

        $this->printFormLines($operation, $target, $items, $lines);

        if ($popup) {
          $this->line->printPopups($popups);
        }
        
        $this->printHiddens($primary, $foreign, $hidden);

        $this->printFormValues($action, $target, $level, $filter, $label, $properties, $recovereds, $interfaces);

        if ($history) {
          $this->printFormHistory($registro, $criador, $alteracao, $responsavel, $target, $module, $entity, $tag, $reference);
        }

      $this->printFormBottom($operation, $items, $operations, $form, $target, $level, $module, $entity, $tag);

      if (($child) and ($layout === 'children' or $layout === '')) {
        $this->line->printChildren($target, $children);
      }

    } else {
      
    }

  }
  
  /**
   * 
   * @param type $target
   * @param type $operation
   * @param string $level
   * @param type $filter
   * @param type $items
   * @param type $recovereds
   * @param type $interfaces
   * @param type $properties
   * @param type $custom
   */
  private function printListContent($target, $operation, $level, $filter, $items, $recovereds, $interfaces, $properties, $custom) {

    $form = isset($interfaces['form']) ? ($interfaces['form'] ? $interfaces['form'] : 'form-' . $target) : "";
    
    $lines = $properties['lines'];
    $module = $properties['module'];
    $entity = $properties['entity'];
    $tag = $properties['tag'];
    $operations = $properties['operations'];
    
    $counter = isset($interfaces['counter']) ? $interfaces['counter'] : "40";
    $commands = isset($interfaces['commands']) ? $interfaces['commands'] : "100";

    $label = "";
    $action = "";
    $op = isset($operations[$operation]) ? $operations[$operation] : "";
    if ($op) {
      $label = $op->label;
      $action = $operation;
    }

    $url = PAGE_APP . 'src/' . $module . '/' . 'json' . '/' . $entity . '.json.php?action=' . $tag . '-j-list' . '&t=' . $target . '&f=' . $filter . '&level=' . $level;
    
    $data = array();
    foreach ($recovereds as $r => $recovered) {
      $data[] = $r . ':"' . $recovered . '"'; 
    }
    $data[] = 'auth: Math.random()';
    
    $post = '{' . implode(',', $data) . '}';
    
    $columns = array();
    $columns[] = $this->formatColumn(array("id"=>"commands","type"=>"string","description"=>"Op��es","title"=>"","grid_width"=>$commands), 'commands');
    $columns[] = $this->formatColumn(array("id"=>"counter","type"=>"int","description"=>"Registros","title"=>"","grid_width"=>""));
    foreach ($items as $key => $item) {
      if (isset($item['hidden'])) {
        if ($item['hidden']) {
          $item['grid'] = 0;
        }
      }
      if ($item['grid'] === 1) {
        $columns[] = $this->formatColumn($item);
      }
      $item['target'] = $target;
      $items[$key] = $item;
    }

    if ($lines) {
      $items = $this->utilities->orderItems($items, $lines);
    }

    ?>
      <div class="box box-default box-condensed">
        <form id="<?php print $form; ?>" name="<?php print $form; ?>" role="form" onsubmit="return false;" action="return false;">
          <?php
            $this->printFormValues($action, $target, $level, $filter, $label, $properties, $recovereds, $interfaces);
          ?>
          <div id="toolbar-<?php print $target; ?>" class="toolbar-list">
            <?php
              $this->printToolbar($operation, $items, $operations, $target);
            ?>
          </div>

          <table id="grid-<?php print $target; ?>" data-toolbar="#toolbar-<?php print $target; ?>">
            <thead>
              <tr>
                <?php
                foreach ($columns as $column) {
                  ?>
                    <th <?php foreach ($column as $property => $value) { print $property . '="' . $value . '"'; } ?> ><?php print $column['description']; ?></th>
                  <?php
                }
                ?>
              </tr>
            </thead>
          </table>

          <div id="message-<?php print $target; ?>" style="display: none;" class="post-notify"></div>

        </form>

      </div>

      <script>
        Application.form.createGrid('<?php print $target; ?>', '<?php print $url; ?>', <?php print $post; ?>, '<?php print $tag; ?>');
      </script>
    <?php

  }
  
  /**
   * 
   * @param type $param
   * @param type $items
   * @param type $operations
   * @param type $form
   * @param type $target
   * @param type $level
   * @param type $module
   * @param type $entity
   * @param type $tag
   */
  private function printFormHeader($param, $items, $operations, $form, $target, $level, $module, $entity, $tag) {

    $label = "";
    $operation = isset($operations[$param]) ? $operations[$param] : "";
    if ($operation) {
      $label = $operation->label;
    }

    ?>
      <div class="box box-default box-condensed">

        <div class="box-header">
          <h3 class="box-title"><?php print $label; ?></h3>
        </div>

        <form id="<?php print $form; ?>" name="<?php print $form; ?>" role="form" onsubmit="return false;" action="return false;">

          <div class="box-body">
          <?
  }
  
  /**
   * 
   * @param type $param
   * @param type $items
   * @param type $operations
   * @param type $form
   * @param type $target
   * @param type $level
   * @param type $module
   * @param type $entity
   * @param type $tag
   */
  private function printFormBottom($param, $items, $operations, $form, $target, $level, $module, $entity, $tag) {
        ?>
          </div>
          <div class="box-footer box-toolbar">
            <div class="form-group toolbar-manager">
              <?php
                $this->printToolbar($param, $items, $operations, $target, $level, $module, $entity, $tag);
              ?>
              <div id="message-<?php print $target; ?>" style="display: inline-block"></div>
            </div>
          </div>
        </form>
      </div>
    <?
  }
  
  /**
   * 
   * @param type $param
   * @param type $items
   * @param type $operations
   * @param type $target
   * @param type $level
   * @param type $module
   * @param type $entity
   * @param type $tag
   */
  private function printToolbar($param, $items, $operations, $target) {

    $operation = isset($operations[$param]) ? $operations[$param] : "";

    if ($operation) {

      $buttons = array();
      foreach ($operation->operations as $index => $class) {
        $o = $this->validateOperation($items, $operations, $index);
        if ($o) {
          if ($o->position === 'toolbar') {
            $o->class = $class ? $class : "btn-default";
            $buttons[] = $o;
          }
        }
      }

      foreach ($buttons as $button) {
        ?>
          <button type="button" id="<?php print $button->action . "-" . $target; ?>" class="btn <?php print $button->class; ?>" data-action="<?php print $button->type; ?>" data-value="<?php print $button->action; ?>" data-layout="<?php print $button->layout; ?>" data-confirm="<?php print $button->confirm; ?>" data-custom="<?php print $button->custom; ?>"><?php print $button->label; ?></button> 
        <?php
      }

    }

  }
  
  /**
   * 
   * @param type $items
   * @param type $operations
   * @param type $index
   * 
   * @return type
   */
  private function validateOperation($items, $operations, $index) {
    $o = null;
    if (isset($operations[$index])) {
      $operation = $operations[$index];
      $error = false;
      if (is_array($operation->conditions)) {
        foreach ($operation->conditions as $key => $condition) {
          if ($items[$key]['value'] !== $condition) {
            $error = true;
          }
        }
      } else if ($operation->conditions === false) {
        $error = true;
      }
      $o = $error ? $o : $operation;
    }
    return $o;
  }

  /**
   *
   * @param type $registro
   * @param type $criador
   * @param type $alteracao
   * @param type $responsavel
   * 
   * @param type $target
   * @param type $module
   * @param type $rotule
   * @param type $entity
   * @param type $tag
   * 
   * @param type $codigo
   */
  public function printFormHistory($registro, $criador, $alteracao, $responsavel, $target, $module, $entity, $tag) {

    ?>
      <div style="width: 100%; height: 15px; white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">
        &Uacute;ltima modifica&ccedil;&atilde;o em <i><?php print System::encode($alteracao, $this->encode); ?></i> por <i><?php print System::encode($responsavel, $this->encode); ?></i>.
        <a href="javascript:void(0);" data-action="alert-history" data-value="{<?php print System::encode($registro, $this->encode); ?>,<?php print System::encode($criador, $this->encode); ?>}">
           Hist&oacute;rico
        </a>
      </div>
    <?
  }

  
  /**
   * 
   * @param type $param
   * @param type $target
   * @param type $items
   * @param type $lines
   */
  public function printFormLines($param, $target, $items, $lines) {

    for ($line = 1; $line <= $lines; $line++) {

      $elements = array();
      foreach ($items as $item) {

        $show = false;
        if (isset($item['line']) && $item['line'] == $line) {
          $show = $this->utilities->getViewManageVisibility($item, $param);
        }
        if ($show) {
          $elements[] = $item;
        }

      }
      $this->line->printLine($target, $line, $elements);

    }

  }
  
  /**
   * 
   * @param type $column
   * @param type $formatter
   * 
   * @return string
   */
  private function formatColumn($column, $formatter = '') {
    /**
     * id: ''
     * type: ''
     * 
     * identifier: true, false
     * converter: ''
     * formatter: ''
     * 
     * align: 'left', 'center', 'right'
     * headerAlign: 'left', 'center', 'right'
     * 
     * sortable: true, false
     * order: '', 'asc', 'desc'
     * 
     * visible: true, false
     */
    $behavior = $column['type'];
    $id = $column['id'];
    if (isset($column['foreign'])) {
      $id = $column['foreign']['description'];
    }
    
    $type = $this->types[$behavior];

    $settings = array();

    $settings['description'] =  System::encode($column['description'], $this->encode);
    $settings['title'] =  System::encode($column['title'], $this->encode);

    $settings['data-column-id'] = $id;
    $settings['data-type'] = $type->type;

    $settings['data-identifier'] = 'false';
    $settings['data-converter'] = '';
    $settings['data-formatter'] = $type->formatter ? $type->type : $formatter;

    $settings['data-align'] = 'left';
    $settings['data-headerAlign'] = 'left';
    $settings['data-sortable'] = 'true';
    $settings['data-order'] = '';
    $settings['data-visible'] = 'true';

    $settings['data-switchable'] = 'false';
    $settings['width'] = $column['grid_width'] ? $column['grid_width'] : '';

    switch ($behavior) {

      case 'pk':

        $settings['data-identifier'] = 'true';

        $settings['data-align'] = 'left';
        $settings['data-switchable'] = 'false';
        $settings['width'] = '50';

        break;

      case 'percent':
      case 'double':
      case 'money':

        $settings['data-align'] = 'right';
        $settings['data-switchable'] = 'false';
        $settings['width'] = '80';

        break;
    }

    return $settings;
  }

  
  /**
   *
   * @param type $primary
   * @param type $foreign
   * @param type $hidden
   */
  public function printHiddens($primary, $foreign, $hidden = null) {

    if (is_array($primary) > 0) {
      foreach ($primary as $item) {
        if ($item['value']) {
          ?>
          <input type="hidden" id="<?php print $item['id']; ?>" name="<?php print $item['id']; ?>" value="<?php print $item['value']; ?>"/>
          <?
        }
      }
    }

    if (is_array($foreign) > 0) {
      foreach ($foreign as $item) {
        if ($item['form'] == 0) {
          if ($item['value']) {
            ?>
              <input type="hidden" id="<?php print $item['id']; ?>" name="<?php print $item['id']; ?>" value="<?php print $item['value']; ?>"/>
            <?
          }
        }
      }
    }

    if (is_array($hidden) > 0) {
      foreach ($hidden as $item) {
        ?>
          <input type="hidden" id="<?php print $item['id']; ?>" name="<?php print $item['name']; ?>" value="<?php print $item['value']; ?>"/>
        <?
      }
    }

  }
  
  /**
   * 
   * @param type $operation
   * @param type $target
   * @param type $level
   * @param type $filter
   * @param type $label
   * @param type $properties
   * @param type $recovereds
   * @param type $interfaces
   */
  public function printFormValues($operation, $target, $level, $filter, $label, $properties, $recovereds, $interfaces) {

    $style = false ? "display: block;" : "display: none;";
    $module = $properties['module'];
    $entity = $properties['entity'];
    $tag = $properties['tag'];

    $identification = '{"module":"' . $module . '","entity":"' . $entity . '","tag":"' . $tag . '","label":"' . $label . '","target":"' . $target . '","level":"' . $level . '"}';

    ?>
    <div id="form-values-<?php print $target; ?>" class="form-values" style="<?php print $style; ?>">
      <?php
      
        ?>
          op:<input type="text" name="operation" id="operation" value="<?php print $operation; ?>" size="5"/>
          i:<input type="text" name="i" id="i" value="<?php print htmlentities($identification); ?>" size="5"/>
          t:<input type="text" name="t" id="t" value="<?php print $target; ?>" size="5"/>
          l:<input type="text" name="l" id="l" value="<?php print $level; ?>" size="5"/>
          f:<input type="text" name="f" id="f" value="<?php print $filter; ?>" size="5"/>
        <?php

        foreach ($recovereds as $recovered => $value) {
          ?>
            <?php print $recovered; ?>:<input type="text" name="<?php print $recovered; ?>" id="<?php print $recovered; ?>" value="<?php print $value; ?>" size="5"/>
          <?php
        }

        foreach ($interfaces as $interface => $value) {
          ?>
            <?php print $interface; ?>:<input type="text" name="<?php print $interface; ?>" id="<?php print $interface; ?>" value="<?php print $value; ?>" size="5"/>
          <?php
        }
      ?>
    </div>
    <?
  }

}