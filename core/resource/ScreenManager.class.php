<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Form
 *
 * @author William
 */
class ScreenManager {

  //put your code here

  private $ext;
  private $encode;

  /**
   *
   * @param type $ext
   * @param type $encode
   */
  public function ScreenManager($ext = "", $encode = false) {
    $this->ext = $ext;
    $this->encode = $encode;
  }

  /**
   *
   * @param type $target
   * @param type $rotule
   * @param type $info
   * @param type $window_width
   */
  public function printManagerHeader($target, $rotule, $info, $window_width) {
    ?>
      <table id="table-<?php print $target; ?>" class="form-table b-bottom" cellpadding="0" cellspacing="0" border="0" style="width:<?php print $window_width; ?>px">
        <?php
        if (!empty($rotule)) {
          ?>
          <tr>
            <td colspan="2" class="form-table-td form-table-top">
              <label><?php print System::encode($info, $this->encode); ?> <?php print System::encode($rotule, $this->encode); ?></label>
            </td>
          </tr>
          <?
        }

  }

  /**
   *
   * @param type $target
   * @param type $rotule
   * @param type $info
   * @param type $window_width
   */
  public function printManagerFormHeader($form, $target, $rotule, $info, $window_width) {
    ?>
    <tr>
      <td colspan="2">
        <form id="<?php print $form; ?>" name="<?php print $form; ?>"  action="return false;" onsubmit="return false;">
          <?
  }

  /**
   *
   * @param type $target
   * @param type $rotule
   * @param type $info
   * @param type $window_width
   */
  public function printManagerLinesHeader($form, $target, $rotule, $info, $window_width, $header = false, $toolbar = true) {

    $width = $header ? $window_width . "px" : "100%";
    $class = $header ? "form-table b-bottom" : "";
    ?>
    <table cellspacing="0" cellpadding="0" border="0"  class="<?php print $class; ?>" style="width:<?php print $width; ?>;">
      <?php
      if ($header) {
        ?>
        <tr>
          <td colspan="2" class="form-label form-table-top">
            <label><?php print System::encode($info, $this->encode); ?> <?php print System::encode($rotule, $this->encode); ?></label>
          </td>
        </tr>
        <?
      }
      if ($toolbar) {
        ?>
        <tr>
          <td colspan="2" class="form-table-toolbar b-left b-right">
            <div id="toolbar-<?php print $target; ?>"></div>
          </td>
        </tr>
        <?
      }

    }

    /**
     *
     * @param type $primary
     * @param type $foreign
     * @param type $hidden
     */
    public function printManagerHiddens($primary, $foreign, $hidden = null) {

      if (is_array($primary) > 0) {
        foreach ($primary as $item) {
          if ($item['value']) {
            ?>
            <input type="hidden" id="<?php print $item['id']; ?>" name="<?php print $item['id']; ?>" value="<?php print $item['value']; ?>"/>
            <?
          }
        }
      }

      if (is_array($foreign) > 0) {
        foreach ($foreign as $item) {
          if ($item['form'] == 0) {
            if ($item['value']) {
              ?>
              <input type="hidden" id="<?php print $item['id']; ?>" name="<?php print $item['id']; ?>" value="<?php print $item['value']; ?>"/>
              <?
            }
          }
        }
      }

      if (is_array($hidden) > 0) {
        foreach ($hidden as $item) {
          if ($item['value']) {
            ?>
            <input type="hidden" id="<?php print $item['id']; ?>" name="<?php print $item['name']; ?>" value="<?php print $item['value']; ?>"/>
            <?
          }
        }
      }

    }

    /**
     *
     * @param type $acesso
     * @param type $param
     * @param type $target
     * @param type $level
     * @param type $modulo
     * @param type $entity
     * @param type $tag
     * @param type $back
     * @param type $execute
     * @param type $validate
     * @param type $load
     * @param type $core
     * @param type $form
     * @param type $html
     * @param type $extras
     * @param type $width
     */
    public function printManagerToolbar($acesso, $param, $target, $level, $modulo, $entity, $tag, $back = true, $execute = '', $validate = '{}', $load = '', $core = false, $form = "", $html = "", $extras = null, $width = "", $settings = null) {

      $modulo_view = PAGE_APP . 'src/' . $modulo . '/view';
      $modulo_post = PAGE_APP . 'src/' . $modulo . '/post';
      if ($core) {
        $modulo_view = 'core/view';
        $modulo_post = 'core/post';
      }

      $form = $form ? $form : 'form-' . $target;

      $width = $width ? $width : WINDOW_WIDTH;

      $hidden_save = isset($settings["save"]) ? $settings["save"] : (($param === "search" or $param === "view") ? "true" : "false");
      $hidden_search = isset($settings["search"]) ? $settings["search"] : (($param === "search") ? "false" : "true");
      $hidden_back = isset($settings["back"]) ? $settings["back"] : (($back) ? "false" : "true");
      $hidden_new = isset($settings["new"]) ? $settings["new"] : ((($back and $param === "add") or ($back and $param === "search")) ? "false" : "true");
      $hidden_copy = isset($settings["copy"]) ? $settings["copy"] : (($back and $param === "edit") ? "false" : "true");
      $hidden_find = isset($settings["find"]) ? $settings["find"] : ((!$back or $param == "search") ? "true" : "false");

      $more = "";
      if (is_array($extras)) {
        foreach ($extras as $buttom) {

          $b = "id: '" . $buttom->id . "', text: '" . System::encode($buttom->text, $this->encode) . "', cls: '" . $buttom->cls . "', width: " . $buttom->width . ", hidden: " . String::booleanText($buttom->hidden);

          if ($buttom->handler) {
            $b = $b . ", handler: function() { " . $buttom->handler . "; }";
          }

          if ($buttom->menu) {
            $i = join(',', $buttom->menu);
            $b = $b . ", menu: { items: [" . $i . "] }";
          }

          $more = $more . "{ " . $b . " },";
        }
      }
          ?>
          <script type="text/javascript">
            var items = [
              {
                id: 'saveit-<?php print $target; ?>',
                text: 'Salvar',
                cls: 'form-buttom',
                hidden: <?php print $hidden_save; ?>,
                width: 95,
                title: 'Salva ou edita este registro'
                        , handler: function() {
                  var items = null;
                  <?php
                    if ($validate) {
                      ?>
                                    items = <?php print System::encode($validate, $this->encode); ?>;
                      <?
                    }
                  ?>
                  var execute = function() {
                    try {
                      if (system.action.close) {
                        system.window.get().close();
                        system.action.close = false;
                      }
                <?php print $execute; ?>

                <?php
                if ($load) {
                  ?>
                    system.manager.list('<?php print $load['form']; ?>', '<?php print PAGE_APP; ?>src/<?php print $load['module']; ?>/view', '<?php print $load['entity']; ?><?php print $this->ext; ?>', '<?php print $load['tag']; ?>', false, '<?php print $load['target']; ?>', '<?php print $load['level']; ?>', '<?php print $load['params']; ?>');
                  <?
                }
                ?>
                } catch (e) {

                }
              };
              if (!this.hidden) {
                system.manager.save('form-<?php print $target; ?>', '<?php print $modulo_post; ?>', '<?php print $entity; ?><?php print $this->ext; ?>', '<?php print $tag; ?>', 'message-<?php print $target; ?>', '<?php print $level; ?>', items, execute);
              }
            }
          },
          {
            id: 'searchit-<?php print $target; ?>',
            text: 'Localizar',
            cls: 'form-buttom',
            hidden: <?php print $hidden_search; ?>,
            width: 95,
            title: 'Pesquisa por registros com os filtros informados'
                    , handler: function() {
              if (!this.hidden) {
                system.manager.list('form-<?php print $target; ?>', '<?php print $modulo_view; ?>', '<?php print $entity; ?><?php print $this->ext; ?>', '<?php print $tag; ?>', true, '<?php print $target; ?>', '<?php print $level; ?>');
              }
            }
          },
          {
            id: 'backit-<?php print $target; ?>',
            text: 'Voltar',
            cls: 'form-buttom',
            hidden: <?php print $hidden_back; ?>,
            width: 95,
            title: 'Volta para a lista de registros'
                    , handler: function() {
              if (!this.hidden) {
                system.manager.list('form-<?php print $target; ?>', '<?php print $modulo_view; ?>', '<?php print $entity; ?><?php print $this->ext; ?>', '<?php print $tag; ?>', false, '<?php print $target; ?>', '<?php print $level; ?>');
              }
            }
          },
          {
            id: 'copyit-<?php print $target; ?>',
            text: 'Copiar',
            cls: 'form-buttom',
            hidden: <?php print $hidden_copy; ?>,
            width: 95,
            title: 'Salva uma c&oacute;pia deste registro'
                    , handler: function() {
              if (!this.hidden) {
                system.manager.copy('form-<?php print $target; ?>', '<?php print $modulo_post; ?>', '<?php print $entity; ?><?php print $this->ext; ?>', '<?php print $tag; ?>', 'message-<?php print $target; ?>', '<?php print $level; ?>');
              }
            }
          },
          {
            id: 'newit-<?php print $target; ?>',
            text: 'Novo',
            cls: 'form-buttom',
            hidden: <?php print $hidden_new; ?>,
            width: 95,
            title: 'Abre um formul&aacute;rio vazio para criar um novo registro'
                    , handler: function() {
              if (!this.hidden) {
                system.manager.add('form-<?php print $target; ?>', '<?php print $modulo_view; ?>', '<?php print $entity; ?><?php print $this->ext; ?>', '<?php print $tag; ?>', '<?php print $target; ?>', '<?php print $level; ?>');
              }
            }
          },
          {
            id: 'findit-<?php print $target; ?>',
            text: 'Pesquisa',
            hidden: <?php print $hidden_find; ?>,
            cls: 'form-buttom',
            width: 95
                    , handler: function() {
              system.manager.search('form-<?php print $target; ?>', '<?php print $modulo_view; ?>', '<?php print $entity; ?><?php print $this->ext; ?>', '<?php print $tag; ?>', '', '<?php print $target; ?>', '<?php print $level; ?>');
            }
          },
            <?php print $more; ?>
          {
            cls: 'form-manager-toolbar',
            html: '<div id="message-<?php print $target; ?>" class="form-manager-message"><?php print $html; ?></div>'
          }
        ];

        var tb = system.form.createToolbar('toolbar-<?php print $target; ?>', items, <?php print $width; ?>);

        system.manager.register('<?php print $target; ?>_container', 'toolbar-<?php print $target; ?>');

      </script>
    <?
  }

  /**
   *
   * @param type $param
   * @param type $items
   * @param type $lines
   * @param type $width_label
   * @param type $width_value
   * @param type $window_width
   */
  public function printManagerLines($param, $items, $lines, $width_label, $width_value, $window_width) {

    $screenUtilities = new ScreenUtilities($this->ext, $this->encode);
    $screenLine = new ScreenLine($this->ext, $this->encode);

    for ($line = 1; $line <= $lines; $line++) {
      $first = null;
      $subitems = null;
      foreach ($items as $item) {
        if (isset($item['line']) && $item['line'] == $line) {
          $show = $screenUtilities->getViewManageVisibility($item, $param);
          if ($show) {
            if ($param == "search") {
              $item['readonly'] = false;
            }
            if ($first == null) {
              $first = $item;
            } else {
              $subitems[] = $item;
            }
          }
        }
      }
      if ($first != null) {
        $screenLine->printLine($first, $width_label, $width_value, $subitems, true, $line);
      }
    }
  }

  /**
   *
   * @param type $target
   * @param type $rotule
   * @param type $info
   * @param type $window_width
   */
  public function printManagerBottom($target, $rotule, $info, $window_width) {
    ?>
      </table>
    <?
  }

  /**
   *
   * @param type $target
   * @param type $rotule
   * @param type $info
   * @param type $window_width
   */
  public function printManagerFormBottom($form, $target, $rotule, $info, $window_width) {
              ?>
          </form>
        </td>
      </tr>
    <?
  }

  /**
   *
   * @param type $target
   * @param type $rotule
   * @param type $info
   * @param type $window_width
   * @param type $header
   * @param type $toolbar
   */
  public function printManagerLinesBottom($target, $rotule, $info, $window_width, $header = false, $toolbar = false) {

    if ($header) {
      ?>
        <tr>
          <td colspan="2" class="form-label form-table-top">
            <label><?php print System::encode($info, $this->encode); ?> <?php print System::encode($rotule, $this->encode); ?></label>
          </td>
        </tr>
      <?
    }
    if ($toolbar) {
      ?>
        <tr>
          <td colspan="2" class="form-table-toolbar b-left b-top b-right">
            <div id="toolbar-<?php print $target; ?>"></div>
          </td>
        </tr>
      <?
    }
    ?>
    </table>
    <?
  }

    /**
     *
     * @param type $target
     * @param type $level
     * @param type $where
     * @param type $group
     * @param type $order
     * @param type $filter
     * @param type $w_description
     * @param type $g_description
     * @param type $o_description
     * @param type $rotule
     * @param type $start
     * @param type $height
     * @param type $width
     * @param type $child]
     */
    public function printFormValues($target, $level, $where, $group, $order, $filter, $w_description, $g_description, $o_description, $rotule, $start, $height, $width, $child) {

      $style = false ? "display: block;" : "display: none;";
      ?>
        <div style="<?php print $style; ?>">

          t:<input type="text" name="t" id="t" value="<?php print $target; ?>"/>
          l:<input type="text" name="l" id="l" value="<?php print $level; ?>"/>
          w:<input type="text" name="w" id="w" value="<?php print $where; ?>"/>
          g:<input type="text" name="g" id="g" value="<?php print $group; ?>"/>
          o:<input type="text" name="o" id="o" value="<?php print $order; ?>"/>
          f:<input type="text" name="f" id="f" value="<?php print $filter; ?>"/>

          wd:<input type="text" name="wd" id="wd" value="<?php print $w_description; ?>"/>
          gd:<input type="text" name="gd" id="gd" value="<?php print $g_description; ?>"/>
          od:<input type="text" name="od" id="od" value="<?php print $o_description; ?>"/>

          rotule:<input type="text" name="rotule" id="rotule" value="<?php print $rotule; ?>"/>
          start:<input type="text" name="start" id="start" value="<?php print $start; ?>"/>

          height:<input type="text" name="height" id="height" value="<?php print $height; ?>"/>
          width:<input type="text" name="width" id="width" value="<?php print $width; ?>"/>

          child:<input type="text" name="child" id="child" value="<?php print $child; ?>"/>

        </div>
    <?
  }

  /**
   *
   * @param type $window_width
   * @param type $registro
   * @param type $criador
   * @param type $alteracao
   * @param type $responsavel
   * @param type $target
   * @param type $module
   * @param type $rotule
   * @param type $entity
   * @param type $tag
   * @param type $id
   */
  public function printFormHistory($window_width, $registro, $criador, $alteracao, $responsavel, $target, $module, $rotule, $entity, $tag, $id, $new = true) {

      if ($new) {
        ?>
          <table id="table-<?php print $target; ?>" class="form-table b-bottom" cellpadding="0" cellspacing="0" border="0" style="width:<?php print $window_width; ?>px">
        <?
      }
      ?>
        <tr>
          <td class="form-table-td form-label b-top" align="left" valign="middle" colspan="2">
            Registrado em <i><?php print System::encode($registro, $this->encode); ?></i> por <i><?php print System::encode($criador, $this->encode); ?></i>. Alterado em <i><?php print System::encode($alteracao, $this->encode); ?></i> por <i><?php print System::encode($responsavel, $this->encode); ?></i>.
          </td>
        </tr>
      <?
      if ($new) {
        ?>
          </table>
        <?
      }

    }

  }
  ?>
