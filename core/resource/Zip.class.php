<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of zip
 *
 * @author WILLIAM
 */
class Zip extends ZipArchive{

  public function addDirectory($dir, $ignore){ // adds directory
    foreach(glob($dir . '/*') as $file){
      if(is_dir($file)){
        $this->addDirectory($file, $ignore);
      }else{
        $localname = str_replace($ignore, '', $file);
        $this->addFile($file, $localname);
      }
    }
  }

}

?>
