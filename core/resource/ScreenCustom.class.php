<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Custom
 *
 * @author William
 */
class ScreenCustom{
  //put your code here

  private $ext;

  /**
   *
   * @param type $ext
   */
  public function ScreenCustom($ext = ""){
    $this->ext = $ext;
  }

  /**
   *
   * @param type $buttons
   * @param type $id
   * @param type $target
   * @param type $width_bar
   */
  public function printCustomToolbar($buttons, $id, $target = "", $width_bar = "") {

    $target = $target ? $target : $id;
    $width_bar = $width_bar ? $width_bar : WINDOW_WIDTH;
    ?>
    <script type="text/javascript">
      var items = [];
        <?php
          foreach ($buttons as $button) {
            $type = isset($button['type']) ? $button['type'] : "";
            $class = isset($button['cls']) ? $button['cls'] : "form-buttom";
            $width = isset($button['width']) ? $button['width'] : "95";
            ?>
              items.push({
                text: '<?php print $button['text'];?>',
                xtype:'<?php print $type;?>',
                cls: '<?php print $class;?>',
                width: <?php print $width;?>,
                handler: function(){
                      <?php print $button['handler'];?>
                  }
                })
            <?
          }
        ?>
      items.push({
        html: '<div id="message-<?php print $target;?>"></div>'
      });
      var tb = system.form.createToolbar('<?php print $id; ?>', items, <?php print $width_bar;?>);

      system.manager.register('<?php print str_replace("custom-", "", $id);?>_container', '<?php print $id; ?>');

    </script>
    <?
  }

}

?>
