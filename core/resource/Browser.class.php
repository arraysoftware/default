<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
// Note: An excellent article on browser IDs can be found at
// http://www.zytrax.com/tech/web/browser_ids.htm

/**
 * Description of Browser
 *
 * @author William
 */
class Browser{

  public $browser = "";
  public $majorVersion = "";
  public $minorVersion = "";
  public $fullVersion = "";
  public $platform = "";
  public $userAgent = "";

  /**
   *
   * @return \Browser
   */
  function Browser(){

    $SUPERCLASS_NAMES = "gecko,mozilla,mosaic,webkit";
    $SUPERCLASS_REGX = "(?:".str_replace(",", ")|(?:", $SUPERCLASS_NAMES).")";

    $SUBCLASS_NAMES = "opera,msie,firefox,chrome,safari,dolhpin";
    $SUBCLASS_REGX = "(?:".str_replace(",", ")|(?:", $SUBCLASS_NAMES).")";

    $browser = "unrecognized";
    $majorVersion = "0";
    $minorVersion = "0";
    $fullVersion = "0.0";
    $platform = 'unrecognized';
    $userAgent = 'unrecognized';

    if(isset($_SERVER['HTTP_USER_AGENT'])){
      $userAgent = strtolower($_SERVER['HTTP_USER_AGENT']);

      $found = preg_match("/(?P<browser>".$SUBCLASS_REGX.")(?:\D*)(?P<majorVersion>\d*)(?P<minorVersion>(?:\.\d*)*)/i", $userAgent, $matches);
      if(!$found){
        $found = preg_match("/(?P<browser>".$SUPERCLASS_REGX.")(?:\D*)(?P<majorVersion>\d*)(?P<minorVersion>(?:\.\d*)*)/i", $userAgent, $matches);
      }

      if($found){
        $browser = $matches["browser"];
        $majorVersion = $matches["majorVersion"];
        $minorVersion = $matches["minorVersion"];
        $fullVersion = $matches["majorVersion"].$matches["minorVersion"];
        if($browser == "safari"){
          if(preg_match("/version\/(?P<majorVersion>\d*)(?P<minorVersion>(?:\.\d*)*)/i", $userAgent, $matches)){
            $majorVersion = $matches["majorVersion"];
            $minorVersion = $matches["minorVersion"];
            $fullVersion = $majorVersion.".".$minorVersion;
          }
        }
      }

      if(strpos($userAgent, 'linux')){
        $platform = 'Linux';
      }else if(strpos($userAgent, 'macintosh') || strpos($userAgent, 'mac platform x')){
        $platform = 'Mac';
      }else if(strpos($userAgent, 'windows') || strpos($userAgent, 'win32')){
        $platform = 'Windows';
      }
    }

    $this->browser = $browser;
    $this->majorVersion = $majorVersion;
    $this->minorVersion = $minorVersion;
    $this->fullVersion = $fullVersion;
    $this->platform = $platform;
    $this->userAgent = $userAgent;

    return $this;
  }

  /**
   *
   * Identifica se o dispositivo de acesso e um dispositivo movel
   * @return <type>
   */
  public function isMobile(){

    $_SERVER['ALL_HTTP'] = isset($_SERVER['ALL_HTTP']) ? $_SERVER['ALL_HTTP'] : '';

    $mobile_browser = '0';

    if(preg_match('/(up.browser|up.link|mmp|symbian|smartphone|midp|wap|phone)/i', strtolower($_SERVER['HTTP_USER_AGENT'])))
      $mobile_browser++;

    if((isset($_SERVER['HTTP_ACCEPT'])) and (strpos(strtolower($_SERVER['HTTP_ACCEPT']), 'application/vnd.wap.xhtml+xml') !== false))
      $mobile_browser++;

    if(isset($_SERVER['HTTP_X_WAP_PROFILE']))
      $mobile_browser++;

    if(isset($_SERVER['HTTP_PROFILE']))
      $mobile_browser++;

    $mobile_ua = strtolower(substr($_SERVER['HTTP_USER_AGENT'], 0, 4));
    $mobile_agents = array('w3c ', 'acs-', 'alav', 'alca', 'amoi', 'audi', 'avan', 'benq', 'bird', 'blac', 'blaz', 'brew', 'cell', 'cldc', 'cmd-', 'dang', 'doco', 'eric', 'hipt', 'inno', 'ipaq', 'java', 'jigs', 'kddi', 'keji', 'leno', 'lg-c', 'lg-d', 'lg-g', 'lge-', 'maui', 'maxo', 'midp', 'mits', 'mmef', 'mobi', 'mot-', 'moto', 'mwbp', 'nec-', 'newt', 'noki', 'oper', 'palm', 'pana', 'pant', 'phil', 'play', 'port', 'prox', 'qwap', 'sage', 'sams', 'sany', 'sch-', 'sec-', 'send', 'seri', 'sgh-', 'shar', 'sie-', 'siem', 'smal', 'smar', 'sony', 'sph-', 'symb', 't-mo', 'teli', 'tim-', 'tosh', 'tsm-', 'upg1', 'upsi', 'vk-v', 'voda', 'wap-', 'wapa', 'wapi', 'wapp', 'wapr', 'webc', 'winw', 'winw', 'xda', 'xda-');

    if(in_array($mobile_ua, $mobile_agents)){
      $mobile_browser++;
    }

    if(strpos(strtolower($_SERVER['ALL_HTTP']), 'operamini') !== false){
      $mobile_browser++;
    }

    if(strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'ipad') !== false){
      $mobile_browser++;
    }

    if(strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'android') !== false){
      $mobile_browser++;
    }

    if(strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'windows') !== false){
      $mobile_browser = 0;
    }

    if($mobile_browser > 0){
      return true;
    }else{
      return false;
    }
  }

}

?>
