<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Message
 *
 * @author William 
 */
class ScreenMessage{

  private $ext;

  /**
   *
   * @param type $ext
   */
  public function ScreenMessage($ext = ""){
    $this->ext = $ext;
  }

  /**
   *
   * @param type $message
   * @param type $complete
   */
  public function printMessageSucess($message, $complete = true) {
    ?>
      <div class="ui-state-highlight ui-corner-all message">
        <p>
          <span class="ui-icon ui-icon-info message-icon" style="float: left;"></span>
          <span class="message-sucess"><?php System::printText($message, true);?></span>
        </p>
      </div>
    <?
  }

  /**
   *
   * @param type $message
   * @param type $complete
   * @param type $validate
   */
  public function printMessageError($message, $complete = true, $validate = "") {
    ?>
      <div class="ui-state-error ui-corner-all message">
        <p>
          <span class="ui-icon ui-icon-alert message-icon" style="float: left;"></span>
          <span class="message-error"><?php System::printText($message, true);?></span>
          <?php
            if($validate){
              $script = "";
              $detail = "";
              if(is_array($validate)){
                foreach($validate as $item){
                  //$script = $script." system.util.addClass(system.util.get('".$item['id']."',true),'form-error');\n";
                  $detail = "<b>".$item['description']."</b>: ".$item['message']."<br>".$detail;
                }
              }
              ?>
                <a href="javascript:void(0);" onclick="system.window.alert('<?php print $detail;?>',null,'ext-mb-error');">Detalhes</a>
                <script><?php print $script;?></script>
              <?
            }
          ?>
        </p>
      </div>
    <?
  }

  /**
   *
   * @param type $message
   * @param type $complete
   */
  public function printMessageWarning($message, $complete = true){
    ?>
      <div class="ui-state-highlight ui-corner-all message">
        <p>
          <span class="ui-icon ui-icon-notice message-icon" style="float: left;"></span>
          <span class="message-warning">
            <b>
              A gera&ccedil;&atilde;o do projeto ocasionou algumas notifica&ccedil;&otilde;es. <span style="font-style: italic; cursor: pointer;" id="warning-<?php print $uniq; ?>">Detalhes</span>
              <div style="display: none; padding-top: 10px;" id="warning-div-<?php print $uniq; ?>"><?php System::printText($message, true); ?></div>
            </b>
          </span>
        </p>
      </div>
    <?
  }

}

?>
