<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Grid
 *
 * @author William
 */
class ScreenList{

  private $ext;
  private $encode;

  public $type = 'default';

  /**
   *
   * @param type $ext
   * @param type $encode
   */
  public function ScreenList($ext = "", $encode = false){
    $this->ext = $ext;
    $this->encode = $encode;
  }

  /**
   *
   * @param type $target
   * @param type $rotule
   * @param type $hidetoolbar
   * @param type $child
   */
  public function printListHeader($target, $rotule, $hidetoolbar, $child){

    if ($this->type === 'default') {
      ?>
        <table id="table-<?php print $target;?>" cellpadding="0" cellspacing="0" border="0">
          <?php
            if(!$child){
              ?>
                <tr>
                  <td class="header">
                      <span id="list-reload-<?php print $target;?>" class="tree-reload">&nbsp;</span>&nbsp;<label><?php print System::encode($rotule, $this->encode);?></label>
                  </td>
                </tr>
              <?
            }
          ?>

          <tr>
            <td colspan="2" class="form-table-toolbar">
              <div id="toolbar-<?php print $target;?>" style="<?php print $hidetoolbar;?>"></div>
            </td>
          </tr>
      <?
    } else if ($this->type === 'card') {
      ?>
        <label class="content-title"><?php print System::encode($rotule, $this->encode);;?></label>
      <?
    }

  }

  /**
   *
   * @param type $acesso
   * @param type $target
   * @param type $level
   * @param type $modulo
   * @param type $entity
   * @param type $tag
   * @param type $fast
   * @param type $whering
   * @param type $core = false
   * @param type $readonly = false
   * @param type $window_width = ""
   * @param type $extras = null
   */
  public function printListToolbar($acesso, $target, $level, $modulo, $entity, $tag, $fast, $whering, $core = false, $readonly = false, $window_width = "", $extras = null) {

    $modulo = PAGE_APP.'src/'.$modulo.'/view';
    if($core){
      $modulo = 'core/view';
    }

    $window_width = $window_width ? $window_width : WINDOW_WIDTH;

    if ($this->type === 'default') {

      $more = "";
      if (is_array($extras)) {
        foreach ($extras as $buttom) {
          $more .=
            ",{" .
            "  id: '" . $buttom->id . "'," .
            "  text: '" . System::encode($buttom->text, $this->encode) . "'," .
            "  cls: '" . $buttom->cls . "'," .
            "  width: " . $buttom->width . "," .
            "  handler: function(){" .
            "    " . $buttom->handler . ";" .
            "  }" .
            "}";
        }
      }

      $hidden_new = $readonly ? "true" : "false";
      $hidden_filter = $whering ? "false" : "true";
      ?>
        <script type="text/javascript">
          var items = [
            {
              id: 'newit-<?php print $target;?>',
              text: 'Novo',
              cls: 'form-buttom',
              hidden: <?php print $hidden_new;?>,
              width: 95
              ,handler: function(){
                system.manager.add('form-<?php print $target;?>','<?php print $modulo;?>','<?php print $entity;?><?php print $this->ext;?>','<?php print $tag;?>','<?php print $target;?>','<?php print $level;?>');
              }
            },
            {
              id: 'searchit-<?php print $target;?>',
              text: 'Pesquisar',
              cls: 'form-buttom',
              width: 95
              ,handler: function(){
                system.manager.search('form-<?php print $target;?>','<?php print $modulo;?>','<?php print $entity;?><?php print $this->ext;?>','<?php print $tag;?>','','<?php print $target;?>','<?php print $level;?>');
              }
            },
            {
              text: ' ',
              iconCls: 'icon-filter',
              hidden: <?php print $hidden_filter;?>,
              width: 20,
              iconAlign: 'left'
              ,title: 'Remover filtro'
              ,handler: function(){
                system.manager.list('form-<?php print $target;?>','<?php print $modulo;?>','<?php print $entity;?><?php print $this->ext;?>','<?php print $tag;?>',true,'<?php print $target;?>','<?php print $level;?>', 'clear=1');
              }
            },
            {
              text: 'Filtro',
              title: 'Exibir filtro',
              hidden: <?php print $hidden_filter;?>
              ,handler: function(){
                    system.window.alert('<?php print System::encode(html_entity_decode(base64_decode($whering)), $this->encode);?>');
              }
            },
            '-',
                {
                  text: 'Limpar'
                  ,handler: function(){
                    if(window.localStorage){
                      window.localStorage.removeItem('state-grid-<?php print $entity;?>');
                    }
                  }
                },
                '-',
            'Localizar: ',
            ' ',
            new Ext.form.TextField({
              id : 'fast-search-<?php print $target;?>',
              name : 'fast-search-<?php print $target;?>',
              value: '<?php print $fast;?>',
              width: 400,
              cls: 'form-search',
                  emptyText: 'Pesquisar',
              enableKeyEvents: true,
              listeners: {
                specialkey: function(f,e){
                  if (e.getKey() == e.ENTER) {
                        try{
                          //system.action.execute('fast');
                          var id = 'fastit-<?php print $target;?>';
                          Ext.get(id).dom.click();
                        }catch(e){
                          e.toString();
                  }
                      }
                },
                focus: function(f,e){
                  f.selectText();
                }
              }
             }),
            {
              id: 'fastit-<?php print $target;?>',
              text: 'Localizar',
              cls: 'form-buttom',
              align: 'right',
              width: 95
              ,handler: function(){
                system.manager.fast('form-<?php print $target;?>','<?php print $modulo;?>','<?php print $entity;?><?php print $this->ext;?>','<?php print $tag;?>','fast=1','<?php print $target;?>','<?php print $level;?>');
              }
            }
            <?php print $more;?>
          ];

          var tb = system.form.createToolbar('toolbar-<?php print $target;?>', items, <?php print $window_width;?>);
        </script>
      <?

    } else if ($this->type === 'card') {
      ?>
        <div class="content-pre"></div>
        <div id="toolbar-<?php print $target;?>"></div>
        <?php
          $hidden_new = $readonly ? "true" : "false";
          $hidden_filter = $whering ? "false" : "true";
        ?>
        <script type="text/javascript">
          var items = [
            {
              id: 'newit-<?php print $target;?>',
              text: 'Novo',
              cls: 'form-buttom',
              hidden: <?php print $hidden_new;?>,
              width: 95
              ,handler: function(){
                system.manager.add('form-<?php print $target;?>','<?php print $modulo;?>','<?php print $entity;?><?php print $this->ext;?>','&back=0&type=card&action=<?php print $tag;?>','<?php print $target;?>','<?php print $level;?>');
               }
            }
          ];

          var tb = system.form.createToolbar('toolbar-<?php print $target;?>', items, <?php print $window_width;?>);
        </script>
      <?php
    }

  }

  /**
   *
   * @param type $id
   * @param type $window_width
   * @param type $window_height
   */
  public function printListMarkup($id, $window_width, $window_height, $custom = "") {
    if ($this->type === 'default') {
      ?>
        <tr>
          <td class="form-table b-bottom b-top reset <?php print $custom; ?>">
            <div id="<?php print $id;?>" style="text-align: left; width: <?php print $window_width;?>px;"></div>
          </td>
        </tr>
      <?
    } else if ($this->type === 'card') {
      ?>
        <div id="<?php print $id;?>" style="text-align: left; width: <?php print $window_width;?>px;"></div>
      <?
    }
  }

  /**
   *
   * @param type $module
   * @param type $rotule
   * @param type $entity
   * @param type $tag
   * @param type $items
   * @param type $target
   * @param type $id
   * @param type $start
   * @param type $end
   * @param type $window_height
   * @param type $window_width
   * @param type $level
   * @param type $filter
   * @param type $where
   * @param type $group
   * @param type $order
   * @param type $option
   * @param type $checkbox
   * @param type $readonly
   * @param type $state
   * @param type $rightclick
   * @param type $template
   * @param type $append
   */
  public function printListGrid($module, $rotule, $entity, $tag, $items, $target, $id, $start, $end, $window_height, $window_width, $level, $filter, $where, $group, $order, $option = "80", $checkbox = false, $readonly = false, $state = "", $rightclick = "", $template = "", $append = "", $editable = false){

    $start = $start ? $start : "0";
    $checkbox = $checkbox ? "1" : "0";
    $readonly = $readonly ? "1" : "0";

    if ($this->type === 'default') {
      ?>
        <script type="text/javascript">
          Ext.QuickTips.init();

          Ext.apply(Ext.QuickTips.getQuickTip(), {
            showDelay: 0,
            trackMouse: true,
            baseCls: 'x-tooltip'
          });

          var reload = system.util.get('list-reload-<?php print $target;?>');
          if(reload){
            reload.onclick = function(){
              system.manager.list('form-<?php print $target;?>','src/<?php print $module;?>/view','<?php print $entity;?><?php print $this->ext;?>','<?php print $tag;?>',true,'<?php print $target;?>','<?php print $level;?>');
            };
          }
        </script>

        <script type="text/javascript">
          var fields = [
            {name: "counter", type: 'int'},
            <?php
              if($checkbox){
                ?>
                  {name: "checkbox", type: 'text'},
                <?
              }
            ?>
            {name: "option", type: 'text'}
            <?php
            foreach($items as $item){
              if($item['grid']){
                $type = "text";
                if ($item['type'] === 'pk') {
                  $type = "int";
                }
                if ($item['type'] === 'money' or $item['type'] === 'double' or $item['type'] === 'number' or $item['type'] === 'percent') {
                  $type = "double";
                }
                if ($item['type'] === 'int') {
                  $type = "int";
                }
                if ($item['type'] === 'date') {
                  $type = ", type: 'date', dateFormat: 'd/m/Y'";
                }
                if ($item['type'] === 'datetime') {
                  $type = ", type: 'date', dateFormat: 'd/m/Y H:i:s'";
                }
                $field = ',{name: "'.$item['id'].'", type: "'.$type.'"}';
                print $field."\n";
              }
            }
            ?>
          ];

          var columns = [
            {header: '&nbsp;', width: 50, sortable: true, dataIndex: 'counter', align: 'left'},
            <?php
              if($checkbox){
                ?>
                  {header: '<input type="checkbox" onclick="system.action.select(this,<?php print "\'".$target."\'";?>);"/>', width: 50, sortable: false, dataIndex: 'checkbox', align: 'center', renderer: system.renderer.renderCheckBox},
                <?
              }
            ?>
            {header: '&nbsp;', width: <?php print $option;?>, sortable: true, dataIndex: 'option', align: 'left', renderer: system.action.viewRender<?php print $entity; ?>, editor: new Ext.form.TextField({readonly: true, style: 'background: #EBF2FB; color: #EBF2FB; border: none;'})}
            <?php
            $pk = '';
            foreach($items as $item){
              if($item['pk']){
                $pk = $item['id'];
              }
              if($item['grid']){
                $utilities = new ScreenUtilities();
                $details = $utilities->getViewListDetail($item);
                $tooltip = $item['description'];
                $column = ",{header: '".$item['description']."', tooltip: '".$tooltip."', sortable: true, dataIndex: '".$item['id']."'".$details."}";

                print System::encode($column."\n", $this->encode);
              }
            }
            ?>
          ];

          var level = '<?php print $level;?>';
          var rotule = '<?php print Encode::decrypt($rotule);?>';
          var url = '<?php print PAGE_APP;?>src/<?php print $module;?>/json/<?php print $entity;?><?php print $this->ext;?>.json.php?action=<?php print $tag;?>-j-list&t=<?php print $target;?>&f=<?php print $filter;?>&l=<?php print $level;?>';
          <?php
            if(!$editable){
              ?>
                var editor = false;
              <?
            }else{
              ?>
                var editor = new Ext.ux.grid.RowEditor({
                  saveText: 'Salvar',
                  cancelText: 'Cancelar',
                  errorText: 'Erros',
                  commitChangesText: 'Salvar ou Cancelar para continuar'
                });
                editor.on('afteredit', function(e){
                  var primary = e.record.get('<?php print $pk;?>');
                  if(primary){
                    var update = 'src/<?php print $module;?>/post/<?php print $entity;?><?php print $this->ext;?>.post.php?action=<?php print $tag;?>-p-save&l=<?php print $level;?>&<?php print $pk;?>='+primary;
                    var changes = e.record.getChanges();
                    Ext.Ajax.request({
                      url: update,
                      params: changes,
                      callback : function(options, success, response){
                        system.notify.show(response.responseText);
                      }
                    });
                  }
                });
              <?
            }
          ?>
          var prefix = '';
          var target = '<?php print $target;?>';
          var id = '<?php print $id;?>';
          var start = <?php print $start;?>;
          var limit = <?php print $end;?>;
          var height = '<?php print $window_height;?>';
          var width = '<?php print $window_width;?>';

          var plugins = [];
          if(editor){
            plugins = [editor];
          }

          var params = {
            w: '<?php print $where;?>',
            g: '<?php print $group;?>',
            o: '<?php print $order;?>',
            rotule: '<?php print $rotule;?>',
            checkbox: '<?php print $checkbox;?>',
            r: '<?php print $readonly;?>'
          };

          system.form.createGrid(rotule, url, prefix, '<?php print $target;?>', '<?php print $id;?>', <?php print $start;?>, <?php print $end;?>, '<?php print $window_height;?>', '<?php print $window_width;?>', fields, columns, '<?php print $entity;?>', '<?php print $state;?>', plugins, params);
          system.form.grids['<?php print $id;?>'].on('rowcontextmenu', function(grid, rowIndex, event){
            event.stopEvent();
            <?php print $rightclick;?>
          });

        </script>
    <?php
    } else if ($this->type === 'card') {

      ?>
        <script type="text/javascript">

          Ext.Ajax.request({
            url: '<?php print PAGE_APP;?>src/<?php print $module;?>/json/<?php print $entity;?><?php print $this->ext;?>.json.php?action=<?php print $tag;?>-j-list&t=<?php print $target;?>&full=true&f=<?php print $filter;?>&l=<?php print $level;?>&w=<?php print $where;?>&g=<?php print $group;?>&o=<?php print $order;?>&rotule=<?php print $rotule;?>&checkbox=<?php print $checkbox;?>&r=<?php print $readonly;?>',
            params: {
              start: <?php print $start;?>,
              limit: <?php print $end;?>
            },
            callback : function(options, success, response){
              system.form.createElementList(eval('('+response.responseText+')'), '<?php print $target;?>', '<?php print $template;?>', '<?php print $append; ?>');
            }
          });

          system.form.createElementList = function(json, target, template, htmlAppend){
            var target = system.util.get(target);
            var firstAppend = document.createElement('div');
            var settings = json;
              settings.total;
              settings.begin;
              settings.end;

              firstAppend.innerHTML = htmlAppend;
              target.appendChild(firstAppend);

              var arr = settings.rows;
              for (var i = 0; i < arr.length; i++) {
                var items = arr[i];
                var e = document.createElement('div');
                e.innerHTML = system.form.aply(items, template);
                target.appendChild(e);
              }
          };

          system.form.aply = function(items, template){
            for (item in items) {
              template = system.util.replace(template, '{'+item+'}', items[item]);
            }
            return template;
          };
        </script>
      <?
    }

  }

  /**
   *
   * @param type $module
   * @param type $rotule
   * @param type $entity
   * @param type $tag
   * @param type $reference
   * @param type $properties
   * @param type $index
   */
  public function printListGridAction($module, $rotule, $entity, $tag, $reference, $properties = null, $index = false){

    if ($this->type === 'default') {

      $actions = array(
          Json::actionView => array("className" => Json::actionView, "title" => "Visualizar", "handler" => "View", "message" => ""),
          Json::actionEdit => array("className" => Json::actionEdit, "title" => "Editar", "handler" => "Edit", "message" => ""),
          Json::actionRemove => array("className" => Json::actionRemove, "title" => "Remover", "handler" => "Remove", "message" => "")
      );

      if ($properties !== null) {
        $v = isset($properties["view"]) ? $properties["view"] : true;
        if (is_array($v)) {
          $actions[Json::actionView] = $properties["view"];
          unset($properties["view"]);
        }
        $e = isset($properties["edit"]) ? $properties["edit"] : true;
        if (is_array($e)) {
          $actions[Json::actionEdit] = $properties["edit"];
          unset($properties["edit"]);
        }
        $r = isset($properties["remove"]) ? $properties["remove"] : true;
        if (is_array($r)) {
          $actions[Json::actionRemove] = $properties["remove"];
          unset($properties["remove"]);
        }
        if (isset($properties['action'])) {
          $actions = array_merge($actions, $properties['action']);
        }
      }

    ?>
      <script type="text/javascript">
        /**
         * 
         * @param val
         * 
         * @return array options
         */
        system.action.viewRender<?php print $entity; ?> = function(val){
          
          var template = '<span class="action [class]" onclick="[onclick]" title="[title]"></span>';
          var properties = {
              <?php
              if (is_array($actions)) {
                $first = true;
                foreach ($actions as $action) {
                  if (!$first) {
                    print ", \n";
                  }
                  if (is_array($action)) {
                    ?>
                      '<?php print $action['className']; ?>' : {
                        <?php
                        $second = true;
                        foreach ($action as $key => $a) {
                          if (!$second) {
                            print ", \n";
                          }
                          ?>
                            '<?php print $key; ?>':'<?php print $a; ?>'
                          <?
                          $second = false;
                        }
                        ?>
                      }
                    <?
                  }
                  $first = false;
                }
              }
            ?>
          };

          var items = val.split('|');
          var options = '';
          for (var i = 0; i <= items.length; i++) {
            if (items[i]) {
              var data = items[i].split(',');
              var item = template;

              var _action_ = data[0];
              var _class_ = '';
              var _title_ = '';
              var _onclick_ = '';

              var action = properties[_action_];
              if (action) {
                _class_ = action.className;
                _title_ = action.title;
                _onclick_ = "system.action.view"+action.handler+"<?php print $entity; ?>('"+data[1]+"', '"+data[2]+"', '"+data[3]+"')";

                  item = item.replace('[class]',_class_);
                  item = item.replace('[title]',_title_);
                  item = item.replace('[onclick]',_onclick_);
                  options = options + item + '&nbsp;';
              } else {
                options = options + 'Not Found: '+ _action_ + '&nbsp;';
              }
            }
          }
          return options;
        };

        system.action.viewView<?php print $entity; ?> = function(value, target, level) {
          <?php
            if ($index === false) {
              ?>
                var params = 'action=<?php print $tag;?>-v-view&<?php print $reference;?>='+system.encrypt.decode(value);
              <?php
            } else {
              ?>
                var params = 'action=<?php print $tag;?>-v-view&i='+value;
              <?php
            }
          ?>
          system.manager.browse('form-'+target,'src/<?php print $module;?>/view', '<?php print $entity;?><?php print $this->ext;?>', params, target, level);
        };

        system.action.viewEdit<?php print $entity; ?> = function(value, target, level) {
          <?php
            if ($index === false) {
              ?>
                var params = '<?php print $reference; ?>='+system.encrypt.decode(value);
              <?php
            } else {
              ?>
                var params = 'i='+value;
              <?php
            }
          ?>
          system.manager.edit('form-'+target,'src/<?php print $module; ?>/view', '<?php print $entity; ?><?php print $this->ext;?>', '<?php print $tag; ?>', params, target, level);
        };

        system.action.viewRemove<?php print $entity; ?> = function(value, target, level) {
          <?php
            if ($index === false) {
              ?>
                var params = '<?php print $reference; ?>='+system.encrypt.decode(value);
              <?php
            } else {
              ?>
                var params = 'i='+value;
              <?php
            }
          ?>
          system.manager.remove('<?php print Encode::decrypt($rotule); ?>', 'form-'+target,'src/<?php print $module; ?>/post', '<?php print $entity; ?><?php print $this->ext;?>', '<?php print $tag; ?>', params, target, level, null, '<?php print $actions[Json::actionRemove]['message'];?>');
        };
      </script>
    <?php

    }else if($this->type === 'card'){

    }

  }

  /**
   *
   * @param type $target
   * @param type $rotule
   * @param type $hidetoolbar
   * @param type $child
   */
  public function printListBottom($target, $rotule, $hidetoolbar, $child){

    if ($this->type === 'default') {
      ?>
        </table>
      <?
    } else if ($this->type === 'card') {
      
    }
    
  }
  
  /**
   * 
   * @param type $module
   * @param type $entity
   * @param type $tag
   * @param type $reference
   * @param type $action
   * @param type $extra
   */
  public function printListAction($module, $entity, $tag, $reference, $action, $extra = ""){
    
    $extra = $extra ? $extra : "";
    
    ?>
      <script type="text/javascript">
        /**
         * 
         * @param value
         * @param target
         * @param level
         */
        system.action.view<?php print $action['handler'];?><?php print $entity;?> = function(value, target, level){

          value = system.encrypt.decode(value);

          var process = function() {
            
            var execute = function() {
              <?php
                if ($action['feedback']) {
                  ?>
                    var feedback = system.util.get('feedback-'+target);
                    if(feedback){
                      system.notify.show(feedback.innerHTML);
                    }
                  <?php
                }
              ?>
              <?php
                if ($action['reload']) {
                  ?>
                    system.form.reloadGrid(target);
                  <?php
                }
              ?>
            };

            var verify = false;

            <?php            
             $this->printListActionOperation($module, $entity, $tag, $reference, $action, $extra);
            ?>
          };
          <?php
            if ($action['confirm']['active']) {
              ?>
                system.confirm(process, '<?php print $action['confirm']['message'];?>', '<?php print $action['confirm']['title'];?>');
              <?php
            } else {
              ?>
                process.call();
              <?php
            }
          ?>
        };
      </script>
    <?php
  }
  
  /**
   * 
   * @param type $module
   * @param type $entity
   * @param type $tag
   * @param type $reference
   * @param type $operation
   * @param type $extra
   */
  public function printListActionOperation($module, $entity, $tag, $reference, $operation, $extra) {
    ?>
      try {
        <?php
          if ($operation['operation']['command'] === 'save') {
            ?>
                system.manager.save('form', 'src/<?php print $module; ?>/post', '<?php print $entity; ?><?php print $this->ext;?>', '<?php print $tag; ?>', 'feedback-'+target, level, null, execute, verify, false, '<?php print $reference; ?>='+value+'&<?php print $operation['operation']['update'];?>');
            <?php
          } else if ($operation['operation']['command'] === 'popup') {
            $width = isset($operation['operation']['width']) ? $operation['operation']['width'] : "null";
            $height = isset($operation['operation']['height']) ? $operation['operation']['height'] : "null";
            $top = isset($operation['operation']['top']) ? $operation['operation']['top'] : "null";
            $left = isset($operation['operation']['left']) ? $operation['operation']['left'] : "null";
            ?>
                system.popup('<?php print $operation['operation']['url'];?>', '<?php print $operation['operation']['title'];?>', <?php print $width;?>, <?php print $height;?>, <?php print $top;?>, <?php print $left;?>);
            <?php
          } else if ($operation['operation']['command'] === 'browse') {
            
            $module = isset($operation['operation']['module']) ? $operation['operation']['module'] : 'src/' . $module . '/post';
            $entity = isset($operation['operation']['entity']) ? $operation['operation']['entity'] : $entity.$this->ext;
            $tag = isset($operation['operation']['tag']) ? $operation['operation']['tag'] : $tag;
            $layer = isset($operation['operation']['layer']) ? $operation['operation']['layer'] : '';
            $action = isset($operation['operation']['action']) ? $operation['operation']['action'] : '';
            $param = isset($operation['operation']['param']) ? $operation['operation']['param'] : '';
            ?>
              system.manager.browse('form-' + target, '<?php print $module; ?>', '<?php print $entity; ?>', 'action=<?php print $tag;?>-<?php print $action;?>&<?php print $reference; ?>='+value+'&<?php print $param;?><?php print $extra;?>'+'&t='+target+'&l='+level, target, level, '<?php print $layer;?>', execute);
            <?php
          } else if ($operation['operation']['command'] === 'process') {
            
            if (!String::position($module, 'src') === false) {
              $module = 'src/' . $module . '/post';
            }
            $layer = isset($operation['operation']['layer']) ? $operation['operation']['layer'] : '';
            $module = isset($operation['operation']['module']) ? $operation['operation']['module'] : $module;
            ?>
              system.manager.browse('form', '<?php print $module; ?>', '<?php print $entity; ?><?php print $this->ext;?>', 'action=<?php print $tag; ?>-<?php print $operation['operation']['action'];?>&<?php print $reference; ?>='+value+'&<?php print $operation['operation']['param'];?>', 'feedback-'+target, level, 'post', execute);
            <?php
          }
        ?>
      } catch(e) {
        system.error.add(e);
      }
    <?php
  }

}