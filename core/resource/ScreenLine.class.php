<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Line 
 *
 * @author William
 */
class ScreenLine {

  public $ext;
  public $encode;
  public $types;

  /**
   * 
   * @param type $ext
   * @param type $encode
   * @param type $types
   */
  public function ScreenLine($ext = "", $encode = "", $types = "") {
    $this->ext = $ext;
    $this->encode = $encode;
    $this->types = $types;
  }

  /**
   * 
   * @param type $target
   * @param type $line
   * @param type $items
   */
  public function printLine($target, $line, $items) {

    $total = 12;
    $columns = count($items);

    if ($columns) {
      
      $id = $target . "-line-" . $line;
      $size = $total / $columns;

      ?>
        <div id="<?php print $id; ?>" class="row">
          <?php
            foreach ($items as $item) {
              ?>
                <div class="col-lg-<?php print $size; ?>">
                  <div class="form-group">
                    <?php
                      $this->printField($item, $id);
                    ?>
                  </div>
                </div>
              <?php
            }
          ?>
        </div>
      <?php
    }

  }
  
  /**
   * 
   * @param type $item
   * @param type $line
   */
  private function printField($item, $line) {

    $type = $item['type'];

    $name = $item['id'];
    $description = System::encode($item['description'], $this->encode);
    $value = System::encode($item['value'], $this->encode);
    $reference = isset($item['reference']) ? System::encode($item['reference'], $this->encode) : "";
    $content = $item['type_content'];
    $target = $item['target'];
    
    $style = isset($item['style']) ? $item['style'] : "";

    $action = $item['action'];
    $width = isset($item['form_width']) ? $item['form_width'] : "";

    $readonly = isset($item['readonly']) ? $item['readonly'] : "";

    $class = $readonly ? "readonly" : "available";
    $disabled = $readonly ? "disabled" : "";

    if (isset($this->types[$type])) {
      
      $behavior = $this->types[$type];

      if ($behavior->htmlentities) {
        $value = htmlentities($value);
      }

      switch ($behavior->component) {

        case 'input[text]':
          ?>
            <label class="control-label"><?php print $description; ?></label>
            <input type="text" name="<?php print $name; ?>" id="<?php print $name; ?>" value="<?php print $value; ?>" class="form-control <?php print $class; ?>" data-action="<?php print $action; ?>" style="<?php print $style; ?>" <?php print $disabled; ?> autocomplete="off" placeholder=""/>
          <?
          break;

        case 'input[number]':
          ?>
            <label class="control-label"><?php print $description; ?></label>
            <input type="number" name="<?php print $name; ?>" id="<?php print $name; ?>" value="<?php print $value; ?>" class="form-control <?php print $class; ?>" data-action="<?php print $action; ?>" style="<?php print $style; ?>" <?php print $disabled; ?> autocomplete="off" placeholder=""/>
          <?
          break;

        case 'input[checkbox]':
          $checked = $value ? 'checked="checked"' : '';
          ?>
            <div class="checkbox">
              <label>
                <input type="checkbox" name="<?php print $name; ?>" id="<?php print $name; ?>" checked="" value="<?php print $value; ?>" data-action="<?php print $action; ?>" style="<?php print $style; ?>" <?php print $disabled; ?> <?php print $checked; ?>> <?php print $description; ?>
              </label>
            </div>
          <?php
          break;

        case 'input[radio]':
           $content = $content ? $content : '0,N�o|1,Sim';
           $radios = $this->parseFieldContent($content, $name, $value, 'option');
          ?>
            <label class="control-label"><?php print $description; ?></label>
            <div class="inline-group">
              <?php
                foreach ($radios as $radio) {
                  ?>
                    <label class="radio radio-inline">
                      <input type="radio" name="<?php print $radio->name; ?>" id="<?php print $radio->id; ?>" value="<?php print $radio->value; ?>" data-action="<?php print $action; ?>" style="<?php print $style; ?>" <?php print $disabled; ?> <?php print $radio->checked; ?>> <?php print $radio->label; ?>
                    </label>
                  <?php
                }
              ?>
            </div>
          <?php
          break;

        case 'input[file]':

          break;

        case 'datetimepicker':
          //data-mask="(999) 999-9999"
          ?>
            <label class="control-label"><?php print $description; ?></label>
            <input type="date" name="<?php print $name; ?>" id="<?php print $name; ?>" value="<?php print $value; ?>" class="form-control <?php print $class; ?>" data-action="<?php print $action; ?>" data-mask="" style="<?php print $style; ?>" <?php print $disabled; ?> autocomplete="off" placeholder=""/>
          <?php
          break;

        case 'select':

          $recovery = ($value) ? $value . ":" . $reference : "";

          $level = '';

          $f_module = '';
          $f_entity = '';
          $f_tag = '';
          $f_key = '';
          $f_description = '';
          if (isset($item['foreign'])) {
            $f_module = $item['foreign']['modulo'];
            $f_entity = $item['foreign']['entity'];
            $f_tag = $item['foreign']['tag'];
            $f_key = $item['foreign']['key'];
            $f_description = $item['foreign']['description'];
          }

          $url = PAGE_APP . 'src/' . $f_module . '/json/' . $f_entity . '.json.php?action=' . $f_tag . '-j-list&t=' . $target . '&f=&l=' . $level;

          ?>
            <label class="control-label"><?php print $description; ?></label>
            <input type="hidden" name="<?php print $name; ?>" id="<?php print $name; ?>" value="<?php print $recovery; ?>" class="form-control <?php print $class; ?>" data-action="<?php print $action; ?>" style="<?php print $style; ?>" <?php print $disabled; ?> autocomplete="off" placeholder=""/>
            <script>
              Application.form.createRemoteComboBox('<?php print $target; ?>', '<?php print $line; ?>', '<?php print $name; ?>', '<?php print $url; ?>', '<?php print $f_key;?>', '<?php print $f_description;?>');
            </script>
          <?php
          break;

        case 'textarea':
          ?>
            <label class="control-label"><?php print $description; ?></label>
            <textarea name="<?php print $name; ?>" id="<?php print $name; ?>" class="form-control <?php print $class; ?>" data-action="<?php print $action; ?>" style="<?php print $style; ?>" <?php print $disabled; ?> autocomplete="off" placeholder="" rows="5"><?php print $value; ?></textarea>
          <?php
          break;

        case 'html':
          ?>
            <div id="<?php print $name; ?>-<?php print $target; ?>"><?php print $value; ?></div>
          <?php
          break;

        default:
          ?>
            <label><?php print $description; ?></label>
            <input type="text" name="<?php print $name; ?>" id="<?php print $name; ?>" value="<?php print $value; ?>" class="form-control <?php print $class; ?>" onchange="<?php print $action; ?>" style="background: red;" <?php print $disabled; ?> autocomplete="off" placeholder=""/>
          <?php
          break;
      }

    } else {

      //System::showMessage("Comportamento '" . $type . "' ainda n&atilde;o possui setup para ele");

    }
  
  }

  /**
   * 
   * @param type $string
   * @param type $name
   * @param type $atual
   * @return \Object
   */
  public function parseFieldContent($string, $name, $atual, $type) {

    $radios = array();
    $contents = explode("|", $string);
    $position = 0;

    foreach ($contents as $content) {
      $info = explode(",", $content);
      $value = $info[0];
      $label = isset($info[1]) ? $info[1] : $value;
      $checked = '';
      $selected = '';
      if ($type === 'option') {
        $checked = ($atual === $value) ? 'checked="checked"' : '';
      }
      if ($type === 'select') {
        $selected = ($atual === $value) ? 'selected' : '';
      }
      $radios[] = new Object(array("id"=>$name . "-" . $position, "name"=>$name, "value"=>$value, "label"=>System::encode($label, $this->encode), "checked"=>$checked, "selected"=>$selected));
      $position++;
    }
    return $radios;
  }
  
  /**
   *
   * @param type $order
   * @param type $position
   * @param type $title
   * @param type $param
   * @param type $items
   * @param type $lines
   * @param type $width_label
   * @param type $width_value
   * @param type $window_width
   * @param type $style
   */
  private function printSubItem($order, $position, $title, $param, $items, $lines, $width_label, $width_value, $window_width, $style = "") {
    $screen = new Screen();
    ?>
      <div id="sub-item-<?php print $order; ?>" style="<?php print $style; ?>">
        <div id="sub-item-tbar-<?php print $order; ?>">
          <label class="sub-item-title"><?php print $title; ?></label>
          <label class="sub-item-order"><?php print $position; ?></label>
        </div>
        <?php
          $screen->manager->printManagerLinesHeader('form', '', '', '', '', false, false);
          $screen->manager->printManagerLines($param, $items, $lines, $width_label, $width_value, $window_width, true);
          $screen->manager->printManagerLinesBottom('', '', '', $width_value);
        ?>
        <div id="sub-item-bbar-<?php print $order; ?>">
          <span class="sub-item-buttom"><input type="button" value="Remover" class="form-buttom"/></span>
          <span class="sub-item-buttom"><input type="button" value="Adicionar" class="form-buttom"/></span>
        </div>
        <br>
      </div>
    <?
  }

  /**
   *
   * @param type $children
   * @param type $window_width
   * @param type $target
   * @param type $width_label
   * @param type $width_value
   * @param type $param
   * @param type $filter
   * @param type $forever
   * @param type $class
   */
  public function printChildren($children, $target, $filter = '', $line = false, $forever = false, $class = "") {

    if (is_array($children) && count($children)) {
      
      $forever = !$forever ? "false" : "true";

      $extension = $this->ext;

      $one = true;
      $first = "";
      if (count($children) > 0) {
        if ($line) {
          ?>
          <tr>
            <td class="form-value b-left b-right reset" align="left" valign="middle" colspan="2" nowrap>
          <?
        } else {
          $default = 'form-table form-value reset ';
          $class = $class ? $default . $class : $default;
        }

        $content = 'children-' . $target;
        ?>
            <div style="height: auto;" class="<?php print $class; ?>">

              <div id="<?php print $content; ?>" class="ui-tab-content b-top b-left b-right b-bottom">
                <ul>
        <?php
        foreach ($children as $x => $item) {
          $child = $item['child'];

          $target_child = $child['target'] ? $child['target'] : $content . '-' . $x;
          if ($children[$x]) {
            $children[$x]['target'] = $target_child;
          }
          $params = isset($child['params']) ? $child['params'] : "";

          $_filter = $filter;
          if ($child['filter'] === null) {
            $_filter = "";
          } else if ($child['filter']) {
            $_filter = $item['child']['filter'];
          } else {
            if (!$filter) {
              $_filter = "'+system.encrypt.encode(system.util.getValue('" . $child['key'] . "','" . $target . "'))+'";
            }
          }

          //atribui o filter a um parametro especifico
          $key = $child['key'] ? $child['key'] . "=" . $child['filter'] : "";
          $action = "system.manager.browse('" . $child['form'] . "', 'src/" . $child['module'] . "/view', '" . $child['entity'] . $extension . "', 'child=list&action=" . $child['tag'] . $child['action'] . "&" . $key . "&f=" . $_filter . "&t=" . $target_child . "&rotule=" . $child['rotule'] . "&width=" . $child['width'] . "&height=" . $child['height'] . "&" . $params . "', '" . $target_child . "', '" . $child['level'] . "');";

          if ($one) {
            $first = $action;
          }
          $one = false;
          ?>
                    <li>
                      <a href="#<?php print $target_child; ?>" onclick="system.action.selectChildren('<?php print $target_child; ?>', function() {<?php print $action; ?>
            },<?php print $forever; ?>);">
                    <?php print System::encode($item['description'], $this->encode); ?>
                      </a>
                    </li>
                    <?
                  }
                  ?>
                </ul>
                  <?php
                  foreach ($children as $x => $item) {
                    $child = $item['child'];

                    $target_child = $child['target'] ? $child['target'] : $content . '-' . $x;
                    ?>
                  <div id="<?php print $target_child; ?>_bkp" style="display: none;"></div>
                  <div id="<?php print $target_child; ?>"></div>
          <?
        }
        ?>
              </div>

            </div>
            <script type="text/javascript">
                <?php print $first; ?>
            </script>
            <script type="text/javascript">
              $(function() {
                $("#children-<?php print $target; ?>").tabs();
              });
            </script>
                <?php
                if ($line) {
                  ?>
            </td>
          </tr>
          <?
        }

      }

    }

  }

  /**
   *
   * @param type $popups
   */
  public function printPopups($popups) {

    if (is_array($popups) && count($popups)) {

      $item = array_shift($popups);
      $subitems = null;
      while ($subitem = array_shift($popups)) {
        $subitems[] = $subitem;
      }

      $this->printLine($item, '', '', $subitems, true);
    }

  }

  /**
   *
   * @param <type> $onchange
   * @param <type> $limit
   * @return <type>
   */
  public function selectLimit($prefix, $onchange, $limit, $target) {

    $onchange = base64_decode($onchange);

    $array = array(5, 6, 7, 8, 9, 10, 25, 50, 100);
    $data = "";
    $id = $prefix . "_" . date('Ymdhis');

    for ($a = 1; $a < count($array); $a++) {
      $data .= "['" . $array[$a] . "']";
      if (($a + 1) < count($array)) {
        $data .= ", ";
      }
    }
    ?>
    <div id="<?php print $id . $target; ?>"></div>
    <script type="text/javascript">
      var combo = new Ext.form.ComboBox({
        name: 'perpage_<?php print $target; ?>',
        width: 60,
        store: new Ext.data.ArrayStore({
          fields: ['value'],
          data: [<?php print $data; ?>]
        }),
        mode: 'local',
        value: '<?php print $limit; ?>',
        listWidth: 75,
        triggerAction: 'all',
        displayField: 'value',
        valueField: 'value',
        editable: false,
        forceSelection: true
      });
      combo.on('select', function(combo, record) {
        system.cookie.set('limit', (parseInt(record.get('value'))), 15);
    <?php print $onchange; ?>
      }, this);
      combo.render('<?php print $id . $target; ?>');
    </script>
    <?
  }

  /**
   *
   * @param <type> $onchange
   * @param <type> $selected
   * @param <type> $total_pages
   * @param <type> $target
   */
  public function selectPage($prefix, $onchange, $selected, $total_pages, $target) {

    $onchange = base64_decode($onchange);
    //print "<textarea>".$onchange."</textarea>";
    $data = "";
    $id = $prefix . "_" . date('Ymdhis');

    if ($total_pages > 0) {
      for ($a = 1; $a < ($total_pages + 1); $a++) {
        $data .= "['" . $a . "','" . $a . " / " . $total_pages . "'] ";
        if (($a + 1) < ($total_pages + 1)) {
          $data .= ", ";
        }
      }
    } else {
      $data = "['1','1 / 1'] ";
    }
    ?>
    <div id="<?php print $id . $target; ?>"></div>
    <script type="text/javascript">
      var combo = new Ext.form.ComboBox({
        name: 'perpage_<?php print $target; ?>',
        width: 75,
        store: new Ext.data.ArrayStore({
          fields: ['value', 'show'],
          data: [<?php print $data; ?>]
        }),
        mode: 'local',
        value: '<?php print $selected; ?>',
        listWidth: 75,
        triggerAction: 'all',
        displayField: 'show',
        valueField: 'value',
        editable: false,
        forceSelection: true
      });

      combo.on('select', function(combo, record) {
    <?php print $onchange; ?>
      }, this);

      combo.render('<?php print $id . $target; ?>');
    </script>
    <?
  }

}