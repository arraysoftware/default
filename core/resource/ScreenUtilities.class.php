<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Interface
 *
 * @author William
 */
class ScreenUtilities {

  //put your code here

  private $ext;

  /**
   *
   * @param type $ext
   */
  public function ScreenUtilities($ext = "") {
    $this->ext = $ext ? $ext : DEFAULT_EXTENSION;
  }

  /**
   *
   * @param type $items
   * @param int $line
   */
  public function orderItems($items, $lines) {
    $order = 0;
    $ordened = array();
    for ($line = 1; $line <= $lines; $line++) {
      foreach ($items as $item) {
        if (isset($item['line']) && $item['line'] === $line) {
          $ordened[$order] = $item;
          $order++;
        }
      }
    }

    return $ordened;
  }

  /**
   * 
   * @param Array $items
   * @return Array
   */
  public function receiveItems($items) {
    
    foreach ($items as $id => $item) {
      
      if ($item['type'] === 'select-multi') {

        $entity = $item['select-multi']['entity'];
        $value = System::request('source-'.$entity);

      } else if ($item['type_behavior'] === 'parent') {

        if (isset($item['parent'])) {

          $class = $item['parent']['entity'];
          System::import('m', $item['parent']['modulo'], $class, 'src', true);
          $obj = new $class();
          $set_value = "set_".$item['parent']['prefix']."_value";
          $get_items = "get_".$item['parent']['prefix']."_items";

          $elements = $obj->$get_items();
          foreach ($elements as $element) {
            $k = $element['id'];
            $v = System::request($k, null);
            $obj->$set_value($k, $v);
          }
          $value = $obj;

        }

      } else if ($item['type'] === 'sub-item') {

        $settings = new Object('{' . $item['type_content'] . '}');

        $module = $settings->module;
        $entity = $settings->entity;
        $prefix = $settings->prefix;
        $minimum = $settings->minimum;

        System::import('c', $module, $entity, 'src');
        System::import('m', $module, $entity, 'src');

        $class = $entity;
        $get_items = "get_" . $prefix . "_items";
        $set_value = "set_" . $prefix . "_value";

        $objects = array();
        $object = new $class();
        $elements = $object->$get_items();
        
        $total = System::request($id, 0);
        for ($i = 0; $i < $total; $i++) {
          $object = new $class();
          $err = 0;
          foreach ($elements as $index => $element) {
            $value = System::request($index, null);
            if (!is_null($value)) {
              $object->$set_value($index, $value);
            } else {
              $err++;
            }
          }
          if ($minimum) {
            $objects[] = $object;
          }
        }

      } else {
        
        $value = System::request($id, null);

      }

      $items[$id]['value'] = $value;
    }

    return $items;
  }
  
  /**
   * 
   * @param Array $items
   * @param int $value
   * 
   * @return Array
   */
  public function processItems($items, $value) {

    $try = 0;
    $added = 0;

    foreach ($items as $item) {

      if ($item['type'] === 'select-multi') {

        if (isset($item['select-multi'])) {

          $select = $item['select-multi'];

          if ($select) {

            $module = $select['module'];
            $entity = $select['entity'];
            $prefix = $select['prefix'];

            System::import('c', $module, $entity, 'src');
            System::import('m', $module, $entity, 'src');

            $remove = "execute" . $entity . "Ctrl";
            $set_value = "set_" . $prefix . "_value";
            $add = "add" . $entity . "Ctrl";

            $entityCtrl = $entity . "Ctrl";
            $objectCtrl = new $entityCtrl(PATH_APP);

            $objectCtrl->$remove($select['foreign']." = '" . $value . "'","");

            $values = $item['value'];
            foreach ($values as $v) {
              if ($v) {
                $o = new $entity();
                $o->$set_value($select['foreign'], $value);
                $o->$set_value($select['source'], $v);

                $w = $objectCtrl->$add($o);
                $try++;
                if ($w) {
                  $added++;
                }
              }
            }

          }
        }
      }

    }

    return $items;
  }

  /**
   *
   * @param type $param
   * @param type $module
   * @param type $entity
   * @param type $tag
   * @param type $rotule
   * @param type $level
   * @param type $_width
   * @param type $_height
   * @param type $filter
   * @param type $key
   * @param type $params
   * @param type $form
   * @param type $target
   *
   * @return type
   */
  public function generateChild($param, $module, $entity, $tag, $rotule, $level, $_width, $_height, $filter, $key, $params, $form = "", $target = "") {

    $form = $form ? $form : "form";

    $item = array();
    $item['description'] = $rotule;

    $item['child'] = array();

    $item['child']['action'] = '-v-' . $param;
    $item['child']['module'] = $module;
    $item['child']['entity'] = $entity;
    $item['child']['tag'] = $tag;
    $item['child']['rotule'] = Encode::encrypt($rotule);
    $item['child']['level'] = $level;
    $item['child']['width'] = $_width;
    $item['child']['height'] = $_height;
    $item['child']['filter'] = $filter;
    $item['child']['key'] = $key;
    $item['child']['params'] = $params;
    $item['child']['form'] = $form;
    $item['child']['target'] = $target;

    return $item;
  }

  /**
   *
   * @param type $width
   * @param type $modal
   *
   * @return type
   */
  public function getChildWidth($width, $modal) {
    $width = $width - 20;

    if ($modal) {
      $width = $width - 20;
    }

    return $width;
  }

  /**
   *
   * @param type $height
   * @param type $layout
   * @param type $modal
   * @param type $lines
   *
   * @return int
   */
  public function getChildHeight($height, $layout, $modal, $lines) {

    if ($layout === 'manager' or $layout === '') {
      $coeficient = $lines;
    } else {
      $coeficient = -0.5;
    }

    $height = $height - (($coeficient * 28) + 60);

    if ($modal) {
      $height = $height - 230;
    }

    return $height;
  }

  /**
   * 
   * @param type $items
   * @param type $filter
   * @param type $layout
   * @param type $lines
   * @param type $modal
   * @param type $target
   * @param type $level
   * @param type $width
   * @param type $height
   * @return type
   */
  public function getChildren($items, $filter, $layout, $lines, $modal, $target, $level, $width, $height) {

    $children = array();

    foreach ($items as $item) {

      if ($item['type_behavior'] == 'child') {

        $_width = $this->getChildWidth($width, $modal);
        $_height = $this->getChildHeight($height, $layout, $modal, $lines);

        $_child = $item['child'];
        $action = "list"; /* se precisar que a funcao em children inicie em outro modo custmoize aqui */
        $_child['target'] = 'children' . '-' . $target . '-' . (count($children) + 1);
        $_child['filter'] = Encode::encrypt($filter); /* se precisar usar outro filtro use o params para passar */
        $params = "t=" . $_child['target'];

        $child = $this->generateChild($action, $_child['module'], $_child['entity'], $_child['tag'], $_child['name'], $level, $_width, $_height, $_child['filter'], $_child['key'], $params, "", $_child['target']);
        $children[] = $child;

      }

    }

    return $children;
  }
  
  /**
   *
   * @param type $items
   * @param type $module
   * @param type $entity
   * @param type $tag
   * @param type $target
   * @param type $level
   * @param type $width
   * @param type $height
   * @return type
   */
  public function getPopups($items, $module, $entity, $tag, $target, $level, $width, $height) {
    $popups = array();
    
    foreach ($items as $item) {

      if ($item['type_behavior'] == 'popup') {

        $_height = $height - 40;
        $_width = $width - 40;

        $_name = $item['popup']['name'];
        $_target = 'popup_' . str_replace('-', '_', $item['popup']['target']);

        $item['line'] = 0;
        $item['type'] = 'popup';
        $item['popup']['height'] = $_height;
        $item['popup']['width'] = $_width;
        $item['popup']['rotule'] = $_name;
        $item['popup']['level'] = $level;
        $item['popup']['params'] = 'width=' . $_width . '&height=' . $_height . '&rotule=' . Encode::encrypt($_name) . '&t=' . $_target . "&f='+system.encrypt.encode(system.util.getValue('" . $item['popup']['key'] . "'))+'";

        $item['popup']['parent']['target'] = $target;
        $item['popup']['parent']['module'] = $module;
        $item['popup']['parent']['entity'] = $entity;
        $item['popup']['parent']['tag'] = $tag;

        $popups[] = $item;

      }

    }

    return $popups;
  }
  
  /**
   * 
   * @param type $items
   * @return type
   */
  public function getHidden($items) {
    $hidden = array();

    foreach ($items as $item) {
      if ($item['hidden']) {
        $hidden[] = array("id" => $item['id'], "name" => $item['id'], "value" => $item['value']);
      }
    }

    return $hidden;
  }
  
  /**
   * 
   * @param type $items
   * @return type
   */
  public function getPrimary($items) {
    $primary = array();

    foreach ($items as $item) {
      if ($item['pk']) {
        $primary[] = $item;
      }
    }

    return $primary;
  }
  
  /**
   * 
   * @param type $items
   * @return type
   */
  public function getForeign($items) {
    $foreign = array();

    foreach ($items as $item) {
      if ($item['fk']) {
        $foreign[] = $item;
      }
    }

    return $foreign;
  }
  
  /**
   * 
   * @param type $items
   * @return type
   */
  public function getValidate($items) {
    $validate = array();

    foreach ($items as $item) {
      if ($item['validate']) {
        $validate[] = $item;
      }
    }

    return $validate;
  }

  /**
   *
   * @param array $item
   * @return type
   */
  public function configureViewManager($item) {
    //0,Nunca|1,Sempre|2,Novo|3,Editar|4,Visualizar

    if ((in_array($item['form'], array("0", "2", "3"))) || ($item['type_behavior'] === 'parent')) {
      $item['form'] = 0;
      $item['line'] = 0;
    } else {
      $item['form'] = 1;
      $item['readonly'] = 1;
      $item['validate'] = "";
    }
    
    return $item;
  }

  /**
   *
   * @param array $item
   * @return type
   */
  public function configureEditManager($item) {
    //0,Nunca|1,Sempre|2,Novo|3,Editar|4,Visualizar

    if ((in_array($item['form'], array("0", "2", "4"))) || ($item['type_behavior'] === 'parent')) {
      $item['form'] = 0;
      $item['line'] = 0;
    } else {
      $item['form'] = 1;
    }

    return $item;
  }

  /**
   *
   * @param array $item
   * @return type
   */
  public function configureAddManager($item) {
    //0,Nunca|1,Sempre|2,Novo|3,Editar|4,Visualizar

    $item['value'] = $item['default_view'];
    if ((in_array($item['form'], array("0", "3", "4"))) || ($item['type_behavior'] === 'parent')) {
      $item['form'] = 0;
      $item['line'] = 0;
    } else {
      $item['form'] = 1;
    }

    return $item;
  }

  /**
   *
   * @param array $item
   * @return type
   */
  public function configureSearchManager($item) {
    //3,Nunca|1,Sempre|0,Avan�ada|2,R�pida

    $item['form'] = 0;

    if (in_array($item['fast'], array("0", "1"))) {

      $item['form'] = 1;
      $item['readonly'] = 0;

      $item['validate'] = '';
      $item['default_view'] = '';

      if ($item['pk']) {
        $item['pk'] = 0;
        $item['type'] = "int";
      }
      if ($item['fk']) {
        $item['fk'] = 0;
        $item['type'] = "string";
      }

      if ($item['type'] === 'date' or $item['type'] === 'dateshort' or $item['type'] === 'datetime') {
        $item['type'] = 'between';
      }

      if ($item['type'] === 'rich-text' or $item['type'] === 'source-code') {
        $item['type'] = 'string';
      }

      if ($item['type'] === 'boolean') {
        $item['type'] = 'yes/no';
      }

      if ($item['type_behavior'] === 'parent' or $item['type'] === 'image' or $item['type'] === 'hash' or $item['type'] === 'calculated') {
        $item['form'] = 0;
        $item['line'] = 0;
      }

    } else if (isset($item['search'])) {

      if (isset($item['search'])) {
        if (!$item['search']) {
          $item['form'] = 0;
          $item['line'] = 0;
        } else {
          $item['form'] = 1;
        }
      }

    } else {

      $item['form'] = 0;
      $item['line'] = 0;

    }

    return $item;
  }

  /**
   *
   * @param type $item
   * @param type $param
   * @return boolean
   */
  public function getViewManageVisibility($item, $param) {

    //chave primaria
    $p = !($item['type'] === 'pk') || (($item['type'] === 'pk') && ($param === "edit" || $param === "search"));
    //visibilidade do campo no contexto
    $v = $item['form'] === 1;
    //se e um item child
    $c = $item['type_behavior'] != 'child';
    //tipo invisivel
    $t = $param === "search";

    $show = (($p) && ($v) && ($c)) || ($t && $c);

    return $show;
  }

  /**
   *
   * @param type $item
   * @return string
   */
  public function getViewListDetail($item) {

    $align = isset($item['grid_align']) ? $item['grid_align'] : "'left'";
    $width = ((int) ($item['grid_width'])) >= 50 ? $item['grid_width'] : 50;
    $renderer = '';
    $editor = '';

    if ($item['type'] === 'pk') {
      $align = isset($item['grid_align']) ? $item['grid_align'] : "'center'";
      $width = ((int) ($item['grid_width'])) >= 80 ? $item['grid_width'] : 80;
    }

    if ($item['type'] === 'int' or $item['type'] === 'number' or $item['type'] === 'money' or $item['type'] === 'double' or $item['type'] === 'percent') {
      $align = isset($item['grid_align']) ? $item['grid_align'] : "'right'";
      $width = ((int) ($item['grid_width'])) >= 80 ? $item['grid_width'] : 80;
      if ($item['type'] === 'money') {
        $renderer = "system.renderer.renderMoney";
        if (!$item['readonly']) {
          $editor = "{xtype: 'masktextfield', mask: '#9.999.990,00', money: true}";
        }
      } else if ($item['type'] === 'percent') {
        $renderer = "system.renderer.renderPercent";
        if (!$item['readonly']) {
          $editor = "{xtype: 'masktextfield', mask: '% #0.0', money: false}";
        }
      } else {
        if (!$item['readonly']) {
          $editor = "{xtype: 'numberfield', allowBlank: true}";
        }
      }
    }

    if ($item['type'] === 'boolean' or $item['type'] === 'yes/no') {
      $align = isset($item['grid_align']) ? $item['grid_align'] : "'center'";
      $renderer = "system.renderer.renderBoolean";
      if ($item['readonly']) {
        $editor = "{xtype: 'textfield', readonly: true, style: 'background: #EBF2FB; color: #EBF2FB; border: none;'}";
      } else {
        $editor = "{xtype: 'checkbox'}";
      }
    }

    if ($item['type'] === 'string' or $item['type'] === 'upper') {
      $width = ((int) ($item['grid_width'])) >= 250 ? $item['grid_width'] : 250;
      if (!$item['readonly']) {
        $editor = "{xtype: 'textfield', allowBlank: true}";
      }
    }

    if ($item['type'] === 'list' or $item['type'] === 'option') {
      $width = ((int) ($item['grid_width'])) >= 100 ? $item['grid_width'] : 100;
      if (!$item['readonly']) {

        $t = $item['type_content'];
        $type_content = explode('|', $t);

        $store_items = "";
        $options_items = "";
        for ($index = 0; $index < count($type_content); $index++) {
          $_content = $type_content[$index];
          $_id = $_content;
          $_value = $_content;
          if (!(strpos($_content, ",") === false)) {
            $_detail = explode(",", $_content);
            $_id = $_detail[0];
            $_value = $_detail[1];
          }
          $store_items .= ",['" . $_id . "', '" . $_value . "']";
          $options_items .= ",'" . $_id . "': '" . $_value . "'";
        }

        $store = substr($store_items, 1);
        $options = substr($options_items, 1);

        $renderer = "system.renderer.renderComboBox({" . $options . "})";
        $editor = "{xtype: 'combo', store:[" . $store . "], mode: 'local', triggerAction: 'all', value: '" . $item['value'] . "', allowBlank: true}";
      }
    }

    if ($item['type'] === 'date') {
      $width = ((int) ($item['grid_width'])) >= 80 ? $item['grid_width'] : 80;
      //$renderer = "Ext.util.Format.dateRenderer('d/m/Y')";
      if (!$item['readonly']) {
        $editor = "{xtype: 'maskdatefield'}";
      }
    }

    if ($item['type'] === 'datetime') {
      $width = ((int) ($item['grid_width'])) >= 110 ? $item['grid_width'] : 110;
      //$renderer = "Ext.util.Format.dateRenderer('d/m/Y H:i:s')";
      if (!$item['readonly']) {
        $editor = "{xtype: 'xdatetime'}";
      }
    }

    if ($item['fk']) {
      $width = ((int) ($item['grid_width'])) >= 50 ? $item['grid_width'] : 250;
    }

    $details = '';
    $details = $details . ', align: ' . $align;
    $details = $details . ', width: ' . $width;
    if ($renderer) {
      $details = $details . ', renderer: ' . $renderer;
    }
    if ($editor) {
      $details = $details . ', editor: ' . $editor;
    }

    return $details;
  }

  /**
   *
   * @param type $acesso_controle
   * @param type $properties
   * @param type $force
   *
   * @return type
   */
  public function getViewListOptionWidth($acesso_controle, $properties, $force = false) {

    $width = $force;
    if ($force === false) {
      $width = 0;
      $size = 30;
      $options = array("view" => true, "edit" => true, "remove" => true);

      $readonly = isset($properties["readonly"]) ? $properties["readonly"] : false;
      if ($readonly) {
        $options['edit'] = false;
        $options['remove'] = false;
      }
      $saveonly = isset($properties["saveonly"]) ? $properties["saveonly"] : false;
      if ($saveonly) {
        $options['edit'] = false;
      }
      $remove = isset($properties["remove"]) ? $properties["remove"] : true;
      if ($remove) {
        $options['remove'] = false;
      }

      foreach ($options as $value) {
        if ($value) {
          $width += $size;
        }
      }
    }

    return $width;
  }

  /**
   *
   * @param type $item
   * @param type $width_label
   * @param type $all
   * @return int
   */
  public function getViewManageWidthLabel($items, $width_label, $all = false) {
    
    if ($all) {
      $width_label = 0;
    } else {
      $items = array($items);
    }
    
    foreach ($items as $item) {
      $width_label = ((strlen($item['description']) * 7) > $width_label) ? ((strlen($item['description']) * 7)) : $width_label;
    }
    return $width_label;
  }

  /**
   *
   * @param type $items
   * @param type $where
   * @param type $group
   * @param type $order
   * @param type $string
   * @return array
   */
  public function recover($items, $where, $group, $order, $string = '') {

    $recovered = System::recover($items, $where, $group, $order, $string);

    return $recovered;
  }

  /**
   * 
   * @param type $item
   * @param type $message
   * @param type $id
   * @param type $url
   * 
   * @return type
   */
  public function convertForeignToHtml($item, $message = "", $id = "", $url = "") {

    if (isset($item['foreign'])) {

      $id = $id ? $id : $item['foreign']['target'];
      $message = $message ? $message : "Selecione uma op&ccedil;&atilde;o acima para atualizar os valores";
      $innerHTML = '<label class="form-readonly">' . $message . '</label> <input type="hidden" id="' . $item['id'] . '" name="' . $item['id'] . '" value=""/></div>';

      $item['type'] = "html";
      $item['value'] = '<div id="' . $id . '">' . $innerHTML . '</div>';

      $item['innerHTML'] = $innerHTML;
      $item['url'] = $url ? $url : PAGE_APP . "src/" . $item['foreign']['modulo'] . "/json/" . $item['foreign']['entity'] . $this->ext . ".json.php?action=" . $item['foreign']['tag'] . "-j-list";
      $item['fields'] = "['" . $item['foreign']['key'] . "', '" . $item['foreign']['description'] . "']";
      $item['valueField'] = $item['foreign']['key'];
      $item['displayField'] = $item['foreign']['description'];
    }

    return $item;
  }

  /**
   * 
   * @param type $selectable
   * @param type $created
   * @param type $property
   * @param type $where
   * @param type $filter
   * @return type
   */
  public function configureOnChange($selectable, $created, $property = "", $where = "", $group = "", $order = "", $filter = "null") {

    $property = $property ? $property : $selectable['id'];
    $where = $where ? $where : $property . " = '?'";
    $onchange = $created['foreign']['encode'] ? base64_decode($created['foreign']['onchange']) : $created['foreign']['onchange'];

    $selectable['foreign']['encode'] = false;
    $selectable['foreign']['onchange'] =
            "system.action.configureOnChange(" .
            'combo, ' . 'record, ' .
            "'" . $created['innerHTML'] . "', " .
            "'" . $created['id'] . "', " .
            "'" . $created['foreign']['target'] . "', '" . $created['url'] . "', " .
            "'" . $property . "', " .
            "'" . addslashes($where) . "', '" . addslashes($group) . "', '" . addslashes($order) . "'," .
            "" . $created['fields'] . ", '" . $created['displayField'] . "', '" . $created['valueField'] . "', " .
            "" . $filter .
            ").on('select', function(combo, record){ " .
            $onchange .
            " }, this);" . $selectable['foreign']['onchange'];

    //print $selectable['id'] . ":" . $selectable['foreign']['onchange']."<hr>";

    return $selectable;
  }

  /**
   * 
   * @param string $codigo
   * @param type $altura
   * @param type $tam
   */
  public function barcode25($codigo, $print = true, $altura = "100", $tam = 2) {

    $aux = 0;
    $codigo2 = '';
    $codigo3 = '';
    // cada n�mero do c�digo � representado por cinco barras. o valor '2' representa uma barra fina. o valor '4' representa uma barra grossa
    $barra = array("22442", "42224", "24224", "44222", "22424", "42422", "24422", "22244", "42242", "24242");

    // verifica se o comprimento � par, caso contr�rio, adiciona um zero no in�cio
    if ((strlen($codigo) / 2) != intval(strlen($codigo) / 2)) {
      $codigo = "0" . $codigo;
    }

    // l� cada d�gito do c�digo a ser gerado, e monta o c�digo a partir da array $barra
    $comprimento = strlen($codigo);

    while ($aux < $comprimento) {
      $indice = $codigo{$aux};
      $codigo2 = $codigo2 . $barra[$indice];
      $aux = $aux + 1;
    }

    // entrela�a o c�digo
    $aux = 0;
    while ($aux < ($comprimento * 5)) {
      $codigo3 = $codigo3 . $codigo2{$aux} . $codigo2{$aux + 5} . $codigo2{$aux + 1} . $codigo2{$aux + 6} . $codigo2{$aux + 2} . $codigo2{$aux + 7} . $codigo2{$aux + 3} . $codigo2{$aux + 8} . $codigo2{$aux + 4} . $codigo2{$aux + 9};
      $aux = $aux + 10;
    }
    // adiciona os c�digos de in�cio e final
    $codigofinal = "2222" . $codigo3 . "422";

    // monta o c�digo de barras, usando divs como barras
    $comprimento = strlen($codigofinal);
    $codigofinal{$comprimento} = '0';
    $aux = 0;

    $barcode = '';
    while ($aux < $comprimento) {
      $barcode .= '<div style="position: relative; float: left; width:' . ($codigofinal{$aux} * $tam) . 'px; height: ' . $altura . 'px; background-color: #000000!important;"></div><div style="position: relative; float: left; width:' . ($codigofinal{$aux + 1} * $tam) . 'px; height: ' . $altura . 'px; background-color: #ffffff;"></div>';
      $aux = $aux + 2;
    }
    $barcode .= '<div style="clear: both;"></div>';

    if ($print) {
      print $barcode;
    }

    return $barcode;
  }

}
