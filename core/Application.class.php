<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Application
 *
 * @author William
 */
class Application {

  private static $installed = './config/installed';

  public static $token = '_t';
  public static $session = '_s';
  public static $remember = '_r';
  public static $width = '_w';
  public static $height = '_h';
  
  public static $login = 'login';
  public static $password = 'password';

  public static $theme = 'theme';
  
  public static $protocol = 'http';
  public static $system = 'system';
  public static $host = '';
  public static $location = '';
  public static $request = '';
  public static $local = '';
  public static $services = '';

  private static $user = "";
  private static $userCtrl = "";
  public static $admin = '';

  public static $return = '';

  public static $routes = array();
  public static $cookies = array();

  public static $menuSystem = array();
  
  /**
   * 
   * lets go boy
   */
  public static function start() {

    self::printHeader();

    $installed = self::install();

    if ($installed) {

      System::import('c', 'manager', 'Usuario', 'src');

      self::$userCtrl = new UsuarioCtrl(PATH_APP);

      $s = PATH_FILE_SEPARATOR;

      self::$host = $_SERVER["HTTP_HOST"];
      self::$location = self::$host . $_SERVER["REQUEST_URI"];

      self::$services = array(
        "css"=>self::$theme . $s . THEME . $s . 'static' . $s . 'assets' . $s . 'css',
        "fonts"=>self::$theme . $s . THEME . $s . 'static' . $s . 'assets' . $s . 'fonts',
        "images"=>self::$theme . $s . THEME . $s . 'static' . $s . 'assets' . $s . 'images',
        "js"=>self::$theme . $s . THEME . $s . 'static' . $s . 'assets' . $s . 'js'
      );
      self::$request = System::request('uri');

      $return = System::request('r');

      $redirect = $return ? $return : "";
      $request = explode("/", self::$request);

      self::$local = $request[0];
      
      $routed = self::route(self::$local);

      if (!$routed) {
        
        $connection = self::createSession($return);

        $user = $connection->user;
        $message = $connection->message;
        $redirect = $connection->redirect;

        $connected = false;

        if (is_object($user)) {
          self::$user = $user;
          $connected = true;
        }

        define('PAGE_CONECTED', $connected);

        self::$admin = self::isAdmin(self::$local);

        if (MANUTENCAO) {
          $redirect = self::manutencao($user);
        } else {
          if (self::$admin ) {
            $redirect = self::validateDomain($user);
          }
        }

        $redirected = self::redirect($redirect);

        if (!$redirected) {

          define('PAGE_USUARIO', $connected ? Encode::decrypt($user->usu_nome) : "");
          define('PAGE_USUARIO_CODIGO', $connected ? $user->usu_codigo : "");
          define('PAGE_USUARIO_TIPO', $connected ? Encode::decrypt($user->usu_tipo) : "");
          
          self::$return = $return;

          self::printScreen(self::$admin, $user, $message, $return);

        }

      }

    }

  }

  /**
   * 
   */
  public static function printHeader() {

    header('Cache-Control: no-cache, must-revalidate');
    header('Expires: ' . gmdate('D, d M Y H:i:s', time() - (60 * 60 * 24 * 365)) . ' GMT');
  }

  /**
   * 
   * @return boolean
   */
  public static function install() {

    $installed = true;
    if (!file_exists(self::$installed)) {
      $installed = false;
      require_once './setup/setup.php';
    }
    return $installed;
  }

  /**
   * 
   * @param type $local
   * @return boolean
   */
  private static function route($local) {
    
    $routed = false;
    if (isset(self::$services[$local])) {

      $url = self::$services[$local] . "/" . String::replace(self::$request, $local . "/", "");
      $service = String::replace($url, "/", PATH_FILE_SEPARATOR);

      if (is_file($service)) {
        $type = self::getContentType($service);
        if ($type) {
          header("Content-Type: " . $type);
        }
        readfile($service);
        $routed = true;
      }

    }
    
    return $routed;
  }

  /**
   * 
   * @param type $filename
   * @return string
   */
  public static function getContentType($filename) {

    $mime_types = array(
        'txt' => 'text/plain',
        'htm' => 'text/html',
        'html' => 'text/html',
        'php' => 'text/html',
        'css' => 'text/css',
        'js' => 'application/javascript',
        'json' => 'application/json',
        'xml' => 'application/xml',
        'swf' => 'application/x-shockwave-flash',
        'flv' => 'video/x-flv',
        // images
        'png' => 'image/png',
        'jpe' => 'image/jpeg',
        'jpeg' => 'image/jpeg',
        'jpg' => 'image/jpeg',
        'gif' => 'image/gif',
        'bmp' => 'image/bmp',
        'ico' => 'image/vnd.microsoft.icon',
        'tiff' => 'image/tiff',
        'tif' => 'image/tiff',
        'svg' => 'image/svg+xml',
        'svgz' => 'image/svg+xml',
        // archives
        'zip' => 'application/zip',
        'rar' => 'application/x-rar-compressed',
        'exe' => 'application/x-msdownload',
        'msi' => 'application/x-msdownload',
        'cab' => 'application/vnd.ms-cab-compressed',
        // audio/video
        'mp3' => 'audio/mpeg',
        'qt' => 'video/quicktime',
        'mov' => 'video/quicktime',
        // adobe
        'pdf' => 'application/pdf',
        'psd' => 'image/vnd.adobe.photoshop',
        'ai' => 'application/postscript',
        'eps' => 'application/postscript',
        'ps' => 'application/postscript',
        // ms office
        'doc' => 'application/msword',
        'rtf' => 'application/rtf',
        'xls' => 'application/vnd.ms-excel',
        'ppt' => 'application/vnd.ms-powerpoint',
        // open office
        'odt' => 'application/vnd.oasis.opendocument.text',
        'ods' => 'application/vnd.oasis.opendocument.spreadsheet',
    );

    $e = pathinfo($filename, PATHINFO_EXTENSION);
    $ext = strtolower($e);
    if (array_key_exists($ext, $mime_types)) {
      $return = $mime_types[$ext];
    } elseif (function_exists('finfo_open')) {
      $finfo = finfo_open(FILEINFO_MIME);
      $mimetype = finfo_file($finfo, $filename);
      finfo_close($finfo);
      $return = $mimetype;
    } else {
      $return = 'application/octet-stream';
    }
    
    return $return;
  }

  /**
   * 
   * @param type $user
   * @return string
   */
  public static function manutencao($user) {

    $redirect = "";
    if (($user->usu_administrador < 10) && !System::request('su')) {
      if (self::$request !== String::replace(PAGE_MANUTENCAO, PAGE_APP, "")) {
        $redirect = PAGE_MANUTENCAO;
      }
    }

    return $redirect;
  }

  /**
   * 
   * @param type $user
   * @return string
   */
  public static function validateDomain($user) {

    $redirect = "";
    
    if (is_object($user)) {

      $subdomain = self::$userCtrl->getColumnUsuarioCtrl('ist_identificador', "MD5(usu_codigo) = '" . $user->usu_codigo . "'");

      if ($subdomain) {
        if ($subdomain !== self::getSubDomain()) {
          $redirect = self::$protocol . "://" . $subdomain . "." . HOST  . $_SERVER["REQUEST_URI"];
        }
      }

    }
    //die("[" . $redirect . "]");
    return $redirect;
  }
  
  /**
   * 
   * @param type $return
   * @return type
   */
  public static function createSession($return) {

    System::import('class', 'base', 'Session', 'core', true);

    $session = new Session();
    $user = null;
    
    $connection = new Object();

    if (!(System::request(self::$login, false) === false)) {

      $login = String::prevent(System::request(self::$login));
      $password = String::prevent(System::request(self::$password));

      $connection = self::login($login, $password);

      if ($connection->message === "") {

        $remember = System::request(self::$remember, 'off');
        $user = $connection->user;
        $session->start($user, true, $remember, PATH_COOKIE, HOST);

      }

    } else {

      if (System::request("logout")) {

        $session->destroy(self::$token, PATH_COOKIE, HOST);
        $connection->redirect = $return;

      } else {

        $preview = System::getUser(false) ? true : false;
        if ($preview) {

          $user = Cookie::get(self::$session);
          if ($user) {
            $json = stripslashes($user);
            $user = json_decode($json);
            $connection->user = $user;
          }

        }

      }

    }

    return $connection;
  }

  /**
   * 
   * @param type $local
   * @return boolean
   */
  public static function isAdmin($local) {

    $admin = false;
    if (PAGE_CONECTED) {

      if ($local === self::$system) {
        $admin = true;
      }

    }
    return $admin;
  }

  /**
   * 
   * @param type $return
   * @return boolean
   */
  private static function redirect($return) {
    
    $redirect = false;

    if ($return) {

      System::import('class', 'resource', 'Screen', 'core', true);

      $screen = new Screen();

      $screen->printScreenRedirect($return);

      $redirect = true;

    }
    
    return $redirect;
  }

  /**
   * 
   * @param string $dir
   * @return string
   */
  public static function getThemeLocation($dir = "") {
    $plus = String::replace($dir, "/", PATH_FILE_SEPARATOR);
    return self::$theme . PATH_FILE_SEPARATOR . THEME . PATH_FILE_SEPARATOR . $plus;
  }

  /**
   * 
   * @param type $path
   * @param type $return
   */
  public static function link($path = "", $return = false) {
    
    $r = "";
    if ($return) {
      $local = "r=" . self::getLocation();
      $r = String::position($path, "?") ? "&" . $local : "?" . $local ;
    }
    $path = $path ? "/" . $path : "";

    print "//" . self::$host . $path . $r; 
  }

  /**
   * 
   * @param type $text
   * @param type $recovered
   */
  public static function text($text, $recovered) {
    if ($recovered) {
      $text = utf8_decode($text);
    }
    print $text;
  }

  /**
   * 
   * @return string
   */
  public static function getLocation($subdomain = "") {
    return self::$protocol . "://" . $subdomain . self::$location;
  }
  
  /**
   * 
   * @param type $admin
   * @param type $user
   * @param type $message
   * @param type $return
   */
  public static function printScreen($admin, $user, $message, $return) {

    System::import('class', 'resource', 'Screen', 'core', true);

    $screen = new Screen();

    $screen->prinScreenHeader($user, $admin);

    self::loadCookies();

    if ($admin) {

      self::loadRoutes($user);

      $screen->printScreenAdmin($user);

    } else {

      $screen->printScreenSite($message, $return);

    }

    $screen->prinScreenBottom($user, $admin);

  }

  /**
   * 
   * @param type $user
   */
  public static function loadRoutes($user) {

    System::import('c', 'manager', 'MenuFuncaoUsuarioTipo', 'src', true, '');

    $menuFuncaoUsuarioTipoCtrl = new MenuFuncaoUsuarioTipoCtrl(PATH_APP);

    $menus = $menuFuncaoUsuarioTipoCtrl->getOptionMenuFuncaoUsuarioTipoCtrl(Encode::decrypt($user->usu_tipo));
    if (count($menus) > 0) {

      foreach ($menus as $item) {

        $menu = $item['menu'];
        $men_descricao = System::encode($menu->get_men_value('men_descricao'), UTF8_PRINT);

        $menuFuncaos = $item['option'];
        $items = array();

        if (count($menuFuncaos) > 0) {

          foreach ($menuFuncaos as $menuFuncao) {

            $mef_descricao = System::encode($menuFuncao->get_mef_value('mef_descricao'), UTF8_PRINT);
            $module = $menuFuncao->get_mef_value('mef_module');
            $entity = $menuFuncao->get_mef_value('mef_entity');
            $tag = String::lower($menuFuncao->get_mef_value('mef_entity'));
            $operation = $menuFuncao->get_mef_value('mef_action');
            $level = Encode::encrypt($menuFuncao->get_mef_value('mef_codigo'));
            $separator = ($menuFuncao->get_mef_value('mef_separator')) ? 'separator' : '';
            $action = $tag . "/" . $operation;
            
            $route = array(
                "root" => "src", 
                "module" => $module, 
                "entity" => $entity, 
                "tag" => $tag, 
                "operation" => $operation, 
                "level" => $level, 
                "separator" => $separator, 
                "men_descricao" => $men_descricao, 
                "mef_descricao" => $mef_descricao
            );

            self::$routes[$action] = $route;
            $items[$action] = $route;

          }

          self::$menuSystem[] = array(
            "descricao"=>$men_descricao,
            "items"=>$items
          );

        }

      }

    }

  }
  
  /**
   * 
   */
  public static function loadCookies() {

    self::$cookies['token'] = self::$token;
    self::$cookies['session'] = self::$session;
    self::$cookies['remember'] = self::$remember;
    self::$cookies['width'] = self::$width;
    self::$cookies['height'] = self::$height;
  }

  /**
   * 
   * @param type $login
   * @param type $password
   * @return object
   */
  public static function login($login, $password) {

    $connection = new Object();
    $user = null;
    $message = "";

    if ($login != "" and $password != "") {


      $user = self::$userCtrl->login($login, $password);

      if (is_object($user)) {

        if ($user->get_usu_value('usu_ativo')) {

          $u = json_encode($user->get_usu_class());
          $user = json_decode($u);

        } else {

          $message = "Usu&aacute;rio inativo <a href='" . PAGE_REMEMBER . "'>O que isso quer dizer?</a>";
        }

      } else {

        if ($user === "404") {
          $message = "O login informado n&atilde;o foi encontrado. <a href='" . PAGE_REMEMBER . "'>Esqueceu seu login?</a>";
        } else if ($user === "403") {
          $message = "A senha que voc&ecirc; forneceu para o usu&aacute;rio <b>" . $login . "</b> est&aacute; incorreta. <a href='" . PAGE_REMEMBER . "'>Esqueceu sua senha?</a>";
        }
      }

    } else {

      if ($login === "" and $password === "") {
        $message = "Informe um usu&aacute;rio e uma senha";
      } else if ($login == "") {
        $message = "Informe um usu&aacute;rio";
      } else if ($password == "") {
        $message = "Informe uma senha";
      }
    }

    $connection->user = $user;
    $connection->message = $message;

    return $connection;
  }

  /**
   * 
   * @param type $index
   * @return type
   */
  public static function get($index = 'name') {
    if ($index === 'avatar') {
      $get = 'profile.jpg';
    } else {
      $get = System::getSessionValue(self::$user, $index);
    }
    return $get;
  }
  
  /**
   * 
   * @param type $www
   * 
   * @return string
   */
  public static function getSubDatabase($www = false) {
    
    $database = self::getSubDomain();
    
    if (!$www) {
      if ($database === "www") {
        $database = DEFAULT_DATABASE;
      }
    }
    
    return $database;
  }
 
  /**
   * 
   * @return string
   */
  public static function getSubDomain() {

    $domain = isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : HOST;
    $url = str_replace(HOST, "", $domain);
    $subdomain = substr($url, 0, strlen($url) - 1);
    
    return $subdomain;
  }

}
