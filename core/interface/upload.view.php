<?php

if(isset($_GET["action"])) {
  $action = $_GET["action"];

  $target = System::request('target');
  $level = System::request('level');
  $rotule = System::request('rotule');

  $module = System::request('m');
  $id = System::request('i');
  
  $field = System::request('fd');
  $table = System::request('tb');
  $where = System::request('wr');
  $group = System::request('gp');
  $order = System::request('od');
  $limit = System::request('lt');
  
  if($action == "form") {
    viewFormReport($id, $module, $target, $level, $rotule);
  }else if($action == "process"){
    viewFormPrint($id, $module, $field, $table, $where, $group, $order, $limit);
  }
  
}

/**
 * 
 * @param type $id
 * @param type $module
 * @param type $target
 * @param type $level
 */
function viewFormReport($id, $module, $target, $level, $rotule){
  
  $level = base64_decode($level);
  require_once PATH_APP."core/header.php";
  require_once PATH_APP."core/base/System.class.php";
  require_once PATH_APP."core/base/Screen.class.php";
  
  $class_name = ucfirst($id);
  core_require('r', $module, $class_name, PATH_APP.'src');
  $report = new $class_name();
  
  $class = "table_grid";
  
  $_field = Encode::encrypt($report->get_field());
  $_table = Encode::encrypt($report->get_table());
  $_where = Encode::encrypt($report->get_where());
  $_order = Encode::encrypt($report->get_order());
  $_group = Encode::encrypt($report->get_group());
  $_limit = Encode::encrypt($report->get_limit());
  
  $items = $report->get_items();
  
  $width_label = 0;
  $lines = 0;
  $param = "search";
  foreach($items as $key => $item){
    $lines = ($item['line'] > $lines) ? $item['line'] : $lines;
    $width_label = $system->getViewManageWidthLabel($item, $width_label);
    
    $items[$key]['pk'] = 0;
    $items[$key]['form'] = 1;
  }
  $width_value = WINDOW_WIDTH - $width_label;
  
  try {
    
    if($acesso_controle >= 2) {
      
      $level = base64_encode($level);
      ?>
        <script type="text/javascript">

          /**
           *
           * @param target
           */
          system.action.reportBack = function(target, clear){
            system.util.toggle('formulario-'+target,'processamento-'+target, 'backit-'+target);//, 'searchit-'+target
            if(clear){
              system.util.get('upl_form_'+target).src = 'app/loading.view.php?action=process';
              system.util.get('printit-'+target).disabled = true;
            }
          };

          /**
           *
           * @param level
           * @param target
           * @param module
           * @param id
           */
          system.action.reportSearch = function(level, target){
            var enviar = false;
            <?php
              if (MODO_LEGADO === "true") {
                ?>
                  enviar = system.navigate('upl_form_' + target, 'core/interface/report.view.php?action=validate&level='+level, 'message_validate_'+target, true, 'message_validate_'+target, false);
                <?php
              } else {
                ?>
                  enviar = system.ajax.post('upl_form_' + target, 'core/interface/report.view.php?action=validate&level=' + level, 'message_validate_' + target, false, true, items);
                <?php
              }
            ?>
            if (enviar) {
              system.action.reportBack(target);
              system.util.get('upl_form_'+target).submit();
              system.util.get('printit-'+target).disabled = false;
            }
          };
          
          /**
           *
           * @param target
           */
          system.action.reportPrint = function(target){
            window.frames['upl_iframe_'+target].print();
          };
          
          /**
           * @param inputs
           * @param conector
           */
          system.util.createWhere = function(inputs, encode, conector) {
            var where = "";
            var input = "";
            var field = "";
            var value = "";
            var first = true;
            
            if (typeof encode == 'undefined') {
              encode = true;
            }
            if (typeof conector == 'undefined') {
              conector = ' AND ';
            }
            
            for (var i = 0; i < inputs.length; i++) {
              var data = inputs[i];
              input = data;
              field = data;
              if (system.util.isArray(data)) {
                input = data[0];
                field = data[1];
              }
              value = $('#'+input).val();
              if (!first) {
                where += conector;
              }
              if (value.length > 0) {
                where += field+" = '"+value+"'";
              }
              first = false;
            }
            
            if (encode) {
              return system.encrypt.encode(where);
            }
            return where;
          };
        </script>

        <center>
          <form id="upl_form_<?php print $target;?>" name="upl_form_<?php print $target;?>" action="core/interface/report.view.php?action=process" target="upl_iframe_<?php print $target;?>" method="POST" onsubmit="">
            <div id="div_validate_<?php print $target;?>" style="display: none;"></div>
            
            <input type="hidden" name="validate" id="validate" value="*upl_*,*name*<>[!=()]"/>
            
            <input type="hidden" name="i" id="i" value="<?php print $id;?>"/>
            <input type="hidden" name="m" id="m" value="<?php print $module;?>"/>
            
            <input type="hidden" name="fd" id="fd" value="<?php print $_field;?>"/>
            <input type="hidden" name="tb" id="tb" value="<?php print $_table;?>"/>
            <input type="hidden" name="wr" id="wr" value="<?php print $_where;?>"/>
            <input type="hidden" name="od" id="od" value="<?php print $_order;?>"/>
            <input type="hidden" name="gp" id="gp" value="<?php print $_group;?>"/>
            <input type="hidden" name="lt" id="lt" value="<?php print $_limit;?>"/>
            
            <table class="<?php print $class;?>" cellpadding="0" cellspacing="0" border="0" width="<?php print WINDOW_WIDTH;?>">
              <tr class="<?php print $class;?>_row_module">
                <td>
                  <img src="resources/images/default/icon/refresh.png" width="16" height="16" alt="Recarregar" title="Recarregar" class="click" onclick="system.navigate('upl_form_<?php print $target; ?>', 'core/interface/report.view.php?action=form&i=<?php print $id; ?>&m=<?php print $module; ?>&rotule=<?php print $rotule; ?>&level=<?php print $level; ?>&target=<?php print $target; ?>','<?php print $target;?>',false,'<?php print $target;?>', false);"/>
                  
                  <label class="text8">&nbsp;<?php print base64_decode($rotule);?></label>
                </td>
              </tr>
              <tr>
                <td>
                  <table class="<?php print $class;?>" cellpadding="0" cellspacing="0" border="0" width="100%">
                    <tr class="<?php print $class;?>_header">
                      <td align="left" class="text3" width="110">
                        <input value="Pesquisar" id="searchit-<?php print $target;?>" onclick="system.action.reportSearch('<?php print $level;?>','<?php print $target;?>');" class="form-buttom" type="button">
                      </td>
                      <td align="left" class="text3" width="110">
                        <input id="printit-<?php print $target;?>" value="Imprimir" onclick="system.action.reportPrint('<?php print $target;?>');" class="form-buttom" type="button" disabled>
                      </td>
                      <td align="left" class="text3" width="110">
                        <input value="Voltar" id="backit-<?php print $target;?>" onclick="system.action.reportBack('<?php print $target;?>', true);" class="form-buttom" type="button" style="display: none;">
                      </td>
                      <td align="left" class="text3" valign="middle">
                        <div id="message_validate_<?php print $target;?>"></div>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>

              <tr>
                <td>
                  <div id="formulario-<?php print $target;?>">
                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                      <?php
                      for($line = 1; $line <= $lines; $line++){
                        $first = null;
                        $subitems = null;
                        foreach($items as $item){
                          if(isset($item['line']) && $item['line'] == $line){
                            $show = $system->getViewManageVisibility($item, $param);
                            if($show){
                              if($param == "search"){
                                $item['readonly'] = false;
                              }
                              if($first == null){
                                $first = $item;
                              }else{
                                $subitems[] = $item;
                              }
                            }
                          }
                        }
                        if($first != null){
                          $screen->printLine($first, $width_label, $width_value, $subitems);
                        }
                      }
                      ?>
                    </table>
                  </div>
                </td>
              </tr>

              <tr>
                <td class="<?php print $class;?>_row_label reset" style="height: auto;">
                  <div id="processamento-<?php print $target;?>" style="display: none;">
                    <iframe name="upl_iframe_<?php print $target;?>" id="upl_iframe_<?php print $target;?>" src="app/loading.view.php?action=process" style="width: 100%; height: <?php print WINDOW_HEIGHT + 26;?>px; border: none; background: #fff;" frameborder="0"></iframe>
                  </div>
                </td>
              </tr>
            </table>
          </form>
        </center>
        <br>
      <?
      
    }else{
      ?>
        <center>
          <label class="error_message">Servi&ccedil;o Indispon&iacute;vel</label>
        </center>
      <?
    }
  }catch (Exception $exception) {
      print "Caught exception: ".$exception->getMessage()."\n";
  }
}

/**
 * 
 * @param type $id
 * @param type $module
 * @param type $_field
 * @param type $_table
 * @param type $_where
 * @param type $_group
 * @param type $_order
 * @param type $_limit
 */
function viewFormPrint($id, $module, $_field, $_table, $_where, $_group, $_order, $_limit){
  
  ini_set("include_path", ini_get("include_path") . ":".PATH_APP."lib/phpreports/");
  
  error_reporting(0);
  
  include "PHPReportMaker.php";
  require_once PATH_APP."core/header.php";
  require_once PATH_APP."core/base/System.class.php";
  
  $class_name = ucfirst($id);
  core_require('r', $module, $class_name, PATH_APP.'src');
  
  $report = new $class_name();
  $rel = new PHPReportMaker();

  $_field = base64_decode($_field);
  $_table = base64_decode($_table);
  $_where = base64_decode($_where);
  $_group = base64_decode($_group);
  $_order = base64_decode($_order);
  $_limit = base64_decode($_limit);
  
  $items = $report->get_items();
  
  $recovered = $system->recover($items, true, true, true);
  
  $where = $recovered['w'];
  if($where != ""){
    if($_where != ""){
      $_where = "(".$_where.") AND (".$where.")";
    }else{
      $_where = $where;
    }
  }
  $w_description = Encode::encrypt($recovered['wd']);
  
  if($_where){
    $_where = " WHERE ".$_where;
  }
  
  if($_group){
    $_group = " GROUP BY ".$_group;
  }
  
  if($_order){
    $_order = " ORDER BY ".$_order;
  }
  
  if($_limit){
    $_limit = " LIMIT ".$_limit;
  }
  
  $sql = "SELECT ".$_field." FROM ".$_table.$_where.$_group.$_order.$_limit;
  
  $javascript = '&resources=1&load=0&check=1&catch=0&compact=0&resume=0';
  if(MODO_TESTE == 'false'){
    $javascript = '&resources=1&load=0&check=1&catch=1&compact=1&resume=0';
    //print $sql;
  }
  
  $css = '&commom=1&fagoc=1';
  
  ?>
  <html>
    <head>
      <link rel="stylesheet" type="text/css" media="all" href="<?php print PAGE_CSS;?>?id=fagoc.br<?php print $css;?>&file=system.css" />
      <style type="text/css">
        body, html{
          background: #fff !important;
        }
      </style>
      <script type="text/javascript" src="../../js/?id=fagoc.br<?php print $javascript;?>&file=system.js"></script>
    </head>
    <body>
      <center>
        <?php 
          $rel->setXML(PATH_APP."src/".$module."/report/".$id.".xml");
          $rel->setSQL($sql);
          $rel->run();
        ?>  
      </center>
    </body>
  </html>
  <?
}

?>