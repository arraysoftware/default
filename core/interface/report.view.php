<?php

System::getUser(true);

$action = "action";
if (isset($_GET[$action])) {
  
  $action = System::request($action);

  $target = System::request('target');
  $level = System::request('level');
  $rotule = System::request('rotule');

  $container = System::request('c');
  $module = System::request('m');
  $id = System::request('i');
  $settings = System::request('s');

  if ($action == "group") {
    viewGroupReport($module, $container, $target, $level, $rotule);
  } else if($action == "form") {
    viewFormReport($module, $id, $target, $level, $rotule, $container, $settings);
  } else if($action == "process") {
    viewPrintReport($module, $id);
  } else {
    ?>
      <style type="text/css">
        body{
          background: url('<?php print PAGE_APP;?>custom/images/loading/engine.gif') no-repeat scroll center 100px transparent;
        }
      </style>
    <?
  }

}

/**
 * 
 * @param type $module
 * @param type $id
 * @param type $target
 * @param type $level
 * @param type $rotule
 */
function viewGroupReport($module, $id, $target, $level, $rotule){
  
  $level = base64_decode($level);

  require_once System::import('file', '', 'header', 'core', false);
  System::import('class', 'resource', 'Screen', 'core', true);

  $class = ucfirst($id);
  System::desire('r', $module, $class, 'src');
  
  //$reportContainer = new ReportContainer();
  $reportContainer = new $class();
  
  $screen = new Screen($reportContainer->get_ext());
  
  if ($acesso_controle >= 2) {
    ?>
      <script type="text/javascript">
        /**
         * 
         * @param {type} target
         * @param {type} level
         * @param {type} container
         * @param {type} module
         * @param {type} id
         * @param {type} name
         * 
         * @returns {undefined}
         */
        system.action.showFormReport = function(target, level, container, module, id, name) {
          system.util.toggle('report-' + target, 'container-' + target);
          <?php
            if (MODO_LEGADO === "true") {
              ?>
                system.navigate('rep_form_container_' + target, 'core/interface/report.view.php?action=form&i=' + id + '&m=' + module + '&c=' + container + '&rotule=' + name + '&level=' + level + '&target=' + target + '', 'report-' + target, false, 'report-' + target, false);
              <?php
            } else {
              ?>
                system.ajax.post('rep_form_container_' + target, 'core/interface/report.view.php?action=form&i=' + id + '&m=' + module + '&c=' + container + '&level=' + level + '&target=' + target, target, false, true, null);
              <?php
            }
          ?>
        };
        
        /**
         * 
         * @param {type} target
         * @param {type} id
         * @param {type} module
         * @param {type} container
         * @param {type} settings
         * @param {type} rotule
         * @param {type} level
         * 
         * @returns {undefined}
         */
        system.action.containerRefresh = function(target, id, module, rotule, level) {
          <?php
            if (MODO_LEGADO === "true") {
              ?>
                system.navigate('rep_form_container_' + target, 'core/interface/report.view.php?action=group&i=' + id + '&m=' + module + '&rotule=' + rotule + '&level=' + level + '&target=' + target, target, false, target, false);
              <?php
            } else {
              ?>
                system.ajax.post('rep_form_container_' + target, 'core/interface/report.view.php?action=group&i=' + id + '&m=' + module + '&rotule=' + rotule + '&level=' + level + '&target=' + target, target, false, true, null);
              <?php
            }
          ?>
        };
     </script>
      <center>
        <div id="container-<?php print $target; ?>">
          <form id="rep_form_container_<?php print $target;?>" name="rep_form_container_<?php print $target;?>" action="core/interface/report.view.php?action=process" target="rep_iframe_<?php print $target;?>" method="POST" onsubmit="">
            <div id="message_validate_<?php print $target;?>" style="display: none;"></div>

            <input type="hidden" name="c" id="c" value="<?php print $id;?>"/>
            <input type="hidden" name="m" id="m" value="<?php print $module;?>"/>

            <table class="form-table" cellpadding="0" cellspacing="0" border="0" width="<?php print WINDOW_WIDTH;?>">
              <tr class="form-label">
                <td class="header" colspan="2">
                  <span class="tree-reload" onclick="system.action.containerRefresh('<?php print $target; ?>', '<?php print $id; ?>', '<?php print $module; ?>', '<?php print $rotule; ?>', '<?php print $level; ?>');">&nbsp;</span>
                  <label class="form-title">&nbsp;<?php print base64_decode($rotule);?></label>
                </td>
              </tr>
              <?php
                $reports = $reportContainer->get_reports();
                foreach ($reports as $rep) {
                  $rep_module = $rep['module'];
                  $rep_id = $rep['id'];
                  $rep_class = ucfirst($rep_id);

                  System::import('r', $rep_module, $rep_class, 'src');

                  $report = new $rep_class();
                  $rep_name = $report->get_name();
                  $rep_description = $report->get_description() ? $report->get_description() : "-";

                  ?>
                    <tr>
                      <td width="250px" class="form-label">
                        <a href="javascript:void(0)" onclick="system.action.showFormReport('<?php print $target; ?>', '<?php print $level; ?>', '<?php print $id; ?>', '<?php print $rep_module; ?>', '<?php print $rep_id; ?>', '<?php print Encode::encrypt($rep_name); ?>');">
                          <?php print $rep_name; ?>
                        </a>
                      </td>
                      <td class="form-value">
                          <b><?php print $rep_description; ?></b>
                      </td>
                    </tr>
                  <?php
                }
              ?>
            </table>
          </form>
        </div>
        <div id="report-<?php print $target; ?>" style="display: none;">
          
        </div>
      </center>
      <br />
    <?
  } else {
    $screen->message->printMessageError("Servi&ccedil;o Indispon&iacute;vel");
  }
}

/**
 * 
 * @param type $module
 * @param type $id
 * @param type $target
 * @param type $level
 * @param type $rotule
 * @param type $settings
 */
function viewFormReport($module, $id, $target, $level, $rotule, $container, $settings){

  $acesso_controle = -1;
  require_once System::desire('file', '', 'header', 'core', false);
  System::desire('class', 'resource', 'Screen', 'core', true);

  $class = ucfirst($id);
  System::desire('r', $module, $class, 'src');
  $report = new $class($settings);

  $screen = new Screen($report->get_ext());

  $validate = array();

  $items = $report->get_items();

  $width_label = 0;
  $lines = 0;
  $param = "search";
  
  $reorder = array();
  $i = 0;
  
  foreach($items as $key => $item){
    $lines = ($item['line'] > $lines) ? $item['line'] : $lines;
    $width_label = $screen->utilities->getViewManageWidthLabel($item, $width_label);

    $items[$key]['pk'] = 0;
    $items[$key]['form'] = 1;

    if($item['validate']){
      $validate[] = $item;
    }
    
//    if(isset($item['onchange'])){
//      print $key . " => " . $item['onchange']['id']."<br>";
//      $items[$item['onchange']['id']] = $screen->utilities->convertForeignToHtml($items[$item['onchange']['id']], $item['onchange']['message']);
//      $items[$key] = $screen->utilities->configureOnChange($items[$key], $items[$item['onchange']['id']], $item['onchange']['property'], $item['onchange']['where'], $item['onchange']['group'], $item['onchange']['order']);
//      print $items[$key]['foreign']['onchange']."<br>";
//    }
      
    $reorder[$i] = $item;
    $i++;
  }
  
  $i--;
  for($i; $i >= 0; $i--){
    $item = $reorder[$i];
    $key = $item['id'];
    if(isset($item['onchange'])){
      $filter = isset($item['onchange']['filter']) ? $item['onchange']['filter'] : "null";
      $items[$item['onchange']['id']] = $screen->utilities->convertForeignToHtml($items[$item['onchange']['id']], $item['onchange']['message']);
      $items[$key] = $screen->utilities->configureOnChange($items[$key], $items[$item['onchange']['id']], $item['onchange']['property'], $item['onchange']['where'], $item['onchange']['group'], $item['onchange']['order'], $filter);
    }
  }

  $prefix = "";
  if ($container) {
    $prefix = "report-";
  }

  $width_value = WINDOW_WIDTH - $width_label;
  $height = WINDOW_HEIGHT + 26;
  
  try {

    if($acesso_controle >= 2) {

      ?>
        <script type="text/javascript">

          /**
           * 
           * @param {type} target
           * @param {type} clear
           * @param {type} container
           * 
           * @returns {undefined}
           */
          system.action.reportBack = function(target, clear, container) {
            if (container) {
              system.util.toggle('report-' + target, 'container-'+target);
            } else {
              system.util.toggle('formulario-'+target,'processamento-'+target, 'backit-'+target, 'searchit-'+target, 'printit-'+target);//, 'saveit-'+target
            }
            if(clear){
              system.util.get('rep_form_'+target).src = 'app/loading.view.php?action=process';
            }
          };

          /**
           * 
           * @param {type} level
           * @param {type} target
           * @param {type} items
           * 
           * @returns {undefined}
           */
          system.action.reportSearch = function(level, target, items) {
            var enviar = false;
            <?php
              if (MODO_LEGADO === "true") {
                ?>
                  enviar = system.navigate('rep_form_' + target, 'core/interface/report.view.php?action=validate&level=' + level, 'message_validate_' + target, true, 'message_validate_' + target, false, null, null, null, null, null, items);
                <?php
              } else {
                ?>
                  enviar = system.ajax.post('rep_form_' + target, 'core/interface/report.view.php?action=validate&level=' + level, 'message_validate_' + target, false, true, items);
                <?php
              }
            ?>
            if (enviar) {
              system.action.reportBack(target);
              system.util.get('rep_form_'+target).submit();
              system.util.get('saveit-'+target).disabled = false;
            }
          };

          /**
           *
           * @param target
           */
          system.action.reportPrint = function(target){
            window.frames['rep_iframe_'+target].print();
          };
          /**
           *
           * @param target
           */
          system.action.reportSave = function(target){
            var name = prompt('Salvar Como:','<?php print base64_decode($rotule); ?>');
          };
          
          /**
           * 
           * @param {type} target
           * @param {type} id
           * @param {type} module
           * @param {type} container
           * @param {type} settings
           * @param {type} rotule
           * @param {type} level
           * @param {type} prefix
           * 
           * @returns {undefined}
           */
          system.action.reportRefresh = function(target, id, module, container, settings, rotule, level, prefix) {
            <?php
              if (MODO_LEGADO === "true") {
                ?>
                  system.navigate('rep_form_' + target, 'core/interface/report.view.php?action=form&i=' + id + '&m=' + module + '&c=' + container + '&s=' + settings + '&rotule=' + rotule + '&level=' + level + '&target=' + target, prefix + target, false, prefix + target, false);
                <?php
              } else {
                ?>
                  system.ajax.post('rep_form_' + target, 'core/interface/report.view.php?action=form&i=' + id + '&m=' + module + '&c=' + container + '&s=' + settings + '&rotule=' + rotule + '&level=' + level + '&target=' + target, prefix + target, false, true, null);
                <?php
              }
            ?>
          };
        </script>
        
        <?php
        
          $report->printJavascriptFormReport($target, $level);

        ?>
        
        <style type="text/css">
          .report-height{
            height: <?php print $height?>px;
          }
          .report-iframe{
            width: 100%;
            border: none;
            background: #fff;
          }
        </style>

        <?php
        
          $report->printCssFormReport($target, $level);

        ?>
        <center>
          <form id="rep_form_<?php print $target;?>" name="rep_form_<?php print $target;?>" action="core/interface/report.view.php?action=process" target="rep_iframe_<?php print $target;?>" method="POST" onsubmit="">
            <div id="message_validate_<?php print $target;?>" style="display: none;"></div>

            <input type="hidden" name="c" id="c" value="<?php print $container;?>"/>
            <input type="hidden" name="i" id="i" value="<?php print $id;?>"/>
            <input type="hidden" name="m" id="m" value="<?php print $module;?>"/>
            <input type="hidden" name="s" id="s" value="<?php print $settings;?>"/>

            <table class="form-table b-bottom" cellpadding="0" cellspacing="0" border="0" width="<?php print WINDOW_WIDTH;?>">
              <tr class="form-label">
                <td class="header">    
                  <span class="tree-reload" onclick="system.action.reportRefresh('<?php print $target; ?>', '<?php print $id; ?>', '<?php print $module; ?>', '<?php print $container; ?>', '<?php print $settings; ?>', '<?php print $rotule; ?>', '<?php print $level; ?>', '<?php print $prefix; ?>');">&nbsp;</span>
                  <label class="form-title">&nbsp;<?php print base64_decode($rotule);?></label>
                </td>
              </tr>
              <tr>
                <td class="form-label reset">
                  <div id="toolbar-<?php print $target; ?>">
                    <?php

                      $itens = "";
                      if (count($validate) > 0) {
                        foreach ($validate as $item) {
                          $itens .= ",'" . $item['id'] . "': {'description':'" . $item['description'] . "', 'value':'" . $item['validate'] . "'" . "}";
                        }
                      }
                      $itens = '{' . substr($itens, 1) . '}';

                      $report->printToolbarReport($acesso_controle, $target, $level, $module, $itens, $container);
                    ?>
                  </div>
                </td>
              </tr>

              <tr>
                <td class="form-label b-bottom reset">
                  <div id="formulario-<?php print $target;?>">
                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                      <?php

                        for($line = 1; $line <= $lines; $line++){
                          $first = null;
                          $subitems = null;
                          foreach($items as $item){
                            if(isset($item['line']) && $item['line'] == $line){
                              $show = $screen->utilities->getViewManageVisibility($item, $param);
                              if($show){
                                if($param == "search"){
                                  $item['readonly'] = false;
                                }
                                if($first == null){
                                  $first = $item;
                                }else{
                                  $subitems[] = $item;
                                }
                              }
                            }
                          }
                          if($first != null){
                            $screen->line->printLine($first, $width_label, $width_value, $subitems);
                          }
                        }

                    ?>
                    </table>
                    <?php
        
                      $report->printHtmlFormReport($target, $level);

                    ?>
                  </div>

                  <div id="processamento-<?php print $target;?>" style="display: none;" class="form-value b-left report-height">
                    <iframe name="rep_iframe_<?php print $target;?>" id="rep_iframe_<?php print $target;?>" src="app/loading.view.php?action=process" class="report-iframe report-height" frameborder="0"></iframe>
                  </div>
                </td>
              </tr>
            </table>
          </form>
        </center>
        <br>
      <?

    }else{
      ?>
        <center>
          <label class="error_message">Servi&ccedil;o Indispon&iacute;vel</label>
        </center>
      <?
    }
  }catch (Exception $exception) {
      print "Caught exception: ".$exception->getMessage()."\n";
  }
}

/**
 *
 * @param type $module
 * @param type $id
 */
function viewPrintReport($module, $id){

  require_once System::import('file', '', 'header', 'core', false);

  $class = ucfirst($id);

  System::import('r', $module, $class, 'src');
  
  $report = new $class();

  $recovered = $report->makeWhere(true, true, true, $report->get_items());
    
  $javascript = '&resources=1&load=0&check=1&catch=0&compact=0&resume=0';
  if(MODO_TESTE == 'false'){
    $javascript = '&resources=1&load=0&check=1&catch=1&compact=1&resume=0';
  }
  $css = '&commom=1&report=1';
  ?>
    <html>
      <head>
        <link rel="stylesheet" type="text/css" media="all" href="<?php print PAGE_CSS;?>?id=fagoc.br<?php print $css;?>&file=system.css" />
        <style type="text/css">
          body, html{
            background: #fff !important;
          }
          p.breakhere { 
            page-break-before:always;
            border:0px;
            margin:0px;
            background:transparent; 
          }
        </style>
      </head>
      <body>
        <center>
          <?php
            $report->run($module, $id, $recovered);
          ?>
        </center>
      </body>
    </html>
  <?
}

?>