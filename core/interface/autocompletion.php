<?php

$module = System::import('m', 'util', 'Autocomplete', 'src', false, '');

if (File::exists($module)) {
  
  if (System::request('word')) {

    $token = System::request('word');

    System::import('c', 'util', 'Autocomplete', 'src', true, '');
    System::import('m', 'util', 'Autocomplete', 'src', true, '');

    $autocompleteCtrl = new AutocompleteCtrl(PATH_APP);

    $words = array();

    $autocompletes = $autocompleteCtrl->getAutocompleteCtrl("atc_token LIKE '" . $token . "%'", "", "atc_token", "", "");
    if (is_array($autocompletes)) {
      foreach ($autocompletes as $autocomplete) {
        $words[] = array("word"=>$autocomplete->get_atc_value('atc_token'), "freq"=>26, "score"=>300, "flags"=>"bc", "syllables"=>"1");/*,*/
      }
    }

    print json_encode($words);

  } else {

    $home = PATH_APP . "core/";
    $ponteiro = opendir($home);

    while ($dir = readdir($ponteiro)) {
      if ($dir != "." && $dir != "..") {
        if (is_dir($home . $dir)) {
          $classes = opendir($home . $dir);
          while ($name = readdir($classes)) {
            $class = $home . $dir . "/" . $name;
            if (!is_dir($class) && $name != "." && $name != "..") {
              if (strpos($class, "class")) {
                require_once $class;
                $c = String::replace($name, ".class.php", "");
                $reflection = new ReflectionClass($c);
                print $reflection->getName() . "<br>";
                $methods = $reflection->getMethods();
                if (is_array($methods)) {
                  foreach ($methods as $method) {

                    $params = "";
                    $parameters = $method->getParameters();
                    if (is_array($parameters)) {
                      foreach ($parameters as $parameter) {
                        $default = "";
                        if (!$method->isInternal()) {
                          if ($parameter->isOptional()) {
                            $value = 'null';
                            if ($parameter->getDefaultValue()) {
                              $value = $parameter->getDefaultValue();
                              if (!is_numeric($value)) {
                                $value = '"' . $value . '"';
                              }
                            }
                            $default = '/* = ' . $value . '*/';
                          }
                        }
                        $params = $params . "\$" . $parameter->getName() . $default . ", ";
                      }
                    }
                    if ($params) {
                      $params = String::substring($params, 0, -2);
                    }

                    if (!$method->isPrivate()) {

                      if ($method->getName() === "__construct" || $method->getName() === $reflection->getName()) {

                        print $reflection->getName() . "(" . $params . ");<br>";

                      } else {

                        if ($method->isStatic()) {
                          print " " . $reflection->getName() . "::" . $method->getName() . "(" . $params . ");<br>";
                        } else {
                          $variable = "\$" . String::lower(String::substring($reflection->getName(), 0, 1)) . String::substring($reflection->getName(), 1);
                          print " " . $variable . "->" . $method->getName() . "(" . $params . ");<br>";
                        }

                      }
                    }

                  }
                }
              }
            }
          }
        }
      }
    }

  }

}