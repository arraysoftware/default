<?php

/**
 *
 * @copyright array software
 *
 * @author Pedro - 20/02/2013 14:46
 * Modulo..........: Geral
 *
 * Responsavel.....: Pedro
 * Alteracao.......: 20/02/2013 14:46
 */
class Report {
  public $debug;
  public $orientation;

  protected $module;
  protected $id;
  protected $ext;
  protected $uniq;
  
  protected $name;
  protected $description;
  protected $items;
  protected $field;
  protected $table;
  protected $where;
  protected $order;
  protected $group;
  protected $limit;

  /**
   * 
   * @param type $settings
   */
  public function Report($settings = false) {
    
    $this->name = "";
    $this->description = "-";

    $this->field = "";
    $this->table = "";
    $this->where = "";
    $this->order = "";
    $this->group = "";
    $this->limit = "";

    $this->debug = $settings;
    $this->orientation = "landscape";
  }

  /**
   *
   * @return type
   */
  public function get_name() {
    return $this->name;
  }

  /**
   *
   * @param type $name
   */
  public function set_name($name) {
    $this->name = $name;
  }

  /**
   * 
   * @return type
   */
  public function get_description() {
    return $this->description;
  }

  /**
   * 
   * @param type $description
   */
  public function set_description($description) {
    $this->description = $description;
  }

  /**
   *
   * @return type
   */
  public function get_module() {
    return $this->module;
  }

  /**
   *
   * @param type $module
   */
  public function set_module($module) {
    $this->module = $module;
  }

  /**
   *
   * @return type
   */
  public function get_id() {
    return $this->id;
  }

  /**
   *
   * @param type $id
   */
  public function set_id($id) {
    $this->id = $id;
  }

  /**
   *
   * @return type
   */
  public function get_uniq() {
    return $this->uniq;
  }

  /**
   *
   * @param type $uniq
   */
  public function set_uniq($uniq) {
    $this->uniq = $uniq;
  }

  /**
   *
   * @return type
   */
  public function get_items() {
    return $this->items;
  }

  /**
   *
   * @param type $items
   */
  public function set_items($items) {
    $this->items = $items;
  }

  /**
   *
   * @return type
   */
  public function get_field() {
    return $this->field;
  }

  /**
   *
   * @param type $field
   */
  public function set_field($field) {
    $this->field = $field;
  }

  /**
   *
   * @return type
   */
  public function get_table() {
    return $this->table;
  }

  /**
   *
   * @param type $table
   */
  public function set_table($table) {
    $this->table = $table;
  }

  /**
   *
   * @return type
   */
  public function get_where() {
    return $this->where;
  }

  /**
   *
   * @param type $where
   */
  public function set_where($where) {
    $this->where = $where;
  }

  /**
   *
   * @return type
   */
  public function get_order() {
    return $this->order;
  }

  /**
   *
   * @param type $order
   */
  public function set_order($order) {
    $this->order = $order;
  }

  /**
   *
   * @return type
   */
  public function get_group() {
    return $this->group;
  }

  /**
   *
   * @param type $group
   */
  public function set_group($group) {
    $this->group = $group;
  }

  /**
   *
   * @return type
   */
  public function get_limit() {
    return $this->limit;
  }

  /**
   *
   * @param type $limit
   */
  public function set_limit($limit) {
    $this->limit = $limit;
  }

  /**
   *
   * @param type $data
   */
  public function add_item($data) {
    $this->items[] = $data;
  }

  /**
   *
   * @return type
   */
  public function get_ext() {
    return $this->ext;
  }

  /**
   *
   * @param type $ext
   */
  public function set_ext($ext) {
    $this->ext = $ext;
  }

  /**
   *
   * @param type $id
   */
  public function remove_item($id) {
    unset($this->items[$id]);
  }

  /**
   *
   * @param type $field
   * @param type $type
   * @param type $as
   * @return type
   */
  public function format($field, $type, $as = "") {
    if ($as == "") {
      $as = $field;
    }

    $format = "";

    switch ($type) {
      case 'data' :
        $format = "DATE_FORMAT(" . $field . ",'%d/%m/%Y')";
        break;

      case 'dia' :
        $format = "DATE_FORMAT(" . $field . ",'%w')";
        break;
    }

    return $format . " AS " . $as;
  }

  /**
   * 
   * @param type $target
   * @param type $level
   */
  public function printHtmlFormReport($target, $level) {
    ?>
    <!--<?php print $target; ?>-->
    <!--<?php print $level; ?>-->
    <?php
  }

  /**
   * 
   * @param type $target
   * @param type $level
   */
  public function printJavascriptFormReport($target, $level) {
    ?>
      <script type="text/javascript">/*<?php print $target; ?>*//*<?php print $level; ?>*/</script>
    <?php
  }

  /**
   * 
   * @param type $target
   * @param type $level
   */
  public function printCssFormReport($target, $level) {
    ?>
      <style type="text/css">/*<?php print $target; ?>*//*<?php print $level; ?>*/</style>
    <?php
  }

  /**
   *
   * @param type $acesso
   * @param type $target
   * @param type $level
   * @param string $modulo
   * @param type $items
   */
  public function printToolbarReport($acesso, $target, $level, $modulo, $items = '{}', $container = false) {
    $modulo = 'src/' . $modulo . '/view';
    ?>
    <table width="100%">
      <?php
        if ($container) {
        ?>
          <td align="left" width="105">
                <input value="Relat&oacute;rios" id="backcontainerit-<?php print $target; ?>" onclick="system.action.reportBack('<?php print $target; ?>', true, true);" class="form-buttom" type="button" style="display: block;">
          </td>
        <?php
        }
      ?>
      <td align="left" width="105">
        <input value="Pesquisar" id="searchit-<?php print $target; ?>" onclick="system.action.reportSearch('<?php print $level; ?>', '<?php print $target; ?>', <?php print $items; ?>);" class="form-buttom" type="button">
        <input value="Voltar" id="backit-<?php print $target; ?>" onclick="system.action.reportBack('<?php print $target; ?>', true);" class="form-buttom" type="button" style="display: none;">
      </td>
      <td align="left" width="105">
        <input id="printit-<?php print $target; ?>" value="Imprimir" onclick="system.action.reportPrint('<?php print $target; ?>');" class="form-buttom" type="button" style="display: none;">
      </td>
      <td align="left" width="105">
        <input id="saveit-<?php print $target; ?>" value="Salvar" onclick="system.action.reportSave('<?php print $target; ?>');" class="form-buttom" type="button" style="display: none;">
      </td>
      <td align="left" valign="middle">
        <div id="message_validate_<?php print $target; ?>"></div>
      </td>
    </table>
    <?
  }

  /**
   * 
   * @param type $title
   * @param type $orientation
   * @param type $name
   */
  public function printTopReport($title = "", $orientation = "landscape", $name = "") {

    $title = $title ? $title : COMPANY . ' :: ' . TITLE;
    $orientation = $this->orientation ? $this->orientation : $orientation;
    $name = $name ? $name : $this->name;
    ?>
      <table align="center" class="<?php print $orientation; ?>">
        <tbody>
          <tr>
            <td>
              <div class="page-header">
                <b><?php print mb_strtoupper($title); ?></b>
              </div>
              <div class="page-header">
                <b><?php print mb_strtoupper($name); ?> [<?php print $this->module; ?>/<?php print $this->id; ?>]</b>
                <span style="float: right;"><?php print mb_strtoupper(System::getUser(true)); ?> [<?php print date('d/m/Y'); ?>]</span>
              </div>
            </td>
          </tr>
        </tbody>
      </table>
      <br>
    <?php
  }

  /**
   * 
   * @param type $orientation
   */
  public function printHeaderReport($orientation = "landscape") {
    
    $orientation = $this->orientation ? $this->orientation : $orientation;
    
    ?>
    <table align="center" class="<?php print $orientation; ?>">
      <tbody>
        <?php
  }

  /**
   * 
   * @param type $w_description
   * @param type $g_description
   * @param type $o_description
   * @param type $orientation
   */
  public function printFooterReport($w_description = "", $g_description = "", $o_description = "", $orientation = "landscape") {
    
    $orientation = $this->orientation ? $this->orientation : $orientation;
    
          ?>
        </tbody>
      </table>
      <br>
      <table align="center" class="<?php print $orientation; ?>">
        <tbody>
          <?php
            if($w_description){
              ?>
                <tr>
                  <td width="50" class="page-header">
                    Filtros: 
                  </td>
                  <td>
                    <?php print $w_description;?>
                  </td>
                </tr>
              <?php
            }
          ?>
          <?php
            if($g_description){
              ?>
                <tr>
                  <td width="50" class="page-header">
                    Grupos: 
                  </td>
                  <td>
                    <?php print $g_description;?>
                  </td>
                </tr>
              <?php
            }
          ?>
          <?php
            if($o_description){
              ?>
                <tr>
                  <td width="50" class="page-header">
                    Ordena&ccedil;&atilde;o: 
                  </td>
                  <td>
                    <?php print $o_description;?>
                  </td>
                </tr>
              <?php
            }
          ?>
        </tbody>
      </table>
      <br>
    <?php
  }
  
  /**
   * 
   * @param type $items
   * @param type $header
   */
  public function printRowReport($items, $header = false){
    ?><?php

      $header = $header ? "header" : "";    
      if (is_array($items)) {
        ?>
          <tr>
            <?php
              foreach ($items as $item) {
                $title = isset($item['title']) ? $item['title'] : "";
                ?>
                  <td colspan="<?php print $item['colspan'];?>" align="<?php print $item['align'];?>" width="<?php print $item['width'];?>" class="<?php print $header;?> <?php print $item['class'];?>" title="<?php print $title;?>">
                    <?php print $item['value'];?>
                  </td>
                <?php
              }
            ?>
          </tr>
        <?php
      }
  }

  /**
   *
   * @param type $sql
   * @param type $index
   * 
   * @return type
   */
  public function getValue($sql, $index) {

    $value = null;

    $query = $this->getData($sql);
    if ($query != NULL) {
      while ($row = mysql_fetch_array($query)) {
        $value = $row[$index];
      }
    }

    return $value;
  }

  /**
   *
   * @param type $sql
   * @return type
   */
  public function getData($sql) {

    require System::desire('file', '', 'header', PATH_APP . 'core', false);
    System::desire('class', 'base', 'DAO', PATH_APP . 'core');

    $dao = new DAO();
    $query = $dao->selectSQL($sql);

    return $query;
  }

  /**
   * 
   * @param type $where
   * @param type $group
   * @param type $order
   * @param type $items
   * @param type $fast
   * 
   * @return type
   */
  public function makeWhere($where, $group, $order, $items, $fast = "") {

    $recovered = System::recover($items, $where, $group, $order, $fast);

    return $recovered;
  }

  /**
   * 
   * @param type $recovered
   * 
   * @return string
   */
  public function makeSql($recovered){
    
    $_where = $this->where;
    $_group = $this->group;
    $_order = $this->order;
    $_limit = $this->limit;

    $where = $recovered['w'];
    if ($where != "") {
      if ($_where != "") {
        $_where = "(" . $_where . ") AND (" . $where . ")";
      } else {
        $_where = $where;
      }
    }

    $group = $recovered['g'];
    if ($group != "") {
      if ($group != "") {
        $_group = $_group . ", " . $group;
      } else {
        $_group = $group;
      }
    }

    $order = $recovered['o'];
    if ($order != "") {
      if ($order != "") {
        $_order = $_order . ", " . $order;
      } else {
        $_order = $order;
      }
    }

    if ($_where) {
      $_where = " WHERE " . $_where;
    }

    if ($_group) {
      $_group = " GROUP BY " . $_group;
    }

    if ($_order) {
      $_order = " ORDER BY " . $_order;
    }

    if ($_limit) {
      $_limit = " LIMIT " . $_limit;
    }

    $sql = "SELECT " . $this->field . " FROM " . $this->table . $_where . $_group . $_order . $_limit;
    
    return $sql;
  }
  
  /**
   * 
   * @param type $module
   * @param type $id
   * @param type $recovered
   */
  public function run($module, $id, $recovered) {

    ini_set("include_path", ini_get("include_path") . ":" . PATH_APP . "lib/phpreports/");

    include "PHPReportMaker.php";

    $reportMaker = new PHPReportMaker();

    $sql = $this->makeSql($recovered);
    if ($this->debug) {
      print $sql;
    }

    $filename = PATH_APP.PATH_TEMP.'report/'.$id.'.html';
    File::saveFile($filename, '');

    $this->printTopReport();
    
    $reportMaker->setXML(PATH_APP . "src/" . $module . "/report/" . $id . ".xml");
    $reportMaker->setSQL($sql);
    $reportMaker->run();
    
    $this->printFooterReport($recovered['wd'], $recovered['gd'], $recovered['od']);

  }

}