<?php

/**
 *
 * @copyright array software
 *
 * @author Pedro - 20/02/2013 14:46
 * Modulo..........: Geral
 *
 * Responsavel.....: Pedro
 * Alteracao.......: 20/02/2013 14:46
 */
class Download {

  protected $module;
  protected $id;
  protected $ext;
  protected $name;
  protected $extension;
  protected $items;
  protected $path;

  /**
   *
   */
  public function Download() {
    $this->name = "";
  }

  /**
   *
   * @return type
   */
  public function get_module() {
    return $this->module;
  }

  /**
   *
   * @param type $module
   */
  public function set_module($module) {
    $this->module = $module;
  }

  /**
   *
   * @return type
   */
  public function get_id() {
    return $this->id;
  }

  /**
   *
   * @param type $id
   */
  public function set_id($id) {
    $this->id = $id;
  }

  /**
   *
   * @return type
   */
  public function get_name() {
    return $this->name;
  }

  /**
   *
   * @param type $name
   */
  public function set_name($name) {
    $this->name = $name;
  }

  /**
   *
   * @return type
   */
  public function get_items() {
    return $this->items;
  }

  /**
   *
   * @param type $items
   */
  public function set_items($items) {
    $this->items = $items;
  }

  /**
   *
   * @return type
   */
  public function get_path() {
    return $this->path;
  }

  /**
   *
   * @param type $path
   */
  public function set_path($path) {
    $this->path = $path;
  }

  /**
   *
   * @return type
   */
  public function get_extension() {
    return $this->extension;
  }

  /**
   *
   * @param type $extension
   */
  public function set_extension($extension) {
    $this->extension = $extension;
  }

  /**
   *
   * @return type
   */
  public function get_ext() {
    return $this->ext;
  }

  /**
   *
   * @param type $ext
   */
  public function set_ext($ext) {
    $this->ext = $ext;
  }

  /**
   *
   * @param type $recovered
   */
  public function generateFile($recovered) {

    print_r($recovered);

    return false;
  }

  /**
   *
   * @param type $sql
   * @return type
   */
  public function getData($sql) {

    require System::import('file', '', 'header', PATH_APP . 'core', false);
    System::import('class', 'base', 'DAO', PATH_APP . 'core');

    $dao = new DAO();
    $query = $dao->selectSQL($sql);

    return $query;
  }

  /**
   *
   * @param type $sql
   * @param type $index
   * @return type
   */
  public function getValue($sql, $index) {

    $value = null;

    $query = $this->getData($sql);
    if ($query != NULL) {
      while ($row = mysql_fetch_array($query)) {
        $value = $row[$index];
      }
    }

    return $value;
  }

  /**
   *
   * @param type $acesso
   * @param type $target
   * @param type $level
   * @param string $modulo
   * @param type $items
   */
  public function printDownloadToolbar($acesso, $target, $level, $modulo, $items = '{}') {
    $modulo = 'src/' . $modulo . '/view';
    ?>
      <table width="100%">
        <td align="left" width="105">
          <input value="Pesquisar" id="searchit-<?php print $target; ?>" onclick="system.action.downloadSearch('<?php print $level; ?>', '<?php print $target; ?>', <?php print $items; ?>);" class="form-buttom" type="button">
          <input value="Voltar" id="backit-<?php print $target; ?>" onclick="system.action.downloadBack('<?php print $target; ?>', true);" class="form-buttom" type="button" style="display: none;">
        </td>
        <td align="left" width="105">
          <input id="printit-<?php print $target; ?>" value="Imprimir" onclick="system.action.downloadPrint('<?php print $target; ?>');" class="form-buttom" type="button" style="display: none;">
        </td>
        <td align="left" valign="middle">
          <div id="message_validate_<?php print $target; ?>"></div>
        </td>
      </table>
    <?
  }

}