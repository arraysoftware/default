<?php
/**
 *
 * @copyright array software
 *
 * @author Pedro - 20/02/2013 14:46
 * Modulo..........: Geral
 *
 * Responsavel.....: Pedro
 * Alteracao.......: 20/02/2013 14:46
 */

class Upload
{
  protected $module;
  protected $id;
  protected $ext;

  protected $name;
  protected $items;
  protected $field;
  protected $table;
  protected $where;
  protected $order;
  protected $group;
  protected $limit;

  public function Upload() {
    $this->name = "";

    $this->field = "";
    $this->table = "";
    $this->where = "";
    $this->order = "";
    $this->group = "";
    $this->limit = "";
  }

  /**
   *
   * @return type
   */
  public function get_name() {
    return $this->name;
  }

  /**
   *
   * @param type $name
   */
  public function set_name($name) {
    $this->name = $name;
  }

  /**
   *
   * @return type
   */
  public function get_module() {
    return $this->module;
  }

  /**
   *
   * @param type $module
   */
  public function set_module($module) {
    $this->module = $module;
  }

  /**
   *
   * @return type
   */
  public function get_id() {
    return $this->id;
  }

  /**
   *
   * @param type $id
   */
  public function set_id($id) {
    $this->id = $id;
  }

  /**
   *
   * @return type
   */
  public function get_items() {
    return $this->items;
  }

  /**
   *
   * @param type $items
   */
  public function set_items($items) {
    $this->items = $items;
  }

  /**
   *
   * @return type
   */
  public function get_field() {
    return $this->field;
  }

  /**
   *
   * @param type $field
   */
  public function set_field($field) {
    $this->field = $field;
  }

  /**
   *
   * @return type
   */
  public function get_ext(){
    return $this->ext;
  }

  /**
   *
   * @param type $ext
   */
  public function set_ext($ext){
    $this->ext = $ext;
  }

  /**
   *
   * @return type
   */
  public function get_table() {
    return $this->table;
  }

  /**
   *
   * @param type $table
   */
  public function set_table($table) {
    $this->table = $table;
  }

  /**
   *
   * @return type
   */
  public function get_where() {
    return $this->where;
  }

  /**
   *
   * @param type $where
   */
  public function set_where($where) {
    $this->where = $where;
  }

  /**
   *
   * @return type
   */
  public function get_order() {
    return $this->order;
  }

  /**
   *
   * @param type $order
   */
  public function set_order($order) {
    $this->order = $order;
  }

  /**
   *
   * @return type
   */
  public function get_group() {
    return $this->group;
  }

  /**
   *
   * @param type $group
   */
  public function set_group($group) {
    $this->group = $group;
  }

  /**
   *
   * @return type
   */
  public function get_limit() {
    return $this->limit;
  }

  /**
   *
   * @param type $limit
   */
  public function set_limit($limit) {
    $this->limit = $limit;
  }

  /**
   *
   * @param type $data
   */
  public function add_item($data) {
    $this->items[] = $data;
  }

  /**
   *
   * @param type $id
   */
  public function remove_item($id) {
    unset($this->items[$id]);
  }

  /**
   *
   * @param type $recovered
   */
  public function processFile($recovered){
    print_r($recovered);

    return false;
  }

  /**
   *
   * @param type $sql
   * @return type
   */
  public function getData($sql){
    require System::desire('file', '', 'header', PATH_APP.'core', false);
    System::desire('class', 'base', 'DAO', PATH_APP.'core');

    $dao = new DAO();
    $query = $dao->selectSQL($sql);

    return $query;
  }

  /**
   *
   * @param type $sql
   * @param type $index
   * @return type
   */
  public function getValue($sql, $index){

    $value = null;

    $query = $this->getData($sql);
    if($query != NULL) {
      while($row = mysql_fetch_array($query)) {
        $value = $row[$index];
      }
    }

    return $value;
  }

  /**
   *
   * @param type $acesso
   * @param type $target
   * @param type $level
   * @param string $modulo
   * @param type $items
   */
  public function printUploadToolbar($acesso, $target, $level, $modulo, $items = '{}') {
    $modulo = 'src/'.$modulo.'/view';
    ?>
      <table width="100%">
        <td align="left" width="105">
          <input value="Importar" id="searchit-<?php print $target;?>" onclick="system.action.uploadSearch('<?php print $level;?>','<?php print $target;?>', <?php print $items; ?>);" class="form-buttom" type="button">
          <input value="Voltar" id="backit-<?php print $target;?>" onclick="system.action.uploadBack('<?php print $target;?>', true);" class="form-buttom" type="button" style="display: none;">
        </td>
        <td align="left" width="105">
          <input id="printit-<?php print $target;?>" value="Imprimir" onclick="system.action.uploadPrint('<?php print $target;?>');" class="form-buttom" type="button" style="display: none;">
        </td>
        <td align="left" valign="middle">
          <div id="message_validate_<?php print $target;?>"></div>
        </td>
      </table>
    <?
  }

}