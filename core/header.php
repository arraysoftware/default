<?php

$acesso_controle = 2;

if (MODO_LEGADO === 'true') {

  require "parameters.php";

} else {

  System::import('c', 'manager', 'MenuFuncaoUsuarioTipo', 'src');

  $menuFuncaoUsuarioTipoCtrl = new MenuFuncaoUsuarioTipoCtrl(PATH_APP);

  if (isset($level)) {
    $mft_cod_MENU_FUNCAO = Encode::decrypt($level);
    if ($mft_cod_MENU_FUNCAO) {
      $acesso_controle = $menuFuncaoUsuarioTipoCtrl->getColumnMenuFuncaoUsuarioTipoCtrl('mft_level', "mft_cod_MENU_FUNCAO = '" . $mft_cod_MENU_FUNCAO . "' AND mft_cod_USUARIO_TIPO = '" . System::getUser('type', false) . "'");
    }
  }

}