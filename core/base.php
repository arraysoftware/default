<?php

$s = (PHP_OS == "Windows" || PHP_OS == "WINNT") ? "\\" : "/";

define('CURDATE', date('d/m/Y'));
define('PATH_FILE_SEPARATOR', $s);
define('PATH_CORE', realpath(dirname(__FILE__)));

define('SHOW_MESSAGE_SUCCESS', '2');//Constante que define o tipo de mensagem a ser exibida
define('SHOW_MESSAGE_ERROR'  , '1');//Constante que define o tipo de mensagem a ser exibida
 
define('MESSAGE_ADD_SUCCESS'      , 'Registro salvo com sucesso.'); //Mensagem exibida ao adicionar um novo registro
define('MESSAGE_ADD_ERROR'        , 'N�o foi poss�vel fazer o registro dos dados.'); //Mensagem de erro ao tentar adicionar um registro
define('MESSAGE_COPY_SUCCESS'     , 'C�pia do registro salva com sucesso.'); //Mensagem de registro de c�pia
define('MESSAGE_COPY_ERROR'       , 'N�o foi poss�el fazer uma c�pia do registro.'); //Mensagem de erro ao adicionar uma c�pia do registro
define('MESSAGE_SET_ERROR'        , 'N�o foi poss�vel atualizar o registro.'); //Mensagem de erro ao alterar um registro
define('MESSAGE_SET_SUCCESS'      , 'Registro atualizado com sucesso.'); //Mensagem de sucesso ao alterar um registro
define('MESSAGE_FORBIDDEN'        , 'Acesso n�o autorizado.'); //Mensagem de acesso negado a uma funcionalidade
define('MESSAGE_REMOVE_SUCCESS'   , 'Registro exclu�do com sucesso.'); //Mensagem de �xito ao remover um registro
define('MESSAGE_REMOVE_ERROR'     , 'N�o foi poss��vel excluir o registro.'); //Mensagem de erro ao tentar remover um registro
define('MESSAGE_SEND_ERROR'       , 'N�o foi poss�vel enviar o e-mail para:'); //Mensagem exibida quando acontece erro ao tentar enviar uma mensagem
define('MESSAGE_SEND_SUCCESS'     , 'E-mail enviado para:'); //Mensagem exibida quando acontece erro ao tentar enviar uma mensagem
define('MESSAGE_DIR_NOT_WRITABLE' , 'N�o foi poss�vel escrever o arquivo em:'); //Informa��o exibida quando um arquivo n�o pode ser escrito em um diret�rio
define('MESSAGE_FILE_NOT_COPY'    , 'N�o foi poss�vel copiar o arquivo para:'); //Mensagem exibida quando n�o � poss�vel copiar um arquivo
define('MESSAGE_SAVEONLY'         , 'O regsitro n�o pode ser atualizado'); //Mensagem exibida quando a entidade n�o der suporte a edi��o
define('EXPIRED_TIME'             , '1800'); //

date_default_timezone_set('America/Sao_Paulo');

$standard = PATH_CORE . PATH_FILE_SEPARATOR . "standard";
$handle = opendir($standard);
while (false !== ($file = readdir($handle))) {
  if (!is_dir($file)) {
    require_once $standard . PATH_FILE_SEPARATOR . $file;
  }
}
closedir($handle);

require PATH_CORE . PATH_FILE_SEPARATOR . 'parameters.php';
require PATH_CORE . PATH_FILE_SEPARATOR . 'error.php';

require_once PATH_CORE . PATH_FILE_SEPARATOR . 'Application.class.php';

define('WINDOW_WIDTH' , Cookie::get(Application::$width, '1180')); //Largura padrao dos componentes de tela
define('WINDOW_HEIGHT', Cookie::get(Application::$height, '650')); //Altura padrao dos componentes de tela
 
//Mantem o copy_right atualizado
if (defined('COPY_RIGHT')) {
  define('DEFAULT_COPYRIGHT', COPY_RIGHT . ' - ' . date('Y'));
} else {
  define('DEFAULT_COPYRIGHT', '');
}
