<?php

/**
 *
 * @param <type> $errno
 * @param <type> $errstr
 * @param <type> $errfile
 * @param <type> $errline
 * @param <type> $errcontext
 */
function core_err($errno, $errstr, $errfile = "", $errline = "", $errcontext = "") {

  $ignored[] = "Use of undefined constant landscape - assumed 'landscape'";
  $ignored[] = "Undefined variable: oForm";
  $ignored[] = "Call-time pass-by-reference has been deprecated";
  $available = true;

  $protocolo = false;
  $show = true;
  $debug = false;

  $bt = debug_backtrace(false);
  $sp = 0;
  $err_rastreio = "";
  foreach ($bt as $k => $v) {
    if (isset($v['line']) && isset($v['file'])) {
      $space = str_repeat("&nbsp;", ++$sp);
      if (strlen($err_rastreio) < String::MAX_SIZE) {
        $err_rastreio .= "<div>" . $space . "[" . $v['function'] . "] linha " . $v['line'] . " em " . $v['file'] . "</div>";
        if ($k == 0) {
          if ($errfile === "" and $errline == "") {
            $errfile = $v['file'];
            $errline = $v['line'];
          }
        }
      } else {
        die($err_rastreio);
      }
    }
  }

  $modulo = System::desire('m', 'manager', 'Erro', 'src', false, "");

  if (file_exists($modulo)) {
    if (System::getUser(false)) {

      if (!in_array($errstr, $ignored)) {

        System::desire('c', 'manager', 'Erro', 'src', true, "");
        System::desire('m', 'manager', 'Erro', 'src', true, "");

        System::desire('class', 'resource', 'Browser', 'core', true);

        $client = new Browser();
        $navegador = $client->browser . ", " . $client->fullVersion . ", " . $client->platform . ", " . $client->userAgent;

        $err_descricao = 'PHP - ' . $errfile . ", linha " . $errline;

        $erro = new Erro();
        $erro->set_err_value('err_referencia', $errno);
        $erro->set_err_value('err_descricao', $err_descricao);
        $erro->set_err_value('err_comando', $errstr);
        $erro->set_err_value('err_rastreio', $err_rastreio);
        $erro->set_err_value('err_navegador', $navegador);
        $erro->set_err_value('err_endereco', System::address());
        $erro->set_err_value('err_responsavel', System::getUser());
        $erro->set_err_value('err_criador', System::getUser());

        $erroCtrl = new ErroCtrl(PATH_APP);
        $protocolo = $erroCtrl->addErroCtrl($erro);

        $mensagem = "Informe este protocolo ao solicitar atendimento.";
      } else {
        $available = false;
      }
    }
  } else {
    print "M&oacute;dulo de <b>ERRO</b> desativado";
  }

  if ($available) {
    $debug = MODO_TESTE === "true" ? true : false;
    //$show = MODO_TESTE === "true" ? true : false;
  }

  if ($debug) {
    $item = "err_" . uniqid();
    ?>
    <div class="message-error">
      &nbsp;<b>Modo de Depura&ccedil;&atilde;o ativado</b>: evento disparado na linha <b><?php print $errline; ?></b> em <b><?php print $errfile; ?></b>
      <label class="click" onclick="toggle('<?php print $item; ?>');"><i>Detalhes</i></label>
      <div id="<?php print $item; ?>" style="display: none;">
        <div>&nbsp; - <b>Detalhes do Erro</b>: <br>
          <div style="padding-left: 20px;"><?php print $errstr; ?></div>
            <?php
              if (!is_array($errcontext) and !($errcontext === null)) {
                ?>
                  <div style="padding-left: 20px;">[<?php print $errcontext; ?>]</div>
                <?
              }
              ?>
          </div>
          <div>&nbsp; - <b>Stack Trace</b>: <br><div style="padding-left: 20px;"><?php print $err_rastreio; ?></div></div>
        </div>
      </div>

      <script type="text/javascript">
          /**
           *
           * @param id
           */
          function toggle(id) {
            var item = document.getElementById(id);
            if (item.style.display == 'none') {
              item.style.display = 'block';
            } else {
              item.style.display = 'none';
            }
          }
      </script>
    <?
  }

  if ($protocolo && $show) {
    ?>
      <span class="message-error">
        &nbsp;<label style="font-weight: 400;">Protocolo:</label> <u><?php print $protocolo; ?></u>. <i><?php print $mensagem; ?></i>&nbsp;
      </span>
    <?
  }
  
}

/**
 *
 */
function core_err_fatal() {

  if (@is_array($e = @error_get_last())) {
    $code = isset($e['type']) ? $e['type'] : 0;
    $msg = isset($e['message']) ? $e['message'] : '';
    $file = isset($e['file']) ? $e['file'] : '';
    $line = isset($e['line']) ? $e['line'] : '';
    if ($code > 0) {
      //print "$code, $msg, $file, $line";
      core_err("Fatal Error", $msg, $file, $line, null);
    }
  }
}

set_error_handler('core_err');
register_shutdown_function('core_err_fatal');
error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
?>
