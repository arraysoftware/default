<?php

$user = System::getUser('code', false);

if ($user) {

  require_once System::desire('class', 'resource', 'Screen', 'core', false);

  $screen = new Screen('{project.rsc}');

  $hash = System::request('hash');
  if (!$hash) {
    $hash = md5($user);
  }

  $items = array();
  $items['csl_option'] = array("pk"=>0, "fk"=>0, "id"=>"csl_option", "description"=>"Op��o", "title"=>"", "type"=>"option", "type_content"=>"php,PHP|base64decode,Decode|base64encode,Encode|decrypt,Decrypt|encrypt,Encrypt", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"php", "default_sql"=>"", "update"=>1, "insert"=>1, "line"=>1, );
  $items['csl_code'] = array("pk"=>0, "fk"=>0, "id"=>"csl_code", "description"=>"Fonte", "title"=>"", "type"=>"source-code", "type_content"=>'"top":"0", "height":"0", "width":"0", "mode":"php", "hash":"' . $hash . '"', "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"height: " . (WINDOW_HEIGHT - 30) . "px;", "validate"=>"[E]", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "update"=>1, "insert"=>1, "line"=>1, );

  $target = System::request('t', 'default');
  $process = System::request('p', false);

  if ($process === "true") {
    foreach ($items as $id => $item) {
      $items[$id]['value'] = System::request($id);
    }
    
    $csl_option = $items['csl_option']['value'];
    $csl_code = $items['csl_code'];

    if ($csl_option === 'php') {
      $code = $csl_code['value'];
      $open = String::substring(String::trim($code), 0, 5);
      if ($open === "<?php") {
        $code = '?>' . $code;
      }
      eval($code);
    } else if ($csl_option === 'base64decode') {
      print base64_decode($csl_code['value']);
    } else if ($csl_option === 'base64encode') {
      print base64_encode($csl_code['value']);
    } else if ($csl_option === 'decrypt') {
      print Encode::decrypt($csl_code['value']);
    } else if ($csl_option === 'encrypt') {
      print Encode::encrypt($csl_code['value']);
    }

  } else {

    $libs = array();
    $libs['firepad'] = '1';

    $compact = (MODO_TESTE === "true") ? '0' : '1';
    $js = '&design=main&browser=&login=0&resources=1&load=0&check=1&catch=1&compact=' . $compact . '&resume=0';
    $css = '&design=main&browser=&commom=1&fagoc=1';
    ?>
      <html>
        <head>
          <script type="text/javascript">
            var mxBasePath = 'lib/mxgraph/javascript/src';
            var mxImageBasePath = 'lib/mxgraph/javascript/src/images';
            var CKEDITOR_BASEPATH = '<?php print PAGE_APP;?>lib/ckeditor/';
          </script>

          <?php
            foreach ($libs as $lib => $level) {
              ?>
                <script type="text/javascript" src="<?php print PAGE_LIBRARY;?>?id=fagoc.br&r=js&<?php print $lib;?>=<?php print $level;?>&v=<?php print VERSION;?>&file=<?php print $lib;?>.js"></script>
                <link rel="stylesheet" type="text/css" media="all" href="<?php print PAGE_LIBRARY;?>?id=fagoc.br&r=css&<?php print $lib;?>=<?php print $level;?>&v=<?php print VERSION;?>&file=<?php print $lib;?>.css"/>
              <?php
            }
          ?>

          <script type="text/javascript" src="<?php print PAGE_JAVASCRIPT;?>?id=fagoc.br<?php print $js;?>&v=<?php print VERSION;?>&file=system.js"></script>
          <link rel="stylesheet" type="text/css" media="all" href="<?php print PAGE_CSS;?>?id=fagoc.br<?php print $css;?>&v=<?php print VERSION;?>&file=system.css"/>

          <script type="text/javascript">
            /**
             * 
             * @type type
             */
            system.console = {
              /**
               * 
               * @param {string} target
               * @param {string} page
               * @returns {undefined}
               */
              apply: function(target, page) {
                var csl_process = system.util.getIn('csl_process', target);
                if (csl_process) {
                  csl_process.onclick = function () {
                    this.style.display = 'none';
                    system.console.process(target, page);
                  };
                }
                var csl_clear = system.util.getIn('csl_clear', target);
                if (csl_clear) {
                  csl_clear.onclick = function () {
                    this.style.display = 'none';
                    system.console.clear(target);
                  };
                }
              },
              /**
               * 
               * @param {type} target
               * @param {type} page
               * @returns {undefined}
               */
              process: function(target, page) {
                system.util.toggle('csl-input-' + target, 'csl-output-' + target);
                var csl_clear = system.util.getIn('csl_clear', target);
                if (csl_clear) {
                  csl_clear.style.display = '';
                }
                system.ajax.post('form-' + target, page + 'core/app/console/?action=console', 'target-' + target, true, false, null, null, false, false, 'console');
              },
              /**
               * 
               * @param {type} target
               * @returns {undefined}
               */
              clear: function(target) {
                var csl_process = system.util.getIn('csl_process', target);
                if (csl_process) {
                  csl_process.style.display = '';
                }
                system.util.toggle('csl-input-' + target, 'csl-output-' + target);
              }
            };
          </script>
        
        </head>
        <body onload="system.console.apply('<?php print $target;?>', '<?php print PAGE_APP;?>');" style="background: #fff;">

          <div id="<?php print $target;?>" style="margin: 0 10px 0 10px;">
            <div>
              <a href="<?php print PAGE_APP; ?>core/app/console/?hash=<?php print $hash; ?>" target="_blank">Colaborar</a>
            </div>
            <div id="csl-input-<?php print $target;?>">
              <form id="form-<?php print $target;?>" name="form-<?php print $target;?>" onsubmit="return false;">
                <?php
                  foreach ($items as $item) {
                    ?>
                      <div>
                        <?php
                          $screen->line->printTypeField($item);
                        ?>
                      </div>
                    <?php
                  }
                ?>
                <input type="hidden" id="t" name="t" value="<?php print $target;?>"/>
                <input type="hidden" id="p" name="p" value="true"/>
              </form>
            </div>
            <div id="csl-output-<?php print $target;?>" style="display: none;">
              <div style="margin: 0 0 3px 0;">
                Resultado:
              </div>
              <div id="target-<?php print $target;?>" class="form-text" style="height: <?php print WINDOW_HEIGHT;?>px; overflow: auto;"></div>
            </div>

            <br>
            <div>
              <input type="button" id="csl_process" name="csl_process" class="form-buttom" value="Processar"/>
              <input type="button" id="csl_clear" name="csl_clear" class="form-buttom" value="Voltar" style="display: none;"/>
            </div>
          </div>
        </body>
      </html>

    <?php
  }
  
}