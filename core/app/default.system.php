<?php

/**
 *
 * @param $app
 * @param $user
 * @param $parameters
 */

$app = isset($app) ? $app : "";
$user = isset($user) ? $user : "";

$conected = $user ? "1" : "0";

if($conected){
  printSystemScreen($app, $user);
}

/**
 *
 * @param type $app
 * @param type $user
 */
function printSystemScreen($app, $user){

  System::desire('c', 'manager', 'MenuFuncaoUsuarioTipo', 'src', true, '');

  $screen = new Screen();

  $administracao = array();
  $administracao[] = array('name'=>'Menu', 'modulo'=>'manager', 'entity'=>'Menu', 'action'=>'menu-v-search', 'level'=>'', 'escope'=>'principal', 'class'=>'');
  $administracao[] = array('name'=>'Fun&ccedil;&atilde;o', 'modulo'=>'manager', 'entity'=>'MenuFuncao', 'action'=>'menufuncao-v-search', 'level'=>'', 'escope'=>'principal', 'class'=>'');
  $administracao[] = array('name'=>'Tipo de Usu&aacute;rio', 'modulo'=>'manager', 'entity'=>'UsuarioTipo', 'action'=>'usuariotipo-v-search', 'level'=>'', 'escope'=>'principal', 'class'=>'ux-menu-separator');
  $administracao[] = array('name'=>'Usu&aacute;rio', 'modulo'=>'manager', 'entity'=>'Usuario', 'action'=>'usuario-v-search', 'level'=>'', 'escope'=>'principal', 'class'=>'');
  $administracao[] = array('name'=>'Hist&oacute;rico', 'modulo'=>'manager', 'entity'=>'Historico', 'action'=>'historico-v-list', 'level'=>'', 'escope'=>'principal', 'class'=>'ux-menu-separator');
  $administracao[] = array('name'=>'Erro', 'modulo'=>'manager', 'entity'=>'Erro', 'action'=>'erro-v-list', 'level'=>'', 'escope'=>'principal', 'class'=>'');
  $administracao[] = array('name'=>'Par&acirc;metros', 'modulo'=>'manager', 'entity'=>'Parametros', 'action'=>'parametros-v-list', 'level'=>'', 'escope'=>'principal', 'class'=>'ux-menu-separator');
  $administracao[] = array('name'=>'Atualiza&ccedil;&atilde;o', 'modulo'=>'manager', 'entity'=>'Atualizacao', 'action'=>'atualizacao-v-list', 'level'=>'', 'escope'=>'principal', 'class'=>'');
  ?>
  <body onload="initialize('<?php print $app;?>');">
    <script type="text/javascript">
      /**
       *
       * @param {type} app
       * @returns {undefined}
       */
      function initialize(app) {
        try{
          if(app){
            system.start('4f78086e6a90f4505ddfea43cedb9cad');
          }
        } catch (e) {
          if(console){
            console.log(e);
          }
        }
      }
    </script>

    <div id="header">
      <span style="float: right"><a href="<?php print PAGE_LOGOUT;?>">Sair</a></span>
    </div>

    <div id="body">
      <div id="ux-menu-location">

        <ul id="simple-horizontal-menu" style="display: none;">
          <?php
            if($user->usu_admin === DEFAULT_USER_ADMIN){
              ?>
                <li>
                  <a href="javascript:void(0);">Administra&ccedil;&atilde;o</a>
                  <ul>
                    <?php
                      foreach($administracao as $funcao){
                        $name = $funcao['name'];
                        $rotule = Encode::encrypt($name);
                        $module = $funcao['modulo'];
                        $entity = $funcao['entity'];
                        $action = $funcao['action'];
                        $level = Encode::encrypt($funcao['level']);
                        $escope = $funcao['escope'];
                        $class = $funcao['class'];
                        ?>
                        <li>
                          <a href="javascript:void(0);" onclick="<?php $screen->printScreenMenuAction($app, $name, $rotule, $module, $entity, $action, $level, $escope); ?>" class="<?php print $class; ?>">
                            <?php System::printText($name, true); ?>
                          </a>
                        </li>
                        <?
                      }
                    ?>
                  </ul>
                </li>
              <?
            }
          ?>

          <?php
            $menuFuncaoUsuarioTipoCtrl = new MenuFuncaoUsuarioTipoCtrl(PATH_APP);

            $menus = $menuFuncaoUsuarioTipoCtrl->getOptionMenuFuncaoUsuarioTipoCtrl(Encode::decrypt($user->usu_tipo));
            if(count($menus) > 0){
              foreach($menus as $item){
                $menu = $item['menu'];
                ?>
                  <li>
                    <a href="javascript:void(0);"><?php print System::encode($menu->get_men_value('men_descricao'), UTF8_PRINT); ?></a>
                    <ul>
                      <?php
                        $menuFuncaos = $item['option'];
                        if(count($menuFuncaos) > 0){
                          $menuFuncao = new MenuFuncao();
                          foreach($menuFuncaos as $menuFuncao){
                            $name = $menuFuncao->get_mef_value('mef_descricao');
                            $rotule = Encode::encrypt($name);
                            $module = $menuFuncao->get_mef_value('mef_module');
                            $entity = $menuFuncao->get_mef_value('mef_entity');
                            $action = $menuFuncao->get_mef_value('mef_action');
                            $level = Encode::encrypt($menuFuncao->get_mef_value('mef_codigo'));
                            $escope = 'principal';
                            $class = ($menuFuncao->get_mef_value('mef_separator')) ? 'ux-menu-separator' : '';
                            ?>
                              <li>
                                <a href="javascript:void(0);" onclick="<?php $screen->printScreenMenuAction($app, $name, $rotule, $module, $entity, $action, $level, $escope); ?>" class="<?php print $class; ?>">
                                  <?php print System::encode(System::printText($name), UTF8_PRINT); ?>
                                </a>
                              </li>
                            <?php
                          }
                        }
                      ?>
                    </ul>
                  </li>
                <?
              }
            }
          ?>
        </ul>

      </div>

    <div id="ux-content-principal"></div>

      <div style="display: none; visibility: hidden;">
        <div id="ux-remove"></div>
        <div id="ux-notify"></div>
        <div id="ux-dock-principal" style="display: none; visibility: hidden;"></div>
        <div id="ux-notification-principal" style="display: none; visibility: hidden;"></div>
        <div id="ux-notification-principal_bkp" style="display: none; visibility: hidden;"></div>
      </div>

    </div>

    <form id="form" name="form" action="return false;" onsubmit="return false;">
      <input type="hidden" name="system_ppid_1" id="system_ppid_1" value="<?php print $user->usu_codigo; ?>"/>
      <input type="hidden" name="system_ppid_2" id="system_ppid_2" value="<?php print $user->usu_nome; ?>"/>
    </form>

  </body>
  <?
}
?>
