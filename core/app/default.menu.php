<?php
/**
 * @param $app
 * @param $name
 * @param $rotule
 * @param $module
 * @param $entity
 * @param $action
 * @param $level
 * @param $escope
 */
$app = isset($app) ? $app : "";
$name = isset($name) ? $name : "";
$rotule = isset($rotule) ? $rotule : "";
$module = isset($module) ? $module : "";
$entity = isset($entity) ? $entity : "";
$action = isset($action) ? $action : "";
$level = isset($level) ? $level : "";
$escope = isset($escope) ? $escope : "";
?>
  system.tab.addTab('form', 'src/<?php print $module;?>/view/<?php print $entity;?>.view.php?action=<?php print $action;?>&rotule=<?php print $rotule;?>&l=<?php print $level;?>','<?php print $name;?>','<?php print $escope;?>');
<?