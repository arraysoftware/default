<?php

/**
 *
 * @param $app
 * @param $message
 */
$app = isset($app) ? $app : "";
$mensagem = isset($mensagem) ? base64_decode($mensagem) : "";

define('PAGE_MENSAGEM', $mensagem);

System::desire('c', 'site', 'Widget', 'src');
System::desire('c', 'site', 'WidgetSegmento', 'src');
System::desire('c', 'site', 'WidgetSegmentoItem', 'src');

$widgetCtrl = new WidgetCtrl(PATH_APP);
$widgetSegmentoCtrl = new WidgetSegmentoCtrl(PATH_APP);
$widgetSegmentoItemCtrl = new WidgetSegmentoItemCtrl(PATH_APP);

?>

<body onload="javascript: initialize();">
  <script type="text/javascript">
    function login() {
      window.document.form.submit();
    }
  </script>

  <script type="text/javascript">
    function initialize() {
      <?php
        if(!PAGE_MOBILE){
          ?>
            var body = document.getElementsByTagName("body")[0];

            var b = 245;
            var t = 145;
            var h = body.scrollHeight;

            var height = h - (b + t);

            var content = document.getElementById('content');
            if(content){
              content.style.minHeight = height+'px';
            }

            var login = document.getElementById('login');
            if(login){
              if(login.className === 'form-input login'){
                login.focus();
              }
            }
            var op = {vs:{i: 9, f: 11, o: 10.6, s: 5, n: 10, c: 12}};
            system.browser.verify(op, null, '#top-menu{top: 40px;} #top-menu-item{top: 105px;}');
          <?
        }
      ?>
    }
  </script>
  <?php

    $segmentos = $widgetSegmentoCtrl->getWidgetSegmentoCtrl("wis_posicao > 0", "", "wis_posicao, wis_ordem", "", "", false);
    if(is_array($segmentos)){
      foreach($segmentos as $widgetSegmento){
        ?>
          <div id="<?php print $widgetSegmento->get_wis_value('wis_nome');?>">
            <?php
              $widgets = $widgetSegmentoItemCtrl->getWidgetSegmentoItemCtrl("wsi_cod_WIDGET_SEGMENTO = '".$widgetSegmento->get_wis_value('wis_codigo')."'", "", "wsi_ordem", "", "", false);
              if(is_array($widgets)){
                foreach($widgets as $widget){
                  $ws = $widgetCtrl->getWidgetCtrl("wid_codigo = '".$widget->get_wsi_value('wsi_cod_WIDGET')."'", "", "", "0", "1", "", "", false);
                  if(is_array($ws)){
                    $w = $ws[0];
                    ?>
                      <div id="<?php print $w->get_wid_value('wid_nome');?>" class="<?php print $w->get_wid_value('wid_class');?> content-widget <?php print PAGE_SIDE_BAR;?>-side">
                        <?php $widgetCtrl->printWidgetCtrl($w);?>
                      </div>
                    <?php
                  }
                }
              }
            ?>
          </div>
        <?
      }
    }else{
      ?>
        <div id="main-login">
          <h1 id="logo">
            <img alt="" src="setup/images/logo.png">
          </h1>
          <div id="login-content">
            <form action="<?php print PAGE_APP;?>system" method="post" name="form" id="form">
              <div style="display:block;">
                <div id="login-message"><?php print PAGE_MENSAGEM;?></div>
                <div class="login-line">
                  <input id="login" name="login" value="" type="text" class="form-input login" placeholder="Login" accesskey="L" tabindex="1">
                </div>
                <div class="login-line">
                  <input id="password" name="password" value="" type="password" class="form-input password" placeholder="Senha" onkeypress="" accesskey="P" tabindex="2">
                  <input type="submit" value="Entrar" class="form-buttom-login" accesskey="E" tabindex="3">
                </div>

                <table width="100%" cellpadding="0" cellspacing="0">
                  <tbody><tr>
                      <td align="left" valign="middle" width="20">
                        <input name="remember" id="remember" type="checkbox" class="form-checkbox" onkeypress="" accesskey="C" tabindex="4">
                      </td>
                      <td align="left" valign="middle">
                        <label for="remember" class="form-checkbox" onkeypress="">
                          <b>Continuar conectado</b>
                        </label>
                      </td>
                      <td align="right" valign="middle">
                        <label>
                          <a href="javascript:void(0);" onclick="">Esqueceu a senha?</a>
                        </label>
                      </td>
                    </tr>
                  </tbody></table>
              </div>
            </form>
          </div>
        </div>
      <?
    }
  ?>
</body>