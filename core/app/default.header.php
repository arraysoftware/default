<?php

/**
 *
 * @param $app
 * @param $user
 * @param $parameters
 */

$app = isset($app) ? $app : "";
$user = isset($user) ? $user : "";
$parameters = isset($parameters) ? $parameters : "";

$conected = $parameters->lib['system'];

$uri = System::request('uri', 'home');

$request = explode("/", $uri);

$modulo = "";
$categoria = "";
$secao = "";
$conteudo = "";
$artigo = "";

if(count($request) > 0){
  $modulo = $request[0];
  if(count($request) > 1){
    $categoria = $request[1];
    if(count($request) > 2){
      $secao = $request[2];
      if(count($request) > 3){
        $conteudo = $request[3];
        if(count($request) > 4){
          $artigo = $request[4];
        }
      }
    }
  }
}

define('PAGE_MODULO', $modulo);
define('PAGE_CATEGORIA', $categoria);
define('PAGE_SECAO', $secao);
define('PAGE_CONTEUDO', $conteudo);
define('PAGE_ARTIGO', $artigo);

System::desire('m', 'site', 'ConteudoArtigo', 'src');
System::desire('c', 'site', 'ConteudoArtigo', 'src');

$conteudoArtigoCtrl = new ConteudoArtigoCtrl(PATH_APP);

$content = "";
$query = "false";
if(PAGE_ARTIGO){
  $query = "cta_slug = '".PAGE_ARTIGO."'";
  $content = 5;
}else if(PAGE_CONTEUDO){
  $query = "cta_slug = '".PAGE_CONTEUDO."'";
  $content = 4;
}else if(PAGE_SECAO){
  $query = "cta_slug = '".PAGE_SECAO."'";
  $content = 3;
}else if(PAGE_CATEGORIA){
  $query = "cta_slug = '".PAGE_CATEGORIA."'";
  $content = 2;
}else if(PAGE_MODULO){
  $query = "cta_slug = '".PAGE_MODULO."'";
  $content = 1;
}

$erro = "";
$conteudoArtigo = new ConteudoArtigo();
$artigos = $conteudoArtigoCtrl->getConteudoArtigoCtrl($query, "", "", "0", "1", false);
if(is_array($artigos)){
  $conteudoArtigo = $artigos[0];
  if($conteudoArtigo->get_cta_value('cta_publicar') === "0"){
    $erro = "500";
  }else if($conteudoArtigo->get_cta_value('cta_publicar') === "2"){
    if(!PAGE_CONECTED){
      $erro = "403";
    }
  }
}else{
  $erro = "404";
}

if($erro){
  $artigos = $conteudoArtigoCtrl->getConteudoArtigoCtrl("cta_slug = '".$erro."'", "", "", "0", "1", false);
  if(is_array($artigos)){
    $conteudoArtigo = $artigos[0];
  }
}

define('PAGE', serialize($conteudoArtigo));
define('PAGE_SIDE_BAR', $conteudoArtigo->get_cta_value('cta_side_bar'));
define('PAGE_CONTENT', $content);
define('PAGE_LOCATION', $conteudoArtigo->get_cta_value('cta_titulo'));

$conteudoArtigoCtrl->hitConteudoArtigoCtrl($conteudoArtigo, false);

$url = PAGE_APP.$uri;
$image = is_file(PATH_APP.$conteudoArtigo->get_cta_value('cta_miniatura')) ? PAGE_APP.$conteudoArtigo->get_cta_value('cta_miniatura') : PAGE_IMAGE."logo.png?".uniqid();

?>

<head>
  <title><?php print SHORT;?> - <?php print $conteudoArtigo->get_cta_value('cta_titulo'); ?></title>
  <link rel="shortcut icon" href="<?php print PAGE_APP; ?>favicon.ico?<?php print uniqid();?>" type="image/ico"/>

  <meta name="viewport" content="width=320,initial-scale=0.9,maximum-scale=1.0,user-scalable=0" />

  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

  <meta property="og:title" content="<?php print $conteudoArtigo->get_cta_value('cta_titulo'); ?>"/>
  <meta property="og:type" content="website"/>
  <meta property="og:url" content="<?php print $url;?>"/>
  <meta property="og:image" content="<?php print $image;?>"/>
  <meta property="og:site_name" content="<?php print SHORT;?>"/>
  <meta property="og:description" content="<?php print $conteudoArtigo->get_cta_value('cta_resumo'); ?>"/>

  <meta name="generator" content=""/>
  <meta name="Author" content="<?php print COPY_RIGHT;?>"/>

  <meta name="description" content="<?php print COMPANY;?>"/>
  <meta name="keywords" content=""/>
  <meta name="Distribution" content="global"/>
  <meta name="revisit-after" content="3 days"/>
  <meta name="robots" content="follow,index"/>

  <script type="text/javascript">
    var configuration = new Object();
    configuration.title = '<?php print TITLE;?>';
    configuration.host = '<?php print PAGE_APP;?>';

    var CKEDITOR_BASEPATH = '<?php print PAGE_LIBRARY;?>ckeditor/';
  </script>

  <?php
    $version = unserialize(VERSION_LIBRARY);
    foreach($parameters->lib as $lib=>$active){
      if($active){
        $v = $version[$lib];
        ?>
          <script type="text/javascript" src="<?php print PAGE_LIBRARY;?>?id=<?php print HOST;?>&r=js&<?php print $lib;?>=<?php print $active;?>&v=<?php print $v;?>&file=<?php print $lib;?>.js"></script>
          <link rel="stylesheet" type="text/css" media="all" href="<?php print PAGE_LIBRARY;?>?id=<?php print HOST;?>&r=css&<?php print $lib;?>=<?php print $active;?>&v=<?php print $v;?>&file=<?php print $lib;?>.css" />
        <?
      }
    }

    $v = isset($version['system']) ? $version['system'] : VERSION;
  ?>

  <link rel="stylesheet" type="text/css" media="all" href="<?php print PAGE_LIBRARY;?>resources/css/?s=<?php print $conected;?>&v=<?php print $v;?>&file=system.css" />
  <script type="text/javascript" src="<?php print PAGE_LIBRARY;?>resources/js/?s=<?php print $conected;?>&v=<?php print $v;?>&file=system.js"></script>

  <link rel="stylesheet" type="text/css" media="all" href="<?php print PAGE_CSS;?>?c=<?php print $conected;?>&v=<?php print VERSION;?>&m=<?php print PAGE_MOBILE;?>&file=custom.css" />
  <script type="text/javascript" src="<?php print PAGE_JAVASCRIPT;?>?c=<?php print $conected;?>&v=<?php print VERSION;?>&file=custom.js"></script>

  <script type="text/javascript">
    if(system.util){
      system.util.doLayout();
    }
  </script>

</head>
