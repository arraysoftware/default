<?php

?>
<!DOCTYPE html>
<html>
  <head>

    <title><?php print TITLE;?> | <?php print COMPANY;?></title>

    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="<?php Application::link();?>/images/favicon.ico">

    <!-- Bootstrap core CSS -->
    <link href="<?php Application::link();?>/theme/default/static/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!--<link href="<?php Application::link();?>/theme/default/static/lib/bootstrap/plugins/bootstrap-table/css/bootstrap-table.min.css" rel="stylesheet">-->
    <link href="<?php Application::link();?>/theme/default/static/lib/bootstrap/plugins/jquery-bootgrid/css/jquery-bootgrid.css" rel="stylesheet">
    <link href="<?php Application::link();?>/theme/default/static/lib/jquery/plugins/select2/select2.css" rel="stylesheet">
    <link href="<?php Application::link();?>/theme/default/static/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="http://fonts.googleapis.com/css?family=Lato:100,300,400" rel="stylesheet" type="text/css">

    <link rel="stylesheet" type="text/css" href="<?php Application::link();?>/css/animate.css" />
    <link rel="stylesheet" type="text/css" href="<?php Application::link();?>/css/elements.css" />
    <link rel="stylesheet" type="text/css" href="<?php Application::link();?>/css/custom.css" />

    <?php
      if ($admin) {
        ?>
          <link href="<?php Application::link();?>/css/admin.css" rel="stylesheet" type="text/css" />
          <link href="<?php Application::link();?>/css/admin.custom.css" rel="stylesheet" type="text/css" />
          <link href="<?php Application::link();?>/css/ionicons.css" rel="stylesheet" type="text/css" />
          <link href="<?php Application::link();?>/css/application.css" rel="stylesheet" type="text/css" />
        <?php
      }
    ?>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <style>
      li.account .avatar {
        background: url(<?php Application::link();?>/images/<?php print Application::get('avatar');?>);
        background-size: 100px 100px;
      }
    </style>
  </head>
  <body class="skin-black fixed">