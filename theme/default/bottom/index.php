<?php

$c = Json::encode(Application::$cookies);
$r = Json::encode(Application::$routes);

$cookies = $c ? $c : "{}";
$routes = $r ? $r : "{}";

?>
    <script src="<?php Application::link();?>/theme/default/static/lib/jquery/js/jquery.min.js" type="text/javascript"></script>
    <script src="<?php Application::link();?>/theme/default/static/lib/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="<?php Application::link();?>/theme/default/static/lib/jquery/plugins/select2/select2.js" type="text/javascript"></script>
    <script src="<?php Application::link();?>/theme/default/static/lib/jquery/plugins/select2/select2_locale_pt-BR.js" type="text/javascript"></script>

    <script src="<?php Application::link();?>/js/custom.js" type="text/javascript"></script>
    <script src="<?php Application::link();?>/js/scrolltopcontrol.js" type="text/javascript"></script>

    <script src="<?php Application::link();?>/js/application.js" type="text/javascript"></script>
    <script src="<?php Application::link();?>/js/listenner.js" type="text/javascript"></script>
    
    <?php
      if ($admin) {
        ?>
          <script src="<?php Application::link();?>/js/admin.js" type="text/javascript"></script>
          <script src="<?php Application::link();?>/theme/default/static/lib/jquery/plugins/jquery.currency.js" type="text/javascript"></script>
          <!--<script src="<?php Application::link();?>/theme/default/static/lib/bootstrap/plugins/bootstrap-table/js/bootstrap-table.min.js" type="text/javascript"></script>-->
          <script src="<?php Application::link();?>/theme/default/static/lib/bootstrap/plugins/jquery-bootgrid/js/jquery-bootgrid.js" type="text/javascript"></script>
        <?php
      }
    ?>
     
    <script type="text/javascript">
       Application.init('<?php print HOST; ?>', <?php print $cookies;?>, <?php print $routes;?>);
    </script>

  </body>
</html>