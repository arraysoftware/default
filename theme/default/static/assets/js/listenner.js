
/**
 * 
 * @param {type} jQuery
 * 
 * @returns {undefined}
 */
(function (jQuery) {
  jQuery(document).on('click', function(e) {
    var $element = jQuery(e.target);
    var a = $element.attr('data-action');
    var v = $element.attr('data-value');
    switch(a) {
      case 'open-menu':
        Application.menu.open($element, v);
        break;
      case 'view':
        Application.manager.view($element, v);
        break;
      case 'post':
        Application.manager.post($element, v);
        break;
      case 'alias':
        Application.manager.alias($element, v);
        break;
    }
  });
  jQuery(document).on('keyup', function(e) {
    var $element = jQuery(e.target);
    var a = $element.attr('data-action');
    var v = $element.attr('data-value');
    switch(a) {
      case 'search-menu':
        Application.menu.search($element);
        break;
    } 
  });
  jQuery(document).on('resize', function() {
      Application.screen.resize();
  });
})(jQuery);
