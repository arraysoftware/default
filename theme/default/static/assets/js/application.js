
var Application = {
  /**
   * 
   * @type type
   */
  config: {
    key: '4f78086e6a90f4505ddfea43cedb9cad',
    target: 'content-target',
    loading: '<div class="content-loading">&nbsp;</div>',
    toggleNavigation: '.navbar-btn.sidebar-toggle',
    widthToggleNavigation: 990,
    system: 'system',
    path: '/',
    host: window.location.host,
    dataAction: 'view',
    cookies: {
      token: 'token',
      session: 'session',
      remember: 'remember',
      width: 'width',
      height: 'height'
    },
    content: {
      title: 'content-title',
      description: 'content-description',
      breadcrumb: 'content-breadcrumb'
    },
    grid: {
      all: 'Todos',
      infos: 'Exibindo de {{ctx.start}} at� {{ctx.end}} de {{ctx.total}} registros',
      loading: '<div class="content-loading">&nbsp;</div>',
      noResults: 'Nenhum resultado encontrado!',
      refresh: 'Recarregar',
      search: 'Pesquisar',
      pageSize: 25
    }
  },
  /**
   * 
   * @param {type} host
   * @param {type} cookies
   * @param {type} routes
   * 
   * @returns {undefined}
   */
  init: function(host, cookies, routes) {

    Application.config.host = host;
    Application.config.cookies = cookies;
    Application.menu.routes = routes;
    Application.screen.resize();

    var path = window.location.pathname.replace('/' + Application.config.system + '/', '');
    if (path) {
      Application.load(path);
    }

    window.onpopstate = function(event) {
      Application.manager.rebrowse(event.state);
    };

  },
  /**
   * 
   * @param {type} path
   * @returns {undefined}
   */
  load: function (path) {
    
  },
  /**
   * 
   * @type type
   */
  ajax: {
    /**
     * 
     * @param {type} url
     * @param {type} data
     * @param {type} sucess
     * @param {type} error
     * @returns {undefined}
     */
    open: function(url, data, sucess, error) {

      jQuery.ajax({
        url: url,
        type: 'POST',
        dataType: 'text',
        data: data,
        success: function(xml, text, xhr) {
          if (text === 'success') {
            if (typeof sucess === 'function') {
              window.setTimeout(sucess, 1, xml);
            }
          }
        },
        error: function(xhr, text, thrown) {
          if (typeof error === 'function') {
            window.setTimeout(error, 1, xhr, text, thrown);
          }
        },
        beforeSend: function(xhr) {
          xhr.setRequestHeader('System', Application.config.key);
        }

      });
    }
  },
  /**
   * 
   * @type type
   */
  menu: {
    /**
     * 
     * @type Object|@call;jQuery
     */
    content: new Object(),
    /**
     * 
     * @type Array|type
     */
    routes: [],
    /**
     * 
     * @param {type} $a
     * @param {type} menu
     * @returns {undefined}
     */
    open: function($a, menu) {

      if (menu) {

        var identification = null;
        if (typeof menu === 'string') {
          identification = Application.menu.routes[menu];
        }

        if (identification) {

          var
            url = Application.manager.parseUrl(identification, 'view', '-v-', identification.operation)
            , param = 't=' + Application.config.target + '&l=' + identification.level
            , target = ''
            , label = ''
            , type = ''
            , action = '';

          jQuery('#' + Application.config.content['title']).html(identification.mef_descricao);
          jQuery('#' + Application.config.content['description']).html('');
          jQuery('#' + Application.config.content['breadcrumb']).html('<li><i class="fa fa-dashboard"></i> Home</li><li>' + identification.men_descricao + '</li><li class="active">' + identification.mef_descricao + '</li>');

          if (jQuery(window).width() < Application.config.widthToggleNavigation) {
            jQuery(Application.config.toggleNavigation).click();
          }

          target = Application.config.target;
          label = identification.mef_descricao;
          type = 'view';
          action = identification.tag + '/' + identification.operation;
          
          Application.manager.browse(url, param, target, label, type, action);
        }

      }

    },
    /**
     * 
     * @param {type} $element
     * @returns {undefined}
     */
    search: function($element) {

      var filename = $element.val();

      var $list = jQuery('.sidebar-menu .function');
      $list.each(function() {
        var $function = jQuery(this);
        if (filename.length > 3) {
          $function.hide();
          if ($function.find("a:contains('" + filename + "')").length > 0) {
            $function.show();
            $function.parent().show();
          }
        } else {
          $function.show();
        }
      });
    }

  },
  /**
   * 
   * @type type
   */
  tab: {
    
    tabs: 0,

    add: function() {
      return Application.tab.tabs++;
    }

  },
  /**
   * 
   * @type type
   */
  cookie: {
    /**
     * 
     * @type String
     */
    EPOCH: 'Thu, 01-Jan-1970 00:00:01 GMT',
    /**
     * 
     * @type Number
     */
    RATIO: 1000 * 60 * 60, //hora
    /**
     * 
     * @type Array
     */
    KEYS: ['expires', 'path', 'domain'],
    /**
     * 
     * @type type
     */
    esc: escape,
    un: unescape,
    doc: document,
    credits: 'Easy Cookie - Copyright (C) 2007 Paul Duncan <pabs@pablotron.org>',
    version: '0.2.1',
    /**
     * @param name
     * @param value
     * @param time (horas)
     */
    set: function(name, value, time) {
      var expires = "";
      if (time) {
        expires = time;
      }
      return this._set(name, value, {
        expires: expires,
        path: Application.config.path,
        domain: Application.config.host
      });
    },
    alive: function() {
      var k = '__EC__',
              v = new Date();
      // generate test value
      v = v.toGMTString();
      // set test value
      this.set(k, v);
      // return cookie test
      this.enabled = (this.remove(k) == v);
      return this.enabled;
    },
    /*
     * .set(
     *   @param key ,// name
     *   @param value {// value
     *   @param expires: 13,// expires in 13 days
     *   @param domain: 'foo.example.com',// restrict to given domain
     *   @param path: '/some/path',// restrict to given path
     *   @param secure: true // secure cookie only
     * });
     *
     */
    _set: function(key, val /*, opt */) {
      var opt = (arguments.length > 2) ? arguments[2] : {}, now = this._now(), expire_at, cfg = {};
      var expiry = " - ";
      if (opt.expires) {
        opt.expires *= this.RATIO;
        cfg.expires = new Date(now.getTime() + opt.expires);
        cfg.expires = cfg.expires.toGMTString();
        expiry = now.getTime() + opt.expires;
      }
      var keys = ['path', 'domain', 'secure'];
      for (var i = 0; i < keys.length; i++)
        if (opt[keys[i]])
          cfg[keys[i]] = opt[keys[i]];
      var r = this._cookify(key, val, cfg);
      this.doc.cookie = r;

      return expiry;
    },
    has: function(key) {
      key = this.esc(key);
      var c = this.doc.cookie,
              ofs = c.indexOf(key + '='),
              len = ofs + key.length + 1,
              sub = c.substring(0, key.length);
      return ((!ofs && key != sub) || ofs < 0) ? false : true;
    },
    get: function(key) {
      key = this.esc(key);
      var c = this.doc.cookie,
              ofs = c.indexOf(key + '='),
              len = ofs + key.length + 1,
              sub = c.substring(0, key.length),
              end;
      if ((!ofs && key != sub) || ofs < 0)
        return null;
      end = c.indexOf(';', len);
      if (end < 0)
        end = c.length;

      return this.un(c.substring(len, end));
    },
    remove: function(k) {
      var r = this.get(k),
              opt = {
                expires: this.EPOCH
              };
      this.doc.cookie = this._cookify(k, '', opt);
      return r;
    },
    keys: function() {
      var c = this.doc.cookie,
              ps = c.split('; '),
              i, p, r = [];
      for (i = 0; i < ps.length; i++) {
        p = ps[i].split('=');
        r.push(this.un(p[0]));
      }
      return r;
    },
    all: function() {
      var c = this.doc.cookie,
              ps = c.split('; '),
              i, p, r = [];
      for (i = 0; i < ps.length; i++) {
        p = ps[i].split('=');
        r.push([this.un(p[0]), this.un(p[1])]);
      }
      return r;
    },
    _now: function() {
      var r = new Date();
      r.setTime(r.getTime());
      return r;
    },
    _cookify: function(c_key, c_val /*, opt */) {
      var i, key, val, r = [],
              opt = (arguments.length > 2) ? arguments[2] : {};
      r.push(this.esc(c_key) + '=' + this.esc(c_val));
      for (i = 0; i < this.KEYS.length; i++) {
        key = this.KEYS[i];
        if (val = opt[key])
          r.push(key + '=' + val);
      }
      if (opt.secure)
        r.push('secure');

      return r.join('; ');
    }

  },
  /**
   * 
   * @type type
   */
  screen: {
    /**
     * 
     * @returns {undefined}
     */
    resize: function() {

      var height = (jQuery(window).height()) - 60;
      var width = jQuery(window).width();

      jQuery('section.sidebar').css("height", height + "px");
      jQuery('aside.right-side').css("height", height + "px");

      Application.cookie.set(Application.config.cookies.height, height);
      Application.cookie.set(Application.config.cookies.width, width);
    },
    /**
     * 
     * @returns {undefined}
     */
    toggle: function() {
      var lenght = arguments.length;
      for (var i = 0; i < lenght; i++) {
        var el = arguments[i];
        if (typeof el === 'string') {
          el = '#' + el;
        }
        try {
          jQuery(el).toggle();
        } catch (e) {
          
        }
      }
    }

  },
  /**
   * 
   * @type type
   */
  string: {
    /**
     * 
     * @param {type} str
     * @returns {unresolved}
     */
    addslashes: function(str) {

      str = str.replace(/\\/g, '\\\\');
      str = str.replace(/\'/g, '\\\'');
      str = str.replace(/\"/g, '\\"');
      str = str.replace(/\0/g, '\\0');

      return str;
    },
    /**
     * 
     * @param {type} str
     * @returns {unresolved}
     */
    stripslashes: function(str) {

      str = str.replace(/\\'/g, '\'');
      str = str.replace(/\\"/g, '"');
      str = str.replace(/\\0/g, '\0');
      str = str.replace(/\\\\/g, '\\');

      return str;
    }
  },
  /**
   * 
   * @type type
   */
  manager: {
    /**
     * 
     * @param {type} target
     * @param {type} html
     * @returns {undefined}
     */
    html: function(target, html) {
      jQuery('#' + target).html(html);
    },
    /**
     * 
     * @param {type} url
     * @param {type} param
     * @param {type} target
     * @param {type} label
     * @param {type} type
     * @param {type} action
     * @returns {undefined}
     */
    browse: function(url, param, target, label, type, action) {

      if (type === 'view') {
        Application.manager.html(target, Application.config.loading);
      }

      Application.ajax.open(url, param,
          function(response) {
            if (type === 'view') {
              Application.manager.html(target, response);
              if (action) {
                var data = {
                  url: url
                  , param: param
                  , target: target
                  , label: label
                  , type: type
                  , action: action
                };
                window.history.pushState(
                  data
                  , label
                  , '//' + window.location.host + '/' + Application.config.system + '/' + action
                );
              }
            } else {
              try {
                window.setTimeout(execute);
              } catch (e) {

              }
            }
          },
          function(xhr, text, thrown){

          }
      );
    },
    /**
     * 
     * @param {type} page
     * @returns {undefined}
     */
    rebrowse: function(page) {
      if (page) {
        Application.manager.browse(page.url, page.param, page.target, page.label, page.type);
      }
    },
    /**
     * 
     * @param {type} action
     * @param {type} layer
     * @param {type} separator
     * @param {type} operation
     * 
     * @returns {String}
     */
    parseUrl: function(action, layer, separator, operation) {
      var op = operation ? '&operation=' + operation : '';
      return '//'
              + window.location.host + '/'
              + 'src'
              + '/' + action.module + '/'
              + layer + '/'
              + action.entity + '.' + layer + '.php?'
              + 'action=' + action.tag + separator + 'operation' + op;
    },
    /**
     * 
     * @param {type} identification
     * @param {type} type
     * @param {type} separator
     * @param {type} label
     * @param {type} operation
     * @param {type} prefix
     * 
     * @returns {undefined}
     */
    go: function($element, type, layer, separator, operation, prefix) {

      var $form = $element.parents('form');
      var reference = Application.manager.parseReference($element)
        , identification = jQuery.parseJSON($form.find('#i').val())
        , interfaces = ''
        , action = ''
        , url = ''
        , label = ''
        , target = ''
        , custom = ''
        , param = ''
        , vetor = '';

      if ((!reference) && (type === 'view')) {
        interfaces = $form.serialize();
      } else if ((!reference) && (type === 'post')) {
        interfaces = $form.serialize();
      } else if ((reference) && (type === 'view')) {
        interfaces = $form.find('.form-values :input').serialize();
      } else if (type === 'alias') {
        operation = '';
        interfaces = $form.serialize();
      }

      if (type === 'view') {
        prefix = '';
        action = identification.tag + '/' + operation;
      }
      
      if (interfaces) {
        interfaces = '&' + interfaces;
      }
      if (reference) {
        reference = '&' + reference;
        vetor = reference.split('=');
        if (vetor.length > 1 && action) {
          action = action + '/' + vetor[1];
        }
      }
      if ($element.attr('data-custom')) {
        custom = '&' + $element.attr('data-custom');
      }

      url = Application.manager.parseUrl(identification, layer, separator, operation);
      param = 't=' + identification.target + '&l=' + identification.level + interfaces + reference + custom;
      target = prefix + identification.target;
      label = identification.label;

      Application.manager.browse(url, param, target, label, type, action);
    },
    /**
     * 
     * @param {type} $element
     * @returns {String}
     */
    parseReference: function($element) {
      var reference = '', r;
      r = $element.attr('data-reference');
      if (r) {
        reference = r;
      }
      return reference;
    },
    /**
     * 
     * @param {type} v
     * @returns {undefined}
     */
    view: function($button, operation) {
      Application.manager.go($button, 'view', 'view', '-v-', operation);
    },
    /**
     * 
     * @param {type} v
     * @returns {undefined}
     */
    post: function($button, operation) {
      Application.manager.go($button, 'post', 'post', '-p-', operation, 'message-');
    },
    /**
     * 
     * @param {type} v
     * @returns {undefined}
     */
    alias: function($button, operation) {
      Application.manager.go($button, 'alias', 'post', '-p-', operation, 'message-');
    }

  },
  storage: {
    /**
     * 
     * @param {type} table
     * @returns {DOMString}
     */
    select: function(table) {
      return JSON.parse(window.localStorage.getItem(table));
    },
    /**
     * 
     * @param {type} table
     * @param {type} data
     * @returns {undefined}
     */
    insert: function(table, data) {
      return window.localStorage.setItem(table, JSON.stringify(data));
    },
    /**
     * 
     * @param {type} table
     * @param {type} next
     * 
     * @returns {undefined}
     */
    update: function (table, next) {
      var previous = Application.storage.select(table);
      for(var index in next) { 
        if (next.hasOwnProperty(index)) {
          previous[index] = next[index];
        }
      }
      return Application.storage.insert(table, previous);
    },
    /**
     * 
     * @param {type} table
     * @param {type} data
     * @returns {DOMString}
     */
    create: function(table, data, update) {
      var create = Application.storage.select(table);
      if (create === null) {
        Application.storage.insert(table, data);
        create = data;
      } else if (update) {
        Application.storage.update(table, data);
      }
      return create;
    }

  },
  /**
   * 
   * @type type
   */
  form: {
    grids: new Array(),
    combos: new Array(),

    /**
     * 
     * @param {type} target
     * @param {type} url
     * @param {type} post
     * @param {type} tag
     * 
     * @returns $element
     */
    createGrid: function(target, url, post, tag) {

      var $grid = jQuery('#grid-' + target)
        , pageSize = Application.config.grid.pageSize
        , start = 0
        , settings = new Object()
        , $toolbar;
      
      $grid.addClass('table table-condensed table-hover table-striped');
      
      if (tag) {
        settings = Application.storage.create(tag, {
          pageSize: pageSize
          , start: start
        });
        pageSize = settings.pageSize;
        start = settings.start;
      }

      $grid = $grid.bootgrid({
        ajax: true
        , target: target
        , tag: tag
        , start: start
        , navigation: 3
        , padding: 2
        , columnSelection: 2
        , pageSize: pageSize
        , rowCount: [5, 10, 15, 25, 50, 100, 200, -1]
        , selection: true
        , multiSelect: true
        , sorting: true
        , multiSort: true
        , post: function () {
          return post;
        }
        , url: url
        , formatters: {
          link: function(column, row) {
            return '<a href="#">' + column.id + ': ' + row.id + '</a>';
          },
          commands: function(column, row) {
            var value = row[column.id];
            var val = "", buttons = jQuery.parseJSON(value), button;
            for (var i = 0; i < buttons.length; i++) {
              button = buttons[i];
              val += '<button type="button" class="btn btn-xs btn-default ' + button.operation + '" title="' + button.label + '" data-action="' + button.type + '" data-value="' + button.operation + '" data-layout="' + button.layout + '" data-reference="' + button.reference + '" data-confirm="' + button.confirm + '">' 
                   + '<span class="fa ' + button.class + '" data-action="' + button.type + '" data-value="' + button.operation + '" data-layout="' + button.layout + '" data-reference="' + button.reference + '" data-confirm="' + button.confirm + '"> </span>' 
                   + '</button>';
            }
            //return '<div class="position-grid">' + val + '</div>';
            return val;
          },
          boolean: function(column, row) {
            var value = row[column.id];
            var val = '<i class="fa fa-square-o"></i>';
            if (value) {
              val = '<i class="fa fa-check-square-o"></i>';
            }
            return val;
          },
          checkbox: function(column, row) {
            var value = row[column.id];
            var val = '';
            if (value) {
              val = '<input type="checkbox" id="checkbox-' + value + '" name="checkbox[]" value="' + value + '"/>';
            }
            return val;
          },
          date: function(column, row) {
            var value = row[column.id];
            var vector = value.substring(0, 10).split("-"), date;
            date = vector[2] + "/" + vector[1] + "/" + vector[0];
            return date;
          },
          file: function(column, row) {
            var value = row[column.id];
            return '<a href="//' + Application.config.host + '/download/?file=' + value + '" target="_blank">' + value.replace(/^.*[\\\/]/, '') + '</a>';
          },
          double: function(column, row) {
            var value = row[column.id];
            var val = value;
            val = '<span style="color:red;">' + value + '</span>';
            if (value > 0) {
              val = '<span style="color:green;">' + value + '</span>';
            } else if (value < 0) {
            }
            return val;
          },
          money: function(column, row) {
            var value = row[column.id];
            return jQuery(Application.renderer.renderDouble(value)).currency({region: 'BRL', thousands: '.', decimal: ','});
          },
          percent: function(column, row) {
            var value = row[column.id];
            var inteiro = Number(value), decimais, result, x = 0, i = 0;

            inteiro = Math.round(inteiro * 100) / 100;
            if (inteiro < 0) {
              inteiro = Math.abs(inteiro);
              x = 1;
            }
            if (isNaN(inteiro) || inteiro === "") {
              inteiro = "0";
            }

            decimais = Math.floor((inteiro * 100 + 0.5) % 100);
            inteiro = Math.floor((inteiro * 100 + 0.5) / 100).toString();

            if (decimais < 10) {
              decimais = "0" + decimais;
              for (i = 0; i < Math.floor((inteiro.length - (1 + i)) / 3); i++) {
                inteiro = inteiro.substring(0, inteiro.length - (4 * i + 3)) + '.' + inteiro.substring(inteiro.length - (4 * i + 3));
              }
            }

            result = inteiro + ',' + decimais + '%';
            if (x == 1) {
              result = ' - ' + result;
            }
            return result;
          }
        }
      });

      $toolbar = jQuery($grid.attr('data-toolbar'));
      if ($toolbar) {
        try {
          jQuery("#grid-" + target + "-header .customBar").html($toolbar.html());
          $toolbar.html('');
        } catch (e) {
          
        }
      }

      Application.form.grids[target] = $grid;
      
      return $grid;
    },

    /**
     * 
     * @param {type} line
     * @param {type} id
     * @param {type} url
     * @param {type} reference
     * @param {type} description
     * 
     * @returns $element
     */
    createRemoteComboBox: function(target, line, id, url, reference, description) {

      var $combo = jQuery('#' + line + ' ' + '#' + id).select2({
        minimumInputLength: 2,
        width: 'off',
        quietMillis: 500,
        maximumSelectionSize: 1,
        placeholder: '.:. Digite para pesquisar .:.',
        tags: [],
        ajax: {
          url: url,
          dataType: 'json',
          type: 'GET',
          data: function(query) {
            return {
              query: query
            };
          },
          transport: function(params) {
            params.beforeSend = function(request) {
              request.setRequestHeader("System", Application.config.key);
            };
            return jQuery.ajax(params);
          },
          results: function(data) {
            return {
              results: jQuery.map(data.rows, function(item) {
                return {
                  text: item[description],
                  id: item[reference]
                };
              })
            };
          }
        },
        initSelection: function(element, callback) {
          var data = [];
          jQuery(element.val().split(",")).each(function(i) {
            var item = this.split(':');
            jQuery(element).val(item[0]);
            data.push({
              id: item[0],
              text: item[1]
            });
          });
          callback(data);
        }
      });
      
      Application.form.combos[target] = $combo;

      return $combo;
    }

  }

};