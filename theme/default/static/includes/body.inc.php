<?php

?>

<div class="wrapper">

  <div class="page-tip animated slideInDown">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <p>Voc� precisar fazer login para acessar o ambiente administrativo. Se ainda n�o tem uma conta acesse <a href="<?php Application::link('sign-up');?>">esta p�gina</a> e crie uma nova conta.</p>
        </div>
      </div>
    </div>
  </div>

  <div class="container">
    <div class="row">
      <div class="col-md-8 col-sm-6">
        <h3 class="hl top-zero">Informe seus dados para acessar</h3>
        <hr>
        <p>Claro que voc� quer acessar, n�s sabemos! Por�m ainda assim precisamos que voc� se identique para poder ter acesso aos recursos do ambiente administrativo.</p>

        <div class="login-social">
          <ul>
            <li id="twitter"><a href="#"><i class="fa fa-twitter"></i></a></li>
            <li id="facebook"><a href="#"><i class="fa fa-facebook"></i></a></li>
            <li id="google"><a href="#"><i class="fa fa-google-plus"></i></a></li>
          </ul>
        </div>
        <div class="clearfix"></div>
      </div>

      <div class="col-md-4 col-sm-6">
        <?php
          System::import('file', 'includes', 'login.inc', Application::getThemeLocation('static'));
        ?>
      </div>
    </div>
  </div>

</div>
