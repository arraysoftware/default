<?php ?>

<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">

  <div class="navbar-header">

    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>

    <a class="navbar-brand" href="<?php Application::link('home'); ?>"><?php print TITLE; ?></a>
  </div>

  <div class="collapse navbar-collapse">
    <ul class="nav navbar-nav">
      <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Meta <b class="caret"></b></a>
        <ul class="dropdown-menu">
          <li><a href="<?php Application::link('home'); ?>">Home</a></li>
          <li><a href="<?php Application::link('sign-up'); ?>">Criar Conta</a></li>
          <li><a href="<?php Application::link('sign-in'); ?>">Entrar</a></li>
          <li><a href="<?php Application::link('privacy-policy'); ?>">Pol�tiva de Privacidade</a></li>
          <li><a href="<?php Application::link('terms-of-service'); ?>">Termos do Servi�o</a></li>
        </ul>
      </li>
      <li class="hidden-sm hidden-md"><a href="<?php Application::link('about'); ?>">Sobre</a></li>
    </ul>

    <ul class="nav navbar-nav navbar-right hidden-xs">
      <?php
        System::import('file', 'includes', 'navbar-profile.inc', Application::getThemeLocation('static'));
      ?>
    </ul>

  </div>
</div>
