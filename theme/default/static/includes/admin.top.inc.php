<?php ?>

<header class="header">

  <a class="logo" href="<?php Application::link('home'); ?>"><?php print TITLE; ?></a>

  <nav class="navbar navbar-static-top" role="navigation">
    
    <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </a>
    
    <div class="navbar-right">

      <ul class="nav navbar-nav">

        <li id="messages-menu" class="dropdown messages-menu">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="fa fa-envelope"></i>
            <!--<span class="label label-success">4</span>-->
          </a>
          <ul class="dropdown-menu">
            <li class="header">Nenhuma nova mensagem</li>
            <li>
              <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 200px;">
                <ul class="menu" style="overflow: hidden; width: 100%; height: 200px;">
                </ul>
              </div>
            </li>
            <li class="footer"><a href="#">Todas as Mensagens</a></li>
          </ul>
        </li>

        <li id="notifications-menu" class="dropdown notifications-menu">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="fa fa-warning"></i>
            <!--<span class="label label-warning">10</span>-->
          </a>
          <ul class="dropdown-menu">
            <li class="header">Nenhuma nova Notificação</li>
            <li>
              <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 200px;">
                <ul class="menu" style="overflow: hidden; width: 100%; height: 200px;"></ul>
              </div>
            </li>
            <li class="footer"><a href="#">Todas as Notificações</a></li>
          </ul>
        </li>

        <li id="tasks-menu" class="dropdown tasks-menu">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="fa fa-tasks"></i>
            <!--<span class="label label-danger">9</span>-->
          </a>
          <ul class="dropdown-menu">
            <li class="header">Nenhuma tarefa pendente</li>
            <li>
              <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 200px;">
                <ul class="menu" style="overflow: hidden; width: 100%; height: 200px;">
                </ul>
              </div>
            </li>
            <li class="footer">
              <a href="#">Todas as Tarefas</a>
            </li>
          </ul>
        </li>

        <?php
          System::import('file', 'includes', 'navbar-profile.inc', Application::getThemeLocation('static'));
        ?>

      </ul>
        
    </div>
  </nav>
</header>