<?php

?>

<footer>
  <div class="container">
    <div class="row">

      <div class="col-sm-4">
        <div class="headline"><h3>Onde estamos?</h3></div>
        <div class="content">
          <p>
            San Francisco, CA 94101<br />
            1987 Lincoln Street<br />
            Phone: +0 000 000 00 00<br />
            Fax: +0 000 000 00 00<br />
            Email: <a href="#">admin@mysite.com</a>
          </p>
        </div>
      </div>

      <div class="col-sm-4">
        <div class="headline"><h3>Quer saber mais?</h3></div>
        <div class="content social">
          <p>Siga-nos:</p>
          <ul>
            <li><a href="#"><i class="fa fa-twitter"></i></a>
            </li>
            <li><a href="#"><i class="fa fa-facebook"></i></a>
            </li>
            <li><a href="#"><i class="fa fa-pinterest"></i></a>
            </li>
            <li><a href="#"><i class="fa fa-youtube"></i></a>
            </li>
            <li><a href="#"><i class="fa fa-github"></i></a>
            </li>
            <li><a href="#"><i class="fa fa-linkedin"></i></a>
            </li>
            <li><a href="#"><i class="fa fa-vk"></i></a>
            </li>
            <li><a href="#"><i class="fa fa-google-plus"></i></a>
            </li>
          </ul>
          <div class="clearfix"></div>
        </div>
      </div>
      <!-- Subscribe 
      =============== -->
      <div class="col-sm-4">
        <div class="headline"><h3>Acompanhe</h3></div>
        <div class="content">
          <p>Informe seu e-mail para acompanhar nossa newsletter.<br/>Sem <b>spam</b>!</p>
          <form class="form" role="form">
            <div class="row">
              <div class="col-sm-8">
                <div class="input-group">
                  <label class="sr-only" for="subscribe-email">E-mail</label>
                  <input type="email" class="form-control" id="subscribe-email" placeholder="Informe seu E-mail">
                  <span class="input-group-btn">
                    <button type="submit" class="btn btn-default">Participar</button>
                  </span>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</footer>
<!-- Legal 
============= -->
<div class="legal">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <p>&copy; <?php print COPY_RIGHT;?>. <a href="<?php Application::link('privacy-policy');?>">Pol�tica de Privacidade</a> | <a href="<?php Application::link('terms-of-service');?>">Termos de Uso</a></p>
      </div>
    </div>
  </div>
</div>
