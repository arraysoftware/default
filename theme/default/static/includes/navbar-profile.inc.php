<?php

if (PAGE_CONECTED) {
  if (!Application::$admin) {
    ?>
      <li id="cogs-menu">
        <a href="<?php Application::link('system'); ?>">
          <i class="fa fa-gears fa-lg"></i>
        </a>
      </li>
    <?php
  }
  ?>
    <li id="profile-menu" class="dropdown user user-menu">
      <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php print Application::get();?> <b class="caret"></b></a>
      <ul class="dropdown-menu">
        <li class="account">
          <div class="avatar"></div>
          <p><b><?php print Application::get();?></b><br/><?php print Application::get('email');?></p>
          <p><a href="<?php Application::link('profile/' . Application::get('login')); ?>">Perfil</a> | <a href="<?php Application::link('sign-out/?logout=all', true); ?>">Sair</a></p>
          <div class="clearfix"></div>
        </li>
      </ul>
    </li>
  <?php
} else {
  ?>
    <li class="show"><a href="<?php Application::link('sign-up'); ?>">Criar Conta</a></li>
    <li class="show"><a href="<?php Application::link('sign-in'); ?>">Entrar</a></li>
  <?php
}