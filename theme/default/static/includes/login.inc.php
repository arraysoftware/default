<?php 

$label = PAGE_CONECTED ? "Voc� j� est� Conectado" : "Acessar Minha Conta";

?>

<div class="form-box">

  <h4><?php print $label; ?></h4>
  <hr>

  <?php
    if (!PAGE_CONECTED) {
      ?>
        <form role="form" method="post">

          <div class="form-group">
            <label class="sr-only" for="login">Login</label>
            <input type="login" name="login" id="login" class="form-control" placeholder="Login ou email">
          </div>
          <div class="form-group">
            <label class="sr-only" for="password">Senha</label>
            <input type="password" name="password" id="password" class="form-control" placeholder="Senha">
          </div>
          <div class="checkbox">
            <label>
              <input type="checkbox"> Lembrar meu acesso
            </label>
          </div>

          <button type="submit" class="btn btn-green">Entrar</button>
        </form>
      <?php
    } else {
      ?>
        <p>
          Ol� <b><?php print Application::get() ;?></b>,
        </p>
        <p align="justify">
          Idenfificamos que voc� j� est� conectdado.
        </p>
        <p align="justify">
          Voc� pode usar o menu acima para navegar para onde deseja que suas credenciais n�o ser�o pedidas novamente enquanto esta sess�o estiver ativa.
        </p>
      <?php
    }
  ?>

  <hr>

  <?php
    if (!PAGE_CONECTED) {
      ?>
        <p>N�o tem uma conta? <a href="<?php Application::link('sign-up'); ?>">Crie uma conta.</a></p>

        <div class="pwd-lost">
          <div class="pwd-lost-q show">Perdeu a senha? <a href="#">Click aqui para recuperar.</a></div>
          <div class="pwd-lost-f hidden">
            <p class="text-muted">Informe seu endere�o de e-mail para receber um link de recupera��o de senha.</p>
            <form class="form-inline" role="form">
              <div class="form-group">
                <label class="sr-only" for="email-pwd">Endere�o de E-mail </label>
                <input type="email" class="form-control" id="email-pwd" placeholder="Informe seu e-mail">
              </div>
              <button type="submit" class="btn btn-blue">Enviar</button>
            </form>
          </div>
        </div>
      <?php
    } else {
      ?>
        <p align="justify">
          Voc� pode acessar a <a href="<?php Application::link(Application::$system); ?>">�rea Administrativa</a> ou navegar pela nossa <a href="<?php Application::link('help'); ?>">Ajuda</a> para se aprofundar sobre as funcionalidades da ferramenta.
        </p>
      <?php
    }
  ?>

</div>

