<?php

?>

<aside class="left-side sidebar-offcanvas">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
      <div class="pull-left image">
        <img src="//<?php Application::link();?>/images/<?php print Application::get('avatar'); ?>" class="img-circle" alt="User Image">
      </div>
      <div class="pull-left info">
        <p>Ol�, <?php print Application::get(); ?></p>
        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
      </div>
    </div>
    <!-- search form -->
    <form action="#" method="get" class="sidebar-form">
      <div class="input-group">
        <input type="text" name="q" class="form-control" placeholder="Pesquisar..." data-action="search-menu">
        <span class="input-group-btn">
          <button type="submit" name="seach" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
        </span>
      </div>
    </form>

    <ul class="sidebar-menu">
      <?php

        foreach (Application::$menuSystem as $menu) {

          $descricao = $menu['descricao'];
          $items = $menu['items'];
          ?>
            <li class="treeview">
              <a href="javascript:void(0);">
                <i class="fa fa-folder"></i>
                <span><?php print $descricao; ?></span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <?php
                  foreach ($items as $action => $route) {

                    $root = $route["root"];
                    $module = $route["module"];
                    $entity = $route["entity"];
                    $level = $route["level"];
                    $separator = $route["separator"];

                    $men_descricao = $route["men_descricao"];
                    $mef_descricao = $route["mef_descricao"];

                    $print = false;
                    if ($men_descricao != $menu) {
                      $menu = $men_descricao;
                      $print = true;
                    }

                    ?>
                      <li class="function">
                        <a href="javascript:void(0);" class="<?php print $separator;?>" data-action="open-menu" data-value="<?php print $action;?>" data-level="<?php print $level;?>">
                          <i class="fa fa-angle-double-right"></i> <?php print $mef_descricao; ?></a>
                      </li>
                    <?php
                  }

                ?>
              </ul>
            </li>
          <?
        }
      ?>
    </ul>

  </section>
    
</aside>
