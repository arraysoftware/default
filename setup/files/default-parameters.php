<?php
/**
* Array Software
* Projeto FLEX PEDIDOS - 2013
* Modulo..........: Principal
* Funcionalidade..: Inicializa variaveis utilizadas no sistema
* Pre-requisitos..: Nao possui
* Data............: 06/05/2013 17:18:40
* Responsavel.....: PEDRO HENRIQUE MAZALA MACHADO
*/

if(basename($_SERVER["PHP_SELF"])== "parameters.php"){
  die("<h1>Not Found</h1><p>The requested URL was not found on this server.</p><hr><address>Apache/2.2.9 (Debian)</address>");
}

$add_slashes_request = "";
if (!defined("ADD_SLASHES_REQUEST")) {
  /**
   * Especifica o uso ou n&atilde;o de slashes nas requisi&ccedil;&otilde;es<br>
   * @name ADD_SLASHES_REQUEST
   * @var String
   * @since 1.0.0.11
   * @author ADMINISTRADOR (08/06/2013 17:28:23)
   * <br><b>Updated by</b> ADMINISTRADOR (08/06/2013 17:28:23)
   */
  define("ADD_SLASHES_REQUEST", "");
}

$company = "";
if (!defined("COMPANY")) {
  /**
   * Nome Fantasia da Empresa<br>
   * @name COMPANY
   * @var String
   * @since 4.0.0
   * @author WILLIAM MARQUES VICENTE GOMES CORR�A (31/08/2010 13:49:42)
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORR&Ecirc;A (25/03/2013 09:13:03)
   */
  define("COMPANY", "");
}

$company_address = "";
if (!defined("COMPANY_ADDRESS")) {
  /**
   * Endere&ccedil;o da institui&ccedil;&atilde;o<br>
   * @name COMPANY_ADDRESS
   * @var String
   * @since 4.0.0
   * @author BRYAN MAGALH�ES SILVA (31/08/2010 13:59:41)
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORR�A (31/01/2011 13:21:03)
   */
  define("COMPANY_ADDRESS", "");
}

$company_cep = "";
if (!defined("COMPANY_CEP")) {
  /**
   * Cep da organiza&ccedil;&atilde;o<br>
   * @name COMPANY_CEP
   * @var String
   * @since 4.0.0
   * @author  (23/09/2010 15:39:50)
   * <br><b>Updated by</b>  (23/09/2010 15:39:50)
   */
  define("COMPANY_CEP", "");
}

$company_city = "";
if (!defined("COMPANY_CITY")) {
  /**
   * Cidade<br>
   * @name COMPANY_CITY
   * @var String
   * @since 4.0.0
   * @author BRYAN MAGALH�ES SILVA (31/08/2010 14:00:28)
   * <br><b>Updated by</b> FELIPE DE FREITAS CARNEIRO (20/06/2012 13:46:28)
   */
  define("COMPANY_CITY", "");
}

$company_cnpj = "";
if (!defined("COMPANY_CNPJ")) {
  /**
   * CNPJ da institui&ccedil;&atilde;o<br>
   * @name COMPANY_CNPJ
   * @var String
   * @since 4.0.0
   * @author BRYAN MAGALH�ES SILVA (31/08/2010 13:59:09)
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORR&Ecirc;A (25/03/2013 09:13:22)
   */
  define("COMPANY_CNPJ", "");
}

$company_cx = "";
if (!defined("COMPANY_CX")) {
  /**
   * Caixa Postal da institui&ccedil;&atilde;o<br>
   * @name COMPANY_CX
   * @var String
   * @since 4.0.0
   * @author WILLIAM MARQUES VICENTE GOMES CORR�A (12/04/2011 21:52:43)
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORR�A (12/04/2011 21:52:43)
   */
  define("COMPANY_CX", "");
}

$company_district = "";
if (!defined("COMPANY_DISTRICT")) {
  /**
   * Corresponde ao bairro onde &eacute; localizada a institui&ccedil;&atilde;o. &Eacute; utilizado na exporta&ccedil;&atilde;o de arquivos de remessa.<br>
   * @name COMPANY_DISTRICT
   * @var String
   * @since 4.0.0
   * @author  (21/10/2010 18:02:46)
   * <br><b>Updated by</b>  (21/10/2010 18:02:46)
   */
  define("COMPANY_DISTRICT", "");
}

$company_email = "";
if (!defined("COMPANY_EMAIL")) {
  /**
   * Email de contato da institui&ccedil;&atilde;o<br>
   * @name COMPANY_EMAIL
   * @var String
   * @since 4.0.0
   * @author WILLIAM MARQUES VICENTE GOMES CORREA (24/04/2012 15:54:09)
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA (24/04/2012 15:54:09)
   */
  define("COMPANY_EMAIL", "");
}

$company_leader = "";

$company_leader_string = "<center></center>";

if (!defined("COMPANY_LEADER")) {
  /**
   * Refer&ecirc;ncia ao nome do representante legal da instui&ccedil;&atilde;o<br>
   * @name COMPANY_LEADER
   * @var String
   * @since 4.0.0
   * @author WILLIAM MARQUES VICENTE GOMES CORR�A (29/06/2011 20:55:22)
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORR�A (29/06/2011 20:55:22)
   */
  define("COMPANY_LEADER", $company_leader);
}

$company_name = "";
if (!defined("COMPANY_NAME")) {
  /**
   * Nome da institui&ccedil;&atilde;o<br>
   * @name COMPANY_NAME
   * @var String
   * @since 4.0.0
   * @author BRYAN MAGALH�ES SILVA (31/08/2010 13:55:06)
   * <br><b>Updated by</b> BRYAN MAGALH�ES SILVA (31/08/2010 14:50:22)
   */
  define("COMPANY_NAME", "");
}

$company_name_sigla = "";
if (!defined("COMPANY_NAME_SIGLA")) {
  /**
   * Sigla referente ao nome da institui&ccedil;&atilde;o<br>
   * @name COMPANY_NAME_SIGLA
   * @var String
   * @since 4.0.0
   * @author RONALDO FORTUNATO LOPES (11/11/2011 13:44:23)
   * <br><b>Updated by</b> RONALDO FORTUNATO LOPES (11/11/2011 13:44:23)
   */
  define("COMPANY_NAME_SIGLA", "");
}

$company_razao_social = "";
if (!defined("COMPANY_RAZAO_SOCIAL")) {
  /**
   * <br>
   * @name COMPANY_RAZAO_SOCIAL
   * @var String
   * @since 4.0.0
   * @author WILLIAM MARQUES VICENTE GOMES CORR�A (02/06/2011 10:58:45)
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORR�A (02/06/2011 10:58:45)
   */
  define("COMPANY_RAZAO_SOCIAL", "");
}

$company_short = "";
if (!defined("COMPANY_SHORT")) {
  /**
   * Descri&ccedil;&atilde;o curta da institui&ccedil;&atilde;o<br>
   * @name COMPANY_SHORT
   * @var String
   * @since 4.0.0
   * @author  (09/12/2010 22:33:46)
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA (14/06/2012 18:11:15)
   */
  define("COMPANY_SHORT", "");
}

$company_site = "";
if (!defined("COMPANY_SITE")) {
  /**
   * Descri&ccedil;&atilde;o do site da institui&ccedil;&atilde;o<br>
   * @name COMPANY_SITE
   * @var String
   * @since 4.0.0
   * @author  (09/12/2010 22:34:17)
   * <br><b>Updated by</b>  (09/12/2010 22:34:17)
   */
  define("COMPANY_SITE", "");
}

$company_state = "";
if (!defined("COMPANY_STATE")) {
  /**
   * Estado<br>
   * @name COMPANY_STATE
   * @var String
   * @since 4.0.0
   * @author BRYAN MAGALH�ES SILVA (31/08/2010 14:00:02)
   * <br><b>Updated by</b> RONALDO FORTUNATO LOPES (28/07/2011 10:28:06)
   */
  define("COMPANY_STATE", "");
}

$company_telefone = "";
if (!defined("COMPANY_TELEFONE")) {
  /**
   * Telefone principal da institui&ccedil;&atilde;o<br>
   * @name COMPANY_TELEFONE
   * @var String
   * @since 4.0.0
   * @author WILLIAM MARQUES VICENTE GOMES CORR�A (08/04/2011 21:38:07)
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORR�A (08/04/2011 21:38:07)
   */
  define("COMPANY_TELEFONE", "");
}

$company_uf = "";
if (!defined("COMPANY_UF")) {
  /**
   * Descri&ccedil;&atilde;o da Unidade Federativa da institui&ccedil;&atilde;o<br>
   * @name COMPANY_UF
   * @var String
   * @since 4.0.0
   * @author  (09/12/2010 22:34:43)
   * <br><b>Updated by</b>  (09/12/2010 22:34:43)
   */
  define("COMPANY_UF", "");
}

$company_z = $company_address." - ".$company_district." ".$company_city."/".$company_uf." CEP ".$company_cep;
if (!defined("COMPANY_Z")) {
  /**
   * Concatena principais dados da empresa<br>
   * @name COMPANY_Z
   * @var String
   * @since 4.0.0
   * @author WILLIAM MARQUES VICENTE GOMES CORR�A (08/04/2011 21:34:57)
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORR�A (08/04/2011 21:34:57)
   */
  define("COMPANY_Z", $company_address." - ".$company_district." ".$company_city."/".$company_uf." CEP ".$company_cep);
}

$copy_right = "� arraysoftware 2009-2013";
if (!defined("COPY_RIGHT")) {
  /**
   * Texto de direitos autorais<br>
   * @name COPY_RIGHT
   * @var String
   * @since 4.0.0
   * @author BRYAN MAGALH�ES SILVA (31/08/2010 14:11:12)
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORR&Ecirc;A (03/04/2013 17:00:51)
   */
  define("COPY_RIGHT", "� arraysoftware 2009-2013");
}

$default_database = "{database}";
if (!defined("DEFAULT_DATABASE")) {
  /**
   * Base de dados padr&atilde;o do sistema<br>
   * @name DEFAULT_DATABASE
   * @var String
   * @since 4.0.2
   * @author WILLIAM MARQUES VICENTE GOMES CORR&Ecirc;A (27/04/2013 17:33:28)
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORR&Ecirc;A (27/04/2013 17:33:28)
   */
  define("DEFAULT_DATABASE", "{database}");
}

$default_extension = "";
if (!defined("DEFAULT_EXTENSION")) {
  /**
   * Solu&ccedil;&atilde;o para projetos em adapta&ccedil;&atilde;o<br>
   * @name DEFAULT_EXTENSION
   * @var String
   * @since 4.0.2
   * @author WILLIAM MARQUES VICENTE GOMES CORR&Ecirc;A (27/04/2013 22:09:57)
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORR&Ecirc;A (27/04/2013 22:09:57)
   */
  define("DEFAULT_EXTENSION", "");
}

$default_user_admin = "c4ca4238a0b923820dcc509a6f75849b";
if (!defined("DEFAULT_USER_ADMIN")) {
  /**
   * C&oacute;digo usado para identificar se o usu&aacute;rio &eacute; Admin ou n&atilde;o<br>
   * @name DEFAULT_USER_ADMIN
   * @var String
   * @since 1.0.0.23
   * @author INSTRUTOR (16/06/2013 17:09:51)
   * <br><b>Updated by</b> INSTRUTOR (16/06/2013 17:09:51)
   */
  define("DEFAULT_USER_ADMIN", "c4ca4238a0b923820dcc509a6f75849b");
}

$default_user_system = "{short} :: ADMINISTRADOR";
if (!defined("DEFAULT_USER_SYSTEM")) {
  /**
   * Usu&aacute;rio padr&atilde;o do sistema<br>
   * @name DEFAULT_USER_SYSTEM
   * @var String
   * @since 4.0.2
   * @author WILLIAM MARQUES VICENTE GOMES CORR&Ecirc;A (24/04/2013 10:33:39)
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORR&Ecirc;A (24/04/2013 10:33:39)
   */
  define("DEFAULT_USER_SYSTEM", "{short} :: ADMINISTRADOR");
}

$dias_de_prazo_para_pagamento = 10;
if (!defined("DIAS_DE_PRAZO_PARA_PAGAMENTO")) {
  /**
   * Prazo para pagamento do boleto<br>
   * @name DIAS_DE_PRAZO_PARA_PAGAMENTO
   * @var Numeric
   * @since 4.0.0
   * @author RENATA APARECIDA BENINI (20/07/2011 10:04:35)
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORR&Ecirc;A (24/10/2012 09:47:54)
   */
  define("DIAS_DE_PRAZO_PARA_PAGAMENTO", 10);
}

$dir_image_app = "upload/image/";
if (!defined("DIR_IMAGE_APP")) {
  /**
   * Diret&oacute;rio do projeto onde fica hospedado o aplicativo de edi&ccedil;&atilde;o de imagens<br>
   * @name DIR_IMAGE_APP
   * @var String
   * @since 4.0.0
   * @author WILLIAM MARQUES VICENTE GOMES CORR&Ecirc;A (28/03/2013 15:47:39)
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORR&Ecirc;A (28/03/2013 15:49:04)
   */
  define("DIR_IMAGE_APP", "upload/image/");
}

$email_assunto = "[{short}]";
if (!defined("EMAIL_ASSUNTO")) {
  /**
   * Assunto padr&atilde;o do envio de email do sistema<br>
   * @name EMAIL_ASSUNTO
   * @var String
   * @since 4.0.0
   * @author WILLIAM MARQUES VICENTE GOMES CORR�A (03/09/2010 17:44:35)
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORR�A (06/12/2011 09:57:18)
   */
  define("EMAIL_ASSUNTO", "[{short}]");
}

$email_debug = "";
if (!defined("EMAIL_DEBUG")) {
  /**
   * Endere&ccedil;o de e-mail para debug<br>
   * @name EMAIL_DEBUG
   * @var String
   * @since 4.0.2
   * @author WILLIAM MARQUES VICENTE GOMES CORR&Ecirc;A (23/04/2013 18:01:42)
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORR&Ecirc;A (23/04/2013 18:01:42)
   */
  define("EMAIL_DEBUG", "");
}

$email_erro = "";
if (!defined("EMAIL_ERRO")) {
  /**
   * Email padr&atilde;o para as mensagens de erro do sistema<br>
   * @name EMAIL_ERRO
   * @var String
   * @since 4.0.0
   * @author  (11/01/2011 19:35:49)
   * <br><b>Updated by</b>  (11/01/2011 19:35:49)
   */
  define("EMAIL_ERRO", "");
}

$email_remetente = "";
if (!defined("EMAIL_REMETENTE")) {
  /**
   * Remetente de email padr&atilde;o do sistema<br>
   * @name EMAIL_REMETENTE
   * @var String
   * @since 4.0.0
   * @author WILLIAM MARQUES VICENTE GOMES CORR�A (03/09/2010 17:04:09)
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORR�A (11/01/2011 19:34:58)
   */
  define("EMAIL_REMETENTE", "");
}

$email_senha = "";
if (!defined("EMAIL_SENHA")) {
  /**
   * Senha do email de envio de notifica&ccedil;&otilde;es padr&atilde;o do Sistema<br>
   * @name EMAIL_SENHA
   * @var String
   * @since 4.0.0
   * @author WILLIAM MARQUES VICENTE GOMES CORR�A (25/05/2011 14:12:22)
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORR�A (25/05/2011 14:12:22)
   */
  define("EMAIL_SENHA", "");
}

$email_smtp_dominio = "";
if (!defined("EMAIL_SMTP_DOMINIO")) {
  /**
   * Endere&ccedil;o do servidor de SMTP padr&atilde;o para envio de notifica&ccedil;&otilde;es do sistema<br>
   * @name EMAIL_SMTP_DOMINIO
   * @var String
   * @since 4.0.0
   * @author WILLIAM MARQUES VICENTE GOMES CORR�A (25/05/2011 14:13:18)
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORR�A (25/05/2011 14:13:18)
   */
  define("EMAIL_SMTP_DOMINIO", "");
}

$email_smtp_porta = 0;
if (!defined("EMAIL_SMTP_PORTA")) {
  /**
   * Porta padr&atilde;o do Servidor de SMTP para envio de mensagens de notifica&ccedil;&otilde;es do sistema<br>
   * @name EMAIL_SMTP_PORTA
   * @var Numeric
   * @since 4.0.0
   * @author WILLIAM MARQUES VICENTE GOMES CORR�A (25/05/2011 14:11:51)
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORR�A (25/05/2011 21:16:44)
   */
  define("EMAIL_SMTP_PORTA", 0);
}

$email_smtp_timeout = 0;
if (!defined("EMAIL_SMTP_TIMEOUT")) {
  /**
   * Configura&ccedil;&atilde;o de tempo de espera para autentica&ccedil;&atilde;o para conex&atilde;o com servidor de SMTP<br>
   * @name EMAIL_SMTP_TIMEOUT
   * @var Numeric
   * @since 4.0.0
   * @author WILLIAM MARQUES VICENTE GOMES CORR�A (25/05/2011 14:13:49)
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORR�A (25/05/2011 21:16:38)
   */
  define("EMAIL_SMTP_TIMEOUT", 0);
}

$encrypt_key = "";
if (!defined("ENCRYPT_KEY")) {
  /**
   * <br>
   * @name ENCRYPT_KEY
   * @var String
   * @since 4.0.0
   * @author BRYAN MAGALH�ES SILVA (31/08/2010 14:37:09)
   * <br><b>Updated by</b> BRYAN MAGALH�ES SILVA (31/08/2010 14:53:32)
   */
  define("ENCRYPT_KEY", "");
}

$expired_time = 1800;
if (!defined("EXPIRED_TIME")) {
  /**
   * <br>
   * @name EXPIRED_TIME
   * @var Numeric
   * @since 4.0.0
   * @author BRYAN MAGALH�ES SILVA (31/08/2010 14:35:44)
   * <br><b>Updated by</b> BRYAN MAGALH�ES SILVA (31/08/2010 14:35:44)
   */
  define("EXPIRED_TIME", 1800);
}

$host = "{host}";
if (!defined("HOST")) {
  /**
   * Dominio do servidor onde est&aacute; hospedado o aplicativo<br>
   * @name HOST
   * @var String
   * @since 4.0.0
   * @author WILLIAM MARQUES VICENTE GOMES CORREA (12/03/2012 21:08:36)
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA (12/03/2012 21:08:52)
   */
  define("HOST", "{host}");
}

$html_entities_request = "";
if (!defined("HTML_ENTITIES_REQUEST")) {
  /**
   * Determina a aplica&ccedil;&atilde;o de convers&atilde;o nas requisi&ccedil;&otilde;es<br>
   * @name HTML_ENTITIES_REQUEST
   * @var String
   * @since 1.0.0.11
   * @author ADMINISTRADOR (08/06/2013 17:27:47)
   * <br><b>Updated by</b> ADMINISTRADOR (08/06/2013 17:27:47)
   */
  define("HTML_ENTITIES_REQUEST", "");
}

$itens = 25;

if(isset($_COOKIE["limit"])) {
  $itens= $_COOKIE["limit"];
}

if (!defined("ITENS")) {
  /**
   * Quantidade de itens padr&atilde;o carregada nas visualiza&ccedil;&otilde;es de itens<br>
   * @name ITENS
   * @var Numeric
   * @since 4.0.0
   * @author BRYAN MAGALH�ES SILVA (31/08/2010 14:35:58)
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORR&Ecirc;A (22/03/2013 17:04:43)
   */
  define("ITENS", $itens);
}

$juros_mora_default = "2.00";
if (!defined("JUROS_MORA_DEFAULT")) {
  /**
   * <br>
   * @name JUROS_MORA_DEFAULT
   * @var String
   * @since 4.0.0
   * @author BRYAN MAGALH�ES SILVA (31/08/2010 14:37:31)
   * <br><b>Updated by</b> BRYAN MAGALH�ES SILVA (31/08/2010 14:53:40)
   */
  define("JUROS_MORA_DEFAULT", "2.00");
}

$juros_rata_default = "0.03";
if (!defined("JUROS_RATA_DEFAULT")) {
  /**
   * <br>
   * @name JUROS_RATA_DEFAULT
   * @var String
   * @since 4.0.0
   * @author BRYAN MAGALH�ES SILVA (31/08/2010 14:38:33)
   * <br><b>Updated by</b> BRYAN MAGALH�ES SILVA (31/08/2010 14:53:44)
   */
  define("JUROS_RATA_DEFAULT", "0.03");
}

$limit = 25;

if(isset($_COOKIE["limit"])) {
  $limit = $_COOKIE["limit"];
}

if (!defined("LIMIT")) {
  /**
   * Quantidade de itens padr&atilde;o carregada nas visualiza&ccedil;&otilde;es de itens<br>
   * @name LIMIT
   * @var Numeric
   * @since 4.0.0
   * @author WILLIAM MARQUES VICENTE GOMES CORR&Ecirc;A (22/03/2013 17:04:45)
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORR&Ecirc;A (22/03/2013 17:05:14)
   */
  define("LIMIT", $limit);
}

$logo = "custom/images/logo/small.png";
if (!defined("LOGO")) {
  /**
   * Logo da Fagoc Tech<br>
   * @name LOGO
   * @var String
   * @since 4.0.0
   * @author BRYAN MAGALH�ES SILVA (31/08/2010 14:35:02)
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORR&Ecirc;A (22/03/2013 14:37:18)
   */
  define("LOGO", "custom/images/logo/small.png");
}

$manutencao = false;
if (!defined("MANUTENCAO")) {
  /**
   * Deixa o sistema em manuten&ccedil;&atilde;o<br>
   * @name MANUTENCAO
   * @var Boolean
   * @since 4.0.0
   * @author WILLIAM MARQUES VICENTE GOMES CORR&Ecirc;A (25/11/2012 04:11:34)
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORR&Ecirc;A (25/11/2012 08:09:34)
   */
  define("MANUTENCAO", false);
}

$message_add_error = "N�o foi poss��vel fazer o registro dos dados.";
if (!defined("MESSAGE_ADD_ERROR")) {
  /**
   * Mensagem de erro ao tentar adicionar um registro<br>
   * @name MESSAGE_ADD_ERROR
   * @var String
   * @since 4.0.0
   * @author WILLIAM MARQUES VICENTE GOMES CORR&Ecirc;A (02/04/2013 20:52:06)
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORR&Ecirc;A (02/04/2013 20:52:06)
   */
  define("MESSAGE_ADD_ERROR", "N�o foi poss��vel fazer o registro dos dados.");
}

$message_add_success = "Registro salvo com sucesso.";
if (!defined("MESSAGE_ADD_SUCCESS")) {
  /**
   * Mensagem exibida ao adicionar um novo registro<br>
   * @name MESSAGE_ADD_SUCCESS
   * @var String
   * @since 4.0.0
   * @author WILLIAM MARQUES VICENTE GOMES CORR&Ecirc;A (02/04/2013 20:50:44)
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORR&Ecirc;A (02/04/2013 21:01:09)
   */
  define("MESSAGE_ADD_SUCCESS", "Registro salvo com sucesso.");
}

$message_copy_error = "N�o foi poss�el fazer uma c�pia do registro.";
if (!defined("MESSAGE_COPY_ERROR")) {
  /**
   * Mensagem de erro ao adicionar uma c&oacute;pia do registro<br>
   * @name MESSAGE_COPY_ERROR
   * @var String
   * @since 4.0.0
   * @author WILLIAM MARQUES VICENTE GOMES CORR&Ecirc;A (02/04/2013 20:54:35)
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORR&Ecirc;A (02/04/2013 20:54:35)
   */
  define("MESSAGE_COPY_ERROR", "N�o foi poss�el fazer uma c�pia do registro.");
}

$message_copy_success = "C�pia do registro salva com sucesso.";
if (!defined("MESSAGE_COPY_SUCCESS")) {
  /**
   * Mensagem de registro de c&oacute;pia<br>
   * @name MESSAGE_COPY_SUCCESS
   * @var String
   * @since 4.0.0
   * @author WILLIAM MARQUES VICENTE GOMES CORR&Ecirc;A (02/04/2013 20:53:07)
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORR&Ecirc;A (02/04/2013 21:01:21)
   */
  define("MESSAGE_COPY_SUCCESS", "C�pia do registro salva com sucesso.");
}

$message_forbidden = "Acesso n�o autorizado.";
if (!defined("MESSAGE_FORBIDDEN")) {
  /**
   * Mensagem de acesso negado a uma funcionalidade<br>
   * @name MESSAGE_FORBIDDEN
   * @var String
   * @since 4.0.0
   * @author WILLIAM MARQUES VICENTE GOMES CORR&Ecirc;A (02/04/2013 20:58:22)
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORR&Ecirc;A (02/04/2013 21:49:05)
   */
  define("MESSAGE_FORBIDDEN", "Acesso n�o autorizado.");
}

$message_remove_error = "N�o foi poss��vel excluir o registro.";
if (!defined("MESSAGE_REMOVE_ERROR")) {
  /**
   * Mensagem de erro ao tentar remover um registro<br>
   * @name MESSAGE_REMOVE_ERROR
   * @var String
   * @since 4.0.0
   * @author WILLIAM MARQUES VICENTE GOMES CORR&Ecirc;A (02/04/2013 21:00:55)
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORR&Ecirc;A (02/04/2013 21:00:55)
   */
  define("MESSAGE_REMOVE_ERROR", "N�o foi poss��vel excluir o registro.");
}

$message_remove_success = "Registro exclu�do com sucesso.";
if (!defined("MESSAGE_REMOVE_SUCCESS")) {
  /**
   * Mensagem de &ecirc;xito ao remover um registro<br>
   * @name MESSAGE_REMOVE_SUCCESS
   * @var String
   * @since 4.0.0
   * @author WILLIAM MARQUES VICENTE GOMES CORR&Ecirc;A (02/04/2013 20:59:59)
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORR&Ecirc;A (02/04/2013 20:59:59)
   */
  define("MESSAGE_REMOVE_SUCCESS", "Registro exclu�do com sucesso.");
}

$message_saveonly = "O regsitro j� foi salvo";
if (!defined("MESSAGE_SAVEONLY")) {
  /**
   * Mensagem exibida quando a entidade n&atilde;o der suporte a edi&ccedil;&atilde;o<br>
   * @name MESSAGE_SAVEONLY
   * @var String
   * @since 4.0.0
   * @author WILLIAM MARQUES VICENTE GOMES CORR&Ecirc;A (12/04/2013 19:26:24)
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORR&Ecirc;A (12/04/2013 19:26:24)
   */
  define("MESSAGE_SAVEONLY", "O regsitro j� foi salvo");
}

$message_send_error = "N�o foi poss�vel enviar o e-mail para:";
if (!defined("MESSAGE_SEND_ERROR")) {
  /**
   * Mensagem exibida quando acontece erro ao tentar enviar uma mensagem<br>
   * @name MESSAGE_SEND_ERROR
   * @var String
   * @since 4.1.2
   * @author WILLIAM MARQUES VICENTE GOMES CORR&Ecirc;A (06/05/2013 00:34:55)
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORR&Ecirc;A (06/05/2013 00:34:55)
   */
  define("MESSAGE_SEND_ERROR", "N�o foi poss�vel enviar o e-mail para:");
}

$message_send_success = "E-mail enviado para:";
if (!defined("MESSAGE_SEND_SUCCESS")) {
  /**
   * Mensagem exibida quando acontece erro ao tentar enviar uma mensagem<br>
   * @name MESSAGE_SEND_SUCCESS
   * @var String
   * @since 4.1.2
   * @author WILLIAM MARQUES VICENTE GOMES CORR&Ecirc;A (06/05/2013 00:47:50)
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORR&Ecirc;A (06/05/2013 00:47:50)
   */
  define("MESSAGE_SEND_SUCCESS", "E-mail enviado para:");
}

$message_set_error = "N�o foi poss�vel atualizar o registro.";
if (!defined("MESSAGE_SET_ERROR")) {
  /**
   * Mensagem de erro ao alterar um registro<br>
   * @name MESSAGE_SET_ERROR
   * @var String
   * @since 4.0.0
   * @author WILLIAM MARQUES VICENTE GOMES CORR&Ecirc;A (02/04/2013 20:56:02)
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORR&Ecirc;A (02/04/2013 20:57:07)
   */
  define("MESSAGE_SET_ERROR", "N�o foi poss�vel atualizar o registro.");
}

$message_set_success = "Registro atualizado com sucesso.";
if (!defined("MESSAGE_SET_SUCCESS")) {
  /**
   * Mensagem de sucesso ao alterar um registro<br>
   * @name MESSAGE_SET_SUCCESS
   * @var String
   * @since 4.0.0
   * @author WILLIAM MARQUES VICENTE GOMES CORR&Ecirc;A (02/04/2013 20:56:21)
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORR&Ecirc;A (02/04/2013 21:01:46)
   */
  define("MESSAGE_SET_SUCCESS", "Registro atualizado com sucesso.");
}

$modo_teste = "true";
if (!defined("MODO_TESTE")) {
  /**
   * Vari&aacute;vel para controle de simula&ccedil;&otilde;es de teste<br>
   * @name MODO_TESTE
   * @var String
   * @since 4.0.0
   * @author  (09/12/2010 22:28:38)
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORR&Ecirc;A (02/04/2013 20:15:05)
   */
  define("MODO_TESTE", "true");
}

$page_app = "{page_app}";
if (!defined("PAGE_APP")) {
  /**
   * Caminho principal<br>
   * @name PAGE_APP
   * @var String
   * @since 4.0.0
   * @author BRYAN MAGALH�ES SILVA (31/08/2010 14:02:23)
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORR&Ecirc;A (22/03/2013 14:28:32)
   */
  define("PAGE_APP", "{page_app}");
}

$page_css = "{page_app}custom/css/";
if (!defined("PAGE_CSS")) {
  /**
   * Url do gerenciador de CSS<br>
   * @name PAGE_CSS
   * @var String
   * @since 4.0.0
   * @author WILLIAM MARQUES VICENTE GOMES CORR&Ecirc;A (29/03/2013 15:20:00)
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORR&Ecirc;A (29/03/2013 15:20:13)
   */
  define("PAGE_CSS", "{page_app}custom/css/");
}

$page_download = "{page_app}download/?file";
if (!defined("PAGE_DOWNLOAD")) {
  /**
   * P&aacute;gina de downloads<br>
   * @name PAGE_DOWNLOAD
   * @var String
   * @since 4.0.0
   * @author WILLIAM MARQUES VICENTE GOMES CORR&Ecirc;A (01/08/2012 18:58:33)
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORR&Ecirc;A (22/03/2013 15:26:42)
   */
  define("PAGE_DOWNLOAD", "{page_app}download/?file");
}

$page_image = "{page_app}custom/images/";
if (!defined("PAGE_IMAGE")) {
  /**
   * URL das imagens do projeto<br>
   * @name PAGE_IMAGE
   * @var String
   * @since 4.0.0
   * @author PEDRO HENRIQUE MAZALA MACHADO (09/04/2013 10:35:08)
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO (09/04/2013 10:35:08)
   */
  define("PAGE_IMAGE", "{page_app}custom/images/");
}

$page_javascript = "{page_app}custom/js/";
if (!defined("PAGE_JAVASCRIPT")) {
  /**
   * Url do gerenciador de Javascript<br>
   * @name PAGE_JAVASCRIPT
   * @var String
   * @since 4.0.0
   * @author WILLIAM MARQUES VICENTE GOMES CORR&Ecirc;A (03/04/2013 17:06:51)
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORR&Ecirc;A (03/04/2013 17:08:20)
   */
  define("PAGE_JAVASCRIPT", "{page_app}custom/js/");
}

$page_library = "http://lib.gennesis.net.br/";
if (!defined("PAGE_LIBRARY")) {
  /**
   * URL onde est&atilde;o hospedadas as bibliotecas do sistema<br>
   * @name PAGE_LIBRARY
   * @var String
   * @since 4.0.0
   * @author WILLIAM MARQUES VICENTE GOMES CORR&Ecirc;A (09/04/2013 11:32:45)
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORR&Ecirc;A (09/04/2013 11:32:45)
   */
  define("PAGE_LIBRARY", "http://lib.gennesis.net.br/");
}

$page_login = "{page_app}user/login";
if (!defined("PAGE_LOGIN")) {
  /**
   * P&aacute;gina de login<br>
   * @name PAGE_LOGIN
   * @var String
   * @since 4.0.0
   * @author BRYAN MAGALH�ES SILVA (31/08/2010 14:01:19)
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA (08/05/2012 19:49:18)
   */
  define("PAGE_LOGIN", "{page_app}user/login");
}

$page_logout = "{page_app}?logout=all";
if (!defined("PAGE_LOGOUT")) {
  /**
   * P&aacute;gina de logout<br>
   * @name PAGE_LOGOUT
   * @var String
   * @since 4.0.0
   * @author BRYAN MAGALH�ES SILVA (31/08/2010 14:01:50)
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA (08/05/2012 19:49:34)
   */
  define("PAGE_LOGOUT", "{page_app}?logout=all");
}

$page_main = "index.php";
if (!defined("PAGE_MAIN")) {
  /**
   * P&aacute;gina principal<br>
   * @name PAGE_MAIN
   * @var String
   * @since 4.0.0
   * @author BRYAN MAGALH�ES SILVA (31/08/2010 14:00:53)
   * <br><b>Updated by</b> BRYAN MAGALH�ES SILVA (31/08/2010 14:51:25)
   */
  define("PAGE_MAIN", "index.php");
}

$page_manutencao = "{page_app}user/manutencao";
if (!defined("PAGE_MANUTENCAO")) {
  /**
   * P&aacute;gina de manuten&ccedil;&atilde;o do sistema<br>
   * @name PAGE_MANUTENCAO
   * @var String
   * @since 4.0.0
   * @author WILLIAM MARQUES VICENTE GOMES CORR&Ecirc;A (25/11/2012 04:51:29)
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORR&Ecirc;A (25/11/2012 04:57:33)
   */
  define("PAGE_MANUTENCAO", "{page_app}user/manutencao");
}

$page_photo = "{page_app}files/foto/";
if (!defined("PAGE_PHOTO")) {
  /**
   * P&aacute;gina de acesso para imagens de alunos<br>
   * @name PAGE_PHOTO
   * @var String
   * @since 4.0.0
   * @author BRYAN MAGALH�ES SILVA (31/08/2010 14:10:35)
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORR&Ecirc;A (01/04/2013 13:58:24)
   */
  define("PAGE_PHOTO", "{page_app}files/foto/");
}

$page_privacity = "http://www.fagoc.br/privacidade";
if (!defined("PAGE_PRIVACITY")) {
  /**
   * P&aacute;gina que possui os termos de Privacidade da Fagoc<br>
   * @name PAGE_PRIVACITY
   * @var String
   * @since 4.0.0
   * @author WILLIAM MARQUES VICENTE GOMES CORREA (22/03/2012 09:35:41)
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA (22/03/2012 09:35:41)
   */
  define("PAGE_PRIVACITY", "http://www.fagoc.br/privacidade");
}

$page_public = "{page_app}files/public";
if (!defined("PAGE_PUBLIC")) {
  /**
   * Url de arquivos p&uacute;blicos<br>
   * @name PAGE_PUBLIC
   * @var String
   * @since 4.0.0
   * @author RENATA APARECIDA BENINI (29/07/2011 17:44:12)
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORR&Ecirc;A (22/03/2013 15:34:37)
   */
  define("PAGE_PUBLIC", "{page_app}files/public");
}

$page_remember = "{page_app}user/change";
if (!defined("PAGE_REMEMBER")) {
  /**
   * Pagina para recuperacao de senha<br>
   * @name PAGE_REMEMBER
   * @var String
   * @since 4.0.0
   * @author FABR�CIO ANDR� DA COSTA (19/01/2012 10:18:43)
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA (08/05/2012 19:49:46)
   */
  define("PAGE_REMEMBER", "{page_app}user/change");
}

$page_services = "{page_app}app/services/";
if (!defined("PAGE_SERVICES")) {
  /**
   * Caminho do service<br>
   * @name PAGE_SERVICES
   * @var String
   * @since 4.0.0
   * @author RENATA APARECIDA BENINI (29/07/2011 17:45:39)
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORR&Ecirc;A (22/03/2013 14:50:28)
   */
  define("PAGE_SERVICES", "{page_app}app/services/");
}

$page_temp = "{page_app}temp/";
if (!defined("PAGE_TEMP")) {
  /**
   * Url de arquivos tempor&aacute;rios<br>
   * @name PAGE_TEMP
   * @var String
   * @since 4.0.0
   * @author BRYAN MAGALH�ES SILVA (31/08/2010 14:08:21)
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORR&Ecirc;A (22/03/2013 15:33:05)
   */
  define("PAGE_TEMP", "{page_app}temp/");
}

$page_term = "http://www.fagoc.br/termo";
if (!defined("PAGE_TERM")) {
  /**
   * P&aacute;gina que exibe os termos de uso das ferramentas da Fagoc<br>
   * @name PAGE_TERM
   * @var String
   * @since 4.0.0
   * @author WILLIAM MARQUES VICENTE GOMES CORREA (22/03/2012 09:36:44)
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA (22/03/2012 09:36:44)
   */
  define("PAGE_TERM", "http://www.fagoc.br/termo");
}

$path_app = "{path_app}";
if (!defined("PATH_APP")) {
  /**
   * Caminho do real do sistema no servidor<br>
   * @name PATH_APP
   * @var String
   * @since 4.0.0
   * @author BRYAN MAGALH�ES SILVA (31/08/2010 14:04:02)
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORR&Ecirc;A (22/03/2013 14:22:50)
   */
  define("PATH_APP", "{path_app}");
}

$path_atendimento = "{path_app}files/atendimento/";
if (!defined("PATH_ATENDIMENTO")) {
  /**
   * Caminho para arquivos do atendmento<br>
   * @name PATH_ATENDIMENTO
   * @var String
   * @since 4.0.0
   * @author FELIPE DE FREITAS CARNEIRO (12/11/2012 17:38:49)
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORR&Ecirc;A (22/03/2013 15:13:49)
   */
  define("PATH_ATENDIMENTO", "{path_app}files/atendimento/");
}

$path_cookie = "/";
if (!defined("PATH_COOKIE")) {
  /**
   * Caminho relativo para registrar cookies<br>
   * @name PATH_COOKIE
   * @var String
   * @since 4.0.0
   * @author WILLIAM MARQUES VICENTE GOMES CORREA (11/05/2012 17:21:23)
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORR&Ecirc;A (22/03/2013 14:58:19)
   */
  define("PATH_COOKIE", "/");
}

$path_image = "{page_app}custom/images/";
if (!defined("PATH_IMAGE")) {
  /**
   * Caminho das imagens<br>
   * @name PATH_IMAGE
   * @var String
   * @since 4.0.0
   * @author BRYAN MAGALH�ES SILVA (31/08/2010 14:02:55)
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORR&Ecirc;A (22/03/2013 14:31:05)
   */
  define("PATH_IMAGE", "{page_app}custom/images/");
}

$path_photo = "{path_app}files/foto/";
if (!defined("PATH_PHOTO")) {
  /**
   * Caminho pasta com fotos dos alunos<br>
   * @name PATH_PHOTO
   * @var String
   * @since 4.0.0
   * @author BRYAN MAGALH�ES SILVA (31/08/2010 14:10:04)
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORR&Ecirc;A (01/04/2013 13:58:17)
   */
  define("PATH_PHOTO", "{path_app}files/foto/");
}

$path_public = "{path_app}files/public/";
if (!defined("PATH_PUBLIC")) {
  /**
   * Pasta de Arquivos p&uacute;blicos<br>
   * @name PATH_PUBLIC
   * @var String
   * @since 4.0.0
   * @author RENATA APARECIDA BENINI (29/07/2011 17:43:18)
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORR&Ecirc;A (22/03/2013 14:49:54)
   */
  define("PATH_PUBLIC", "{path_app}files/public/");
}

$path_temp = "files/temp/";
if (!defined("PATH_TEMP")) {
  /**
   * Caminho da pasta de arquivos tempor&aacute;rios<br>
   * @name PATH_TEMP
   * @var String
   * @since 4.0.0
   * @author BRYAN MAGALH�ES SILVA (31/08/2010 14:08:00)
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORR&Ecirc;A (22/03/2013 14:43:36)
   */
  define("PATH_TEMP", "files/temp/");
}

$short = "{short}";
if (!defined("SHORT")) {
  /**
   * Descri&ccedil;&atilde;o curta do nome do sistema<br>
   * @name SHORT
   * @var String
   * @since 4.0.0
   * @author  (26/11/2010 11:40:40)
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORR&Ecirc;A (03/04/2013 17:11:10)
   */
  define("SHORT", "{short}");
}

$show_message_error = 2;
if (!defined("SHOW_MESSAGE_ERROR")) {
  /**
   * Constante que define o tipo de mensagem a ser exibida<br>
   * @name SHOW_MESSAGE_ERROR
   * @var Numeric
   * @since 4.1.2
   * @author WILLIAM MARQUES VICENTE GOMES CORR&Ecirc;A (06/05/2013 00:40:35)
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORR&Ecirc;A (06/05/2013 00:40:35)
   */
  define("SHOW_MESSAGE_ERROR", 2);
}

$show_message_success = 1;
if (!defined("SHOW_MESSAGE_SUCCESS")) {
  /**
   * Constante que define o tipo de mensagem a ser exibida<br>
   * @name SHOW_MESSAGE_SUCCESS
   * @var Numeric
   * @since 4.1.2
   * @author WILLIAM MARQUES VICENTE GOMES CORR&Ecirc;A (06/05/2013 00:40:10)
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORR&Ecirc;A (06/05/2013 00:40:10)
   */
  define("SHOW_MESSAGE_SUCCESS", 1);
}

$title = "{title}";
if (!defined("TITLE")) {
  /**
   * &lt;p&gt;T&iacute;tulo da P&aacute;gina&lt;/p&gt;<br>
   * @name TITLE
   * @var String
   * @since 4.0.0
   * @author WILLIAM MARQUES VICENTE GOMES CORR�A (31/08/2010 13:48:23)
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORR&Ecirc;A (29/04/2013 21:27:23)
   */
  define("TITLE", "{title}");
}

$user_image = "custom/images/logo/logo.png";
if (!defined("USER_IMAGE")) {
  /**
   * Imagem que sera exibida como plano de fundo do sistema<br>
   * @name USER_IMAGE
   * @var String
   * @since 4.0.0
   * @author  (09/12/2010 22:27:48)
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORR&Ecirc;A (22/03/2013 14:36:42)
   */
  define("USER_IMAGE", "custom/images/logo/logo.png");
}

$utf8_print = "";
if (!defined("UTF8_PRINT")) {
  /**
   * Especifica qual a codifica&ccedil;&atilde;o usada para a impress&atilde;o dos dados<br>
   * @name UTF8_PRINT
   * @var String
   * @since 1.0.0.11
   * @author ADMINISTRADOR (08/06/2013 17:18:14)
   * <br><b>Updated by</b> ADMINISTRADOR (09/06/2013 09:53:37)
   */
  define("UTF8_PRINT", "");
}

$utf8_request = "decode";
if (!defined("UTF8_REQUEST")) {
  /**
   * Define a codifica&ccedil;&atilde;o usada nas requisi&ccedil;&otilde;es<br>
   * @name UTF8_REQUEST
   * @var String
   * @since 1.0.0.11
   * @author ADMINISTRADOR (08/06/2013 17:26:58)
   * <br><b>Updated by</b> ADMINISTRADOR (09/06/2013 09:51:31)
   */
  define("UTF8_REQUEST", "decode");
}

$utf8_system = "encode";
if (!defined("UTF8_SYSTEM")) {
  /**
   * Especifica qual a codifica&ccedil;&atilde;o usada para a impress&atilde;o dos dados<br>
   * @name UTF8_SYSTEM
   * @var String
   * @since 1.0.0.11
   * @author ADMINISTRADOR (09/06/2013 09:51:57)
   * <br><b>Updated by</b> ADMINISTRADOR (09/06/2013 09:53:49)
   */
  define("UTF8_SYSTEM", "encode");
}

$version = "1.0.0.0";
if (!defined("VERSION")) {
  /**
   * Vers&atilde;o atual do sistema<br>
   * @name VERSION
   * @var String
   * @since 4.0.0
   * @author BRYAN MAGALH�ES SILVA (06/05/2013 17:18:40)
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO (06/05/2013 17:18:40)
   */
  define("VERSION", "1.0.0.0");
}

$version_library = serialize(array("system"=>"1.0.0.0","extjs"=>"3.2.1.0","jquery"=>"1.5.1.0","codemirror"=>"2.3.0.0","ckeditor"=>"4.1.1.0"));
if (!defined("VERSION_LIBRARY")) {
  /**
   * Vers&atilde;o de cada biblioteca a ser carregada<br>
   * @name VERSION_LIBRARY
   * @var String
   * @since 1.0.0.32
   * @author ADMINISTRADOR (18/06/2013 20:59:59)
   * <br><b>Updated by</b> ADMINISTRADOR (18/06/2013 21:13:30)
   */
  define("VERSION_LIBRARY", serialize(array("system"=>"1.0.0.0","extjs"=>"3.2.1.0","jquery"=>"1.5.1.0","codemirror"=>"2.3.0.0","ckeditor"=>"4.1.1.0")));
}

$window_height = 650;

$window_height = Cookie::get('height', $window_height);

if (!defined("WINDOW_HEIGHT")) {
  /**
   * Altura padr&atilde;o dos componentes de tela<br>
   * @name WINDOW_HEIGHT
   * @var Numeric
   * @since 4.0.0
   * @author BRYAN MAGALH�ES SILVA (31/08/2010 14:36:43)
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO (24/03/2013 17:52:58)
   */
  define("WINDOW_HEIGHT", $window_height);
}

$window_width = 1180;

$window_width = Cookie::get('width', $window_width);

if (!defined("WINDOW_WIDTH")) {
  /**
   * Largura padr&atilde;o dos componentes de tela<br>
   * @name WINDOW_WIDTH
   * @var Numeric
   * @since 4.0.0
   * @author BRYAN MAGALH�ES SILVA (31/08/2010 14:36:11)
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORR&Ecirc;A (27/04/2013 17:46:07)
   */
  define("WINDOW_WIDTH", $window_width);
}

