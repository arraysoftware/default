
DELETE FROM `TBL_PARAMETROS` WHERE `par_codigo` <= 73;
INSERT INTO `TBL_PARAMETROS` (`par_codigo`,`par_variavel`,`par_string`,`par_descricao`,`par_valor`,`par_version`,`par_procedimento`,`par_responsavel`,`par_alteracao`,`par_registro`,`par_criador`) VALUES
 ( 1,'short'                       ,1,'Descri��o curta do nome do sistema'                                                 ,'{short}'                             ,'0.0.0.1','','ADMINISTRADOR',NOW(),NOW(),'ADMINISTRADOR'),
 ( 2,'title'                       ,1,'T�tulo da P�gina'                                                                   ,'{title}'                             ,'0.0.0.1','','ADMINISTRADOR',NOW(),NOW(),'ADMINISTRADOR'),
 ( 3,'company'                     ,1,'Nome Fantasia da Empresa'                                                           ,'Nome Fantasia da Empresa'            ,'0.0.0.1','','ADMINISTRADOR',NOW(),NOW(),'ADMINISTRADOR'),
 ( 4,'company_name'                ,1,'Nome da institui��o'                                                                ,'Nome da institui��o'                 ,'0.0.0.1','','ADMINISTRADOR',NOW(),NOW(),'ADMINISTRADOR'),
 ( 5,'company_name_sigla'          ,1,'Sigla referente ao nome da institui��o'                                             ,''                                    ,'0.0.0.1','','ADMINISTRADOR',NOW(),NOW(),'ADMINISTRADOR'),
 ( 6,'company_cnpj'                ,1,'CNPJ da institui��o'                                                                ,'CNPJ da institui��o'                 ,'0.0.0.1','','ADMINISTRADOR',NOW(),NOW(),'ADMINISTRADOR'),
 ( 7,'company_address'             ,1,'Endere�o da institui��o'                                                            ,'Endere�o da institui��o'             ,'0.0.0.1','','ADMINISTRADOR',NOW(),NOW(),'ADMINISTRADOR'),
 ( 8,'company_state'               ,1,'Estado'                                                                             ,'Estado da institui��o'               ,'0.0.0.1','','ADMINISTRADOR',NOW(),NOW(),'ADMINISTRADOR'),
 ( 9,'company_city'                ,1,'Cidade'                                                                             ,'Cidade da institui��o'               ,'0.0.0.1','','ADMINISTRADOR',NOW(),NOW(),'ADMINISTRADOR'),
 (10,'company_cep'                 ,1,'Cep da organiza��o'                                                                 ,''                                    ,'0.0.0.1','','ADMINISTRADOR',NOW(),NOW(),'ADMINISTRADOR'),
 (11,'company_email'               ,1,'Email de contato da institui��o'                                                    ,''                                    ,'0.0.0.1','','ADMINISTRADOR',NOW(),NOW(),'ADMINISTRADOR'),
 (12,'company_district'            ,1,'Bairro da Institui��o'                                                              ,''                                    ,'0.0.0.1','','ADMINISTRADOR',NOW(),NOW(),'ADMINISTRADOR'),
 (13,'company_short'               ,1,'Descri��o curta da institui��o'                                                     ,''                                    ,'0.0.0.1','','ADMINISTRADOR',NOW(),NOW(),'ADMINISTRADOR'),
 (14,'company_site'                ,1,'Descri��o do site da institui��o'                                                   ,'{page_app}'                          ,'0.0.0.1','','ADMINISTRADOR',NOW(),NOW(),'ADMINISTRADOR'),
 (15,'company_uf'                  ,1,'Descri��o da Unidade Federativa da institui��o'                                     ,'UF'                                  ,'0.0.0.1','','ADMINISTRADOR',NOW(),NOW(),'ADMINISTRADOR'),
 (16,'company_telefone'            ,1,'Telefone principal da institui��o'                                                  ,'(32) 3021-1219'                      ,'0.0.0.1','','ADMINISTRADOR',NOW(),NOW(),'ADMINISTRADOR'),
 (17,'company_cx'                  ,1,'Caixa Postal da institui��o'                                                        ,''                                    ,'0.0.0.1','','ADMINISTRADOR',NOW(),NOW(),'ADMINISTRADOR'),
 (18,'company_razao_social'        ,1,'Raz�o Social da Empresa'                                                            ,''                                    ,'0.0.0.1','','ADMINISTRADOR',NOW(),NOW(),'ADMINISTRADOR'),
 (19,'company_leader'              ,1,'Refer�ncia ao nome do representante legal da instui��o'                             ,''                                    ,'0.0.0.1','','ADMINISTRADOR',NOW(),NOW(),'ADMINISTRADOR'),
 (20,'company_z'                   ,1,'Concatena principais dados da empresa'                                              ,''                                    ,'0.0.0.1','','ADMINISTRADOR',NOW(),NOW(),'ADMINISTRADOR'),
                              
 (21,'page_login'                  ,1,'P�gina de login'                                                                    ,'{page_app}user/login'                ,'0.0.0.1','','ADMINISTRADOR',NOW(),NOW(),'ADMINISTRADOR'),
 (22,'page_logout'                 ,1,'P�gina de logout'                                                                   ,'{page_app}user/logout'               ,'0.0.0.1','','ADMINISTRADOR',NOW(),NOW(),'ADMINISTRADOR'),
 (23,'page_app'                    ,1,'Caminho principal'                                                                  ,'{page_app}'                          ,'0.0.0.1','','ADMINISTRADOR',NOW(),NOW(),'ADMINISTRADOR'),
 (24,'path_image'                  ,1,'Caminho das imagens'                                                                ,'{page_app}custom/images/'            ,'0.0.0.1','','ADMINISTRADOR',NOW(),NOW(),'ADMINISTRADOR'),
 (25,'page_temp'                   ,1,'Url de arquivos tempor�rios'                                                        ,'{page_app}temp/'                     ,'0.0.0.1','','ADMINISTRADOR',NOW(),NOW(),'ADMINISTRADOR'),
 (26,'page_photo'                  ,1,'P�gina de acesso para imagens de alunos'                                            ,'{page_app}files/foto/'               ,'0.0.0.1','','ADMINISTRADOR',NOW(),NOW(),'ADMINISTRADOR'),
 (27,'page_public'                 ,1,'Url de arquivos p�blicos'                                                           ,'{page_app}files/public'              ,'0.0.0.1','','ADMINISTRADOR',NOW(),NOW(),'ADMINISTRADOR'),
 (28,'page_services'               ,1,'Caminho do service'                                                                 ,'{page_app}app/services/'             ,'0.0.0.1','','ADMINISTRADOR',NOW(),NOW(),'ADMINISTRADOR'),
 (29,'page_remember'               ,1,'Pagina para recuperacao de senha'                                                   ,'{page_app}user/change'               ,'0.0.0.1','','ADMINISTRADOR',NOW(),NOW(),'ADMINISTRADOR'),
 (30,'page_privacity'              ,1,'P�gina que possui os termos de Privacidade do Aplicativo'                           ,'{page_app}privacidade'               ,'0.0.0.1','','ADMINISTRADOR',NOW(),NOW(),'ADMINISTRADOR'),
 (31,'page_term'                   ,1,'P�gina que exibe os termos de uso da ferramenta'                                    ,'{page_app}termo'                     ,'0.0.0.1','','ADMINISTRADOR',NOW(),NOW(),'ADMINISTRADOR'),
 (32,'page_download'               ,1,'P�gina de downloads'                                                                ,'{page_app}download/?file'            ,'0.0.0.1','','ADMINISTRADOR',NOW(),NOW(),'ADMINISTRADOR'),
 (33,'page_manutencao'             ,1,'P�gina de manuten��o do sistema'                                                    ,'{page_app}user/manutencao'           ,'0.0.0.1','','ADMINISTRADOR',NOW(),NOW(),'ADMINISTRADOR'),
 (34,'page_css'                    ,1,'Url do gerenciador de CSS'                                                          ,'{page_app}custom/css/'               ,'0.0.0.1','','ADMINISTRADOR',NOW(),NOW(),'ADMINISTRADOR'),
 (35,'page_javascript'             ,1,'Url do gerenciador de Javascript'                                                   ,'{page_app}custom/js/'                ,'0.0.0.1','','ADMINISTRADOR',NOW(),NOW(),'ADMINISTRADOR'),
 (36,'page_image'                  ,1,'URL das imagens do projeto'                                                         ,'{page_app}custom/images/'            ,'0.0.0.1','','ADMINISTRADOR',NOW(),NOW(),'ADMINISTRADOR'),
 (37,'page_library'                ,1,'URL onde est�o hospedadas as bibliotecas do sistema'                                ,'{page_app}lib/'                      ,'0.0.0.1','','ADMINISTRADOR',NOW(),NOW(),'ADMINISTRADOR'),
                             
 (38,'path_app'                    ,1,'Caminho do real do sistema no servidor'                                             ,'{path_app}'                          ,'0.0.0.1','','ADMINISTRADOR',NOW(),NOW(),'ADMINISTRADOR'),
 (39,'path_photo'                  ,1,'Caminho pasta com fotos dos alunos'                                                 ,'{path_app}files/foto/'               ,'0.0.0.1','','ADMINISTRADOR',NOW(),NOW(),'ADMINISTRADOR'),
 (40,'path_public'                 ,1,'Pasta de Arquivos p�blicos'                                                         ,'{path_app}files/public/'             ,'0.0.0.1','','ADMINISTRADOR',NOW(),NOW(),'ADMINISTRADOR'),
                             
 (41,'page_main'                   ,1,'P�gina principal'                                                                   ,'index.php'                           ,'0.0.0.1','','ADMINISTRADOR',NOW(),NOW(),'ADMINISTRADOR'),
                             
 (42,'path_temp'                   ,1,'Caminho da pasta de arquivos tempor�rios'                                           ,'files/temp/'                         ,'0.0.0.1','','ADMINISTRADOR',NOW(),NOW(),'ADMINISTRADOR'),
 (43,'path_cookie'                 ,1,'Caminho relativo para registrar cookies'                                            ,'/'                                   ,'0.0.0.1','','ADMINISTRADOR',NOW(),NOW(),'ADMINISTRADOR'),
                     
 (44,'copy_right'                  ,1,'Texto de direitos autorais'                                                         ,'all rights reserved � {host} {year}' ,'0.0.0.1','','ADMINISTRADOR',NOW(),NOW(),'ADMINISTRADOR'),
 (45,'version'                     ,1,'Vers�o atual do sistema'                                                            ,'0.0.0.1'                             ,'0.0.0.1','','ADMINISTRADOR',NOW(),NOW(),'ADMINISTRADOR'),
 (46,'host'                        ,1,'Dominio do servidor onde est� hospedado o aplicativo'                               ,'{host}'                              ,'0.0.0.1','','ADMINISTRADOR',NOW(),NOW(),'ADMINISTRADOR'),
 (47,'default_user_system'         ,1,'Usu�rio padr�o do sistema'                                                          ,'{short} :: ADMINISTRADOR'            ,'0.0.0.1','','ADMINISTRADOR',NOW(),NOW(),'ADMINISTRADOR'),
 (48,'default_database'            ,1,'Base de dados padr�o do sistema'                                                    ,'{database}'                          ,'0.0.0.1','','ADMINISTRADOR',NOW(),NOW(),'ADMINISTRADOR'),
 (49,'default_extension'           ,1,'Solu��o para projetos em adapta��o'                                                 ,''                                    ,'0.0.0.1','','ADMINISTRADOR',NOW(),NOW(),'ADMINISTRADOR'),
 (50,'logo'                        ,1,'Logo Pequeno'                                                                       ,'custom/images/logo/small.png'        ,'0.0.0.1','','ADMINISTRADOR',NOW(),NOW(),'ADMINISTRADOR'),
 (51,'encrypt_key'                 ,1,''                                                                                   ,''                                    ,'0.0.0.1','','ADMINISTRADOR',NOW(),NOW(),'ADMINISTRADOR'),
 (52,'itens'                       ,0,'Quantidade de itens padr�o carregada nas visualiza��es de itens'                    ,'25'                                  ,'0.0.0.1','','ADMINISTRADOR',NOW(),NOW(),'ADMINISTRADOR'),
              
 (53,'modo_teste'                  ,1,'Vari�vel para controle de simula��es de teste'                                      ,'false'                               ,'0.0.0.1','','ADMINISTRADOR',NOW(),NOW(),'ADMINISTRADOR'),
 (54,'modo_legado'                 ,1,'Adapta o sistema para se comportar com aplica��es antigas'                          ,'false'                               ,'0.0.0.1','','ADMINISTRADOR',NOW(),NOW(),'ADMINISTRADOR'),
 (55,'utf8_print'                  ,1,'Especifica qual a codifica��o usada para a impress�o dos dados'                     ,''                                    ,'0.0.0.1','','ADMINISTRADOR',NOW(),NOW(),'ADMINISTRADOR'),
 (59,'utf8_system'                 ,1,'Especifica qual a codifica��o usada para a impress�o dos dados'                     ,'encode'                              ,'0.0.0.1','','ADMINISTRADOR',NOW(),NOW(),'ADMINISTRADOR'),
 (56,'utf8_request'                ,1,'Define a codifica��o usada nas requisi��es'                                         ,'decode'                              ,'0.0.0.1','','ADMINISTRADOR',NOW(),NOW(),'ADMINISTRADOR'),
 (57,'html_entities_request'       ,1,'Determina a aplica��o de convers�o nas requisi��es'                                 ,''                                    ,'0.0.0.1','','ADMINISTRADOR',NOW(),NOW(),'ADMINISTRADOR'),
 (58,'add_slashes_request'         ,1,'Especifica o uso ou n�o de slashes nas requisi��es'                                 ,''                                    ,'0.0.0.1','','ADMINISTRADOR',NOW(),NOW(),'ADMINISTRADOR'),
 (60,'manutencao'                  ,0,'Deixa o sistema em manuten��o'                                                      ,'false'                               ,'0.0.0.1','','ADMINISTRADOR',NOW(),NOW(),'ADMINISTRADOR'),
 (61,'dir_image_app'               ,1,'Diret�rio do projeto onde fica hospedado o aplicativo de edi��o de imagens'         ,'upload/image/'                       ,'0.0.0.1','','ADMINISTRADOR',NOW(),NOW(),'ADMINISTRADOR'),
               
 (62,'juros_mora_default'          ,1,''                                                                                   ,'2.00'                                ,'0.0.0.1','','ADMINISTRADOR',NOW(),NOW(),'ADMINISTRADOR'),
 (63,'juros_rata_default'          ,1,''                                                                                   ,'0.03'                                ,'0.0.0.1','','ADMINISTRADOR',NOW(),NOW(),'ADMINISTRADOR'),
 (64,'dias_de_prazo_para_pagamento',0,'Prazo para pagamento do boleto'                                                     ,'10'                                  ,'0.0.0.1','','ADMINISTRADOR',NOW(),NOW(),'ADMINISTRADOR'),
 (65,'codigo_banco_default'        ,0,'C�digo referente ao banco padr�o para gera��o de boletos'                           ,'0'                                   ,'0.0.0.1','','ADMINISTRADOR',NOW(),NOW(),'ADMINISTRADOR'),
               
 (66,'email_debug'                 ,1,'Endere�o de e-mail para debug'                                                      ,''                                    ,'0.0.0.1','','ADMINISTRADOR',NOW(),NOW(),'ADMINISTRADOR'),
 (68,'email_assunto'               ,1,'Assunto padr�o do envio de email do sistema'                                        ,'[{short}]'                           ,'0.0.0.1','','ADMINISTRADOR',NOW(),NOW(),'ADMINISTRADOR'),
 (69,'email_erro'                  ,1,'Email padr�o para as mensagens de erro do sistema'                                  ,'{admin_email}'                       ,'0.0.0.1','','ADMINISTRADOR',NOW(),NOW(),'ADMINISTRADOR'),
 (72,'email_smtp_dominio'          ,1,'Endere�o do servidor de SMTP padr�o para envio de notifica��es do sistema'          ,'{host}'                              ,'0.0.0.1','','ADMINISTRADOR',NOW(),NOW(),'ADMINISTRADOR'),
 (67,'email_remetente'             ,1,'Remetente de email padr�o do sistema'                                               ,'contato@{host}'                      ,'0.0.0.1','','ADMINISTRADOR',NOW(),NOW(),'ADMINISTRADOR'),
 (71,'email_senha'                 ,1,'Senha do email de envio de notifica��es padr�o do Sistema'                          ,''                                    ,'0.0.0.1','','ADMINISTRADOR',NOW(),NOW(),'ADMINISTRADOR'),
 (70,'email_smtp_porta'            ,0,'Porta padr�o do Servidor de SMTP para envio de mensagens de notifica��es do sistema','0'                                   ,'0.0.0.1','','ADMINISTRADOR',NOW(),NOW(),'ADMINISTRADOR'),
 (73,'email_smtp_timeout'          ,0,'Configura��o de tempo de espera para autentica��o para conex�o com servidor de SMTP','1800'                                ,'0.0.0.1','','ADMINISTRADOR',NOW(),NOW(),'ADMINISTRADOR');

DELETE FROM `TBL_USUARIO` WHERE `usu_codigo` = 1;
INSERT INTO `TBL_USUARIO` (`usu_codigo`,`usu_cod_USUARIO_TIPO`,`usu_login`,`usu_password`,`usu_nome`,`usu_admin`,`usu_email`,`usu_ativo`,`usu_auth`,`usu_responsavel`,`usu_criador`,`usu_alteracao`,`usu_registro`) VALUES 
 (1,1,'admin',MD5(NOW()),'ADMINISTRADOR',1,'',1,'','ADMINISTRADOR','ADMINISTRADOR',NOW(),NOW());

DELETE FROM `TBL_USUARIO_TIPO` WHERE `ust_codigo` = 1;
INSERT INTO `TBL_USUARIO_TIPO` (`ust_codigo`,`ust_descricao`,`ust_ativo`,`ust_responsavel`,`ust_criador`,`ust_alteracao`,`ust_registro`) VALUES 
 (1,'Administrador',1,'ADMINISTRADOR','ADMINISTRADOR',NOW(),NOW());

DELETE FROM `TBL_MENU` WHERE `men_codigo` <= 3;
INSERT INTO `TBL_MENU` (`men_codigo`,`men_descricao`,`men_ordem`,`men_ativo`,`men_alteracao`,`men_registro`,`men_responsavel`,`men_criador`) VALUES
 (1,'Administra��o',1 ,1,NOW(),NOW(),'ADMINISTRADOR','ADMINISTRADOR'),
 (2,'Site'         ,2 ,1,NOW(),NOW(),'ADMINISTRADOR','ADMINISTRADOR'),
 (3,'Ajuda'        ,10,1,NOW(),NOW(),'ADMINISTRADOR','ADMINISTRADOR');

DELETE FROM `TBL_MENU_FUNCAO` WHERE `mef_codigo` <= 40;
INSERT INTO `TBL_MENU_FUNCAO` (`mef_codigo`,`mef_cod_MENU`,`mef_descricao`,`mef_module`,`mef_entity`,`mef_action`,`mef_separator`,`mef_order`,`mef_ativo`,`mef_alteracao`,`mef_registro`,`mef_responsavel`,`mef_criador`) VALUES
 (21,2,'M�dulo'           ,'site','ConteudoModulo'   ,'conteudomodulo-v-list'   ,0,1,1,NOW(),NOW(),'ADMINISTRADOR','ADMINISTRADOR'),
 (22,2,'Categoria'        ,'site','ConteudoCategoria','conteudocategoria-v-list',0,2,1,NOW(),NOW(),'ADMINISTRADOR','ADMINISTRADOR'),
 (23,2,'Se��o'            ,'site','ConteudoSecao'    ,'conteudosecao-v-list'    ,0,3,1,NOW(),NOW(),'ADMINISTRADOR','ADMINISTRADOR'),
 (24,2,'Conte�do'         ,'site','Conteudo'         ,'conteudo-v-list'         ,0,4,1,NOW(),NOW(),'ADMINISTRADOR','ADMINISTRADOR'),
 (25,2,'Artigos'          ,'site','ConteudoArtigo'   ,'conteudoartigo-v-list'   ,1,5,1,NOW(),NOW(),'ADMINISTRADOR','ADMINISTRADOR'),
 (26,2,'Segmentos'        ,'site','WidgetSegmento'   ,'widgetsegmento-v-list'   ,1,6,1,NOW(),NOW(),'ADMINISTRADOR','ADMINISTRADOR'),
 (27,2,'Widget'           ,'site','Widget'           ,'widget-v-list'           ,0,7,1,NOW(),NOW(),'ADMINISTRADOR','ADMINISTRADOR'),
 (31,3,'Conte�do da Ajuda','help','Ajuda'            ,'help'                    ,0,1,1,NOW(),NOW(),'ADMINISTRADOR','ADMINISTRADOR'),
 (32,3,'Suporte Online'   ,'help','Suporte'          ,'suport'                  ,1,2,1,NOW(),NOW(),'ADMINISTRADOR','ADMINISTRADOR'),
 (33,3,'Sobre'            ,'help','Sobre'            ,'about'                   ,0,3,1,NOW(),NOW(),'ADMINISTRADOR','ADMINISTRADOR');

DELETE FROM `TBL_CONTEUDO_ARTIGO` WHERE `cta_codigo` <= 6;
INSERT INTO `TBL_CONTEUDO_ARTIGO` (`cta_codigo`,`cta_titulo`,`cta_slug`,`cta_resumo`,`cta_tipo`,`cta_side_bar`,`cta_widget`,`cta_conteudo`,`cta_publicar`,`cta_data_publicacao`,`cta_hit`,`cta_featured`,`cta_miniatura`,`cta_alteracao`,`cta_registro`,`cta_responsavel`,`cta_criador`) VALUES
 (1, '{title}'              ,'home'  ,''                                                                                                            ,5,'left',1,'',1,'2013-05-28 14:05:39',0,0,'',NOW(),NOW(),'ADMINISTRADOR','ADMINISTRADOR'),
 (2, 'P�gina de Login'      ,'login' ,''                                                                                                            ,5,'left',0,'',1,'0000-00-00 00:00:00',0,0,'',NOW(),NOW(),'ADMINISTRADOR','ADMINISTRADOR'),
 (3, 'P�gina de Pesquisa'   ,'search',''                                                                                                            ,5,'hide',1,'',1,'0000-00-00 00:00:00',0,0,'',NOW(),NOW(),'ADMINISTRADOR','ADMINISTRADOR'),
 (4, 'P�gina privada'       ,'403'   ,'A p�gina informada � privada. Para acess�-la � preciso estar conectado'                                      ,0,'left',1,'',1,'0000-00-00 00:00:00',0,0,'',NOW(),NOW(),'ADMINISTRADOR','ADMINISTRADOR'),
 (5, 'P�gina n�o encontrada','404'   ,'A p�gina informada n�o foi encontrada :('                                                                    ,0,'left',1,'',1,'0000-00-00 00:00:00',0,0,'',NOW(),NOW(),'ADMINISTRADOR','ADMINISTRADOR'),
 (6, 'P�gina n�o publicada' ,'500'   ,'A p�gina informada n�o est� publicada no momento. Ela pode ter sido removida do site ou estar em manuten��o.',0,'left',1,'',1,'0000-00-00 00:00:00',0,0,'',NOW(),NOW(),'ADMINISTRADOR','ADMINISTRADOR');
