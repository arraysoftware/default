DROP TABLE IF EXISTS `TBL_ERRO`;
CREATE TABLE `TBL_ERRO` (
  `err_codigo` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `err_referencia` varchar(250) NOT NULL,
  `err_descricao` varchar(250) NOT NULL,
  `err_comando` text NOT NULL,
  `err_rastreio` text NOT NULL,
  `err_navegador` varchar(250) NOT NULL,
  `err_endereco` varchar(250) NOT NULL,
  `err_corrigido` tinyint(1) NOT NULL,
  `err_alteracao` datetime NOT NULL,
  `err_registro` datetime NOT NULL,
  `err_responsavel` varchar(250) NOT NULL,
  `err_criador` varchar(250) NOT NULL,
  PRIMARY KEY (`err_codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `TBL_ATUALIZACAO` (
  `atl_codigo` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `atl_versao` varchar(250) NOT NULL,
  `atl_tipo` varchar(250) NOT NULL,
  `atl_resumo` longtext NOT NULL,
  `atl_arquivo` text NOT NULL,
  `atl_alteracao` datetime NOT NULL,
  `atl_registro` datetime NOT NULL,
  `atl_responsavel` varchar(250) NOT NULL,
  `atl_criador` varchar(250) NOT NULL,
  PRIMARY KEY (`atl_codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `TBL_CONTEUDO` (
  `ctd_codigo` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ctd_cod_CONTEUDO_SECAO` int(10) unsigned NOT NULL,
  `ctd_cod_CONTEUDO_ARTIGO` int(10) unsigned NOT NULL,
  `ctd_descricao` varchar(250) NOT NULL,
  `ctd_alteracao` datetime NOT NULL,
  `ctd_registro` datetime NOT NULL,
  `ctd_responsavel` varchar(250) NOT NULL,
  `ctd_criador` varchar(250) NOT NULL,
  PRIMARY KEY (`ctd_codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

CREATE TABLE IF NOT EXISTS `TBL_CONTEUDO_ARTIGO` (
  `cta_codigo` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cta_titulo` varchar(250) NOT NULL,
  `cta_slug` varchar(250) NOT NULL,
  `cta_resumo` longtext NOT NULL,
  `cta_tipo` int(10) unsigned NOT NULL,
  `cta_side_bar` varchar(250) NOT NULL,
  `cta_widget` tinyint(1) NOT NULL,
  `cta_conteudo` longtext NOT NULL,
  `cta_publicar` tinyint(1) NOT NULL,
  `cta_data_publicacao` datetime NOT NULL,
  `cta_hit` int(10) NOT NULL,
  `cta_featured` tinyint(3) unsigned NOT NULL,
  `cta_miniatura` text NOT NULL,
  `cta_alteracao` datetime NOT NULL,
  `cta_registro` datetime NOT NULL,
  `cta_responsavel` varchar(250) NOT NULL,
  `cta_criador` varchar(250) NOT NULL,
  PRIMARY KEY (`cta_codigo`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `TBL_CONTEUDO_ARTIGO_WIDGET` (
  `caw_codigo` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `caw_cod_CONTEUDO_ARTIGO` int(10) unsigned NOT NULL,
  `caw_cod_WIDGET` int(10) unsigned NOT NULL,
  `caw_descricao` text NOT NULL,
  `caw_local` varchar(250) NOT NULL,
  `caw_tamanho` varchar(250) NOT NULL,
  `caw_ordem` int(10) NOT NULL,
  `caw_alteracao` datetime NOT NULL,
  `caw_registro` datetime NOT NULL,
  `caw_responsavel` varchar(250) NOT NULL,
  `caw_criador` varchar(250) NOT NULL,
  PRIMARY KEY (`caw_codigo`),
  KEY `FK_caw_cta_codigo` (`caw_cod_CONTEUDO_ARTIGO`),
  KEY `FK_caw_wid_codigo` (`caw_cod_WIDGET`),
  CONSTRAINT `FK_caw_cta_codigo` FOREIGN KEY (`caw_cod_CONTEUDO_ARTIGO`) REFERENCES `TBL_CONTEUDO_ARTIGO` (`cta_codigo`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_caw_wid_codigo` FOREIGN KEY (`caw_cod_WIDGET`) REFERENCES `TBL_WIDGET` (`wid_codigo`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `TBL_CONTEUDO_CATEGORIA` (
  `ctc_codigo` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ctc_cod_CONTEUDO_MODULO` int(10) unsigned NOT NULL,
  `ctc_cod_CONTEUDO_ARTIGO` int(10) unsigned NOT NULL,
  `ctc_descricao` varchar(250) NOT NULL,
  `ctc_exibir_menu` tinyint(1) NOT NULL,
  `ctc_ordem` int(10) unsigned NOT NULL,
  `ctc_alteracao` datetime NOT NULL,
  `ctc_registro` datetime NOT NULL,
  `ctc_responsavel` varchar(250) NOT NULL,
  `ctc_criador` varchar(250) NOT NULL,
  PRIMARY KEY (`ctc_codigo`),
  KEY `FK_ctc_ctm_codigo` (`ctc_cod_CONTEUDO_MODULO`),
  KEY `FK_TBL_CONTEUDO_CATEGORIA_2` (`ctc_cod_CONTEUDO_ARTIGO`),
  CONSTRAINT `FK_ctc_ctm_codigo` FOREIGN KEY (`ctc_cod_CONTEUDO_MODULO`) REFERENCES `TBL_CONTEUDO_MODULO` (`ctm_codigo`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_TBL_CONTEUDO_CATEGORIA_2` FOREIGN KEY (`ctc_cod_CONTEUDO_ARTIGO`) REFERENCES `TBL_CONTEUDO_ARTIGO` (`cta_codigo`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `TBL_CONTEUDO_MODULO` (
  `ctm_codigo` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ctm_cod_CONTEUDO_ARTIGO` int(10) unsigned NOT NULL,
  `ctm_descricao` varchar(250) NOT NULL,
  `ctm_top` tinyint(1) NOT NULL,
  `ctm_footer` tinyint(1) NOT NULL,
  `ctm_ordem` int(10) unsigned NOT NULL,
  `ctm_alteracao` datetime NOT NULL,
  `ctm_registro` datetime NOT NULL,
  `ctm_responsavel` varchar(250) NOT NULL,
  `ctm_criador` varchar(250) NOT NULL,
  PRIMARY KEY (`ctm_codigo`),
  KEY `FK_TBL_CONTEUDO_MODULO_1` (`ctm_cod_CONTEUDO_ARTIGO`),
  CONSTRAINT `FK_TBL_CONTEUDO_MODULO_1` FOREIGN KEY (`ctm_cod_CONTEUDO_ARTIGO`) REFERENCES `TBL_CONTEUDO_ARTIGO` (`cta_codigo`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `TBL_CONTEUDO_SECAO` (
  `cts_codigo` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cts_cod_CONTEUDO_CATEGORIA` int(10) unsigned NOT NULL,
  `cts_cod_CONTEUDO_ARTIGO` int(10) unsigned NOT NULL,
  `cts_descricao` varchar(250) NOT NULL,
  `cts_ordem` int(10) unsigned NOT NULL,
  `cts_alteracao` datetime NOT NULL,
  `cts_registro` datetime NOT NULL,
  `cts_responsavel` varchar(250) NOT NULL,
  `cts_criador` varchar(250) NOT NULL,
  PRIMARY KEY (`cts_codigo`),
  KEY `FK_TBL_CONTEUDO_SECAO_1` (`cts_cod_CONTEUDO_ARTIGO`),
  KEY `FK_TBL_CONTEUDO_SECAO_2` (`cts_cod_CONTEUDO_CATEGORIA`),
  CONSTRAINT `FK_TBL_CONTEUDO_SECAO_1` FOREIGN KEY (`cts_cod_CONTEUDO_ARTIGO`) REFERENCES `TBL_CONTEUDO_ARTIGO` (`cta_codigo`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_TBL_CONTEUDO_SECAO_2` FOREIGN KEY (`cts_cod_CONTEUDO_CATEGORIA`) REFERENCES `TBL_CONTEUDO_CATEGORIA` (`ctc_codigo`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

CREATE TABLE IF NOT EXISTS `TBL_EMAIL_ENVIO` (
  `eme_codigo` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `eme_id` varchar(250) NOT NULL,
  `eme_email` varchar(250) NOT NULL,
  `eme_assunto` varchar(250) NOT NULL,
  `eme_corpo` text NOT NULL,
  `eme_enviado` tinyint(1) NOT NULL,
  `eme_modelo` varchar(250) NOT NULL,
  `eme_alteracao` datetime NOT NULL,
  `eme_registro` datetime NOT NULL,
  `eme_responsavel` varchar(250) NOT NULL,
  `eme_criador` varchar(250) NOT NULL,
  PRIMARY KEY (`eme_codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `TBL_EMAIL_MODELO` (
  `emo_codigo` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `emo_descricao` varchar(250) NOT NULL,
  `emo_id` varchar(250) NOT NULL,
  `emo_resposta` varchar(250) NOT NULL,
  `emo_assunto` varchar(250) NOT NULL,
  `emo_corpo` longtext NOT NULL,
  `emo_alteracao` datetime NOT NULL,
  `emo_registro` datetime NOT NULL,
  `emo_responsavel` varchar(250) NOT NULL,
  `emo_criador` varchar(250) NOT NULL,
  PRIMARY KEY (`emo_codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `TBL_HISTORICO` (
  `hst_codigo` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `hst_tipo` varchar(250) NOT NULL,
  `hst_tabela` varchar(250) NOT NULL,
  `hst_comando` text NOT NULL,
  `hst_info` varchar(250) NOT NULL,
  `hst_backup` text NOT NULL,
  `hst_ip` varchar(250) NOT NULL,
  `hst_alteracao` datetime NOT NULL,
  `hst_registro` datetime NOT NULL,
  `hst_responsavel` varchar(250) NOT NULL,
  `hst_criador` varchar(250) NOT NULL,
  PRIMARY KEY (`hst_codigo`)
) ENGINE=InnoDB AUTO_INCREMENT=8120 DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `TBL_MENU` (
  `men_codigo` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `men_descricao` varchar(250) NOT NULL,
  `men_ordem` int(10) NOT NULL,
  `men_ativo` tinyint(1) NOT NULL,
  `men_alteracao` datetime NOT NULL,
  `men_registro` datetime NOT NULL,
  `men_responsavel` varchar(250) NOT NULL,
  `men_criador` varchar(250) NOT NULL,
  PRIMARY KEY (`men_codigo`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `TBL_MENU_FUNCAO` (
  `mef_codigo` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `mef_cod_MENU` int(10) unsigned NOT NULL,
  `mef_descricao` varchar(250) NOT NULL,
  `mef_module` varchar(250) NOT NULL,
  `mef_entity` varchar(250) NOT NULL,
  `mef_action` varchar(250) NOT NULL,
  `mef_separator` tinyint(1) NOT NULL,
  `mef_order` int(10) NOT NULL,
  `mef_ativo` tinyint(1) NOT NULL,
  `mef_alteracao` datetime NOT NULL,
  `mef_registro` datetime NOT NULL,
  `mef_responsavel` varchar(250) NOT NULL,
  `mef_criador` varchar(250) NOT NULL,
  PRIMARY KEY (`mef_codigo`),
  KEY `FK_mef_men_codigo` (`mef_cod_MENU`),
  CONSTRAINT `FK_mef_men_codigo` FOREIGN KEY (`mef_cod_MENU`) REFERENCES `TBL_MENU` (`men_codigo`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `TBL_PARAMETROS` (
  `par_codigo` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `par_variavel` varchar(255) NOT NULL,
  `par_string` tinyint(1) NOT NULL,
  `par_descricao` varchar(255) NOT NULL,
  `par_valor` varchar(255) NOT NULL,
  `par_version` varchar(45) NOT NULL,
  `par_procedimento` varchar(255) NOT NULL,
  `par_responsavel` varchar(255) NOT NULL,
  `par_alteracao` datetime NOT NULL,
  `par_registro` datetime NOT NULL,
  `par_criador` varchar(255) NOT NULL,
  PRIMARY KEY (`par_codigo`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=408 DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `TBL_USUARIO` (
  `usu_codigo` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `usu_cod_USUARIO_TIPO` int(10) unsigned NOT NULL,
  `usu_login` varchar(200) NOT NULL,
  `usu_password` varchar(200) NOT NULL,
  `usu_nome` varchar(255) NOT NULL,
  `usu_admin` tinyint(1) NOT NULL,
  `usu_email` varchar(200) NOT NULL,
  `usu_ativo` tinyint(1) NOT NULL,
  `usu_auth` text NOT NULL,
  `usu_responsavel` varchar(100) NOT NULL,
  `usu_criador` varchar(100) NOT NULL,
  `usu_alteracao` datetime NOT NULL,
  `usu_registro` datetime NOT NULL,
  PRIMARY KEY (`usu_codigo`) USING BTREE,
  KEY `FK_tb_usuario_usuario_tipo` (`usu_cod_USUARIO_TIPO`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

CREATE TABLE IF NOT EXISTS `TBL_USUARIO_TIPO` (
  `ust_codigo` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ust_descricao` varchar(45) COLLATE latin1_general_ci NOT NULL,
  `ust_ativo` tinyint(1) NOT NULL,
  `ust_responsavel` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `ust_criador` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `ust_alteracao` datetime NOT NULL,
  `ust_registro` datetime NOT NULL,
  PRIMARY KEY (`ust_codigo`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE IF NOT EXISTS `TBL_MENU_FUNCAO_USUARIO_TIPO` (
  `mft_codigo` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `mft_cod_MENU_FUNCAO` INT(10) UNSIGNED NOT NULL,
  `mft_cod_USUARIO_TIPO` INT(10) UNSIGNED NOT NULL,
  `mft_level` VARCHAR(250) NOT NULL COLLATE LATIN1_SWEDISH_CI,
  `mft_alteracao` DATETIME NOT NULL,
  `mft_registro` DATETIME NOT NULL,
  `mft_responsavel` VARCHAR(250) NOT NULL COLLATE LATIN1_SWEDISH_CI,
  `mft_criador` VARCHAR(250) NOT NULL COLLATE LATIN1_SWEDISH_CI,

  PRIMARY KEY (`mft_codigo`)
  ,CONSTRAINT `FK_mft_mef_codigo` FOREIGN KEY `FK_mft_mef_codigo` (`mft_cod_MENU_FUNCAO`)
    REFERENCES `TBL_MENU_FUNCAO` (`mef_codigo`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT
  ,CONSTRAINT `FK_mft_ust_codigo` FOREIGN KEY `FK_mft_ust_codigo` (`mft_cod_USUARIO_TIPO`)
    REFERENCES `TBL_USUARIO_TIPO` (`ust_codigo`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT
)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `TBL_WIDGET` (
  `wid_codigo` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `wid_descricao` varchar(250) NOT NULL,
  `wid_nome` varchar(250) NOT NULL,
  `wid_conteudo` longtext NOT NULL,
  `wid_id` varchar(250) NOT NULL,
  `wid_default` tinyint(1) NOT NULL,
  `wid_class` varchar(250) NOT NULL,
  `wid_alteracao` datetime NOT NULL,
  `wid_registro` datetime NOT NULL,
  `wid_responsavel` varchar(250) NOT NULL,
  `wid_criador` varchar(250) NOT NULL,
  PRIMARY KEY (`wid_codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `TBL_WIDGET_SEGMENTO` (
  `wis_codigo` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `wis_nome` varchar(250) NOT NULL,
  `wis_posicao` varchar(250) NOT NULL,
  `wis_ordem` int(11) NOT NULL,
  `wis_conteudo` longtext NOT NULL,
  `wis_alteracao` datetime NOT NULL,
  `wis_registro` datetime NOT NULL,
  `wis_responsavel` varchar(250) NOT NULL,
  `wis_criador` varchar(250) NOT NULL,
  PRIMARY KEY (`wis_codigo`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `TBL_WIDGET_SEGMENTO_ITEM` (
  `wsi_codigo` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `wsi_descricao` text NOT NULL,
  `wsi_cod_WIDGET_SEGMENTO` int(10) unsigned NOT NULL,
  `wsi_cod_WIDGET` int(10) unsigned NOT NULL,
  `wsi_ordem` int(11) NOT NULL,
  `wsi_alteracao` datetime NOT NULL,
  `wsi_registro` datetime NOT NULL,
  `wsi_responsavel` varchar(250) NOT NULL,
  `wsi_criador` varchar(250) NOT NULL,
  PRIMARY KEY (`wsi_codigo`),
  KEY `FK_wsi_wid_codigo` (`wsi_cod_WIDGET`),
  KEY `FK_TBL_WIDGET_SEGMENTO_ITEM_2` (`wsi_cod_WIDGET_SEGMENTO`),
  CONSTRAINT `FK_TBL_WIDGET_SEGMENTO_ITEM_2` FOREIGN KEY (`wsi_cod_WIDGET_SEGMENTO`) REFERENCES `TBL_WIDGET_SEGMENTO` (`wis_codigo`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_wsi_wid_codigo` FOREIGN KEY (`wsi_cod_WIDGET`) REFERENCES `TBL_WIDGET` (`wid_codigo`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;
