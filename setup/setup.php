<?php

error_reporting(E_ALL);

require_once "core/base.php";
require_once "core/base/Install.class.php";

if (!defined('PATH_APP')) {
  define('PATH_APP', getcwd() . "/");
}

if (!defined('MODO_LEGADO')) {
  define('MODO_LEGADO', "false");
}

$step = 0;
if (isset($_POST['step'])) {
  $step = $_POST['step'];
}

$weblog_page = $_SERVER["HTTP_HOST"];
if (empty($_SERVER["HTTPS"]) || $_SERVER["HTTPS"] !== "on") {
  $weblog_page = "http" . "://" . $weblog_page;
} else {
  $weblog_page = "https" . "://" . $weblog_page;
}
$weblog_path = getcwd();
$weblog_uri = isset($_SERVER["REQUEST_URI"]) ? String::replace($_SERVER["REQUEST_URI"], "index.php", "") : "/";

Install::load($weblog_page, $weblog_path, $weblog_uri);

?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="pt-BR" xml:lang="pt-BR">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Configura&ccedil;&atilde;o da Instala&ccedil;&atilde;o</title>
    <link rel="stylesheet" href="./setup/css/setup.css" type="text/css" />
    <link rel="shortcut icon" href="../favicon.ico" type="image/ico"/>
  </head>
  <body>
    <h1 id="logo">
      <img alt="" src="./setup/images/logo.png" />
    </h1>
    <form method="post" action="">
      <?php

        $message = "";
        if ($step) {
          $message = Install::validate($step);
        }
        if (!$message) {
          $step++;
        }
        Install::process($step, $message);
      ?>
      <input type="hidden" name="step" id="step" value="<?php print $step; ?>"/>
    </form>
  </body>
</html>