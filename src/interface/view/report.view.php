<?php

if(isset($_GET["action"])) {
  $action = $_GET["action"];

  $target = System::request('t');
  $level = System::request('l');
  $rotule = System::request('rotule');

  $module = System::request('m');
  $id = System::request('i');

  $field = System::request('fd');
  $table = System::request('tb');
  $where = System::request('wr');
  $group = System::request('gp');
  $order = System::request('od');
  $limit = System::request('lt');

  if($action == "form") {
    viewFormReport($id, $module, $target, $level, $rotule);
  }else if($action == "process"){
    viewPrintReport($id, $module, $field, $table, $where, $group, $order, $limit);
  }else{
    viewLoadingReport();
  }

}

/**
 *
 * @param type $id
 * @param type $module
 * @param type $target
 * @param type $level
 * @param type $rotule
 */
function viewFormReport($id, $module, $target, $level, $rotule){

  require_once System::desire('file', '', 'header', 'core', false);
  System::desire('class', 'resource', 'Screen', 'core', true);

  $class = ucfirst($id);
  System::desire('r', $module, $class, 'src');
  $report = new $class();

  $screen = new Screen($report->get_ext());

  $validate = array();

  $_field = Encode::encrypt($report->get_field());
  $_table = Encode::encrypt($report->get_table());
  $_where = Encode::encrypt($report->get_where());
  $_order = Encode::encrypt($report->get_order());
  $_group = Encode::encrypt($report->get_group());
  $_limit = Encode::encrypt($report->get_limit());

  $items = $report->get_items();

  $width_label = 0;
  $lines = 0;
  $param = "search";
  foreach($items as $key => $item){
    $lines = ($item['line'] > $lines) ? $item['line'] : $lines;
    $width_label = $screen->utilities->getViewManageWidthLabel($item, $width_label);

    $items[$key]['pk'] = 0;
    $items[$key]['form'] = 1;

    if($item['validate']){
      $validate[] = $item;
    }
  }
  $width_value = WINDOW_WIDTH - $width_label;

  try {

    if($acesso_controle >= 2) {

      $level = base64_encode($level);
      ?>
        <script type="text/javascript">

          /**
           * $('#iframeID').contents().find('#someID').html();
           * @param target
           */
          system.action.reportBack = function(target, clear){
            system.util.toggle('formulario-'+target,'processamento-'+target, 'backit-'+target, 'searchit-'+target, 'printit-'+target);//, 'saveit-'+target
            if(clear){
              system.util.get('report_form_'+target).src = 'src/interface/view/report.view.php?action=loading';
            }
          };

          /**
           *
           * @param level
           * @param target
           * @param module
           * @param id
           */
          system.action.reportSearch = function(level, target, items){
            var enviar = system.ajax.post('report_form_'+target, 'src/interface/view/report.view.php?action=validate&level='+level, 'message_validate_'+target, false, true, items);
            if(enviar){
              system.action.reportBack(target);
              system.util.get('report_form_'+target).submit();
              system.util.get('saveit-'+target).disabled = false;
            }
          };

          /**
           *
           * @param target
           */
          system.action.reportPrint = function(target){
            window.frames['report_iframe_'+target].print();
          };

          /**
           *
           * @param target
           */
          siga.action.reportSave = function(target){
            var name = prompt('Salvar Como:','<?php print base64_decode($rotule);?>');
          };
        </script>

        <center>
          <form id="report_form_<?php print $target;?>" name="report_form_<?php print $target;?>" action="src/interface/view/report.view.php?action=process" target="report_iframe_<?php print $target;?>" method="POST" onsubmit="">
            <div id="message_validate_<?php print $target;?>" style="display: none;"></div>

            <input type="hidden" name="i" id="i" value="<?php print $id;?>"/>
            <input type="hidden" name="m" id="m" value="<?php print $module;?>"/>

            <input type="hidden" name="fd" id="fd" value="<?php print $_field;?>"/>
            <input type="hidden" name="tb" id="tb" value="<?php print $_table;?>"/>
            <input type="hidden" name="wr" id="wr" value="<?php print $_where;?>"/>
            <input type="hidden" name="od" id="od" value="<?php print $_order;?>"/>
            <input type="hidden" name="gp" id="gp" value="<?php print $_group;?>"/>
            <input type="hidden" name="lt" id="lt" value="<?php print $_limit;?>"/>

            <table class="form-table b-bottom" cellpadding="0" cellspacing="0" border="0" width="<?php print WINDOW_WIDTH;?>">
              <tr class="form-label">
                <td class="header">
                  <span class="tree-reload" onclick="system.ajax.post('report_form_<?php print $target; ?>', 'src/interface/view/report.view.php?action=form&i=<?php print $id; ?>&m=<?php print $module; ?>&rotule=<?php print $rotule; ?>&level=<?php print $level; ?>&target=<?php print $target; ?>','<?php print $target;?>',false, false);">&nbsp;</span>
                  <label class="form-title">&nbsp;<?php print base64_decode($rotule);?></label>
                </td>
              </tr>
              <tr>
                <td class="form-label reset">
                  <div id="toolbar-<?php print $target; ?>">
                    <?php

                      $itens = "";
                      if (count($validate) > 0) {
                        foreach ($validate as $item) {
                          $itens .= ",'" . $item['id'] . "': {'description':'" . $item['description'] . "', 'value':'" . $item['validate'] . "'" . "}";
                        }
                      }
                      $itens = '{' . substr($itens, 1) . '}';

                      $report->printReportToolbar($acesso_controle, $target, $level, $module, $itens);
                    ?>
                  </div>
                </td>
              </tr>

              <tr>
                <td class="form-label b-bottom reset">
                  <div id="formulario-<?php print $target;?>">
                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                      <?php

                        for($line = 1; $line <= $lines; $line++){
                          $first = null;
                          $subitems = null;
                          foreach($items as $item){
                            if(isset($item['line']) && $item['line'] == $line){
                              $show = $screen->utilities->getViewManageVisibility($item, $param);
                              if($show){
                                if($param == "search"){
                                  $item['readonly'] = false;
                                }
                                if($first == null){
                                  $first = $item;
                                }else{
                                  $subitems[] = $item;
                                }
                              }
                            }
                          }
                          if($first != null){
                            $screen->line->printLine($first, $width_label, $width_value, $subitems);
                          }
                        }

                    ?>
                    </table>
                  </div>

                  <div id="processamento-<?php print $target;?>" style="display: none; height: <?php print WINDOW_HEIGHT + 26;?>px;" class="form-value b-left">
                    <iframe name="report_iframe_<?php print $target;?>" id="report_iframe_<?php print $target;?>" src="src/interface/view/report.view.php?action=loading" style="width: 100%; height: <?php print WINDOW_HEIGHT + 26;?>px; border: none; background: #fff;" frameborder="0"></iframe>
                  </div>
                </td>
              </tr>
            </table>
          </form>
        </center>
        <br>
      <?

    }else{
      ?>
        <center>
          <label class="error_message">Servi&ccedil;o Indispon&iacute;vel</label>
        </center>
      <?
    }
  }catch (Exception $exception) {
      print "Caught exception: ".$exception->getMessage()."\n";
  }
}

/**
 *
 * @param type $id
 * @param type $module
 * @param type $_field
 * @param type $_table
 * @param type $_where
 * @param type $_group
 * @param type $_order
 * @param type $_limit
 */
function viewPrintReport($id, $module, $_field, $_table, $_where, $_group, $_order, $_limit){

  ini_set("include_path", ini_get("include_path") . ":".PATH_APP."lib/phpreports/");

  error_reporting(E_ALL);

  include "PHPReportMaker.php";
  require_once System::desire('file', '', 'header', 'core', false);

  $class = ucfirst($id);
  System::desire('r', $module, $class, 'src');
  System::desire('class', 'resource', 'Screen', 'core', true);

  $screen = new Screen();

  $report = new $class();
  $rel = new PHPReportMaker();

  $_field = base64_decode($_field);
  $_table = base64_decode($_table);
  $_where = base64_decode($_where);
  $_group = base64_decode($_group);
  $_order = base64_decode($_order);
  $_limit = base64_decode($_limit);

  $items = $report->get_items();

  $recovered = $screen->utilities->recover($items, true, true, true);

  $where = $recovered['w'];
  if($where != ""){
    if($_where != ""){
      $_where = "(".$_where.") AND (".$where.")";
    }else{
      $_where = $where;
    }
  }
  $w_description = Encode::encrypt($recovered['wd']);

  if($_where){
    $_where = " WHERE ".$_where;
  }

  if($_group){
    $_group = " GROUP BY ".$_group;
  }

  if($_order){
    $_order = " ORDER BY ".$_order;
  }

  if($_limit){
    $_limit = " LIMIT ".$_limit;
  }

  $sql = "SELECT ".$_field." FROM ".$_table.$_where.$_group.$_order.$_limit;

  $javascript = '&resources=1&load=0&check=1&catch=0&compact=0&resume=0';
  if(MODO_TESTE == 'false'){
    $javascript = '&resources=1&load=0&check=1&catch=1&compact=1&resume=0';
  }

  $css = '&commom=1&report=1';

  //$filename = PATH_APP.PATH_TEMP.'report/'.$id.'.html';
  //File::saveFile($filename, '');

  if($report->debug){
    print $sql;
  }

  ?>
  <html>
    <head>
      <link rel="stylesheet" type="text/css" media="all" href="<?php print PAGE_CSS;?>?id=<?php print HOST;?><?php print $css;?>&file=system.css" />
      <style type="text/css">
        body, html{
          background: #fff !important;
        }
      </style>
    </head>
    <body>
      <center>
        <?php
          $rel->setXML(PATH_APP."src/".$module."/report/".$id.".xml");
          $rel->setSQL($sql);
          $rel->run();
        ?>
      </center>
    </body>
  </html>
  <?
}

/**
 *
 *
 */
function viewLoadingReport(){
  ?>
    <html>
      <head>
        <style type="text/css">
          body{
            background: url('<?php print PAGE_APP;?>custom/images/loading/engine.gif') no-repeat scroll center 100px transparent;
          }
        </style>
      </head>
      <body>
      </body>
    </html>
  <?
}

?>
