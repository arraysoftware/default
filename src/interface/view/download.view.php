<?php

if(isset($_GET["action"])) {
  $action = $_GET["action"];

  $target = System::request('t');
  $level = System::request('l');
  $rotule = System::request('rotule');

  $module = System::request('m');
  $id = System::request('i');

  if($action == "form") {
    viewFormDownload($id, $module, $target, $level, $rotule);
  }else if($action == "process"){
    viewExecuteDownload($id, $module, $level);
  }else{
    viewLoadingDownload();
  }

}

/**
 *
 * @param type $id
 * @param type $module
 * @param type $target
 * @param type $level
 */
function viewFormDownload($id, $module, $target, $level, $rotule){

  require_once System::desire('file', '', 'header', 'core', false);
  System::desire('class', 'resource', 'Screen', 'core', true);

  $class = ucfirst($id);
  System::desire('f', $module, $class, 'src');
  $download = new $class();

  $screen = new Screen($download->get_ext());

  $validate = array();

  $items = $download->get_items();

  $width_label = 0;
  $lines = 0;
  $param = "search";
  foreach($items as $key => $item){
    $lines = ($item['line'] > $lines) ? $item['line'] : $lines;
    $width_label = $screen->utilities->getViewManageWidthLabel($item, $width_label);

    $items[$key]['pk'] = 0;
    $items[$key]['form'] = 1;

    if($item['validate']){
      $validate[] = $item;
    }
  }
  $width_value = WINDOW_WIDTH - $width_label;

  try {

    if($acesso_controle >= 2) {

      $level = base64_encode($level);
      ?>
        <script type="text/javascript">

          /**
           * $('#iframeID').contents().find('#someID').html();
           * @param target
           */
          system.action.downloadBack = function(target, clear){
            system.util.toggle('formulario-'+target,'processamento-'+target, 'backit-'+target, 'searchit-'+target, 'printit-'+target);//, 'saveit-'+target
            if(clear){
              system.util.get('down_form_'+target).src = 'src/interface/view/download.view.php?action=loading';
            }
          };

          /**
           *
           * @param level
           * @param target
           * @param module
           * @param id
           */
          system.action.downloadSearch = function(level, target, items){
            var enviar = system.ajax.post('down_form_'+target, 'src/interface/view/download.view.php?action=validate&level='+level, 'message_validate_'+target, false, true, items);
            if(enviar){
              system.action.downloadBack(target);
              system.util.get('down_form_'+target).submit();
              system.util.get('saveit-'+target).disabled = false;
            }
          };

          /**
           *
           * @param target
           */
          system.action.downloadPrint = function(target){
            window.frames['down_iframe_'+target].print();
          };
        </script>

        <center>
          <form id="down_form_<?php print $target;?>" name="down_form_<?php print $target;?>" action="src/interface/view/download.view.php?action=process" target="down_iframe_<?php print $target;?>" method="POST" onsubmit="">

            <input type="hidden" name="i" id="i" value="<?php print $id;?>"/>
            <input type="hidden" name="m" id="m" value="<?php print $module;?>"/>

            <table class="form-table b-bottom" cellpadding="0" cellspacing="0" border="0" width="<?php print WINDOW_WIDTH;?>">
              <tr class="form-label">
                <td class="header">
                  <span class="tree-reload" onclick="system.ajax.post('down_form_<?php print $target; ?>', 'src/interface/view/download.view.php?action=form&i=<?php print $id; ?>&m=<?php print $module; ?>&rotule=<?php print $rotule; ?>&level=<?php print $level; ?>&target=<?php print $target; ?>', '<?php print $target;?>', false, false, false);">&nbsp;</span>
                  <label class="form-title">&nbsp;<?php print base64_decode($rotule);?></label>
                </td>
              </tr>
              <tr>
                <td class="form-label reset">
                  <div id="toolbar-<?php print $target; ?>">
                    <?php

                      $itens = "";
                      if (count($validate) > 0) {
                        foreach ($validate as $item) {
                          $itens .= ",'" . $item['id'] . "': {'description':'" . $item['description'] . "', 'value':'" . $item['validate'] . "'" . "}";
                        }
                      }
                      $itens = '{' . substr($itens, 1) . '}';

                      $download->printDownloadToolbar($acesso_controle, $target, $level, $module, $itens);
                    ?>
                  </div>
                </td>
              </tr>

              <tr>
                <td class="form-label b-bottom reset">
                  <div id="formulario-<?php print $target;?>">
                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                      <?php

                        for($line = 1; $line <= $lines; $line++){
                          $first = null;
                          $subitems = null;
                          foreach($items as $item){
                            if(isset($item['line']) && $item['line'] == $line){
                              $show = $screen->utilities->getViewManageVisibility($item, $param);
                              if($show){
                                if($param == "search"){
                                  $item['readonly'] = false;
                                }
                                if($first == null){
                                  $first = $item;
                                }else{
                                  $subitems[] = $item;
                                }
                              }
                            }
                          }
                          if($first != null){
                            $screen->line->printLine($first, $width_label, $width_value, $subitems);
                          }
                        }

                    ?>
                    </table>
                  </div>

                  <div id="processamento-<?php print $target;?>" style="display: none; height: <?php print WINDOW_HEIGHT + 20;?>px;" class="form-value b-left">
                    <iframe name="down_iframe_<?php print $target;?>" id="down_iframe_<?php print $target;?>" src="src/interface/view/download.view.php?action=loading" style="width: 100%; height: <?php print WINDOW_HEIGHT + 20;?>px; border: none; background: #fff;" frameborder="0"></iframe>
                  </div>
                </td>
              </tr>
            </table>
          </form>
        </center>
        <br>
      <?

    }else{
      ?>
        <center>
          <label class="error_message"><?php print System::encode(MESSAGE_FORBIDDEN);?></label>
        </center>
      <?
    }
  }catch (Exception $exception) {
      print "Caught exception: ".$exception->getMessage()."\n";
  }

}

/**
 *
 * @param type $id
 * @param type $module
 * @param type $level
 */
function viewExecuteDownload($id, $module, $level){

  require System::desire('file', '', 'header', 'core', false);

  $Class = ucfirst($id);
  System::desire('f', $module, $Class, PATH_APP.'src');

  $download = new $Class();

  $items = $download->get_items();

  $recovered = System::recover($items, true, true, true);

  foreach($items as $item){
    $key = $item['id'];
    $items[$key]['value'] = System::request($key);
  }

  $download->set_items($items);

  $javascript = '&resources=1&load=0&check=1&catch=0&compact=0&resume=1';
  if(MODO_TESTE == 'false'){
    $javascript = '&resources=1&load=0&check=1&catch=1&compact=1&resume=1';
  }

  $css = '&design=services&commom=1&fagoc=1';

  ?>
  <html>
    <head>

      <?php
        $conected = 1;
        $version = unserialize(VERSION_LIBRARY);
        $v = isset($version['system']) ? $version['system'] : VERSION;
      ?>

      <link rel="stylesheet" type="text/css" media="all" href="<?php print PAGE_LIBRARY;?>resources/css/?s=<?php print $conected;?>&v=<?php print $v;?>&file=system.css" />
      <script type="text/javascript" src="<?php print PAGE_LIBRARY;?>resources/js/?s=<?php print $conected;?>&v=<?php print $v;?>&file=system.js"></script>

      <link rel="stylesheet" type="text/css" media="all" href="<?php print PAGE_CSS;?>?c=<?php print $conected;?>&v=<?php print VERSION;?>&file=custom.css" />
      <script type="text/javascript" src="<?php print PAGE_JAVASCRIPT;?>?c=<?php print $conected;?>&v=<?php print VERSION;?>&file=custom.js"></script>

      <style type="text/css">
        body, html{
          background: #fff !important;
          padding: 10px;
          height: auto;
        }
        .ui-state-error{
          padding: 10px;
          border: 1px solid #CD0A0A;
          background: #FEF6F3;
        }
      </style>
    </head>
    <body>
      <?php
        $filename = $download->generateFile($recovered);
        if(file_exists(PATH_APP.$filename)){
          ?>
            <input type="button" class="form-buttom" value="Download" onclick="system.window.external('<?php print PAGE_DOWNLOAD;?>=<?php print $filename;?>', 'Download', 500, 310, 100, 100);"/>
          <?php
        }else{
          System::showMessage($filename);
        }
      ?>
    </body>
  </html>
  <?
}

/**
 *
 *
 */
function viewLoadingDownload(){
  ?>
    <html>
      <head>
        <style type="text/css">
          body{
            background: url('<?php print PAGE_APP;?>custom/images/loading/engine.gif') no-repeat scroll center 100px transparent;
          }
        </style>
      </head>
      <body>
      </body>
    </html>
  <?
}
?>