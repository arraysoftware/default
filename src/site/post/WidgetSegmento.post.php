<?php
/**
 * @copyright array software
 *
 * @author WILLIAM MARQUES VICENTE GOMES CORREA - 24/05/2013 15:48:44
 * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 24/05/2013 23:35:50
 * @category post
 * @package site
 *
 * Responsavel.....: ADMINISTRADOR
 * Alteracao.......: 11/06/2013 22:14:39
 */


if (System::request('action')) {
  $action = System::request('action');


 if ($action == 'widgetsegmento-p-save') {
    $level = System::request('l');
    $copy = System::request('c', false);
    $presave = System::request('p');

    postSaveWidgetSegmento($level, $copy, $presave);
  } else if ($action == 'widgetsegmento-p-copy') {
    $level = System::request('l');
    $copy = System::request('c', true);
    $presave = System::request('p');

    postSaveWidgetSegmento($level, $copy, $presave);
  } else if ($action == 'widgetsegmento-p-remove') {
    $level = System::request('l');

    postRemoveWidgetSegmento($level);
  }
}

  /**
   * M�todo que permite registrar, editar ou copiar uma inst�ncia da entidade na base de dados
   * 
   * @param string $level
   * @param boolean $copy
   * @param boolean $presave
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 24/05/2013 15:48:48
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 24/05/2013 15:48:48
   */
 function postSaveWidgetSegmento($level, $copy = false, $presave = false){
?><?php

  $acesso_controle = -1;

  require System::desire('file','' , 'header', 'core', false);
  require System::desire('class', 'resource', 'Screen', 'core', false);

  System::desire('m', 'site', 'WidgetSegmento', 'src', true, '{project.rsc}');
  System::desire('c', 'site', 'WidgetSegmento', 'src', true, '{project.rsc}');

  $screen = new Screen();

  if($acesso_controle >= 1){

    $mode = '';
    $message_sucess = MESSAGE_ADD_SUCCESS;
    $message_error = MESSAGE_ADD_ERROR;

    $saved = "";
		$validate = "";
    $widgetSegmento = new WidgetSegmento();
    $properties = $widgetSegmento->get_wis_properties();
    $reference = $properties['reference'];
		$database = isset($properties["database"]) ? $properties["database"] : DEFAULT_DATABASE;

    $pk = System::request($reference);
    if($copy){
      $copy = $pk;
      $pk = '';
      $message_sucess = MESSAGE_COPY_SUCCESS;
      $message_error = MESSAGE_COPY_ERROR;
    }

    if(empty($pk)){
      $mode = 'add';
    }else{
      $mode = 'edit';
      $message_sucess = MESSAGE_SET_SUCCESS;
      $message_error = MESSAGE_SET_ERROR;
    }

    $widgetSegmentoCtrl = new WidgetSegmentoCtrl(PATH_APP, $database);

    $items = $widgetSegmento->get_wis_items();

    $widgetSegmento->set_wis_value($reference, $pk);

    foreach($items as $item){
      $key = $item['id'];
      $value = System::request($key, null);

			if($item['type'] === 'select-multi'){
        $entity = $item['select-multi']['entity'];
        $value = System::request('source-'.$entity);
      } else if($item['type_behavior'] === 'parent') {
        if (isset($item['parent'])) {
          $class = $item['parent']['entity'];
          System::desire('m', $item['parent']['modulo'], $class, 'src', true);
          $obj = new $class();
          $set_value = "set_".$item['parent']['prefix']."_value";
          $get_items = "get_".$item['parent']['prefix']."_items";

          $itens = $obj->$get_items();
          foreach ($itens as $iten) {
            $k = $iten['id'];
            $v = System::request($k, null);
            $obj->$set_value($k, $v);
          }
          $value = $obj;
        }
      }

			$message = System::validate($item, $value);
      if($message === ""){
        $widgetSegmento->set_wis_value($key, $value);
      }else{
        $validate[] = $message;
      }

    }

    $widgetSegmento->set_wis_value('wis_responsavel', System::getUser());
    $widgetSegmento->set_wis_value('wis_criador', System::getUser());

		if($validate === ""){
		  if ($mode === 'add') {
        $save = $widgetSegmentoCtrl->addWidgetSegmentoCtrl($widgetSegmento, false, false, $presave, $copy);
        $pk = $save;
      } else if ($mode === 'edit') {
				if(isset($properties['saveonly']) && $properties['saveonly']){
          $message_error = MESSAGE_SAVEONLY;
          $save = false;
        }else{
          $save = $widgetSegmentoCtrl->setWidgetSegmentoCtrl($widgetSegmento);
        }
      }	
		}else{
			$save = false;
		}
		
    $saved = '<input type="hidden" name="'.$reference.'" id="'.$reference.'" value="'.$pk.'"/>';

    if ($save) {
      $screen->message->printMessageSucess($message_sucess.$saved);
    } else {
      $screen->message->printMessageError($message_error.$saved, true, $validate);
    }

  }else{
    $screen->message->printMessageError(MESSAGE_FORBIDDEN);
  }
}

  /**
   * Inicia o processo de remo��o de uma inst�ncia da entidade da base de dados
   * 
   * @param string $level
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 24/05/2013 15:48:48
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 24/05/2013 15:48:48
   */
 function postRemoveWidgetSegmento($level){
  ?><?php

  $acesso_controle = -1;

  require System::desire('file', '', 'header', 'core', false);
  
  require System::desire('class', 'resource', 'Screen', 'core', false);
  
  System::desire('m','site', 'WidgetSegmento', 'src', true, '{project.rsc}');
  System::desire('c','site', 'WidgetSegmento', 'src', true, '{project.rsc}');
  
  $screen = new Screen();

  if($acesso_controle >= 2){
      
    $widgetSegmento = new WidgetSegmento();
    
    $properties = $widgetSegmento->get_wis_properties();
    $reference = $properties['reference'];
		$database = isset($properties["database"]) ? $properties["database"] : DEFAULT_DATABASE;

    $key = $reference;
    $value = System::request($key);
    $widgetSegmento->set_wis_value($key, $value);
    $widgetSegmento->set_wis_value('wis_responsavel', System::getUser());
		
    $widgetSegmentoCtrl = new WidgetSegmentoCtrl(PATH_APP, $database);
    $remove = $widgetSegmentoCtrl->removeWidgetSegmentoCtrl($widgetSegmento);
		
		$message_sucess = MESSAGE_REMOVE_SUCCESS;
    $message_error = MESSAGE_REMOVE_ERROR;

    $r = isset($properties["remove"]) ? $properties["remove"] : true;
    if(isset($r["success"])){
      $message_sucess = $r["success"];
    }

    if($remove){
      $screen->message->printMessageSucess($message_sucess, false);
    }else{
      $screen->message->printMessageError($message_error, false);
    }
    
  }else{
    $screen->message->printMessageError(MESSAGE_FORBIDDEN);
  }
}

