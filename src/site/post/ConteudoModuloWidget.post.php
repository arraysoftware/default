<?php
/**
 * @copyright array software
 *
 * @author WILLIAM MARQUES VICENTE GOMES CORREA - 24/05/2013 23:55:16
 * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/05/2013 01:02:27
 * @category post
 * @package site
 *
 * Responsavel.....: WILLIAM MARQUES VICENTE GOMES CORREA
 * Alteracao.......: 25/05/2013 01:02:29
 */


if (System::request('action')) {
  $action = System::request('action');


 if ($action == 'conteudomodulowidget-p-save') {
    $level = System::request('l');
    $copy = System::request('c', false);
    $presave = System::request('p');

    postSaveConteudoModuloWidget($level, $copy, $presave);
  } else if ($action == 'conteudomodulowidget-p-copy') {
    $level = System::request('l');
    $copy = System::request('c', true);
    $presave = System::request('p');

    postSaveConteudoModuloWidget($level, $copy, $presave);
  } else if ($action == 'conteudomodulowidget-p-remove') {
    $level = System::request('l');

    postRemoveConteudoModuloWidget($level);
  }
}

  /**
   * M�todo que permite registrar, editar ou copiar uma inst�ncia da entidade na base de dados
   * 
   * @param string $level
   * @param boolean $copy
   * @param boolean $presave
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 25/05/2013 00:19:38
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/05/2013 00:19:38
   */
 function postSaveConteudoModuloWidget($level, $copy = false, $presave = false){
?><?php

  $acesso_controle = -1;

  require System::desire('file','' , 'header', 'core', false);
  require System::desire('class', 'resource', 'Screen', 'core', false);

  System::desire('m', 'site', 'ConteudoModuloWidget', 'src', true, '{project.rsc}');
  System::desire('c', 'site', 'ConteudoModuloWidget', 'src', true, '{project.rsc}');

  $screen = new Screen();

  if($acesso_controle >= 1){

    $mode = '';
    $message_sucess = MESSAGE_ADD_SUCCESS;
    $message_error = MESSAGE_ADD_ERROR;

    $saved = "";
		$validate = "";
    $conteudoModuloWidget = new ConteudoModuloWidget();
    $properties = $conteudoModuloWidget->get_cmw_properties();
    $reference = $properties['reference'];
		$database = isset($properties["database"]) ? $properties["database"] : DEFAULT_DATABASE;

    $pk = System::request($reference);
    if($copy){
      $copy = $pk;
      $pk = '';
      $message_sucess = MESSAGE_COPY_SUCCESS;
      $message_error = MESSAGE_COPY_ERROR;
    }

    if(empty($pk)){
      $mode = 'add';
    }else{
      $mode = 'edit';
      $message_sucess = MESSAGE_SET_SUCCESS;
      $message_error = MESSAGE_SET_ERROR;
    }

    $conteudoModuloWidgetCtrl = new ConteudoModuloWidgetCtrl(PATH_APP, $database);

    $items = $conteudoModuloWidget->get_cmw_items();

    $conteudoModuloWidget->set_cmw_value($reference, $pk);

    foreach($items as $item){
      $key = $item['id'];
      $value = System::request($key, null);

			if($item['type'] === 'select-multi'){
        $entity = $item['select-multi']['entity'];
        $value = System::request('source-'.$entity);
      } else if($item['type_behavior'] === 'parent') {
        if (isset($item['parent'])) {
          $class = $item['parent']['entity'];
          System::desire('m', $item['parent']['modulo'], $class, 'src', true);
          $obj = new $class();
          $set_value = "set_".$item['parent']['prefix']."_value";
          $get_items = "get_".$item['parent']['prefix']."_items";

          $itens = $obj->$get_items();
          foreach ($itens as $iten) {
            $k = $iten['id'];
            $v = System::request($k, null);
            $obj->$set_value($k, $v);
          }
          $value = $obj;
        }
      }

			$message = System::validate($item, $value);
      if($message === ""){
        $conteudoModuloWidget->set_cmw_value($key, $value);
      }else{
        $validate[] = $message;
      }

    }

    $conteudoModuloWidget->set_cmw_value('cmw_responsavel', System::getUser());
    $conteudoModuloWidget->set_cmw_value('cmw_criador', System::getUser());

		if($validate === ""){
		  if ($mode === 'add') {
        $save = $conteudoModuloWidgetCtrl->addConteudoModuloWidgetCtrl($conteudoModuloWidget, false, false, $presave, $copy);
        $pk = $save;
      } else if ($mode === 'edit') {
				if(isset($properties['saveonly']) && $properties['saveonly']){
          $message_error = MESSAGE_SAVEONLY;
          $save = false;
        }else{
          $save = $conteudoModuloWidgetCtrl->setConteudoModuloWidgetCtrl($conteudoModuloWidget);
        }
      }	
		}else{
			$save = false;
		}
		
    $saved = '<input type="hidden" name="'.$reference.'" id="'.$reference.'" value="'.$pk.'"/>';

    if ($save) {
      $screen->message->printMessageSucess($message_sucess.$saved);
    } else {
      $screen->message->printMessageError($message_error.$saved, true, $validate);
    }

  }else{
    $screen->message->printMessageError(MESSAGE_FORBIDDEN);
  }
}

  /**
   * Inicia o processo de remo��o de uma inst�ncia da entidade da base de dados
   * 
   * @param string $level
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 25/05/2013 00:19:38
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/05/2013 00:19:38
   */
 function postRemoveConteudoModuloWidget($level){
  ?><?php

  $acesso_controle = -1;

  require System::desire('file', '', 'header', 'core', false);
  
  require System::desire('class', 'resource', 'Screen', 'core', false);
  
  System::desire('m','site', 'ConteudoModuloWidget', 'src', true, '{project.rsc}');
  System::desire('c','site', 'ConteudoModuloWidget', 'src', true, '{project.rsc}');
  
  $screen = new Screen();

  if($acesso_controle >= 2){
      
    $conteudoModuloWidget = new ConteudoModuloWidget();
    
    $properties = $conteudoModuloWidget->get_cmw_properties();
    $reference = $properties['reference'];
		$database = isset($properties["database"]) ? $properties["database"] : DEFAULT_DATABASE;

    $key = $reference;
    $value = System::request($key);
    $conteudoModuloWidget->set_cmw_value($key, $value);
    $conteudoModuloWidget->set_cmw_value('cmw_responsavel', System::getUser());
		
    $conteudoModuloWidgetCtrl = new ConteudoModuloWidgetCtrl(PATH_APP, $database);
    $remove = $conteudoModuloWidgetCtrl->removeConteudoModuloWidgetCtrl($conteudoModuloWidget);
		
		$message_sucess = MESSAGE_REMOVE_SUCCESS;
    $message_error = MESSAGE_REMOVE_ERROR;

    $r = isset($properties["remove"]) ? $properties["remove"] : true;
    if(isset($r["success"])){
      $message_sucess = $r["success"];
    }

    if($remove){
      $screen->message->printMessageSucess($message_sucess, false);
    }else{
      $screen->message->printMessageError($message_error, false);
    }
    
  }else{
    $screen->message->printMessageError(MESSAGE_FORBIDDEN);
  }
}


