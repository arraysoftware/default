<?php
/**
 * @copyright array software
 *
 * @author  - 23/03/2013 23:11:23
 * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 28/05/2013 14:05:10
 * @category post
 * @package site
 *
 * Responsavel.....: ADMINISTRADOR
 * Alteracao.......: 11/06/2013 22:14:39
 */


if (System::request('action')) {
  $action = System::request('action');


 if ($action == 'conteudosecao-p-save') {
    $level = System::request('l');
    $copy = System::request('c', false);
    $presave = System::request('p');

    postSaveConteudoSecao($level, $copy, $presave);
  } else if ($action == 'conteudosecao-p-copy') {
    $level = System::request('l');
    $copy = System::request('c', true);
    $presave = System::request('p');

    postSaveConteudoSecao($level, $copy, $presave);
  } else if ($action == 'conteudosecao-p-remove') {
    $level = System::request('l');

    postRemoveConteudoSecao($level);
  }
}

  /**
   * M�todo que permite registrar, editar ou copiar uma inst�ncia da entidade na base de dados
   * 
   * @param string $level
   * @param boolean $copy
   * @param boolean $presave
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 26/05/2013 16:19:10
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 26/05/2013 16:19:10
   */
 function postSaveConteudoSecao($level, $copy = false, $presave = false){
?><?php

  $acesso_controle = -1;

  require System::desire('file','' , 'header', 'core', false);
  require System::desire('class', 'resource', 'Screen', 'core', false);

  System::desire('m', 'site', 'ConteudoSecao', 'src', true, '{project.rsc}');
  System::desire('c', 'site', 'ConteudoSecao', 'src', true, '{project.rsc}');

  $screen = new Screen();

  if($acesso_controle >= 1){

    $mode = '';
    $message_sucess = MESSAGE_ADD_SUCCESS;
    $message_error = MESSAGE_ADD_ERROR;

    $saved = "";
		$validate = "";
    $conteudoSecao = new ConteudoSecao();
    $properties = $conteudoSecao->get_cts_properties();
    $reference = $properties['reference'];
		$database = isset($properties["database"]) ? $properties["database"] : DEFAULT_DATABASE;

    $pk = System::request($reference);
    if($copy){
      $copy = $pk;
      $pk = '';
      $message_sucess = MESSAGE_COPY_SUCCESS;
      $message_error = MESSAGE_COPY_ERROR;
    }

    if(empty($pk)){
      $mode = 'add';
    }else{
      $mode = 'edit';
      $message_sucess = MESSAGE_SET_SUCCESS;
      $message_error = MESSAGE_SET_ERROR;
    }

    $conteudoSecaoCtrl = new ConteudoSecaoCtrl(PATH_APP, $database);

    $items = $conteudoSecao->get_cts_items();

    $conteudoSecao->set_cts_value($reference, $pk);

    foreach($items as $item){
      $key = $item['id'];
      $value = System::request($key, null);

			if($item['type'] === 'select-multi'){
        $entity = $item['select-multi']['entity'];
        $value = System::request('source-'.$entity);
      } else if($item['type_behavior'] === 'parent') {
        if (isset($item['parent'])) {
          $class = $item['parent']['entity'];
          System::desire('m', $item['parent']['modulo'], $class, 'src', true);
          $obj = new $class();
          $set_value = "set_".$item['parent']['prefix']."_value";
          $get_items = "get_".$item['parent']['prefix']."_items";

          $itens = $obj->$get_items();
          foreach ($itens as $iten) {
            $k = $iten['id'];
            $v = System::request($k, null);
            $obj->$set_value($k, $v);
          }
          $value = $obj;
        }
      }

			$message = System::validate($item, $value);
      if($message === ""){
        $conteudoSecao->set_cts_value($key, $value);
      }else{
        $validate[] = $message;
      }

    }

    $conteudoSecao->set_cts_value('cts_responsavel', System::getUser());
    $conteudoSecao->set_cts_value('cts_criador', System::getUser());

		if($validate === ""){
		  if ($mode === 'add') {
        $save = $conteudoSecaoCtrl->addConteudoSecaoCtrl($conteudoSecao, false, false, $presave, $copy);
        $pk = $save;
      } else if ($mode === 'edit') {
				if(isset($properties['saveonly']) && $properties['saveonly']){
          $message_error = MESSAGE_SAVEONLY;
          $save = false;
        }else{
          $save = $conteudoSecaoCtrl->setConteudoSecaoCtrl($conteudoSecao);
        }
      }	
		}else{
			$save = false;
		}
		
    $saved = '<input type="hidden" name="'.$reference.'" id="'.$reference.'" value="'.$pk.'"/>';

    if ($save) {
      $screen->message->printMessageSucess($message_sucess.$saved);
    } else {
      $screen->message->printMessageError($message_error.$saved, true, $validate);
    }

  }else{
    $screen->message->printMessageError(MESSAGE_FORBIDDEN);
  }
}

  /**
   * Inicia o processo de remo��o de uma inst�ncia da entidade da base de dados
   * 
   * @param string $level
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 26/05/2013 16:19:10
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 26/05/2013 16:19:10
   */
 function postRemoveConteudoSecao($level){
  ?><?php

  $acesso_controle = -1;

  require System::desire('file', '', 'header', 'core', false);
  
  require System::desire('class', 'resource', 'Screen', 'core', false);
  
  System::desire('m','site', 'ConteudoSecao', 'src', true, '{project.rsc}');
  System::desire('c','site', 'ConteudoSecao', 'src', true, '{project.rsc}');
  
  $screen = new Screen();

  if($acesso_controle >= 2){
      
    $conteudoSecao = new ConteudoSecao();
    
    $properties = $conteudoSecao->get_cts_properties();
    $reference = $properties['reference'];
		$database = isset($properties["database"]) ? $properties["database"] : DEFAULT_DATABASE;

    $key = $reference;
    $value = System::request($key);
    $conteudoSecao->set_cts_value($key, $value);
    $conteudoSecao->set_cts_value('cts_responsavel', System::getUser());
		
    $conteudoSecaoCtrl = new ConteudoSecaoCtrl(PATH_APP, $database);
    $remove = $conteudoSecaoCtrl->removeConteudoSecaoCtrl($conteudoSecao);
		
		$message_sucess = MESSAGE_REMOVE_SUCCESS;
    $message_error = MESSAGE_REMOVE_ERROR;

    $r = isset($properties["remove"]) ? $properties["remove"] : true;
    if(isset($r["success"])){
      $message_sucess = $r["success"];
    }

    if($remove){
      $screen->message->printMessageSucess($message_sucess, false);
    }else{
      $screen->message->printMessageError($message_error, false);
    }
    
  }else{
    $screen->message->printMessageError(MESSAGE_FORBIDDEN);
  }
}

