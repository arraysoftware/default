<?php
/**
 * @copyright array software
 *
 * @author WILLIAM MARQUES VICENTE GOMES CORREA - 24/05/2013 23:55:16
 * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/05/2013 01:02:27
 * @category model
 * @package site
 *
 * Responsavel.....: WILLIAM MARQUES VICENTE GOMES CORREA
 * Alteracao.......: 25/05/2013 01:02:28
 */


class ConteudoModuloWidget
{
  private  $cmw_items = array();
  private  $cmw_properties = array();

  /**
   * Construtor do Modelo da Classe
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 25/05/2013 00:19:35
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/05/2013 00:19:35
   */
  public function ConteudoModuloWidget(){
    ?><?php
    $this->cmw_items = array();
    
    // Atributos
    $this->cmw_items["cmw_codigo"] = array("pk"=>1, "fk"=>0, "id"=>"cmw_codigo", "description"=>"C�digo", "title"=>"", "type"=>"pk", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>1, "grid"=>0, "grid_width"=>"", "form"=>0, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"", "update"=>0, "insert"=>0, "line"=>1, );
    $this->cmw_items["cmw_descricao"] = array("pk"=>0, "fk"=>0, "id"=>"cmw_descricao", "description"=>"Descri��o", "title"=>"", "type"=>"calculated", "type_content"=>"CONCAT(ctm_descricao,' - ', cmw_ordem)", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>0, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"", "update"=>1, "insert"=>1, "line"=>4, );
    $this->cmw_items["cmw_local"] = array("pk"=>0, "fk"=>0, "id"=>"cmw_local", "description"=>"Local", "title"=>"", "type"=>"option", "type_content"=>"0,Lateral|1,Central", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[R]", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"0", "default_sql"=>"", "update"=>1, "insert"=>1, "line"=>5, );
    $this->cmw_items["cmw_ordem"] = array("pk"=>0, "fk"=>0, "id"=>"cmw_ordem", "description"=>"Ordem", "title"=>"", "type"=>"int", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "update"=>1, "insert"=>1, "line"=>6, );


    // Atributos FK
    $this->cmw_items["cmw_cod_CONTEUDO_ARTIGO"] = array("pk"=>0, "fk"=>1, "id"=>"cmw_cod_CONTEUDO_ARTIGO", "description"=>"Artigo", "title"=>"", "type"=>"fk", "type_content"=>"", "type_behavior"=>"foreign", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[S]", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "update"=>1, "insert"=>1, "line"=>2, "foreign"=>array("modulo"=>"site", "entity"=>"ConteudoArtigo", "table"=>"TBL_CONTEUDO_ARTIGO", "prefix"=>"cta", "tag"=>"conteudoartigo", "key"=>"cta_codigo", "description"=>"cta_titulo", "form"=>"form", "target"=>"div-cmw_cod_CONTEUDO_ARTIGO-".rand()."-".date("Hisu"), "onchange"=>"", "encode"=>true, "width"=>"400", "where"=>"", "filter"=>""));
    $this->cmw_items["cmw_cod_WIDGET"] = array("pk"=>0, "fk"=>1, "id"=>"cmw_cod_WIDGET", "description"=>"Widget", "title"=>"", "type"=>"fk", "type_content"=>"", "type_behavior"=>"foreign", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[S]", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "update"=>1, "insert"=>1, "line"=>3, "foreign"=>array("modulo"=>"site", "entity"=>"Widget", "table"=>"TBL_WIDGET", "prefix"=>"wid", "tag"=>"widget", "key"=>"wid_codigo", "description"=>"wid_descricao", "form"=>"form", "target"=>"div-cmw_cod_WIDGET-".rand()."-".date("Hisu"), "onchange"=>"", "encode"=>true, "width"=>"400", "where"=>"", "filter"=>""));


    // Atributos CHILD

    
    // Atributos padrao
    $this->cmw_items['cmw_alteracao'] = array('pk'=>false, 'fk'=>false, 'id'=>'cmw_alteracao', 'description'=>'Altera��o', 'title'=>'', 'type'=>'alteracao', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>true, 'insert'=>true, 'line'=>0);
    $this->cmw_items['cmw_registro'] = array('pk'=>false, 'fk'=>false, 'id'=>'cmw_registro', 'description'=>'Registro', 'title'=>'', 'type'=>'registro', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>false, 'insert'=>true, 'line'=>0);
    $this->cmw_items['cmw_responsavel'] = array('pk'=>false, 'fk'=>false, 'id'=>'cmw_responsavel', 'description'=>'Respons�vel', 'title'=>'', 'type'=>'responsavel', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>true, 'insert'=>true, 'line'=>0);
    $this->cmw_items['cmw_criador'] = array('pk'=>false, 'fk'=>false, 'id'=>'cmw_criador', 'description'=>'Criador', 'title'=>'', 'type'=>'criador', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>false, 'insert'=>true, 'line'=>0);

    $lines = 0;
    foreach($this->cmw_items as $item){
      $lines = ($item['line'] > $lines) ? $item['line'] : $lines;
    }
     
    $j = array();
    foreach($this->cmw_items as $item){
      if($item['fk']){
        if(isset($item['foreign']) or isset($item['parent'])){
          $table = "";
					$key = "";
					if(isset($item['foreign'])){
						$table = $item['foreign']['table'];
						$key = $item['foreign']['key'];
					}else if(isset($item['parent'])){
						$table = $item['parent']['table'];
						$key = $item['parent']['key'];
					}
          $j[$table] = " LEFT JOIN ".$table." ON (".$item['id']." = ".$key.") ";
        }
      }
    }
    $join = " ".join(' ', $j);
    
    $this->cmw_properties = array(
      'module'=>'site',
      'entity'=>'ConteudoModuloWidget',
      'table'=>'TBL_CONTEUDO_MODULO_WIDGET',
			'join'=>$join,
      'tag'=>'conteudomodulowidget',
      'prefix'=>'cmw',
      'order'=>'',
      'group'=>'',
      'where'=>'',
      'search'=>'',
      'layout'=>'',
			'checkbox'=>false,
      'saveonly'=>false,//desabilita a edi��o de entidade
      'readonly'=>false,//desabilita a cria��o de novos registros
      'remove'=>'',//false or
			 /**
        * array(
        *  "field"=>"prefix_id",
        *  "value"=>"1",
        *  "className"=>"icon-remove",
        *  "title"=>"Excluir",
        *  "message"=>"Deseja realmente remover este registro?",
        *  "success"=>"Registro removido com sucesso"
        * )
        */
			'database'=>null,
      'reference'=>'cmw_codigo',
      'description'=>'cmw_descricao',
      'lines'=>$lines
    );
    
    if (!$this->cmw_properties['reference']) {
      foreach ($this->cmw_items as $id=>$array) {
        if ($array['pk'] == 1) {
          $this->cmw_properties['reference'] = $id;
          break;
        }
      }
    }
    if (!$this->cmw_properties['description']) {
      foreach ($this->cmw_items as $id=>$array) {
        if ($array['type'] == "string") {
          $this->cmw_properties['reference'] = $id;
          break;
        }
      }
    }
  }

  /**
   * Recupera as principais propriedades da entidade
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 25/05/2013 00:19:35
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/05/2013 00:19:35
   */
  public function get_cmw_properties(){
    ?><?php
    return $this->cmw_properties;
  }

  /**
   * Recupera todos os atributos da entidade
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 25/05/2013 00:19:35
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/05/2013 00:19:35
   */
  public function get_cmw_items(){
    ?><?php
    return $this->cmw_items;
  }

  /**
   * Recupera um atributo da entidade com todas as suas propriedades
   * 
   * @param string $key
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 25/05/2013 00:19:35
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/05/2013 00:19:35
   */
  public function get_cmw_item($key){
    ?><?php
    return $this->cmw_items[$key];
  }

  /**
   * 
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 25/05/2013 00:19:35
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/05/2013 00:19:35
   */
  public function get_cmw_reference(){
    ?><?php
    $key = $this->cmw_properties['reference'];
    return $key;
  }

  /**
   * Recupera o valor de um objeto
   * 
   * @param string $key
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 25/05/2013 00:19:35
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/05/2013 00:19:35
   */
  public function get_cmw_value($key){
    ?><?php
    return $this->cmw_items[$key]['value'];
  }

  /**
   * Altera o valor de um atributo da inst�ncia da entidade
   * 
   * @param string $key
   * @param object $value
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 25/05/2013 00:19:35
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/05/2013 00:19:35
   */
  public function set_cmw_value($key, $value){
    ?><?php
    $this->cmw_items[$key]['value'] = $value;
  }

  /**
   * Altera o tipo de um atributo da inst�ncia da entidade
   * 
   * @param string $key
   * @param string $type
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 25/05/2013 00:19:35
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/05/2013 00:19:35
   */
  public function set_cmw_type($key, $type){
    ?><?php
    $this->cmw_items[$key]['type'] = $type;
  }


}