<?php
/**
 * @copyright array software
 *
 * @author WILLIAM MARQUES VICENTE GOMES CORREA - 24/05/2013 15:48:44
 * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 24/05/2013 23:35:50
 * @category model
 * @package site
 *
 * Responsavel.....: ADMINISTRADOR
 * Alteracao.......: 11/06/2013 22:14:35
 */


class WidgetSegmento
{
  private  $wis_items = array();
  private  $wis_properties = array();

  /**
   * Construtor do Modelo da Classe
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 24/05/2013 15:48:45
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 24/05/2013 15:48:45
   */
  public function WidgetSegmento(){
    ?><?php
    $this->wis_items = array();
    
    // Atributos
    $this->wis_items["wis_codigo"] = array("pk"=>1, "fk"=>0, "id"=>"wis_codigo", "description"=>"C�digo", "title"=>"", "type"=>"pk", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"", "update"=>0, "insert"=>0, "line"=>1, );
    $this->wis_items["wis_nome"] = array("pk"=>0, "fk"=>0, "id"=>"wis_nome", "description"=>"Nome", "title"=>"", "type"=>"string", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "update"=>1, "insert"=>1, "line"=>2, );
    $this->wis_items["wis_posicao"] = array("pk"=>0, "fk"=>0, "id"=>"wis_posicao", "description"=>"Posi��o", "title"=>"", "type"=>"option", "type_content"=>"0,Lateral|1,Superior|2,Central|3,Inferior", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[R]", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"2", "default_sql"=>"", "update"=>1, "insert"=>1, "line"=>3, );
    $this->wis_items["wis_ordem"] = array("pk"=>0, "fk"=>0, "id"=>"wis_ordem", "description"=>"Ordem", "title"=>"", "type"=>"int", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "update"=>1, "insert"=>1, "line"=>4, );


    // Atributos FK


    // Atributos CHILD
    $this->wis_items["wis_fk_wsi_cod_WIDGET_SEGMENTO_542"] = array("pk"=>0, "fk"=>0, "id"=>"wis_fk_wsi_cod_WIDGET_SEGMENTO_542", "description"=>"Widgets", "title"=>"", "type"=>"", "type_content"=>"", "type_behavior"=>"child", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"50", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "update"=>0, "insert"=>0, "line"=>0, "child"=>array("module"=>"site", "entity"=>"WidgetSegmentoItem", "tag"=>"widgetsegmentoitem", "prefix"=>"wsi", "name"=>"Widgets", "filter"=>"wsi_codigo", "key"=>"wis_codigo", "params"=>"child=true&width=&height=&rotule=&t=&f=", "target"=>"div-".rand()."-".date("Hisu"), "form"=>"form"));

    
    // Atributos padrao
    $this->wis_items['wis_alteracao'] = array('pk'=>false, 'fk'=>false, 'id'=>'wis_alteracao', 'description'=>'Altera��o', 'title'=>'', 'type'=>'alteracao', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>true, 'insert'=>true, 'line'=>0);
    $this->wis_items['wis_registro'] = array('pk'=>false, 'fk'=>false, 'id'=>'wis_registro', 'description'=>'Registro', 'title'=>'', 'type'=>'registro', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>false, 'insert'=>true, 'line'=>0);
    $this->wis_items['wis_responsavel'] = array('pk'=>false, 'fk'=>false, 'id'=>'wis_responsavel', 'description'=>'Respons�vel', 'title'=>'', 'type'=>'responsavel', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>true, 'insert'=>true, 'line'=>0);
    $this->wis_items['wis_criador'] = array('pk'=>false, 'fk'=>false, 'id'=>'wis_criador', 'description'=>'Criador', 'title'=>'', 'type'=>'criador', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>false, 'insert'=>true, 'line'=>0);

    $lines = 0;
    foreach($this->wis_items as $item){
      $lines = ($item['line'] > $lines) ? $item['line'] : $lines;
    }
     
    $j = array();
    foreach($this->wis_items as $item){
      if($item['fk']){
        if(isset($item['foreign']) or isset($item['parent'])){
          $table = "";
					$key = "";
					if(isset($item['foreign'])){
						$table = $item['foreign']['table'];
						$key = $item['foreign']['key'];
					}else if(isset($item['parent'])){
						$table = $item['parent']['table'];
						$key = $item['parent']['key'];
					}
          $j[$table] = " LEFT JOIN ".$table." ON (".$item['id']." = ".$key.") ";
        }
      }
    }
    $join = " ".join(' ', $j);
    
    $this->wis_properties = array(
      'module'=>'site',
      'entity'=>'WidgetSegmento',
      'table'=>'TBL_WIDGET_SEGMENTO',
			'join'=>$join,
      'tag'=>'widgetsegmento',
      'prefix'=>'wis',
      'order'=>'wis_posicao,wis_ordem',
      'group'=>'',
      'where'=>'',
      'search'=>'',
      'layout'=>'',
			'checkbox'=>false,
      'saveonly'=>false,//desabilita a edi��o de entidade
      'readonly'=>false,//desabilita a cria��o de novos registros
      'remove'=>'',//false or
			 /**
        * array(
        *  "field"=>"prefix_id",
        *  "value"=>"1",
        *  "className"=>"icon-remove",
        *  "title"=>"Excluir",
        *  "message"=>"Deseja realmente remover este registro?",
        *  "success"=>"Registro removido com sucesso"
        * )
        */
			'database'=>null,
      'reference'=>'wis_codigo',
      'description'=>'wis_nome',
      'lines'=>$lines
    );
    
    if (!$this->wis_properties['reference']) {
      foreach ($this->wis_items as $id=>$array) {
        if ($array['pk'] == 1) {
          $this->wis_properties['reference'] = $id;
          break;
        }
      }
    }
    if (!$this->wis_properties['description']) {
      foreach ($this->wis_items as $id=>$array) {
        if ($array['type'] == "string") {
          $this->wis_properties['reference'] = $id;
          break;
        }
      }
    }
  }

  /**
   * Recupera as principais propriedades da entidade
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 24/05/2013 15:48:45
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 24/05/2013 15:48:45
   */
  public function get_wis_properties(){
    ?><?php
    return $this->wis_properties;
  }

  /**
   * Recupera todos os atributos da entidade
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 24/05/2013 15:48:45
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 24/05/2013 15:48:45
   */
  public function get_wis_items(){
    ?><?php
    return $this->wis_items;
  }

  /**
   * Recupera um atributo da entidade com todas as suas propriedades
   * 
   * @param string $key
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 24/05/2013 15:48:45
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 24/05/2013 15:48:45
   */
  public function get_wis_item($key){
    ?><?php
    return $this->wis_items[$key];
  }

  /**
   * 
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 24/05/2013 15:48:45
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 24/05/2013 15:48:45
   */
  public function get_wis_reference(){
    ?><?php
    $key = $this->wis_properties['reference'];
    return $key;
  }

  /**
   * Recupera o valor de um objeto
   * 
   * @param string $key
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 24/05/2013 15:48:45
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 24/05/2013 15:48:45
   */
  public function get_wis_value($key){
    ?><?php
    return $this->wis_items[$key]['value'];
  }

  /**
   * Altera o valor de um atributo da inst�ncia da entidade
   * 
   * @param string $key
   * @param object $value
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 24/05/2013 15:48:45
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 24/05/2013 15:48:45
   */
  public function set_wis_value($key, $value){
    ?><?php
    $this->wis_items[$key]['value'] = $value;
  }

  /**
   * Altera o tipo de um atributo da inst�ncia da entidade
   * 
   * @param string $key
   * @param string $type
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 24/05/2013 15:48:45
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 24/05/2013 15:48:45
   */
  public function set_wis_type($key, $type){
    ?><?php
    $this->wis_items[$key]['type'] = $type;
  }


}