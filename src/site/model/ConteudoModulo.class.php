<?php
/**
 * @copyright array software
 *
 * @author  - 23/03/2013 22:52:32
 * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/05/2013 22:24:38
 * @category model
 * @package site
 *
 * Responsavel.....: ADMINISTRADOR
 * Alteracao.......: 11/06/2013 22:14:34
 */


class ConteudoModulo
{
  private  $ctm_items = array();
  private  $ctm_properties = array();

  /**
   * Construtor do Modelo da Classe
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 23/05/2013 21:09:57
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/05/2013 00:04:00
   */
  public function ConteudoModulo(){
    ?><?php
    $this->ctm_items = array();
    
    // Atributos
    $this->ctm_items["ctm_codigo"] = array("pk"=>1, "fk"=>0, "id"=>"ctm_codigo", "description"=>"C�digo", "title"=>"", "type"=>"pk", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"", "update"=>0, "insert"=>0, "line"=>1, );
    $this->ctm_items["ctm_descricao"] = array("pk"=>0, "fk"=>0, "id"=>"ctm_descricao", "description"=>"Descri��o", "title"=>"", "type"=>"string", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "update"=>1, "insert"=>1, "line"=>3, );
    $this->ctm_items["ctm_top"] = array("pk"=>0, "fk"=>0, "id"=>"ctm_top", "description"=>"Menu Superior", "title"=>"", "type"=>"yes/no", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[R]", "fast"=>0, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "update"=>1, "insert"=>1, "line"=>4, );
    $this->ctm_items["ctm_footer"] = array("pk"=>0, "fk"=>0, "id"=>"ctm_footer", "description"=>"Menu Inferior", "title"=>"", "type"=>"yes/no", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[R]", "fast"=>0, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "update"=>1, "insert"=>1, "line"=>4, );
    $this->ctm_items["ctm_ordem"] = array("pk"=>0, "fk"=>0, "id"=>"ctm_ordem", "description"=>"Ordem", "title"=>"", "type"=>"int", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "update"=>1, "insert"=>1, "line"=>4, );


    // Atributos FK
    $this->ctm_items["ctm_cod_CONTEUDO_ARTIGO"] = array("pk"=>0, "fk"=>1, "id"=>"ctm_cod_CONTEUDO_ARTIGO", "description"=>"Artigo", "title"=>"", "type"=>"fk", "type_content"=>"bottom", "type_behavior"=>"parent", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[S]", "fast"=>1, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "update"=>1, "insert"=>1, "line"=>2, "parent"=>array("modulo"=>"site", "entity"=>"ConteudoArtigo", "table"=>"TBL_CONTEUDO_ARTIGO", "prefix"=>"cta", "tag"=>"conteudoartigo", "key"=>"cta_codigo", "description"=>"cta_titulo", "form"=>"form", "target"=>"div-ctm_cod_CONTEUDO_ARTIGO-".rand()."-".date("Hisu"), "onchange"=>"", "encode"=>true, "width"=>"400", "where"=>"", "filter"=>""));


    // Atributos CHILD

    
    // Atributos padrao
    $this->ctm_items['ctm_alteracao'] = array('pk'=>false, 'fk'=>false, 'id'=>'ctm_alteracao', 'description'=>'Altera��o', 'title'=>'', 'type'=>'alteracao', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>true, 'insert'=>true, 'line'=>0);
    $this->ctm_items['ctm_registro'] = array('pk'=>false, 'fk'=>false, 'id'=>'ctm_registro', 'description'=>'Registro', 'title'=>'', 'type'=>'registro', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>false, 'insert'=>true, 'line'=>0);
    $this->ctm_items['ctm_responsavel'] = array('pk'=>false, 'fk'=>false, 'id'=>'ctm_responsavel', 'description'=>'Respons�vel', 'title'=>'', 'type'=>'responsavel', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>true, 'insert'=>true, 'line'=>0);
    $this->ctm_items['ctm_criador'] = array('pk'=>false, 'fk'=>false, 'id'=>'ctm_criador', 'description'=>'Criador', 'title'=>'', 'type'=>'criador', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>false, 'insert'=>true, 'line'=>0);

    $lines = 0;
    foreach($this->ctm_items as $item){
      $lines = ($item['line'] > $lines) ? $item['line'] : $lines;
    }
     
    $j = array();
    foreach($this->ctm_items as $item){
      if($item['fk']){
        if(isset($item['foreign']) or isset($item['parent'])){
          $table = "";
					$key = "";
					if(isset($item['foreign'])){
						$table = $item['foreign']['table'];
						$key = $item['foreign']['key'];
					}else if(isset($item['parent'])){
						$table = $item['parent']['table'];
						$key = $item['parent']['key'];
					}
          $j[$table] = " LEFT JOIN ".$table." ON (".$item['id']." = ".$key.") ";
        }
      }
    }
    $join = " ".join(' ', $j);
    
    $this->ctm_properties = array(
      'module'=>'site',
      'entity'=>'ConteudoModulo',
      'table'=>'TBL_CONTEUDO_MODULO',
			'join'=>$join,
      'tag'=>'conteudomodulo',
      'prefix'=>'ctm',
      'order'=>'ctm_top DESC, ctm_footer DESC, ctm_ordem',
      'group'=>'',
      'where'=>'',
      'search'=>'',
      'layout'=>'children',
			'checkbox'=>false,
      'saveonly'=>false,//desabilita a edi��o de entidade
      'readonly'=>false,//desabilita a cria��o de novos registros
      'remove'=>'',//false or
			 /**
        * array(
        *  "field"=>"prefix_id",
        *  "value"=>"1",
        *  "className"=>"icon-remove",
        *  "title"=>"Excluir",
        *  "message"=>"Deseja realmente remover este registro?",
        *  "success"=>"Registro removido com sucesso"
        * )
        */
			'database'=>null,
      'reference'=>'ctm_codigo',
      'description'=>'ctm_descricao',
      'lines'=>$lines
    );
    
    if (!$this->ctm_properties['reference']) {
      foreach ($this->ctm_items as $id=>$array) {
        if ($array['pk'] == 1) {
          $this->ctm_properties['reference'] = $id;
          break;
        }
      }
    }
    if (!$this->ctm_properties['description']) {
      foreach ($this->ctm_items as $id=>$array) {
        if ($array['type'] == "string") {
          $this->ctm_properties['reference'] = $id;
          break;
        }
      }
    }
  }

  /**
   * Recupera as principais propriedades da entidade
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 23/05/2013 21:09:57
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 23/05/2013 21:09:57
   */
  public function get_ctm_properties(){
    ?><?php
    return $this->ctm_properties;
  }

  /**
   * Recupera todos os atributos da entidade
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 23/05/2013 21:09:57
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 23/05/2013 21:09:57
   */
  public function get_ctm_items(){
    ?><?php
    return $this->ctm_items;
  }

  /**
   * Recupera um atributo da entidade com todas as suas propriedades
   * 
   * @param string $key
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 23/05/2013 21:09:57
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 23/05/2013 21:09:57
   */
  public function get_ctm_item($key){
    ?><?php
    return $this->ctm_items[$key];
  }

  /**
   * 
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 23/05/2013 21:09:57
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 23/05/2013 21:09:57
   */
  public function get_ctm_reference(){
    ?><?php
    $key = $this->ctm_properties['reference'];
    return $key;
  }

  /**
   * Recupera o valor de um objeto
   * 
   * @param string $key
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 23/05/2013 21:09:57
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 23/05/2013 21:09:57
   */
  public function get_ctm_value($key){
    ?><?php
    return $this->ctm_items[$key]['value'];
  }

  /**
   * Altera o valor de um atributo da inst�ncia da entidade
   * 
   * @param string $key
   * @param object $value
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 23/05/2013 21:09:57
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 23/05/2013 21:09:57
   */
  public function set_ctm_value($key, $value){
    ?><?php
    $this->ctm_items[$key]['value'] = $value;
  }

  /**
   * Altera o tipo de um atributo da inst�ncia da entidade
   * 
   * @param string $key
   * @param string $type
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 23/05/2013 21:09:57
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 23/05/2013 21:09:57
   */
  public function set_ctm_type($key, $type){
    ?><?php
    $this->ctm_items[$key]['type'] = $type;
  }


}