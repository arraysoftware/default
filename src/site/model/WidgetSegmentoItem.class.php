<?php
/**
 * @copyright array software
 *
 * @author WILLIAM MARQUES VICENTE GOMES CORREA - 24/05/2013 16:55:59
 * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/05/2013 00:25:57
 * @category model
 * @package site
 *
 * Responsavel.....: ADMINISTRADOR
 * Alteracao.......: 11/06/2013 22:14:35
 */


class WidgetSegmentoItem
{
  private  $wsi_items = array();
  private  $wsi_properties = array();

  /**
   * Construtor do Modelo da Classe
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 24/05/2013 16:55:59
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 24/05/2013 16:55:59
   */
  public function WidgetSegmentoItem(){
    ?><?php
    $this->wsi_items = array();
    
    // Atributos
    $this->wsi_items["wsi_codigo"] = array("pk"=>1, "fk"=>0, "id"=>"wsi_codigo", "description"=>"C�digo", "title"=>"", "type"=>"pk", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"", "update"=>0, "insert"=>0, "line"=>1, );
    $this->wsi_items["wsi_descricao"] = array("pk"=>0, "fk"=>0, "id"=>"wsi_descricao", "description"=>"Descri��o", "title"=>"", "type"=>"calculated", "type_content"=>"CONCAT(wis_nome, ' - ', wid_nome)", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>0, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"", "update"=>1, "insert"=>1, "line"=>2, );
    $this->wsi_items["wsi_ordem"] = array("pk"=>0, "fk"=>0, "id"=>"wsi_ordem", "description"=>"Ordem", "title"=>"", "type"=>"int", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "update"=>1, "insert"=>1, "line"=>5, );


    // Atributos FK
    $this->wsi_items["wsi_cod_WIDGET_SEGMENTO"] = array("pk"=>0, "fk"=>1, "id"=>"wsi_cod_WIDGET_SEGMENTO", "description"=>"Segmento", "title"=>"", "type"=>"fk", "type_content"=>"", "type_behavior"=>"foreign", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "update"=>1, "insert"=>1, "line"=>3, "foreign"=>array("modulo"=>"site", "entity"=>"WidgetSegmento", "table"=>"TBL_WIDGET_SEGMENTO", "prefix"=>"wis", "tag"=>"widgetsegmento", "key"=>"wis_codigo", "description"=>"wis_nome", "form"=>"form", "target"=>"div-wsi_cod_WIDGET_SEGMENTO-".rand()."-".date("Hisu"), "onchange"=>"", "encode"=>true, "width"=>"400", "where"=>"", "filter"=>""));
    $this->wsi_items["wsi_cod_WIDGET"] = array("pk"=>0, "fk"=>1, "id"=>"wsi_cod_WIDGET", "description"=>"Widget", "title"=>"", "type"=>"fk", "type_content"=>"", "type_behavior"=>"foreign", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "update"=>1, "insert"=>1, "line"=>4, "foreign"=>array("modulo"=>"site", "entity"=>"Widget", "table"=>"TBL_WIDGET", "prefix"=>"wid", "tag"=>"widget", "key"=>"wid_codigo", "description"=>"wid_descricao", "form"=>"form", "target"=>"div-wsi_cod_WIDGET-".rand()."-".date("Hisu"), "onchange"=>"", "encode"=>true, "width"=>"400", "where"=>"", "filter"=>""));


    // Atributos CHILD

    
    // Atributos padrao
    $this->wsi_items['wsi_alteracao'] = array('pk'=>false, 'fk'=>false, 'id'=>'wsi_alteracao', 'description'=>'Altera��o', 'title'=>'', 'type'=>'alteracao', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>true, 'insert'=>true, 'line'=>0);
    $this->wsi_items['wsi_registro'] = array('pk'=>false, 'fk'=>false, 'id'=>'wsi_registro', 'description'=>'Registro', 'title'=>'', 'type'=>'registro', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>false, 'insert'=>true, 'line'=>0);
    $this->wsi_items['wsi_responsavel'] = array('pk'=>false, 'fk'=>false, 'id'=>'wsi_responsavel', 'description'=>'Respons�vel', 'title'=>'', 'type'=>'responsavel', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>true, 'insert'=>true, 'line'=>0);
    $this->wsi_items['wsi_criador'] = array('pk'=>false, 'fk'=>false, 'id'=>'wsi_criador', 'description'=>'Criador', 'title'=>'', 'type'=>'criador', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>false, 'insert'=>true, 'line'=>0);

    $lines = 0;
    foreach($this->wsi_items as $item){
      $lines = ($item['line'] > $lines) ? $item['line'] : $lines;
    }
     
    $j = array();
    foreach($this->wsi_items as $item){
      if($item['fk']){
        if(isset($item['foreign']) or isset($item['parent'])){
          $table = "";
					$key = "";
					if(isset($item['foreign'])){
						$table = $item['foreign']['table'];
						$key = $item['foreign']['key'];
					}else if(isset($item['parent'])){
						$table = $item['parent']['table'];
						$key = $item['parent']['key'];
					}
          $j[$table] = " LEFT JOIN ".$table." ON (".$item['id']." = ".$key.") ";
        }
      }
    }
    $join = " ".join(' ', $j);
    
    $this->wsi_properties = array(
      'module'=>'site',
      'entity'=>'WidgetSegmentoItem',
      'table'=>'TBL_WIDGET_SEGMENTO_ITEM',
			'join'=>$join,
      'tag'=>'widgetsegmentoitem',
      'prefix'=>'wsi',
      'order'=>'',
      'group'=>'',
      'where'=>'',
      'search'=>'',
      'layout'=>'',
			'checkbox'=>false,
      'saveonly'=>false,//desabilita a edi��o de entidade
      'readonly'=>false,//desabilita a cria��o de novos registros
      'remove'=>'',//false or
			 /**
        * array(
        *  "field"=>"prefix_id",
        *  "value"=>"1",
        *  "className"=>"icon-remove",
        *  "title"=>"Excluir",
        *  "message"=>"Deseja realmente remover este registro?",
        *  "success"=>"Registro removido com sucesso"
        * )
        */
			'database'=>null,
      'reference'=>'wsi_codigo',
      'description'=>'wsi_descricao',
      'lines'=>$lines
    );
    
    if (!$this->wsi_properties['reference']) {
      foreach ($this->wsi_items as $id=>$array) {
        if ($array['pk'] == 1) {
          $this->wsi_properties['reference'] = $id;
          break;
        }
      }
    }
    if (!$this->wsi_properties['description']) {
      foreach ($this->wsi_items as $id=>$array) {
        if ($array['type'] == "string") {
          $this->wsi_properties['reference'] = $id;
          break;
        }
      }
    }
  }

  /**
   * Recupera as principais propriedades da entidade
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 24/05/2013 16:55:59
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 24/05/2013 16:55:59
   */
  public function get_wsi_properties(){
    ?><?php
    return $this->wsi_properties;
  }

  /**
   * Recupera todos os atributos da entidade
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 24/05/2013 16:55:59
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 24/05/2013 16:55:59
   */
  public function get_wsi_items(){
    ?><?php
    return $this->wsi_items;
  }

  /**
   * Recupera um atributo da entidade com todas as suas propriedades
   * 
   * @param string $key
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 24/05/2013 16:56:00
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 24/05/2013 16:56:00
   */
  public function get_wsi_item($key){
    ?><?php
    return $this->wsi_items[$key];
  }

  /**
   * 
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 24/05/2013 16:56:00
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 24/05/2013 16:56:00
   */
  public function get_wsi_reference(){
    ?><?php
    $key = $this->wsi_properties['reference'];
    return $key;
  }

  /**
   * Recupera o valor de um objeto
   * 
   * @param string $key
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 24/05/2013 16:56:00
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 24/05/2013 16:56:00
   */
  public function get_wsi_value($key){
    ?><?php
    return $this->wsi_items[$key]['value'];
  }

  /**
   * Altera o valor de um atributo da inst�ncia da entidade
   * 
   * @param string $key
   * @param object $value
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 24/05/2013 16:56:00
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 24/05/2013 16:56:00
   */
  public function set_wsi_value($key, $value){
    ?><?php
    $this->wsi_items[$key]['value'] = $value;
  }

  /**
   * Altera o tipo de um atributo da inst�ncia da entidade
   * 
   * @param string $key
   * @param string $type
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 24/05/2013 16:56:00
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 24/05/2013 16:56:00
   */
  public function set_wsi_type($key, $type){
    ?><?php
    $this->wsi_items[$key]['type'] = $type;
  }


}