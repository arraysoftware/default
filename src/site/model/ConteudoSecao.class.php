<?php
/**
 * @copyright array software
 *
 * @author  - 23/03/2013 23:11:23
 * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 28/05/2013 14:05:10
 * @category model
 * @package site
 *
 * Responsavel.....: ADMINISTRADOR
 * Alteracao.......: 11/06/2013 22:14:34
 */


class ConteudoSecao
{
  private  $cts_items = array();
  private  $cts_properties = array();

  /**
   * Construtor do Modelo da Classe
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 26/05/2013 16:19:06
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 26/05/2013 16:22:30
   */
  public function ConteudoSecao(){
    ?><?php
    $this->cts_items = array();
    
    // Atributos
    $this->cts_items["cts_codigo"] = array("pk"=>1, "fk"=>0, "id"=>"cts_codigo", "description"=>"C�digo", "title"=>"", "type"=>"pk", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"", "update"=>1, "insert"=>1, "line"=>1, );
    $this->cts_items["cts_descricao"] = array("pk"=>0, "fk"=>0, "id"=>"cts_descricao", "description"=>"Descri��o", "title"=>"", "type"=>"string", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "update"=>1, "insert"=>1, "line"=>5, );
    $this->cts_items["cts_ordem"] = array("pk"=>0, "fk"=>0, "id"=>"cts_ordem", "description"=>"Ordem", "title"=>"", "type"=>"int", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "update"=>1, "insert"=>1, "line"=>5, );


    // Atributos FK
    $this->cts_items["cts_cod_CONTEUDO_CATEGORIA"] = array("pk"=>0, "fk"=>1, "id"=>"cts_cod_CONTEUDO_CATEGORIA", "description"=>"Categoria", "title"=>"", "type"=>"fk", "type_content"=>"", "type_behavior"=>"foreign", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "update"=>1, "insert"=>1, "line"=>2, "foreign"=>array("modulo"=>"site", "entity"=>"ConteudoCategoria", "table"=>"TBL_CONTEUDO_CATEGORIA", "prefix"=>"ctc", "tag"=>"conteudocategoria", "key"=>"ctc_codigo", "description"=>"ctc_descricao", "form"=>"form", "target"=>"div-cts_cod_CONTEUDO_CATEGORIA-".rand()."-".date("Hisu"), "onchange"=>"", "encode"=>true, "width"=>"400", "where"=>"", "filter"=>""));
    $this->cts_items["cts_cod_CONTEUDO_ARTIGO"] = array("pk"=>0, "fk"=>1, "id"=>"cts_cod_CONTEUDO_ARTIGO", "description"=>"Artigo", "title"=>"", "type"=>"fk", "type_content"=>"", "type_behavior"=>"parent", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[S]", "fast"=>1, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "update"=>1, "insert"=>1, "line"=>4, "parent"=>array("modulo"=>"site", "entity"=>"ConteudoArtigo", "table"=>"TBL_CONTEUDO_ARTIGO", "prefix"=>"cta", "tag"=>"conteudoartigo", "key"=>"cta_codigo", "description"=>"cta_titulo", "form"=>"form", "target"=>"div-cts_cod_CONTEUDO_ARTIGO-".rand()."-".date("Hisu"), "onchange"=>"", "encode"=>true, "width"=>"400", "where"=>"", "filter"=>""));


    // Atributos CHILD

    
    // Atributos padrao
    $this->cts_items['cts_alteracao'] = array('pk'=>false, 'fk'=>false, 'id'=>'cts_alteracao', 'description'=>'Altera��o', 'title'=>'', 'type'=>'alteracao', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>true, 'insert'=>true, 'line'=>0);
    $this->cts_items['cts_registro'] = array('pk'=>false, 'fk'=>false, 'id'=>'cts_registro', 'description'=>'Registro', 'title'=>'', 'type'=>'registro', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>false, 'insert'=>true, 'line'=>0);
    $this->cts_items['cts_responsavel'] = array('pk'=>false, 'fk'=>false, 'id'=>'cts_responsavel', 'description'=>'Respons�vel', 'title'=>'', 'type'=>'responsavel', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>true, 'insert'=>true, 'line'=>0);
    $this->cts_items['cts_criador'] = array('pk'=>false, 'fk'=>false, 'id'=>'cts_criador', 'description'=>'Criador', 'title'=>'', 'type'=>'criador', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>false, 'insert'=>true, 'line'=>0);

    $lines = 0;
    foreach($this->cts_items as $item){
      $lines = ($item['line'] > $lines) ? $item['line'] : $lines;
    }
     
    $j = array();
    foreach($this->cts_items as $item){
      if($item['fk']){
        if(isset($item['foreign']) or isset($item['parent'])){
          $table = "";
					$key = "";
					if(isset($item['foreign'])){
						$table = $item['foreign']['table'];
						$key = $item['foreign']['key'];
					}else if(isset($item['parent'])){
						$table = $item['parent']['table'];
						$key = $item['parent']['key'];
					}
          $j[$table] = " LEFT JOIN ".$table." ON (".$item['id']." = ".$key.") ";
        }
      }
    }
    $join = " ".join(' ', $j);
    
    $this->cts_properties = array(
      'module'=>'site',
      'entity'=>'ConteudoSecao',
      'table'=>'TBL_CONTEUDO_SECAO',
			'join'=>$join,
      'tag'=>'conteudosecao',
      'prefix'=>'cts',
      'order'=>'cts_cod_CONTEUDO_CATEGORIA,cts_descricao',
      'group'=>'',
      'where'=>'',
      'search'=>'',
      'layout'=>'children',
			'checkbox'=>false,
      'saveonly'=>false,//desabilita a edi��o de entidade
      'readonly'=>false,//desabilita a cria��o de novos registros
      'remove'=>'',//false or
			 /**
        * array(
        *  "field"=>"prefix_id",
        *  "value"=>"1",
        *  "className"=>"icon-remove",
        *  "title"=>"Excluir",
        *  "message"=>"Deseja realmente remover este registro?",
        *  "success"=>"Registro removido com sucesso"
        * )
        */
			'database'=>null,
      'reference'=>'cts_codigo',
      'description'=>'cts_descricao',
      'lines'=>$lines
    );
    
    if (!$this->cts_properties['reference']) {
      foreach ($this->cts_items as $id=>$array) {
        if ($array['pk'] == 1) {
          $this->cts_properties['reference'] = $id;
          break;
        }
      }
    }
    if (!$this->cts_properties['description']) {
      foreach ($this->cts_items as $id=>$array) {
        if ($array['type'] == "string") {
          $this->cts_properties['reference'] = $id;
          break;
        }
      }
    }
  }

  /**
   * Recupera as principais propriedades da entidade
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 26/05/2013 16:19:06
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 26/05/2013 16:19:06
   */
  public function get_cts_properties(){
    ?><?php
    return $this->cts_properties;
  }

  /**
   * Recupera todos os atributos da entidade
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 26/05/2013 16:19:06
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 26/05/2013 16:19:06
   */
  public function get_cts_items(){
    ?><?php
    return $this->cts_items;
  }

  /**
   * Recupera um atributo da entidade com todas as suas propriedades
   * 
   * @param string $key
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 26/05/2013 16:19:06
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 26/05/2013 16:19:06
   */
  public function get_cts_item($key){
    ?><?php
    return $this->cts_items[$key];
  }

  /**
   * 
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 26/05/2013 16:19:06
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 26/05/2013 16:19:06
   */
  public function get_cts_reference(){
    ?><?php
    $key = $this->cts_properties['reference'];
    return $key;
  }

  /**
   * Recupera o valor de um objeto
   * 
   * @param string $key
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 26/05/2013 16:19:06
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 26/05/2013 16:19:06
   */
  public function get_cts_value($key){
    ?><?php
    return $this->cts_items[$key]['value'];
  }

  /**
   * Altera o valor de um atributo da inst�ncia da entidade
   * 
   * @param string $key
   * @param object $value
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 26/05/2013 16:19:06
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 26/05/2013 16:19:06
   */
  public function set_cts_value($key, $value){
    ?><?php
    $this->cts_items[$key]['value'] = $value;
  }

  /**
   * Altera o tipo de um atributo da inst�ncia da entidade
   * 
   * @param string $key
   * @param string $type
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 26/05/2013 16:19:06
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 26/05/2013 16:19:06
   */
  public function set_cts_type($key, $type){
    ?><?php
    $this->cts_items[$key]['type'] = $type;
  }


}