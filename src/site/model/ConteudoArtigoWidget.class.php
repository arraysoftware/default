<?php
/**
 * @copyright array software
 *
 * @author WILLIAM MARQUES VICENTE GOMES CORREA - 24/05/2013 23:55:16
 * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/05/2013 18:02:21
 * @category model
 * @package site
 *
 * Responsavel.....: ADMINISTRADOR
 * Alteracao.......: 11/06/2013 22:14:35
 */


class ConteudoArtigoWidget
{
  private  $caw_items = array();
  private  $caw_properties = array();

  /**
   * Construtor do Modelo da Classe
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 25/05/2013 01:38:41
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/05/2013 01:38:41
   */
  public function ConteudoArtigoWidget(){
    ?><?php
    $this->caw_items = array();
    
    // Atributos
    $this->caw_items["caw_codigo"] = array("pk"=>1, "fk"=>0, "id"=>"caw_codigo", "description"=>"C�digo", "title"=>"", "type"=>"pk", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>1, "grid"=>0, "grid_width"=>"", "form"=>0, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"", "update"=>0, "insert"=>0, "line"=>1, );
    $this->caw_items["caw_descricao"] = array("pk"=>0, "fk"=>0, "id"=>"caw_descricao", "description"=>"Descri��o", "title"=>"", "type"=>"calculated", "type_content"=>"CONCAT(wid_descricao,' - ', caw_ordem)", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>0, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"", "update"=>1, "insert"=>1, "line"=>4, );
    $this->caw_items["caw_local"] = array("pk"=>0, "fk"=>0, "id"=>"caw_local", "description"=>"Local", "title"=>"", "type"=>"option", "type_content"=>"0,Lateral|1,Central", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[R]", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"0", "default_sql"=>"", "update"=>1, "insert"=>1, "line"=>5, );
    $this->caw_items["caw_tamanho"] = array("pk"=>0, "fk"=>0, "id"=>"caw_tamanho", "description"=>"Tamanho", "title"=>"", "type"=>"option", "type_content"=>"default,Usar 1/3 da Tela|columns-2,Metade da Tela|columns-3,Ocupar 2/3 da Tela|columns-1,Toda a tela", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[R]", "fast"=>0, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"0", "default_sql"=>"", "update"=>1, "insert"=>1, "line"=>6, );
    $this->caw_items["caw_ordem"] = array("pk"=>0, "fk"=>0, "id"=>"caw_ordem", "description"=>"Ordem", "title"=>"", "type"=>"int", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "update"=>1, "insert"=>1, "line"=>7, );


    // Atributos FK
    $this->caw_items["caw_cod_CONTEUDO_ARTIGO"] = array("pk"=>0, "fk"=>1, "id"=>"caw_cod_CONTEUDO_ARTIGO", "description"=>"Artigo", "title"=>"", "type"=>"fk", "type_content"=>"", "type_behavior"=>"foreign", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[S]", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "update"=>1, "insert"=>1, "line"=>2, "foreign"=>array("modulo"=>"site", "entity"=>"ConteudoArtigo", "table"=>"TBL_CONTEUDO_ARTIGO", "prefix"=>"cta", "tag"=>"conteudoartigo", "key"=>"cta_codigo", "description"=>"cta_titulo", "form"=>"form", "target"=>"div-caw_cod_CONTEUDO_ARTIGO-".rand()."-".date("Hisu"), "onchange"=>"", "encode"=>true, "width"=>"400", "where"=>"", "filter"=>""));
    $this->caw_items["caw_cod_WIDGET"] = array("pk"=>0, "fk"=>1, "id"=>"caw_cod_WIDGET", "description"=>"Widget", "title"=>"", "type"=>"fk", "type_content"=>"", "type_behavior"=>"foreign", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[S]", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "update"=>1, "insert"=>1, "line"=>3, "foreign"=>array("modulo"=>"site", "entity"=>"Widget", "table"=>"TBL_WIDGET", "prefix"=>"wid", "tag"=>"widget", "key"=>"wid_codigo", "description"=>"wid_descricao", "form"=>"form", "target"=>"div-caw_cod_WIDGET-".rand()."-".date("Hisu"), "onchange"=>"", "encode"=>true, "width"=>"400", "where"=>"", "filter"=>""));


    // Atributos CHILD

    
    // Atributos padrao
    $this->caw_items['caw_alteracao'] = array('pk'=>false, 'fk'=>false, 'id'=>'caw_alteracao', 'description'=>'Altera��o', 'title'=>'', 'type'=>'alteracao', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>true, 'insert'=>true, 'line'=>0);
    $this->caw_items['caw_registro'] = array('pk'=>false, 'fk'=>false, 'id'=>'caw_registro', 'description'=>'Registro', 'title'=>'', 'type'=>'registro', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>false, 'insert'=>true, 'line'=>0);
    $this->caw_items['caw_responsavel'] = array('pk'=>false, 'fk'=>false, 'id'=>'caw_responsavel', 'description'=>'Respons�vel', 'title'=>'', 'type'=>'responsavel', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>true, 'insert'=>true, 'line'=>0);
    $this->caw_items['caw_criador'] = array('pk'=>false, 'fk'=>false, 'id'=>'caw_criador', 'description'=>'Criador', 'title'=>'', 'type'=>'criador', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>false, 'insert'=>true, 'line'=>0);

    $lines = 0;
    foreach($this->caw_items as $item){
      $lines = ($item['line'] > $lines) ? $item['line'] : $lines;
    }
     
    $j = array();
    foreach($this->caw_items as $item){
      if($item['fk']){
        if(isset($item['foreign']) or isset($item['parent'])){
          $table = "";
					$key = "";
					if(isset($item['foreign'])){
						$table = $item['foreign']['table'];
						$key = $item['foreign']['key'];
					}else if(isset($item['parent'])){
						$table = $item['parent']['table'];
						$key = $item['parent']['key'];
					}
          $j[$table] = " LEFT JOIN ".$table." ON (".$item['id']." = ".$key.") ";
        }
      }
    }
    $join = " ".join(' ', $j);
    
    $this->caw_properties = array(
      'module'=>'site',
      'entity'=>'ConteudoArtigoWidget',
      'table'=>'TBL_CONTEUDO_ARTIGO_WIDGET',
			'join'=>$join,
      'tag'=>'conteudoArtigowidget',
      'prefix'=>'caw',
      'order'=>'caw_local,caw_ordem',
      'group'=>'',
      'where'=>'',
      'search'=>'',
      'layout'=>'',
			'checkbox'=>false,
      'saveonly'=>false,//desabilita a edi��o de entidade
      'readonly'=>false,//desabilita a cria��o de novos registros
      'remove'=>'',//false or
			 /**
        * array(
        *  "field"=>"prefix_id",
        *  "value"=>"1",
        *  "className"=>"icon-remove",
        *  "title"=>"Excluir",
        *  "message"=>"Deseja realmente remover este registro?",
        *  "success"=>"Registro removido com sucesso"
        * )
        */
			'database'=>null,
      'reference'=>'caw_codigo',
      'description'=>'caw_descricao',
      'lines'=>$lines
    );
    
    if (!$this->caw_properties['reference']) {
      foreach ($this->caw_items as $id=>$array) {
        if ($array['pk'] == 1) {
          $this->caw_properties['reference'] = $id;
          break;
        }
      }
    }
    if (!$this->caw_properties['description']) {
      foreach ($this->caw_items as $id=>$array) {
        if ($array['type'] == "string") {
          $this->caw_properties['reference'] = $id;
          break;
        }
      }
    }
  }

  /**
   * Recupera as principais propriedades da entidade
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 25/05/2013 01:38:41
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/05/2013 01:38:41
   */
  public function get_caw_properties(){
    ?><?php
    return $this->caw_properties;
  }

  /**
   * Recupera todos os atributos da entidade
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 25/05/2013 01:38:41
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/05/2013 01:38:41
   */
  public function get_caw_items(){
    ?><?php
    return $this->caw_items;
  }

  /**
   * Recupera um atributo da entidade com todas as suas propriedades
   * 
   * @param string $key
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 25/05/2013 01:38:41
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/05/2013 01:38:41
   */
  public function get_caw_item($key){
    ?><?php
    return $this->caw_items[$key];
  }

  /**
   * 
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 25/05/2013 01:38:41
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/05/2013 01:38:41
   */
  public function get_caw_reference(){
    ?><?php
    $key = $this->caw_properties['reference'];
    return $key;
  }

  /**
   * Recupera o valor de um objeto
   * 
   * @param string $key
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 25/05/2013 01:38:41
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/05/2013 01:38:41
   */
  public function get_caw_value($key){
    ?><?php
    return $this->caw_items[$key]['value'];
  }

  /**
   * Altera o valor de um atributo da inst�ncia da entidade
   * 
   * @param string $key
   * @param object $value
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 25/05/2013 01:38:41
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/05/2013 01:38:41
   */
  public function set_caw_value($key, $value){
    ?><?php
    $this->caw_items[$key]['value'] = $value;
  }

  /**
   * Altera o tipo de um atributo da inst�ncia da entidade
   * 
   * @param string $key
   * @param string $type
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 25/05/2013 01:38:41
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/05/2013 01:38:41
   */
  public function set_caw_type($key, $type){
    ?><?php
    $this->caw_items[$key]['type'] = $type;
  }


}