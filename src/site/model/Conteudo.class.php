<?php
/**
 * @copyright array software
 *
 * @author  - 23/03/2013 23:12:20
 * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 26/05/2013 15:50:37
 * @category model
 * @package site
 *
 * Responsavel.....: ADMINISTRADOR
 * Alteracao.......: 11/06/2013 22:14:34
 */


class Conteudo
{
  private  $ctd_items = array();
  private  $ctd_properties = array();

  /**
   * Construtor do Modelo da Classe
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 25/05/2013 18:44:23
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/05/2013 18:46:14
   */
  public function Conteudo(){
    ?><?php
    $this->ctd_items = array();
    
    // Atributos
    $this->ctd_items["ctd_codigo"] = array("pk"=>1, "fk"=>0, "id"=>"ctd_codigo", "description"=>"C�digo", "title"=>"", "type"=>"pk", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"", "update"=>0, "insert"=>0, "line"=>1, );
    $this->ctd_items["ctd_descricao"] = array("pk"=>0, "fk"=>0, "id"=>"ctd_descricao", "description"=>"Descri��o", "title"=>"", "type"=>"string", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "update"=>1, "insert"=>1, "line"=>5, );


    // Atributos FK
    $this->ctd_items["ctd_cod_CONTEUDO_SECAO"] = array("pk"=>0, "fk"=>1, "id"=>"ctd_cod_CONTEUDO_SECAO", "description"=>"Se��o", "title"=>"", "type"=>"fk", "type_content"=>"", "type_behavior"=>"foreign", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "update"=>1, "insert"=>1, "line"=>2, "foreign"=>array("modulo"=>"site", "entity"=>"ConteudoSecao", "table"=>"TBL_CONTEUDO_SECAO", "prefix"=>"cts", "tag"=>"conteudosecao", "key"=>"cts_codigo", "description"=>"cts_descricao", "form"=>"form", "target"=>"div-ctd_cod_CONTEUDO_SECAO-".rand()."-".date("Hisu"), "onchange"=>"", "encode"=>true, "width"=>"400", "where"=>"", "filter"=>""));
    $this->ctd_items["ctd_cod_CONTEUDO_ARTIGO"] = array("pk"=>0, "fk"=>1, "id"=>"ctd_cod_CONTEUDO_ARTIGO", "description"=>"Artigo", "title"=>"", "type"=>"fk", "type_content"=>"bottom", "type_behavior"=>"parent", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>1, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "update"=>1, "insert"=>1, "line"=>4, "parent"=>array("modulo"=>"site", "entity"=>"ConteudoArtigo", "table"=>"TBL_CONTEUDO_ARTIGO", "prefix"=>"cta", "tag"=>"conteudoartigo", "key"=>"cta_codigo", "description"=>"cta_titulo", "form"=>"form", "target"=>"div-ctd_cod_CONTEUDO_ARTIGO-".rand()."-".date("Hisu"), "onchange"=>"", "encode"=>true, "width"=>"400", "where"=>"", "filter"=>""));


    // Atributos CHILD

    
    // Atributos padrao
    $this->ctd_items['ctd_alteracao'] = array('pk'=>false, 'fk'=>false, 'id'=>'ctd_alteracao', 'description'=>'Altera��o', 'title'=>'', 'type'=>'alteracao', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>true, 'insert'=>true, 'line'=>0);
    $this->ctd_items['ctd_registro'] = array('pk'=>false, 'fk'=>false, 'id'=>'ctd_registro', 'description'=>'Registro', 'title'=>'', 'type'=>'registro', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>false, 'insert'=>true, 'line'=>0);
    $this->ctd_items['ctd_responsavel'] = array('pk'=>false, 'fk'=>false, 'id'=>'ctd_responsavel', 'description'=>'Respons�vel', 'title'=>'', 'type'=>'responsavel', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>true, 'insert'=>true, 'line'=>0);
    $this->ctd_items['ctd_criador'] = array('pk'=>false, 'fk'=>false, 'id'=>'ctd_criador', 'description'=>'Criador', 'title'=>'', 'type'=>'criador', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>false, 'insert'=>true, 'line'=>0);

    $lines = 0;
    foreach($this->ctd_items as $item){
      $lines = ($item['line'] > $lines) ? $item['line'] : $lines;
    }
     
    $j = array();
    foreach($this->ctd_items as $item){
      if($item['fk']){
        if(isset($item['foreign']) or isset($item['parent'])){
          $table = "";
					$key = "";
					if(isset($item['foreign'])){
						$table = $item['foreign']['table'];
						$key = $item['foreign']['key'];
					}else if(isset($item['parent'])){
						$table = $item['parent']['table'];
						$key = $item['parent']['key'];
					}
          $j[$table] = " LEFT JOIN ".$table." ON (".$item['id']." = ".$key.") ";
        }
      }
    }
    $join = " ".join(' ', $j);
    
    $this->ctd_properties = array(
      'module'=>'site',
      'entity'=>'Conteudo',
      'table'=>'TBL_CONTEUDO',
			'join'=>$join,
      'tag'=>'conteudo',
      'prefix'=>'ctd',
      'order'=>'ctd_descricao',
      'group'=>'',
      'where'=>'',
      'search'=>'',
      'layout'=>'children',
			'checkbox'=>false,
      'saveonly'=>false,//desabilita a edi��o de entidade
      'readonly'=>false,//desabilita a cria��o de novos registros
      'remove'=>'',//false or
			 /**
        * array(
        *  "field"=>"prefix_id",
        *  "value"=>"1",
        *  "className"=>"icon-remove",
        *  "title"=>"Excluir",
        *  "message"=>"Deseja realmente remover este registro?",
        *  "success"=>"Registro removido com sucesso"
        * )
        */
			'database'=>null,
      'reference'=>'ctd_codigo',
      'description'=>'ctd_descricao',
      'lines'=>$lines
    );
    
    if (!$this->ctd_properties['reference']) {
      foreach ($this->ctd_items as $id=>$array) {
        if ($array['pk'] == 1) {
          $this->ctd_properties['reference'] = $id;
          break;
        }
      }
    }
    if (!$this->ctd_properties['description']) {
      foreach ($this->ctd_items as $id=>$array) {
        if ($array['type'] == "string") {
          $this->ctd_properties['reference'] = $id;
          break;
        }
      }
    }
  }

  /**
   * Recupera as principais propriedades da entidade
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 25/05/2013 18:44:23
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/05/2013 18:44:23
   */
  public function get_ctd_properties(){
    ?><?php
    return $this->ctd_properties;
  }

  /**
   * Recupera todos os atributos da entidade
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 25/05/2013 18:44:23
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/05/2013 18:44:23
   */
  public function get_ctd_items(){
    ?><?php
    return $this->ctd_items;
  }

  /**
   * Recupera um atributo da entidade com todas as suas propriedades
   * 
   * @param string $key
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 25/05/2013 18:44:23
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/05/2013 18:44:23
   */
  public function get_ctd_item($key){
    ?><?php
    return $this->ctd_items[$key];
  }

  /**
   * 
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 25/05/2013 18:44:23
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/05/2013 18:44:23
   */
  public function get_ctd_reference(){
    ?><?php
    $key = $this->ctd_properties['reference'];
    return $key;
  }

  /**
   * Recupera o valor de um objeto
   * 
   * @param string $key
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 25/05/2013 18:44:23
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/05/2013 18:44:23
   */
  public function get_ctd_value($key){
    ?><?php
    return $this->ctd_items[$key]['value'];
  }

  /**
   * Altera o valor de um atributo da inst�ncia da entidade
   * 
   * @param string $key
   * @param object $value
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 25/05/2013 18:44:23
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/05/2013 18:44:23
   */
  public function set_ctd_value($key, $value){
    ?><?php
    $this->ctd_items[$key]['value'] = $value;
  }

  /**
   * Altera o tipo de um atributo da inst�ncia da entidade
   * 
   * @param string $key
   * @param string $type
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 25/05/2013 18:44:23
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/05/2013 18:44:23
   */
  public function set_ctd_type($key, $type){
    ?><?php
    $this->ctd_items[$key]['type'] = $type;
  }


}