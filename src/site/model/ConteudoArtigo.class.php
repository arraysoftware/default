<?php
/**
 * @copyright array software
 *
 * @author  - 23/03/2013 23:13:17
 * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 28/05/2013 13:56:28
 * @category model
 * @package site
 *
 * Responsavel.....: ADMINISTRADOR
 * Alteracao.......: 11/06/2013 22:14:35
 */


class ConteudoArtigo
{
  private  $cta_items = array();
  private  $cta_properties = array();

  /**
   * Construtor do Modelo da Classe
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 25/05/2013 01:14:40
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/05/2013 23:03:26
   */
  public function ConteudoArtigo(){
    ?><?php
    $this->cta_items = array();
    
    // Atributos
    $this->cta_items["cta_codigo"] = array("pk"=>1, "fk"=>0, "id"=>"cta_codigo", "description"=>"C�digo", "title"=>"", "type"=>"pk", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"", "update"=>0, "insert"=>0, "line"=>1, );
    $this->cta_items["cta_titulo"] = array("pk"=>0, "fk"=>0, "id"=>"cta_titulo", "description"=>"T�tulo", "title"=>"T&iacute;tulo principal do conte&uacute;do", "type"=>"string", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "update"=>1, "insert"=>1, "line"=>2, );
    $this->cta_items["cta_slug"] = array("pk"=>0, "fk"=>0, "id"=>"cta_slug", "description"=>"Identificador", "title"=>"Url amig&aacute;vel", "type"=>"string", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "update"=>1, "insert"=>1, "line"=>2, );
    $this->cta_items["cta_tipo"] = array("pk"=>0, "fk"=>0, "id"=>"cta_tipo", "description"=>"Tipo", "title"=>"Texto - Exibe o conte&uacute;do informado na p&aacute;gina
Redirecionamento - Quando uma p&aacute;gina ser&aacute; apenas um redirecionamento para outro conte&uacute;do
Galeria de Imagens - Quando uma p&aacute;gina ser&aacute; uma galeria de imagens
Galeria de Arquivos - Transforma a p&aacute;gina em uma central de downloads
Portif&oacute;lio - Uma p&aacute;gina com imagens e informa&ccedil;&otilde;es sobre elas", "type"=>"option", "type_content"=>"0,Texto|1,Redirecionamento|2,Galeria de Imagens|3,Galeria de Arquivos|4,Portif&oacute;lio|5,Default", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[S]", "fast"=>0, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"0", "default_sql"=>"", "update"=>1, "insert"=>1, "line"=>3, );
    $this->cta_items["cta_side_bar"] = array("pk"=>0, "fk"=>0, "id"=>"cta_side_bar", "description"=>"Barra Lateral", "title"=>"", "type"=>"option", "type_content"=>"left,Esquerda|right,Direita|hide,Ocultar", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"left", "default_sql"=>"", "update"=>1, "insert"=>1, "line"=>4, );
    $this->cta_items["cta_widget"] = array("pk"=>0, "fk"=>0, "id"=>"cta_widget", "description"=>"Widgets Padr�o", "title"=>"", "type"=>"yes/no", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>0, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"1", "default_sql"=>"", "update"=>1, "insert"=>1, "line"=>4, );
    $this->cta_items["cta_data_publicacao"] = array("pk"=>0, "fk"=>0, "id"=>"cta_data_publicacao", "description"=>"Publica��o", "title"=>"", "type"=>"datetime", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[D]", "fast"=>0, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "update"=>1, "insert"=>1, "line"=>3, );
    $this->cta_items["cta_publicar"] = array("pk"=>0, "fk"=>0, "id"=>"cta_publicar", "description"=>"Publicar", "title"=>"Permite ou n&atilde;o a exibi&ccedil;&atilde;o do campo", "type"=>"option", "type_content"=>"1,P&uacute;blico|2,Privado|0,N&atilde;o", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[R]", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"1", "default_sql"=>"", "update"=>1, "insert"=>1, "line"=>4, );
    $this->cta_items["cta_miniatura"] = array("pk"=>0, "fk"=>0, "id"=>"cta_miniatura", "description"=>"Miniatura", "title"=>"", "type"=>"image", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "update"=>1, "insert"=>1, "line"=>5, );
    $this->cta_items["cta_resumo"] = array("pk"=>0, "fk"=>0, "id"=>"cta_resumo", "description"=>"Resumo", "title"=>"Breve resumo sobre o material", "type"=>"text", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>1, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "update"=>1, "insert"=>1, "line"=>6, );
    $this->cta_items["cta_featured"] = array("pk"=>0, "fk"=>0, "id"=>"cta_featured", "description"=>"Destacar", "title"=>"", "type"=>"yes/no", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[R]", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "update"=>1, "insert"=>1, "line"=>5, );
    $this->cta_items["cta_conteudo"] = array("pk"=>0, "fk"=>0, "id"=>"cta_conteudo", "description"=>"Conte�do", "title"=>"Campo que cont&eacute;m o conte&uacute;do que ser&aacute; exibido", "type"=>"rich-text", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>1, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "update"=>1, "insert"=>1, "line"=>7, );
    $this->cta_items["cta_hit"] = array("pk"=>0, "fk"=>0, "id"=>"cta_hit", "description"=>"Acessos", "title"=>"Contador com a quantidade de acessos do item", "type"=>"int", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"", "update"=>1, "insert"=>1, "line"=>8, );


    // Atributos FK


    // Atributos CHILD
    $this->cta_items["cta_fk_cmw_cod_CONTEUDO_ARTIGO_544"] = array("pk"=>0, "fk"=>0, "id"=>"cta_fk_cmw_cod_CONTEUDO_ARTIGO_544", "description"=>"Widgets", "title"=>"", "type"=>"", "type_content"=>"", "type_behavior"=>"child", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"50", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "update"=>0, "insert"=>0, "line"=>0, "child"=>array("module"=>"site", "entity"=>"ConteudoArtigoWidget", "tag"=>"conteudoArtigowidget", "prefix"=>"caw", "name"=>"Widgets", "filter"=>"caw_codigo", "key"=>"cta_codigo", "params"=>"child=true&width=&height=&rotule=&t=&f=", "target"=>"div-".rand()."-".date("Hisu"), "form"=>"form"));

    
    // Atributos padrao
    $this->cta_items['cta_alteracao'] = array('pk'=>false, 'fk'=>false, 'id'=>'cta_alteracao', 'description'=>'Altera��o', 'title'=>'', 'type'=>'alteracao', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>true, 'insert'=>true, 'line'=>0);
    $this->cta_items['cta_registro'] = array('pk'=>false, 'fk'=>false, 'id'=>'cta_registro', 'description'=>'Registro', 'title'=>'', 'type'=>'registro', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>false, 'insert'=>true, 'line'=>0);
    $this->cta_items['cta_responsavel'] = array('pk'=>false, 'fk'=>false, 'id'=>'cta_responsavel', 'description'=>'Respons�vel', 'title'=>'', 'type'=>'responsavel', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>true, 'insert'=>true, 'line'=>0);
    $this->cta_items['cta_criador'] = array('pk'=>false, 'fk'=>false, 'id'=>'cta_criador', 'description'=>'Criador', 'title'=>'', 'type'=>'criador', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>false, 'insert'=>true, 'line'=>0);

    $lines = 0;
    foreach($this->cta_items as $item){
      $lines = ($item['line'] > $lines) ? $item['line'] : $lines;
    }
     
    $j = array();
    foreach($this->cta_items as $item){
      if($item['fk']){
        if(isset($item['foreign']) or isset($item['parent'])){
          $table = "";
					$key = "";
					if(isset($item['foreign'])){
						$table = $item['foreign']['table'];
						$key = $item['foreign']['key'];
					}else if(isset($item['parent'])){
						$table = $item['parent']['table'];
						$key = $item['parent']['key'];
					}
          $j[$table] = " LEFT JOIN ".$table." ON (".$item['id']." = ".$key.") ";
        }
      }
    }
    $join = " ".join(' ', $j);
    
    $this->cta_properties = array(
      'module'=>'site',
      'entity'=>'ConteudoArtigo',
      'table'=>'TBL_CONTEUDO_ARTIGO',
			'join'=>$join,
      'tag'=>'conteudoartigo',
      'prefix'=>'cta',
      'order'=>'cta_codigo',
      'group'=>'',
      'where'=>'',
      'search'=>'',
      'layout'=>'children',
			'checkbox'=>false,
      'saveonly'=>false,//desabilita a edi��o de entidade
      'readonly'=>false,//desabilita a cria��o de novos registros
      'remove'=>'',//false or
			 /**
        * array(
        *  "field"=>"prefix_id",
        *  "value"=>"1",
        *  "className"=>"icon-remove",
        *  "title"=>"Excluir",
        *  "message"=>"Deseja realmente remover este registro?",
        *  "success"=>"Registro removido com sucesso"
        * )
        */
			'database'=>null,
      'reference'=>'cta_codigo',
      'description'=>'cta_titulo',
      'lines'=>$lines
    );
    
    if (!$this->cta_properties['reference']) {
      foreach ($this->cta_items as $id=>$array) {
        if ($array['pk'] == 1) {
          $this->cta_properties['reference'] = $id;
          break;
        }
      }
    }
    if (!$this->cta_properties['description']) {
      foreach ($this->cta_items as $id=>$array) {
        if ($array['type'] == "string") {
          $this->cta_properties['reference'] = $id;
          break;
        }
      }
    }
  }

  /**
   * Recupera as principais propriedades da entidade
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 25/05/2013 01:14:40
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/05/2013 01:14:40
   */
  public function get_cta_properties(){
    ?><?php
    return $this->cta_properties;
  }

  /**
   * Recupera todos os atributos da entidade
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 25/05/2013 01:14:40
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/05/2013 01:14:40
   */
  public function get_cta_items(){
    ?><?php
    return $this->cta_items;
  }

  /**
   * Recupera um atributo da entidade com todas as suas propriedades
   * 
   * @param string $key
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 25/05/2013 01:14:40
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/05/2013 01:14:40
   */
  public function get_cta_item($key){
    ?><?php
    return $this->cta_items[$key];
  }

  /**
   * 
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 25/05/2013 01:14:40
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/05/2013 01:14:40
   */
  public function get_cta_reference(){
    ?><?php
    $key = $this->cta_properties['reference'];
    return $key;
  }

  /**
   * Recupera o valor de um objeto
   * 
   * @param string $key
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 25/05/2013 01:14:41
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/05/2013 01:14:41
   */
  public function get_cta_value($key){
    ?><?php
    return $this->cta_items[$key]['value'];
  }

  /**
   * Altera o valor de um atributo da inst�ncia da entidade
   * 
   * @param string $key
   * @param object $value
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 25/05/2013 01:14:41
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/05/2013 01:14:41
   */
  public function set_cta_value($key, $value){
    ?><?php
    $this->cta_items[$key]['value'] = $value;
  }

  /**
   * Altera o tipo de um atributo da inst�ncia da entidade
   * 
   * @param string $key
   * @param string $type
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 25/05/2013 01:14:41
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/05/2013 01:14:41
   */
  public function set_cta_type($key, $type){
    ?><?php
    $this->cta_items[$key]['type'] = $type;
  }


}