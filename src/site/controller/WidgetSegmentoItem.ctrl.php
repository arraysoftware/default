<?php
/**
 * @copyright array software
 *
 * @author WILLIAM MARQUES VICENTE GOMES CORREA - 24/05/2013 16:55:59
 * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/05/2013 00:25:57
 * @category controller
 * @package site
 *
 * Responsavel.....: ADMINISTRADOR
 * Alteracao.......: 11/06/2013 22:14:36
 */


class WidgetSegmentoItemCtrl
{
  private  $path;
  private  $database;

  /**
   * Construtor da classe de processamento e interface de acesso a dados da entidade
   * 
   * @param string $path
   * @param string $database
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 24/05/2013 16:56:00
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 24/05/2013 16:56:00
   */
  public function WidgetSegmentoItemCtrl($path, $database = ""){
    ?><?php
    $this->path = $path;
    $this->database = $database;

    return $this;
  }

  /**
   * Recupera inst�ncias da entidade na base de dados
   * 
   * @param string $where
   * @param string $group
   * @param string $order
   * @param string $start
   * @param string $end
   * @param boolean $validate
   * @param boolean $debug
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 24/05/2013 16:56:00
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 24/05/2013 16:56:00
   */
  public function getWidgetSegmentoItemCtrl($where, $group, $order, $start = "", $end = "", $validate = true, $debug = false){
    ?><?php
    System::desire("d", 'site', 'WidgetSegmentoItem', 'src', true, '{project.rsc}');
    
    $widgetSegmentoItemDAO = new WidgetSegmentoItemDAO($this->path, $this->database);
    $widgetSegmentoItems = $widgetSegmentoItemDAO->getWidgetSegmentoItemDAO($where, $group, $order, $start, $end, $validate, $debug);
    
    return $widgetSegmentoItems;
  }

  /**
   * Recupera um valor inteiro referente ao n�mero de linhas de determina consulta
   * 
   * @param string $where
   * @param string $group
   * @param boolean $validate
   * @param boolean $debug
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 24/05/2013 16:56:00
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 24/05/2013 16:56:00
   */
  public function getCountWidgetSegmentoItemCtrl($where, $group = "", $validate = true, $debug = false){
    ?><?php
    System::desire("d", 'site', 'WidgetSegmentoItem', 'src', true, '{project.rsc}');
    
    $widgetSegmentoItemDAO = new WidgetSegmentoItemDAO($this->path, $this->database);
    $total = $widgetSegmentoItemDAO->getCountWidgetSegmentoItemDAO($where, $group, $validate, $debug);
    
    return $total;
  }

  /**
   * Adiciona um novo registro desta entidade na base de dados
   * 
   * @param object $object
   * @param boolean $validate
   * @param boolean $debug
   * @param boolean $presave
   * @param int $copy
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 24/05/2013 16:56:01
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 24/05/2013 16:56:01
   */
  public function addWidgetSegmentoItemCtrl($object, $validate = true, $debug = false, $presave = false, $copy = 0){
    ?><?php
    System::desire("d", 'site', 'WidgetSegmentoItem', 'src', true, '{project.rsc}');    
    
    $widgetSegmentoItemDAO = new WidgetSegmentoItemDAO($this->path, $this->database);
    $this->beforeWidgetSegmentoItemCtrl($object, "add");
    $add = $widgetSegmentoItemDAO->addWidgetSegmentoItemDAO($object, $validate, $debug, $presave, $copy);
    $this->afterWidgetSegmentoItemCtrl($object, "add", $add, $copy);
    
    return $add;
  }

  /**
   * Remove uma instancia da entidade da base de dados
   * 
   * @param object $object
   * @param boolean $validate
   * @param boolean $debug
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 24/05/2013 16:56:01
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 24/05/2013 16:56:01
   */
  public function removeWidgetSegmentoItemCtrl($object, $validate = true, $debug = false){
    ?><?php
    System::desire("d", 'site', 'WidgetSegmentoItem', 'src', true, '{project.rsc}');
    
    $widgetSegmentoItemDAO = new WidgetSegmentoItemDAO($this->path, $this->database);
    $this->beforeWidgetSegmentoItemCtrl($object, "remove");
    $remove = $widgetSegmentoItemDAO->removeWidgetSegmentoItemDAO($object, $validate, $debug);
    $this->afterWidgetSegmentoItemCtrl($object, "remove");
    
    return $remove;
  }

  /**
   * Atualiza uma inst�ncia da entidade na base de dados
   * 
   * @param object $object
   * @param boolean $validate
   * @param boolean $debug
   * @param boolean $trigger
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 24/05/2013 16:56:01
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 24/05/2013 16:56:01
   */
  public function setWidgetSegmentoItemCtrl($object, $validate = true, $debug = false, $trigger = true){
    ?><?php
    System::desire("d", 'site', 'WidgetSegmentoItem', 'src', true, '{project.rsc}');
    
    $widgetSegmentoItemDAO = new WidgetSegmentoItemDAO($this->path, $this->database);
    $this->beforeWidgetSegmentoItemCtrl($object, "set");
    $set = $widgetSegmentoItemDAO->setWidgetSegmentoItemDAO($object, $validate, $debug, $trigger);
    if($trigger === true){
			$this->afterWidgetSegmentoItemCtrl($object, "set");
		}
    
    return $set;
  }

  /**
   * M�todo de gatilho executado antes de cada atualiza��o e remo��o das inst�ncias da entidade
   * 
   * @param object $object
   * @param string $param
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 24/05/2013 16:56:01
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 24/05/2013 16:56:01
   */
  public function beforeWidgetSegmentoItemCtrl($object, $param){
    ?><?php

    if($param === 'add' or $param === 'set'){

      $items = $object->get_wsi_items();
      
      foreach($items as $item){
        if($item['type_behavior'] == 'parent'){
          if(isset($item['parent'])){
            $class = $item['parent']['entity'];
            System::desire('c', $item['parent']['modulo'], $class, 'src', true);
            $classCtrl = $class."Ctrl";
            $obj = new $classCtrl(PATH_APP);
            $entity = $item['value'];
            if($param === 'add'){
              $action = "add".$class."Ctrl";
            }else{
              $action = "set".$class."Ctrl";
            }
            $value = $obj->$action($entity);
            if($param === 'add'){
              $object->set_wsi_value($item['id'], $value);
            }else{
              $get_value = "get_".$item['parent']['prefix']."_value";
              $object->set_wsi_value($item['id'], $entity->$get_value($item['parent']['key']));
            }
          }
        }
      }


    }

    return $object;
  }

  /**
   * M�todo de gatilho executado depois de cada atualiza��o e remo��o das inst�ncias da entidade
   * 
   * @param object $object
   * @param string $param
   * @param int $value
   * @param int $copy
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 24/05/2013 16:56:01
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 24/05/2013 16:56:01
   */
  public function afterWidgetSegmentoItemCtrl($object, $param, $value = 0, $copy = 0){
    ?><?php
    
    $add = false;
    
    if($param === 'set'){
      $value = $object->get_wsi_value($object->get_wsi_reference());
    }

    if($param === 'add'){
      $object->set_wsi_value($object->get_wsi_reference(), $value);
    }

    if($param === 'set' or $param === 'add'){
      $items = $object->get_wsi_items();

      $try = 0;
      $added = 0;
      
      foreach ($items as $item) {
        if($item['type'] === 'select-multi'){
          
          if(isset($item['select-multi'])){
            $select = $item['select-multi'];

            $module = $select['module'];
            $entity = $select['entity'];
            $prefix = $select['prefix'];

            System::desire('c', $module, $entity, 'src');
            System::desire('m', $module, $entity, 'src');

            $remove = "execute".$entity."Ctrl";
            $set_value = 'set_'.$prefix.'_value';
            $add = "add".$entity."Ctrl";

            $entityCtrl = $entity.'Ctrl';
            $objectCtrl = new $entityCtrl(PATH_APP);

            $objectCtrl->$remove($select['foreign']."='".$value."'","");

            $values = $item['value'];
            foreach ($values as $v) {
							if($v){
								$object = new $entity();
								$object->$set_value($select['foreign'],$value);
								$object->$set_value($select['source'],$v);
								$object->$set_value($prefix.'_responsavel',System::getUser());
								$object->$set_value($prefix.'_criador',System::getUser());
	
								$w = $objectCtrl->$add($object);
								$try++;
								if($w){
									$added++;
								}
							}
            }
          }
        }
      }
      
      $add = $try == $added;
      
      if($copy > 0){
        // input here the code to create the complete copy of instance
      }
      
    }

    return $add;
  }

  /**
   * Recupera um dado atrav�s de uma consulta personalizada
   * 
   * @param string $column
   * @param string $where
   * @param string $table
   * @param boolean $validate
   * @param boolean $debug
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 24/05/2013 16:56:01
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 24/05/2013 16:56:01
   */
  public function getColumnWidgetSegmentoItemCtrl($column, $where, $table = "", $validate = true, $debug = false){
    ?><?php
    System::desire("d", 'site', 'WidgetSegmentoItem', 'src', true, '{project.rsc}');
    
    $widgetSegmentoItemDAO = new WidgetSegmentoItemDAO($this->path, $this->database);
    $column = $widgetSegmentoItemDAO->getColumnWidgetSegmentoItemDAO($column, $where, $table, $validate, $debug);
    
    return $column;
  }

  /**
   * Remove um grupo de items ou atualiza uma quantidade relativamente grande 
   * 
   * @param string $where
   * @param string $update
   * @param boolean $validate
   * @param boolean $debug
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 24/05/2013 16:56:01
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 24/05/2013 16:56:01
   */
  public function executeWidgetSegmentoItemCtrl($where, $update, $validate = true, $debug = false){
  ?><?php
    System::desire('d', 'site', 'WidgetSegmentoItem', 'src', true, '{project.rsc}');

    $widgetSegmentoItemDAO = new WidgetSegmentoItemDAO($this->path, $this->database);
    $executed = $widgetSegmentoItemDAO->executeWidgetSegmentoItemDAO($where, $update, $validate, $debug);

    return $executed;
  }


}