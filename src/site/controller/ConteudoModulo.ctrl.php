<?php
/**
 * @copyright array software
 *
 * @author  - 23/03/2013 22:52:32
 * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/05/2013 22:24:38
 * @category controller
 * @package site
 *
 * Responsavel.....: ADMINISTRADOR
 * Alteracao.......: 11/06/2013 22:14:36
 */


class ConteudoModuloCtrl
{
  private  $path;
  private  $database;

  /**
   * Construtor da classe de processamento e interface de acesso a dados da entidade
   * 
   * @param string $path
   * @param string $database
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 23/05/2013 21:09:58
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 23/05/2013 21:09:58
   */
  public function ConteudoModuloCtrl($path, $database = ""){
    ?><?php
    $this->path = $path;
    $this->database = $database;

    return $this;
  }

  /**
   * Recupera inst�ncias da entidade na base de dados
   * 
   * @param string $where
   * @param string $group
   * @param string $order
   * @param string $start
   * @param string $end
   * @param boolean $validate
   * @param boolean $debug
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 23/05/2013 21:09:58
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 23/05/2013 21:09:58
   */
  public function getConteudoModuloCtrl($where, $group, $order, $start = "", $end = "", $validate = true, $debug = false){
    ?><?php
    System::desire("d", 'site', 'ConteudoModulo', 'src', true, '{project.rsc}');
    
    $conteudoModuloDAO = new ConteudoModuloDAO($this->path, $this->database);
    $conteudoModulos = $conteudoModuloDAO->getConteudoModuloDAO($where, $group, $order, $start, $end, $validate, $debug);
    
    return $conteudoModulos;
  }

  /**
   * Recupera um valor inteiro referente ao n�mero de linhas de determina consulta
   * 
   * @param string $where
   * @param string $group
   * @param boolean $validate
   * @param boolean $debug
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 23/05/2013 21:09:58
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 23/05/2013 21:09:58
   */
  public function getCountConteudoModuloCtrl($where, $group = "", $validate = true, $debug = false){
    ?><?php
    System::desire("d", 'site', 'ConteudoModulo', 'src', true, '{project.rsc}');
    
    $conteudoModuloDAO = new ConteudoModuloDAO($this->path, $this->database);
    $total = $conteudoModuloDAO->getCountConteudoModuloDAO($where, $group, $validate, $debug);
    
    return $total;
  }

  /**
   * Adiciona um novo registro desta entidade na base de dados
   * 
   * @param object $object
   * @param boolean $validate
   * @param boolean $debug
   * @param boolean $presave
   * @param int $copy
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 23/05/2013 21:09:59
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 23/05/2013 21:09:59
   */
  public function addConteudoModuloCtrl($object, $validate = true, $debug = false, $presave = false, $copy = 0){
    ?><?php
    System::desire("d", 'site', 'ConteudoModulo', 'src', true, '{project.rsc}');    
    
    $conteudoModuloDAO = new ConteudoModuloDAO($this->path, $this->database);
    $this->beforeConteudoModuloCtrl($object, "add");
    $add = $conteudoModuloDAO->addConteudoModuloDAO($object, $validate, $debug, $presave, $copy);
    $this->afterConteudoModuloCtrl($object, "add", $add, $copy);
    
    return $add;
  }

  /**
   * Remove uma instancia da entidade da base de dados
   * 
   * @param object $object
   * @param boolean $validate
   * @param boolean $debug
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 23/05/2013 21:09:59
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 23/05/2013 21:09:59
   */
  public function removeConteudoModuloCtrl($object, $validate = true, $debug = false){
    ?><?php
    System::desire("d", 'site', 'ConteudoModulo', 'src', true, '{project.rsc}');
    
    $conteudoModuloDAO = new ConteudoModuloDAO($this->path, $this->database);
    $this->beforeConteudoModuloCtrl($object, "remove");
    $remove = $conteudoModuloDAO->removeConteudoModuloDAO($object, $validate, $debug);
    $this->afterConteudoModuloCtrl($object, "remove");
    
    return $remove;
  }

  /**
   * Atualiza uma inst�ncia da entidade na base de dados
   * 
   * @param object $object
   * @param boolean $validate
   * @param boolean $debug
   * @param boolean $trigger
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 23/05/2013 21:09:59
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 23/05/2013 21:09:59
   */
  public function setConteudoModuloCtrl($object, $validate = true, $debug = false, $trigger = true){
    ?><?php
    System::desire("d", 'site', 'ConteudoModulo', 'src', true, '{project.rsc}');
    
    $conteudoModuloDAO = new ConteudoModuloDAO($this->path, $this->database);
    $this->beforeConteudoModuloCtrl($object, "set");
    $set = $conteudoModuloDAO->setConteudoModuloDAO($object, $validate, $debug, $trigger);
    if($trigger === true){
			$this->afterConteudoModuloCtrl($object, "set");
		}
    
    return $set;
  }

  /**
   * M�todo de gatilho executado antes de cada atualiza��o e remo��o das inst�ncias da entidade
   * 
   * @param object $object
   * @param string $param
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 23/05/2013 21:09:59
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 23/05/2013 21:09:59
   */
  public function beforeConteudoModuloCtrl($object, $param){
    ?><?php

    if($param === 'add' or $param === 'set'){

      $items = $object->get_ctm_items();
      
      foreach($items as $item){
        if($item['type_behavior'] == 'parent'){
          if(isset($item['parent'])){
            $class = $item['parent']['entity'];
            System::desire('c', $item['parent']['modulo'], $class, 'src', true);
            $classCtrl = $class."Ctrl";
            $obj = new $classCtrl(PATH_APP);
            $entity = $item['value'];
            if($param === 'add'){
              $action = "add".$class."Ctrl";
            }else{
              $action = "set".$class."Ctrl";
            }
            $value = $obj->$action($entity);
            if($param === 'add'){
              $object->set_ctm_value($item['id'], $value);
            }else{
              $get_value = "get_".$item['parent']['prefix']."_value";
              $object->set_ctm_value($item['id'], $entity->$get_value($item['parent']['key']));
            }
          }
        }
      }


    }

    return $object;
  }

  /**
   * M�todo de gatilho executado depois de cada atualiza��o e remo��o das inst�ncias da entidade
   * 
   * @param object $object
   * @param string $param
   * @param int $value
   * @param int $copy
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 23/05/2013 21:09:59
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 23/05/2013 21:09:59
   */
  public function afterConteudoModuloCtrl($object, $param, $value = 0, $copy = 0){
    ?><?php
    
    $add = false;
    
    if($param === 'set'){
      $value = $object->get_ctm_value($object->get_ctm_reference());
    }

    if($param === 'add'){
      $object->set_ctm_value($object->get_ctm_reference(), $value);
    }

    if($param === 'set' or $param === 'add'){
      $items = $object->get_ctm_items();

      $try = 0;
      $added = 0;
      
      foreach ($items as $item) {
        if($item['type'] === 'select-multi'){
          
          if(isset($item['select-multi'])){
            $select = $item['select-multi'];

            $module = $select['module'];
            $entity = $select['entity'];
            $prefix = $select['prefix'];

            System::desire('c', $module, $entity, 'src');
            System::desire('m', $module, $entity, 'src');

            $remove = "execute".$entity."Ctrl";
            $set_value = 'set_'.$prefix.'_value';
            $add = "add".$entity."Ctrl";

            $entityCtrl = $entity.'Ctrl';
            $objectCtrl = new $entityCtrl(PATH_APP);

            $objectCtrl->$remove($select['foreign']."='".$value."'","");

            $values = $item['value'];
            foreach ($values as $v) {
							if($v){
								$object = new $entity();
								$object->$set_value($select['foreign'],$value);
								$object->$set_value($select['source'],$v);
								$object->$set_value($prefix.'_responsavel',System::getUser());
								$object->$set_value($prefix.'_criador',System::getUser());
	
								$w = $objectCtrl->$add($object);
								$try++;
								if($w){
									$added++;
								}
							}
            }
          }
        }
      }
      
      $add = $try == $added;
      
      if($copy > 0){
        // input here the code to create the complete copy of instance
      }
      
    }

    return $add;
  }

  /**
   * Recupera um dado atrav�s de uma consulta personalizada
   * 
   * @param string $column
   * @param string $where
   * @param string $table
   * @param boolean $validate
   * @param boolean $debug
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 23/05/2013 21:09:59
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 23/05/2013 21:09:59
   */
  public function getColumnConteudoModuloCtrl($column, $where, $table = "", $validate = true, $debug = false){
    ?><?php
    System::desire("d", 'site', 'ConteudoModulo', 'src', true, '{project.rsc}');
    
    $conteudoModuloDAO = new ConteudoModuloDAO($this->path, $this->database);
    $column = $conteudoModuloDAO->getColumnConteudoModuloDAO($column, $where, $table, $validate, $debug);
    
    return $column;
  }

  /**
   * Remove um grupo de items ou atualiza uma quantidade relativamente grande 
   * 
   * @param string $where
   * @param string $update
   * @param boolean $validate
   * @param boolean $debug
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 23/05/2013 21:09:59
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 23/05/2013 21:09:59
   */
  public function executeConteudoModuloCtrl($where, $update, $validate = true, $debug = false){
  ?><?php
    System::desire('d', 'site', 'ConteudoModulo', 'src', true, '{project.rsc}');

    $conteudoModuloDAO = new ConteudoModuloDAO($this->path, $this->database);
    $executed = $conteudoModuloDAO->executeConteudoModuloDAO($where, $update, $validate, $debug);

    return $executed;
  }


}