<?php
/**
 * @copyright array software
 *
 * @author WILLIAM MARQUES VICENTE GOMES CORREA - 23/03/2013 22:31:37
 * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/05/2013 17:13:53
 * @category controller
 * @package site
 *
 * Responsavel.....: ADMINISTRADOR
 * Alteracao.......: 11/06/2013 22:14:36
 */


class WidgetCtrl
{
  private  $path;
  private  $database;

  /**
   * Construtor da classe de processamento e interface de acesso a dados da entidade
   * 
   * @param string $path
   * @param string $database
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 24/05/2013 11:35:38
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 24/05/2013 11:35:38
   */
  public function WidgetCtrl($path, $database = ""){
    ?><?php
    $this->path = $path;
    $this->database = $database;

    return $this;
  }

  /**
   * Recupera inst�ncias da entidade na base de dados
   * 
   * @param string $where
   * @param string $group
   * @param string $order
   * @param string $start
   * @param string $end
   * @param boolean $validate
   * @param boolean $debug
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 24/05/2013 11:35:38
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 24/05/2013 11:35:38
   */
  public function getWidgetCtrl($where, $group, $order, $start = "", $end = "", $validate = true, $debug = false){
    ?><?php
    System::desire("d", 'site', 'Widget', 'src', true, '{project.rsc}');
    
    $widgetDAO = new WidgetDAO($this->path, $this->database);
    $widgets = $widgetDAO->getWidgetDAO($where, $group, $order, $start, $end, $validate, $debug);
    
    return $widgets;
  }

  /**
   * Recupera um valor inteiro referente ao n�mero de linhas de determina consulta
   * 
   * @param string $where
   * @param string $group
   * @param boolean $validate
   * @param boolean $debug
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 24/05/2013 11:35:38
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 24/05/2013 11:35:38
   */
  public function getCountWidgetCtrl($where, $group = "", $validate = true, $debug = false){
    ?><?php
    System::desire("d", 'site', 'Widget', 'src', true, '{project.rsc}');
    
    $widgetDAO = new WidgetDAO($this->path, $this->database);
    $total = $widgetDAO->getCountWidgetDAO($where, $group, $validate, $debug);
    
    return $total;
  }

  /**
   * Adiciona um novo registro desta entidade na base de dados
   * 
   * @param object $object
   * @param boolean $validate
   * @param boolean $debug
   * @param boolean $presave
   * @param int $copy
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 24/05/2013 11:35:38
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 24/05/2013 11:35:38
   */
  public function addWidgetCtrl($object, $validate = true, $debug = false, $presave = false, $copy = 0){
    ?><?php
    System::desire("d", 'site', 'Widget', 'src', true, '{project.rsc}');    
    
    $widgetDAO = new WidgetDAO($this->path, $this->database);
    $this->beforeWidgetCtrl($object, "add");
    $add = $widgetDAO->addWidgetDAO($object, $validate, $debug, $presave, $copy);
    $this->afterWidgetCtrl($object, "add", $add, $copy);
    
    return $add;
  }

  /**
   * Remove uma instancia da entidade da base de dados
   * 
   * @param object $object
   * @param boolean $validate
   * @param boolean $debug
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 24/05/2013 11:35:39
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 24/05/2013 11:35:39
   */
  public function removeWidgetCtrl($object, $validate = true, $debug = false){
    ?><?php
    System::desire("d", 'site', 'Widget', 'src', true, '{project.rsc}');
    
    $widgetDAO = new WidgetDAO($this->path, $this->database);
    $this->beforeWidgetCtrl($object, "remove");
    $remove = $widgetDAO->removeWidgetDAO($object, $validate, $debug);
    $this->afterWidgetCtrl($object, "remove");
    
    return $remove;
  }

  /**
   * Atualiza uma inst�ncia da entidade na base de dados
   * 
   * @param object $object
   * @param boolean $validate
   * @param boolean $debug
   * @param boolean $trigger
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 24/05/2013 11:35:39
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 24/05/2013 11:35:39
   */
  public function setWidgetCtrl($object, $validate = true, $debug = false, $trigger = true){
    ?><?php
    System::desire("d", 'site', 'Widget', 'src', true, '{project.rsc}');
    
    $widgetDAO = new WidgetDAO($this->path, $this->database);
    $this->beforeWidgetCtrl($object, "set");
    $set = $widgetDAO->setWidgetDAO($object, $validate, $debug, $trigger);
    if($trigger === true){
			$this->afterWidgetCtrl($object, "set");
		}
    
    return $set;
  }

  /**
   * M�todo de gatilho executado antes de cada atualiza��o e remo��o das inst�ncias da entidade
   * 
   * @param object $object
   * @param string $param
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 24/05/2013 11:35:39
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 24/05/2013 19:16:21
   */
  public function beforeWidgetCtrl($object, $param){
    ?><?php

		$filename = PATH_APP."files/widgets/".$object->get_wid_value('wid_id').".wid.php";

    if(file_exists($filename)){
      unlink($filename);
    }

    if($param === 'add' or $param === 'set'){

      $items = $object->get_wid_items();
      
      foreach($items as $item){
        if($item['type_behavior'] == 'parent'){
          if(isset($item['parent'])){
            $class = $item['parent']['entity'];
            System::desire('c', $item['parent']['modulo'], $class, 'src', true);
            $classCtrl = $class."Ctrl";
            $obj = new $classCtrl(PATH_APP);
            $entity = $item['value'];
            if($param === 'add'){
              $action = "add".$class."Ctrl";
            }else{
              $action = "set".$class."Ctrl";
            }
            $value = $obj->$action($entity);
            if($param === 'add'){
              $object->set_wid_value($item['id'], $value);
            }else{
              $get_value = "get_".$item['parent']['prefix']."_value";
              $object->set_wid_value($item['id'], $entity->$get_value($item['parent']['key']));
            }
          }
        }
      }


    }

    return $object;
  }

  /**
   * M�todo de gatilho executado depois de cada atualiza��o e remo��o das inst�ncias da entidade
   * 
   * @param object $object
   * @param string $param
   * @param int $value
   * @param int $copy
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 24/05/2013 11:35:39
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 24/05/2013 11:35:39
   */
  public function afterWidgetCtrl($object, $param, $value = 0, $copy = 0){
    ?><?php
    
    $add = false;
    
    if($param === 'set'){
      $value = $object->get_wid_value($object->get_wid_reference());
    }

    if($param === 'add'){
      $object->set_wid_value($object->get_wid_reference(), $value);
    }

    if($param === 'set' or $param === 'add'){
      $items = $object->get_wid_items();

      $try = 0;
      $added = 0;
      
      foreach ($items as $item) {
        if($item['type'] === 'select-multi'){
          
          if(isset($item['select-multi'])){
            $select = $item['select-multi'];

            $module = $select['module'];
            $entity = $select['entity'];
            $prefix = $select['prefix'];

            System::desire('c', $module, $entity, 'src');
            System::desire('m', $module, $entity, 'src');

            $remove = "execute".$entity."Ctrl";
            $set_value = 'set_'.$prefix.'_value';
            $add = "add".$entity."Ctrl";

            $entityCtrl = $entity.'Ctrl';
            $objectCtrl = new $entityCtrl(PATH_APP);

            $objectCtrl->$remove($select['foreign']."='".$value."'","");

            $values = $item['value'];
            foreach ($values as $v) {
							if($v){
								$object = new $entity();
								$object->$set_value($select['foreign'],$value);
								$object->$set_value($select['source'],$v);
								$object->$set_value($prefix.'_responsavel',System::getUser());
								$object->$set_value($prefix.'_criador',System::getUser());
	
								$w = $objectCtrl->$add($object);
								$try++;
								if($w){
									$added++;
								}
							}
            }
          }
        }
      }
      
      $add = $try == $added;
      
      if($copy > 0){
        // input here the code to create the complete copy of instance
      }
      
    }

    return $add;
  }

  /**
   * Recupera um dado atrav�s de uma consulta personalizada
   * 
   * @param string $column
   * @param string $where
   * @param string $table
   * @param boolean $validate
   * @param boolean $debug
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 24/05/2013 11:35:39
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 24/05/2013 11:35:39
   */
  public function getColumnWidgetCtrl($column, $where, $table = "", $validate = true, $debug = false){
    ?><?php
    System::desire("d", 'site', 'Widget', 'src', true, '{project.rsc}');
    
    $widgetDAO = new WidgetDAO($this->path, $this->database);
    $column = $widgetDAO->getColumnWidgetDAO($column, $where, $table, $validate, $debug);
    
    return $column;
  }

  /**
   * Remove um grupo de items ou atualiza uma quantidade relativamente grande 
   * 
   * @param string $where
   * @param string $update
   * @param boolean $validate
   * @param boolean $debug
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 24/05/2013 11:35:39
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 24/05/2013 11:35:39
   */
  public function executeWidgetCtrl($where, $update, $validate = true, $debug = false){
  ?><?php
    System::desire('d', 'site', 'Widget', 'src', true, '{project.rsc}');

    $widgetDAO = new WidgetDAO($this->path, $this->database);
    $executed = $widgetDAO->executeWidgetDAO($where, $update, $validate, $debug);

    return $executed;
  }

  /**
   * Realiza a impress�o do Widget
   * 
   * @param object $widget
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 24/05/2013 19:17:09
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/05/2013 17:13:52
   */
  public function printWidgetCtrl($widget){
  ?><?php
    $filename = PATH_APP."files/widgets/".$widget->get_wid_value('wid_id').".wid.php";

    if(!file_exists($filename)){
      File::saveFile($filename, $widget->get_wid_value('wid_conteudo'), 'w', false);
    }

    include $filename;
  }


}