<?php
/**
 * @copyright array software
 *
 * @author WILLIAM MARQUES VICENTE GOMES CORREA - 23/03/2013 22:31:37
 * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/05/2013 17:13:53
 * @category view
 * @package site
 *
 * Responsavel.....: ADMINISTRADOR
 * Alteracao.......: 11/06/2013 22:14:38
 */


if (System::request('action')) {
  $action = System::request('action');


 if ($action == 'widget-v-edit') {
    $param = System::request('param', "edit");
    $target = System::request('t');
    $level = System::request('l');
    $where = System::request('w');
    $group = System::request('g');
    $order = System::request('o');
    $filter = System::request('f');
    $rotule = System::request('rotule');
    $w_description = System::request('wd');
    $g_description = System::request('gd');
    $o_description = System::request('od');
    $start = System::request('start', "0");

    viewManagerWidget($param, $target, $level, $where, $group, $order, $filter, $rotule, $w_description, $g_description, $o_description, $start);
  } else if ($action == 'widget-v-view') {
    $param = System::request('param', "view");
    $target = System::request('t');
    $level = System::request('l');
    $where = System::request('w');
    $group = System::request('g');
    $order = System::request('o');
    $filter = System::request('f');
    $rotule = System::request('rotule');
    $w_description = System::request('wd');
    $g_description = System::request('gd');
    $o_description = System::request('od');
    $start = System::request('start', "0");

    viewManagerWidget($param, $target, $level, $where, $group, $order, $filter, $rotule, $w_description, $g_description, $o_description, $start);
  } else if ($action == 'widget-v-add') {
    $param = System::request('param', "add");
    $target = System::request('t');
    $level = System::request('l');
    $where = System::request('w');
    $group = System::request('g');
    $order = System::request('o');
    $filter = System::request('f');
    $rotule = System::request('rotule');
    $w_description = System::request('wd');
    $g_description = System::request('gd');
    $o_description = System::request('od');
    $start = System::request('start', "0");

    viewManagerWidget($param, $target, $level, $where, $group, $order, $filter, $rotule, $w_description, $g_description, $o_description, $start);
  } else if ($action == 'widget-v-search') {
    $param = System::request('param', "search");
    $target = System::request('t');
    $level = System::request('l');
    $where = System::request('w');
    $group = System::request('g');
    $order = System::request('o');
    $filter = System::request('f');
    $rotule = System::request('rotule');
    $w_description = System::request('wd');
    $g_description = System::request('gd');
    $o_description = System::request('od');
    $start = System::request('start', "0");

    viewManagerWidget($param, $target, $level, $where, $group, $order, $filter, $rotule, $w_description, $g_description, $o_description, $start);
  } else if ($action == 'widget-v-list') {
    $target = System::request('t');
    $level = System::request('l');
    $where = System::request('w');
    $group = System::request('g');
    $order = System::request('o');
    $filter = System::request('f');
    $rotule = System::request('rotule');
    $w_description = System::request('wd');
    $g_description = System::request('gd');
    $o_description = System::request('od');
    $start = System::request('start', "0");

    viewListWidget($target, $level, $where, $group, $order, $filter, $rotule, $w_description, $g_description, $o_description, $start);
  } else if ($action == 'widget-v-select') {
    $id = System::request('id');
    $level = System::request('l');
    $where = System::request('w');
    $group = System::request('g');
    $order = System::request('o');
    $name = System::request('name');
    $selected = System::request('selected');
    $onchange = System::request('onchange');
    $encode = System::request('encode');
    $width = System::request('width');
    $filter = System::request('f');

    viewSelectWidget($id, $level, $where, $group, $order, $name, $selected, $onchange, $encode, $width, $filter);
  }
}

  /**
   * Exibe um formul�rio para administrar a Entidade
   * 
   * @param string $param
   * @param string $target
   * @param string $level
   * @param string $where
   * @param string $group
   * @param string $order
   * @param string $filter
   * @param string $rotule
   * @param string $w_description
   * @param string $g_description
   * @param string $o_description
   * @param string $start
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 24/05/2013 11:35:39
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 24/05/2013 11:35:39
   */
 function viewManagerWidget($param, $target, $level, $where, $group, $order, $filter, $rotule, $w_description, $g_description, $o_description, $start){
  ?><?php
    $acesso_controle = -1;

    require System::desire('file', '', 'header', 'core', false);
    require System::desire('class', 'resource', 'Screen', 'core', false);

    System::desire('m', 'site', 'Widget', 'src', true, '{project.rsc}');
    System::desire('c', 'site', 'Widget', 'src', true, '{project.rsc}');

    $screen = new Screen('{project.rsc}');

    $widget = new Widget();

    $items = $widget->get_wid_items();
    $primary = array();
    $foreign = array();
    $hidden = array();
    $validate = array();
    $children = array();
    $popups = array();
		$parent = null;
    $load = "";

    $properties = $widget->get_wid_properties();
    $module = $properties["module"];
    $entity = $properties["entity"];
    $tag = $properties["tag"];
    $lines = $properties["lines"];
    $reference = $properties['reference'];
    $description = $properties['description'];
    $layout = isset($properties['layout']) ? $param !== "search" ? $properties['layout'] : "" : "";
    if(System::request('layout')){
      $layout = System::request('layout');
    }
    $form = System::request('form');
    $modal = Boolean::parse(System::request('modal'));
    $back = System::request('back',true);
    $execute = System::request('execute','');
    $core = isset($properties["core"]) ? $properties["core"] : false;
    $database = isset($properties["database"]) ? $properties["database"] : DEFAULT_DATABASE;

    $width_label = 0;

    $width = System::request('width', 0);
    $window_width = WINDOW_WIDTH;
    if($width){
      $window_width = $width;
    }

    $window_height = WINDOW_HEIGHT;
    $height = System::request('height', 0);
    if($height){
      $window_height = $height;
    }

    $extra_child = System::request('child');

    $registro = date('d/m/Y H:i:s');
    $criador = System::getUser();
    $alteracao = date('d/m/Y H:i:s');
    $responsavel = System::getUser();

    $widgetCtrl = new WidgetCtrl(PATH_APP, $database);

    /* PARENT */
    foreach($items as $item){
      if($item['type_behavior'] == 'parent'){
        if(isset($item['parent'])){
          $p = $item['parent'];
          $class = $p['entity'];
          System::desire('m', $p['modulo'], $class, 'src', true);
          $obj = new $class();
          $get_properties = "get_".$p['prefix']."_properties";
          $get_items = "get_".$p['prefix']."_items";

          $pos = $item['type_content'] ? $item['type_content'] : "bottom";
          $prop = $obj->$get_properties();
          $itens = $obj->$get_items();

          $parent = array("object"=>$obj,"module"=>$prop["module"],"entity"=>$class,"prefix"=>$p['prefix'],"id"=>$item['id'],"key"=>$p['key'],"position"=>$pos,"lines"=>$prop["lines"],"items"=>$itens);
        }
      }
    }
    /* PARENT */


    $suported = array("view","edit","add","search");
    if(in_array($param, $suported)){

      if($param == "view") {
        $info = "Visualizar ";
      }else if($param == "edit"){
        $info = "Editar ";
      }else if($param == "add"){
        $info = "Novo";
      }else if($param == "search"){
        $info = "Pesquisar";
      }

      $before = 0;
      $after = 0;
      if($parent){
        $before = $parent['position'] === 'top' ? $parent['lines'] : $before;
        $after = $parent['position'] === 'bottom' ? $lines : $after;
        $lines = $lines + $parent['lines'];
      }

      if($param == "view" or $param == "edit"){

        $info = "Editar ";

        $s = "";
        $conector = " AND ";
        foreach($items as $item){
          if($item['pk']){
            $key = $item['id'];
            $s .= $conector.$key." = '".System::request($key)."'";
          }
        }
        $search = substr($s, strlen($conector));

        $widgets = $widgetCtrl->getWidgetCtrl($search, "", "", "", "");
        if(is_array($widgets)){
          $widget = $widgets[0];
        }
      }

      $items = $widget->get_wid_items();

      foreach($items as $item){
        $key = $item['id'];
        $items[$key]['line'] = $before + $items[$key]['line'];

        if($param == "view"){
          $items[$key] = $screen->utilities->configureViewManager($item);
        }else if($param == "edit"){
          $items[$key]['value'] = $widget->get_wid_value($key);
          if($item['type'] === 'select-multi'){
            $items[$key]['select-multi']['filter'] = $widget->get_wid_value($items[$key]['select-multi']['key']);
          }
          $items[$key] = $screen->utilities->configureEditManager($item);
        }else if($param == "add"){
          $items[$key] = $screen->utilities->configureAddManager($item);
        }else if($param == "search"){
          $items[$key] = $screen->utilities->configureSearchManager($item);
        }
      }

      if($parent){
        $itens = $parent['items'];

        if($param == "view" or $param == "edit"){
          $relation = $widget->get_wid_value($parent['id']);
          if($relation){
            $class = $parent['entity'];
            System::desire('c', $parent['module'], $class, 'src', true);
            $classCtrl = $class."Ctrl";
            $objCtrl = new $classCtrl(PATH_APP);
            $getCtrl = "get".$classCtrl;
            $get_items = "get_".$parent['prefix']."_items";

            $objs = $objCtrl->$getCtrl($parent['key']." = '".$relation."'","","");
            if(is_array($objs)){
              $obj = $objs[0];
            }

            $itens = $obj->$get_items();
          }
        }

        $parent['items'] = null;

        foreach($itens as $item){
          $key = $item['id'];

          $item['line'] = $after + $item['line'];

          if($param == "view"){
            $item = $screen->utilities->configureViewManager($item);
          }else if($param == "add"){
            $item = $screen->utilities->configureAddManager($item);
          }else if($param == "edit"){
            $item = $screen->utilities->configureEditManager($item);
          }else if($param == "search"){
            $item = $screen->utilities->configureSearchManager($item);
          }
          if($item['pk']){
            $hidden[] = array("id"=>$item['id'],"name"=>$item['id'],"value"=>$item['value']);
            $item['line'] = 0;
            $item['form'] = 0;
          }

          $parent['items'][] = $item;
        }
      }

      if($param == "view" or $param == "edit"){

        $registro = $widget->get_wid_value('wid_registro');
        $criador = $widget->get_wid_value('wid_criador');
        $alteracao = $widget->get_wid_value('wid_alteracao');
        $responsavel = $widget->get_wid_value('wid_responsavel');

      }

    }

    $one = true;
    $__filter = explode(",", $filter);
    $__fk = -1;

    foreach($items as $item){
      $key = $item['id'];

      if($item['type_behavior'] == 'child'){

        $_width = $screen->utilities->getChildWidth($window_width, $modal);
        $_height = $screen->utilities->getChildHeight($window_height, $layout, $modal, $lines);

        $_child = $item['child'];
        $action = "list";/*se precisar que a funcao em children inicie em outro modo custmoize aqui*/
        $_child['target'] = 'children'.'-'.$target.'-'.count($children);
        $_child['filter'] = Encode::encrypt($widget->get_wid_value($reference));/*se precisar usar outro filtro use o params para passar*/
        $params = "t=".$_child['target'];

        $child = $screen->utilities->generateChild($action, $_child['module'], $_child['entity'], $_child['tag'], $_child['name'], $level, $_width, $_height, $_child['filter'], $_child['key'], $params, "", $_child['target']);
        $children[] = $child;

        if($param == 'add' and $one){
          $load = array("form"=>$_child['form'],"module"=>$_child['module'],"entity"=>$_child['entity'],"tag"=>$_child['tag'],"target"=>$_child['target'],"level"=>$level,"params"=>$params);
          $one = false;
        }

      }

      if ($item['type_behavior'] == 'popup') {
        $_height = $window_height - 40;
        $_width = $window_width - 40;

        $_key = $item['popup']['key'];
        $_name = $item['popup']['name'];
        $_filter = $item['popup']['filter'];
        $_target = 'popup_'.str_replace('-', '_', $item['popup']['target']);

        $item['line'] = 0;
        $item['type'] = 'popup';
        $item['popup']['height'] = $_height;
        $item['popup']['width'] = $_width;
        $item['popup']['rotule'] = $_name;
        $item['popup']['level'] = $level;
        $item['popup']['params'] = 'width='.$_width.'&height='.$_height.'&rotule='.Encode::encrypt($_name).'&t='.$_target."&f='+system.encrypt.encode(system.util.getValue('".$item['popup']['key']."'))+'";

        $item['popup']['parent']['target'] = $target;
        $item['popup']['parent']['module'] = $module;
        $item['popup']['parent']['entity'] = $entity;
        $item['popup']['parent']['tag'] = $tag;

        $popups[] = $item;
      }

      if($item['fk'] && $filter){
        $__fk++;
        if($param == 'add'){
          if(isset($__filter[$__fk])){
            $item['value'] = Encode::decrypt($__filter[$__fk]);
            if($item['type_behavior'] == 'foreign' && $filter){
              //usar quando preciso
              //$item['foreign']['where'] = Encode::encrypt($item['foreign']['key']."='".Encode::decrypt($filter)."'");
            }
          }
        }
      }

      if($item['pk']){
        $primary[] = $item;
      }

      if($item['fk']){
        $foreign[] = $item;
      }

      if(isset($item['hidden'])){
        if($item['hidden']){
          $hidden[] = array("id"=>$item['id'],"name"=>$item['id'],"value"=>$item['value']);
        }
      }

      if($item['validate']){
        $validate[] = $item;
      }

      $items[$key] = $item;
      $width_label = $screen->utilities->getViewManageWidthLabel($item, $width_label);

    }

    if($parent){
      if($parent['position'] === 'top'){
        $items = array_merge($parent['items'], $items);
      }else{
        $items = array_merge($items, $parent['items']);
      }
    }

    $width_value = $window_width - $width_label;

    try{
      if($acesso_controle >= 1 || $param == "view"){

        $_id = $widget->get_wid_value($reference);
        $form = $form ? $form : 'form-'.$target;

        ?>
        <center>
          <?php 
            if($layout === 'manager' or $layout === ''){
              
              $name = Encode::decrypt($rotule);
              
              if($layout === 'manager'){
                $info = "";
                $filter = "";
                $height = "";
                $width = "";
                $extra_child = "";
              }
              
              $screen->manager->printManagerHeader($target, $name, $info, $window_width);
              
                $screen->manager->printManagerFormHeader($form, $target, Encode::decrypt($rotule), $info, $window_width);

                  $screen->manager->printManagerHiddens($primary, $foreign, $hidden);

                  $screen->manager->printFormValues($target, $level, $where, $group, $order, $filter, $w_description, $g_description, $o_description, $rotule, $start, $height, $width, $extra_child);

                  $screen->manager->printManagerLinesHeader($form, $target, Encode::decrypt($rotule), $info, $window_width);
                  
                    $screen->manager->printManagerLines($param, $items, $lines, $width_label, $width_value, $window_width);

                    if($param != "search"){
                      $screen->line->printPopups($popups);
                    }
                    
                  $screen->manager->printManagerLinesBottom($target, Encode::decrypt($rotule), $info, $window_width);

                $screen->manager->printManagerFormBottom($form, $target, Encode::decrypt($rotule), $info, $window_width);

                if($param != "search"){
                  if($layout === 'children' or $layout === ''){
                    $screen->line->printChildren($children, $window_width, $target, $width_label, $width_value, $param, '', true);
                  }
                }
							
                if($param != "search"){
                  $screen->manager->printFormHistory($window_width, $registro, $criador, $alteracao, $responsavel, $target, $module, $rotule, $entity, $tag, $_id, false);
                }

              $screen->manager->printManagerBottom($target, Encode::decrypt($rotule), $info, $window_width);

            }else{

              $name = "";

              $screen->manager->printManagerHeader($target, $name, $info, $window_width);

								$screen->manager->printManagerLinesHeader($form, $target, Encode::decrypt($rotule), $info, $window_width, true);

                  if($layout === 'children'){

                    $_key = $reference;
                    $_name = Encode::decrypt($rotule);
                    $_width = $screen->utilities->getChildWidth($window_width, $modal);
                    $_height = $screen->utilities->getChildHeight($window_height, $layout, $modal, $lines);
                    $params = "&layout=manager&t=".$target."&f=".$filter."&rotule=".$rotule."&start=".$start."&form=".$form."&w=".$where."&g=".$group."&o=".$order."&wd=".$w_description."&gd=".$g_description."&od=".$o_description;

                    $item = $screen->utilities->generateChild($param, $module, $entity, $tag, $_name, $level, $_width, $_height, $widget->get_wid_value($_key), $_key, $params);

                    array_unshift($children, $item);
                  }

                  if($param != "search"){
                    if($layout === 'children' or $layout === ''){
                      $screen->line->printChildren($children, $window_width, $target, $width_label, $width_value, $param, '', true);
                    }
                  }

							  $screen->manager->printManagerLinesBottom($target, Encode::decrypt($rotule), $info, $window_width);

              $screen->manager->printManagerBottom($target, Encode::decrypt($rotule), $info, $window_width);

            }
          ?>
          <br>
        </center>

        <?php

          if($layout === 'children' or $layout === ''){
            $v = "";
            if(count($validate) > 0){
              foreach($validate as $item){
                $details = "";
                if(isset($item['foreign'])){
                  $details = ", 'target':'ext-".$item['foreign']['target']."'";
                }
                $v .= ",'".$item['id']."': {'description':'".$item['description']."', 'value':'".$item['validate']."'".$details."}";
              }
            }

            $valid = ($v != "") ? '{'.substr($v, 1).'}' : "";

            $value = $_id ? $_id : 'undefined';
            
            $extras = null;

            $html = '<input type="hidden" id="pme_codigo" name="pme_codigo" value="'.$value.'"/>';
            
            $screen->manager->printManagerToolbar($acesso_controle, $param, $target, $level, $module, $entity, $tag, $back, $execute, $valid, $load, $core, $form, $html, $extras, $window_width);
          }

      }else{
        $screen->message->printMessageError(MESSAGE_FORBIDDEN);
      }
    }catch (Exception $exception){
      print "Caught exception: ".$exception->getMessage()."n";
    }
}

  /**
   * Exibe um formul�rio com uma listagem semi-din�mica para pesquisa e consulta das inst�ncias da entidade na base de dados
   * 
   * @param string $target
   * @param string $level
   * @param string $where
   * @param string $group
   * @param string $order
   * @param string $filter
   * @param string $rotule
   * @param string $w_description
   * @param string $g_description
   * @param string $o_description
   * @param string $start
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 24/05/2013 11:35:40
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 24/05/2013 11:35:40
   */
 function viewListWidget($target, $level, $where, $group, $order, $filter, $rotule, $w_description, $g_description, $o_description, $start){
  ?><?php
  $acesso_controle = -1;
  
  require System::desire('file', '', 'header',  'core', false);  
  require System::desire('class', 'resource', 'Screen', 'core', false);
  
  System::desire('m','site', 'Widget', 'src', true, '{project.rsc}');
  System::desire('c','site', 'Widget', 'src', true, '{project.rsc}');

  $screen = new Screen('{project.rsc}');

  $widget = new Widget();
  
  $items = $widget->get_wid_items();
  $fast = '';
  
  $properties = $widget->get_wid_properties();
  $reference = $properties["reference"];
  $module = $properties["module"];
  $entity = $properties["entity"];
  $tag = $properties["tag"];
  $lines = $properties["lines"];
  
  $core = isset($properties["core"]) ? $properties["core"] : false;
  $readonly = isset($properties["readonly"]) ? $properties["readonly"] : false;
  $saveonly = isset($properties["saveonly"]) ? $properties["saveonly"] : false;
  $checkbox = isset($properties["checkbox"]) ? $properties["checkbox"] : false;
  
  $child = System::request('child', false);
  $hidetoolbar = '';
  
  if($child == 'add'){
    $filter = ($filter == "") ? Encode::encrypt('FALSE') : $filter;
    $hidetoolbar = 'display:none;';
  }
  
  $height = System::request('height', 0);
  $window_height = WINDOW_HEIGHT + 30;
  if($height){
    $window_height = $height;
  }
  
  $width = System::request('width', 0);
  $window_width = WINDOW_WIDTH;
  if($width){
    $window_width = $width;
  }
  
  if($acesso_controle >= 0){
    
    try{
      
      $reload = System::request("reload", false);
      if($reload){
        $recovered = System::recover($items, true, true, true);
        $where = Encode::encrypt($recovered['w']);
        $w_description = Encode::encrypt($recovered['wd']);
        $group = Encode::encrypt($recovered['g']);
        $g_description = Encode::encrypt($recovered['gd']);
        $order = Encode::encrypt($recovered['o']);
        $o_description = Encode::encrypt($recovered['od']);
        $start = 0;
      }
      
      $fast = System::request("fast");
      if($fast){
        $fast  = System::request('fast-search-'.$target);
        $recovered = System::recover($items, true, false, false, $fast);
        $where = Encode::encrypt($recovered['w']);
        $w_description = Encode::encrypt($recovered['wd']);
        $group = "";
        $g_description = "";
        $order = "";
        $o_description = "";
        $start = 0;
      }
      
      $items = $screen->utilities->orderItems($items, $lines);
      
      $id = "grid-".$target;
      $end = ITENS;
      
      ?>
        <center>
          <form id="form-<?php print $target;?>" name="form-<?php print $target;?>" action="return false;" onsubmit="return false;">
            <?php
              $screen->list->printListHeader($target, Encode::decrypt($rotule), $hidetoolbar, $child);
      
              $screen->list->printListMarkup($id, $window_width, $window_height);
      
              $screen->list->printListBottom($target, Encode::decrypt($rotule), $hidetoolbar, $child);
      
              $screen->manager->printFormValues($target, $level, $where, $group, $order, $filter, $w_description, $g_description, $o_description, $rotule, $start, $height, $width, $child);
            ?>
          </form>
        </center>

      <?php

			$option = $screen->utilities->getViewListOptionWidth($acesso_controle, $properties, false);
      
      $screen->list->printListToolbar($acesso_controle, $target, $level, $module, $entity, $tag, $fast, $w_description, $core, $readonly, $window_width);
      
      $screen->list->printListGridAction($module, $rotule, $entity, $tag, $reference, $properties);
      
      $screen->list->printListGrid($module, $rotule, $entity, $tag, $items, $target, $id, $start, $end, $window_height, $window_width, $level, $filter, $where, $group, $order, $option, $checkbox);
      
    }catch (Exception $exception){
      print "Caught exception: ".$exception->getMessage()."n";
    }
  }else{
    
    $screen->printMessageError(MESSAGE_FORBIDDEN);
    
  }
}

  /**
   * Carrega um combobox para sele��o de uma inst�ncia da entidade dentro de um formul�rio
   * 
   * @param string $id
   * @param string $level
   * @param string $where
   * @param string $group
   * @param string $order
   * @param string $name
   * @param string $selected
   * @param text $onchange
   * @param boolean $encode
   * @param int $width
   * @param string $filter
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 24/05/2013 11:35:40
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 24/05/2013 11:35:40
   */
 function viewSelectWidget($id, $level, $where, $group, $order, $name, $selected, $onchange, $encode, $width = 400, $filter = ""){
  ?><?php
  $acesso_controle = -1;
  
  require System::desire('file', '', 'header', 'core', false);  
  require System::desire('class', 'resource', 'Screen', 'core', false);

  System::desire('m','site', 'Widget', 'src', true, '{project.rsc}');
  System::desire('c','site', 'Widget', 'src', true, '{project.rsc}');
  
  $screen = new Screen('{project.rsc}');
  
  try {

    if ($acesso_controle >= 0) {

      $widget = new Widget();

      $properties = $widget->get_wid_properties();
      $module = $properties["module"];
      $entity = $properties["entity"];
      $tag = $properties["tag"];
      $reference = $properties["reference"];
      $description = $properties["description"];

      $render = 'relationship-' . date('dmYHis') . '-' . rand();
      ?>

      <div id="<?php print $render; ?>"></div>

      <script type="text/javascript">
              
        var wid_url = 'src/<?php print $module; ?>/json/<?php print $entity; ?>.gen.json.php?action=<?php print $tag; ?>-j-list&t=<?php print $id; ?>&l=<?php print $level; ?>&w=<?php print $where; ?>&g=<?php print $group; ?>&o=<?php print $order; ?>&f=<?php print $filter; ?>&rotule=&full=0';
        var wid_fields = ['<?php print $reference ?>','<?php print $description ?>'];
        var wid_field_value = '<?php print $reference ?>';
        var wid_field_display = '<?php print $description ?>';
              
        var combo = system.form.createComboBox('<?php print $name; ?>', '<?php print $render; ?>', true, null, wid_url, <?php print $width; ?>, '<?php print $selected; ?>', wid_fields, wid_field_display, wid_field_value, null);
              
        combo.on('select', function(combo, record){
          <?php 
            if($encode){
              $onchange = Encode::decrypt($onchange);
            }
            print $onchange; 
          ?>}
          ,this
        );
              
      </script>
      <?
    } else {
      $screen->printMessageError(MESSAGE_FORBIDDEN);
    }
  } catch (Exception $exception) {
    print "Caught exception: " . $exception->getMessage() . "n";
  }
}

