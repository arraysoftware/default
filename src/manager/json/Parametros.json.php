<?php
/**
 * @copyright array software
 *
 * @author ADMINISTRADOR - 18/01/2013 11:31:53
 * <br><b>Updated by</b> ADMINISTRADOR - 24/07/2013 10:54:11
 * @category json
 * @package manager
 *
 * Responsavel.....: ADMINISTRADOR
 * Alteracao.......: 28/07/2013 09:30:11
 */


if (System::request('action')) {
  $action = System::request('action');


 if ($action == 'parametros-j-list') {
    $target = System::request('t');
    $level = System::request('l');
    $rotule = System::request('rotule');
    $filter = System::request('f');
    $where = System::request('w');
    $group = System::request('g');
    $order = System::request('o');
    $start = System::request('start');
    $end = System::request('limit');
    $full = System::request('full', true);
    $checkbox = System::request('checkbox', false);

    jsonListParametros($target, $level, $rotule, $filter, $where, $group, $order, $start, $end, $full, $checkbox);
  }
}

  /**
   * Recupera os dados da entidade convertidos em JSON para interoperabilidade das consultas
   * 
   * @param string $target
   * @param string $level
   * @param string $rotule
   * @param string $filter
   * @param string $where
   * @param string $group
   * @param string $order
   * @param string $start
   * @param string $end
   * @param boolean $full
   * @param boolean $checkbox
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 02/05/2013 19:15:17
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 02/05/2013 19:15:17
   */
 function jsonListParametros($target, $level, $rotule, $filter, $where, $group, $order, $start, $end, $full = true, $checkbox = false){
  ?><?php
  $acesso_controle = -1;
  
  require System::desire('file', '', 'header', 'core', false);
  
  System::desire('m','manager', 'Parametros', 'src', true, '');
  System::desire('c','manager', 'Parametros', 'src', true, '');

  $informacao = "";
  $arrayJson = "";
  
  $parametros = new Parametros();
  
  $properties = $parametros->get_par_properties();
  $reference = $properties['reference'];
  $description = $properties['description'];
  
  $__where = isset($properties["where"]) ? $properties["where"] : "";
  $__group = isset($properties["group"]) ? $properties["group"] : "";
  $__order = isset($properties["order"]) ? $properties["order"] : "";

  $saveonly = isset($properties["saveonly"]) ? $properties["saveonly"] : false;
  $remove = isset($properties["remove"]) ? $properties["remove"] : true;
  $readonly = isset($properties["readonly"]) ? $properties["readonly"] : true;
  $database = isset($properties["database"]) ? $properties["database"] : DEFAULT_DATABASE;
  
  $search = "";
  if($filter){
    
    $conector = " AND ";
    $__filter = explode(",", $filter);
    $__fk = -1;
    
    $items = $parametros->get_par_items();
  
    foreach($items as $item){
      $key = $item['id'];
      if($item['fk']){
        $__fk++;
        if(isset($__filter[$__fk])){
          $search = $conector.$key." = '".Encode::decrypt($__filter[$__fk])."'";
        }
      }
    }
    if(substr($search, 0, strlen($conector)) == $conector){
      $search = substr($search, strlen($conector));
    }
    
  }
  
  $query = System::request('query');
  if($query){
    $field = $description;
    if(isset($properties['search'])){
      $field = $properties['search'] ? $properties['search'] : $field;
    }
    $query = $field." LIKE '".$query."%'";
    $search = empty($search) ? $query : $search." AND (".$query.")";
  }
  
  $code = System::request('code');
  if($code){
    $code = $reference." = '".$code."'";
    $search = empty($search) ? $code : $search." AND (".$code.")";
  }

  if($where == ""){
    //default order
    if($__where)
      $search = $search ? $search." AND (".$__where.")" : $__where;
  }

  if($group == ""){
    //default group
    if($__group)
      $group = Encode::encrypt($__group);
  }

  if($order == ""){
    //default order
    if($__order)
      $order = Encode::encrypt($__order);
  }
  
  if($acesso_controle >= 0){
    
    $_where = Encode::decrypt($where);
    if($_where == ''){
      $_where = $search;
    }else{
      if($search){
        $search = ' AND ('.$search.')';
      }
      $_where = $_where.$search;
    }
    
    $parametrosCtrl = new ParametrosCtrl(PATH_APP, $database);
    $total = $parametrosCtrl->getCountParametrosCtrl($_where);
    $parametross = $parametrosCtrl->getParametrosCtrl($_where, Encode::decrypt($group), Encode::decrypt($order), $start, $end);
    
    $parametros = new Parametros();
    for($i = 0; $i < count($parametross); $i++) {

      $parametros = $parametross[$i];

      $counter = ($start + $i) + 1;

      $id = '';
      $view = '';
      $edit = '';
      $delete = '';
      if($full){
        $id = Encode::encrypt($parametros->get_par_value($reference));
        $view = Json::actionView.','.$id.','.$target.','.$level;
        if($acesso_controle >= 1 and !$saveonly and !$readonly){
          $edit = Json::actionEdit.','.$id.','.$target.','.$level;
        }
        if($acesso_controle >= 2 and $remove !== false and !$readonly){
          $delete = Json::actionRemove.','.$id.','.$target.','.$level;
        }
      }
      
      $items = $parametros->get_par_items();
    
      $json = array();
      if($full){
        $json['counter'] = $counter;
        if($checkbox){
          $json['checkbox'] = $id;
        }
        $json['option'] = Json::encodeAction($view.'|'.$edit.'|'.$delete);
      }
      foreach($items as $item){
        $exibir = $item['grid'];
        if($exibir){
          $key = $item['id'];
          if(isset($item['foreign'])){
            $custom = isset($item['foreign']['custom']) ? $item['foreign']['custom'] : $item['foreign']['description'];
            $description = $parametrosCtrl->getColumnParametrosCtrl($custom, $item['foreign']['key']." = '".$item['value']."'", $item['foreign']['table'], true, false);
            $value = Json::encodeValue($description);
          }else{
            $value = Json::render($parametros, $key, 'par');
          }
      
          $json[$key] = $value;
        }
      }
      
      $arrayJson[] = $json;
    }
    
  }
  
  print json_encode(
          array("rows" => $arrayJson,
          "total" => $total,
          "begin" => $start,
          "end" => $end,
          "info" => $informacao)
  );
}

