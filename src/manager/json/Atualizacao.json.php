<?php
/**
 * @copyright array software
 *
 * @author WILLIAM MARQUES VICENTE GOMES CORREA - 10/05/2013 15:25:43
 * <br><b>Updated by</b> ADMINISTRADOR - 15/06/2013 18:51:33
 * @category json
 * @package manager
 *
 * Responsavel.....: ADMINISTRADOR
 * Alteracao.......: 28/07/2013 09:30:10
 */


if (System::request('action')) {
  $action = System::request('action');


 if ($action == 'atualizacao-j-list') {
    $target = System::request('t');
    $level = System::request('l');
    $rotule = System::request('rotule');
    $filter = System::request('f');
    $where = System::request('w');
    $group = System::request('g');
    $order = System::request('o');
    $start = System::request('start', "0");
    $end = System::request('limit');
    $full = System::request('full', true);
    $checkbox = System::request('checkbox', false);
    $readonly = System::request('r', false);

    jsonListAtualizacao($target, $level, $rotule, $filter, $where, $group, $order, $start, $end, $full, $checkbox, $readonly);
  }
}

  /**
   * Recupera os dados da entidade convertidos em JSON para interoperabilidade das consultas
   * 
   * @param string $target
   * @param string $level
   * @param string $rotule
   * @param string $filter
   * @param string $where
   * @param string $group
   * @param string $order
   * @param string $start
   * @param string $end
   * @param boolean $full
   * @param boolean $checkbox
   * @param boolean $readonly
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 10/05/2013 15:25:46
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 10/05/2013 15:25:46
   */
 function jsonListAtualizacao($target, $level, $rotule, $filter, $where, $group, $order, $start, $end, $full = false, $checkbox = false, $readonly = false){
  ?><?php
  $acesso_controle = -1;
  
  require System::desire('file', '', 'header', 'core', false);
  
  System::desire('m','manager', 'Atualizacao', 'src', true, '');
  System::desire('c','manager', 'Atualizacao', 'src', true, '');

  $informacao = "";
  $arrayJson = "";
  
  $atualizacao = new Atualizacao();
  
  $properties = $atualizacao->get_atl_properties();
  $reference = $properties['reference'];
  $description = $properties['description'];
  
  $__where = isset($properties["where"]) ? $properties["where"] : "";
  $__group = isset($properties["group"]) ? $properties["group"] : "";
  $__order = isset($properties["order"]) ? $properties["order"] : "";

  $saveonly = isset($properties["saveonly"]) ? $properties["saveonly"] : false;
  $remove = isset($properties["remove"]) ? $properties["remove"] : true;
  $database = isset($properties["database"]) ? $properties["database"] : DEFAULT_DATABASE;
  if(!$readonly){
		$readonly = isset($properties["readonly"]) ? $properties["readonly"] : true;
	}
  
  $search = "";
  if($filter){
    
    $conector = " AND ";
    $__filter = explode(",", Encode::decrypt($filter));
    $__fk = -1;
    
    $items = $atualizacao->get_atl_items();
  
    foreach($items as $item){
      $key = $item['id'];
      if($item['fk']){
        $__fk++;
        if(isset($__filter[$__fk])){
          $search = $conector.$key." = '".$__filter[$__fk]."'";
        }
      }
    }
    if(substr($search, 0, strlen($conector)) == $conector){
      $search = substr($search, strlen($conector));
    }
    
  }
  
  $query = System::request('query');
  if($query){
    $field = $description;
    if(isset($properties['search'])){
      $field = $properties['search'] ? $properties['search'] : $field;
    }
    $query = $field." LIKE '".$query."%'";
    $search = empty($search) ? $query : "(".$search.") AND (".$query.")";
  }
  
  $code = System::request('code');
  if($code){
    $code = $reference." = '".$code."'";
    $search = empty($search) ? $code : "(".$search.") AND (".$code.")";
  }

  if($where == ""){
    //default order
    if($__where)
      $search = empty($search) ? $__where : "(".$search.") AND (".$__where.")";
  }

  if($group == ""){
    //default group
    if($__group)
      $group = Encode::encrypt($__group);
  }

  if($order == ""){
    //default order
    if($__order)
      $order = Encode::encrypt($__order);
  }
  
  if($acesso_controle >= 0){
    
    $_where = Encode::decrypt($where);
    if($_where == ''){
      $_where = $search;
    }else{
      if($search){
        $search = ' AND ('.$search.')';
      }
      $_where = '('.$_where.')'.$search;
    }
    
    $atualizacaoCtrl = new AtualizacaoCtrl(PATH_APP, $database);
    $total = $atualizacaoCtrl->getCountAtualizacaoCtrl($_where);
    $atualizacaos = $atualizacaoCtrl->getAtualizacaoCtrl($_where, Encode::decrypt($group), Encode::decrypt($order), $start, $end);
    
    $atualizacao = new Atualizacao();
    for($i = 0; $i < count($atualizacaos); $i++) {

      $atualizacao = $atualizacaos[$i];

      $counter = ($start + $i) + 1;

      $id = '';
      $view = '';
      $edit = '';
      $delete = '';
      if($full){
        $id = Encode::encrypt($atualizacao->get_atl_value($reference));
        $view = Json::actionView.','.$id.','.$target.','.$level;
        if($acesso_controle >= 1 and !$saveonly and !$readonly){
          $edit = Json::actionEdit.','.$id.','.$target.','.$level;
        }
        if($acesso_controle >= 2 and $remove !== false and !$readonly){
          $delete = Json::actionRemove.','.$id.','.$target.','.$level;
        }
      }
      
      $items = $atualizacao->get_atl_items();
    
      $json = array();
      if($full){
        $json['counter'] = $counter;
        if($checkbox){
          $json['checkbox'] = $id;
        }
        $json['option'] = Json::encodeAction($view.'|'.$edit.'|'.$delete);
      }
      foreach($items as $item){
        $exibir = $item['grid'];
        if($exibir){
          $key = $item['id'];
          if(isset($item['foreign'])){
            $description = $atualizacaoCtrl->getColumnAtualizacaoCtrl($item['foreign']['description'], $item['foreign']['key']." = '".$item['value']."'", $item['foreign']['table'], true, false);
            $value = Json::encodeValue($description);
          }else if($item['type'] == 'calculated'){
            $description = $atualizacaoCtrl->getColumnAtualizacaoCtrl($item['type_content'], $reference." = '".$atualizacao->get_atl_value($reference)."'");
            $value = Json::encodeValue($description);
          }else{
            $value = Json::render($atualizacao, $key, 'atl');
          }
      
          $json[$key] = $value;
        }
      }
      
      $arrayJson[] = $json;
    }
    
  }
  
  print json_encode(
          array("rows" => $arrayJson,
          "total" => $total,
          "begin" => $start,
          "end" => $end,
          "info" => $informacao)
  );
}

