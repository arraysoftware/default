<?php
/**
 * @copyright array software
 *
 * @author WILLIAM MARQUES VICENTE GOMES CORREA - 20/04/2013 12:46:48
 * <br><b>Updated by</b> ADMINISTRADOR - 15/06/2013 18:51:44
 * @category json
 * @package manager
 *
 * Responsavel.....: ADMINISTRADOR
 * Alteracao.......: 28/07/2013 09:30:10
 */


if (System::request('action')) {
  $action = System::request('action');


 if ($action == 'usuario-j-list') {
    $target = System::request('t');
    $level = System::request('l');
    $rotule = System::request('rotule');
    $filter = System::request('f');
    $where = System::request('w');
    $group = System::request('g');
    $order = System::request('o');
    $start = System::request('start', "0");
    $end = System::request('limit');
    $full = System::request('full', true);
    $checkbox = System::request('checkbox', false);
    $readonly = System::request('r', false);

    jsonListUsuario($target, $level, $rotule, $filter, $where, $group, $order, $start, $end, $full, $checkbox, $readonly);
  }
}

  /**
   * Recupera os dados da entidade convertidos em JSON para interoperabilidade das consultas
   * 
   * @param string $target
   * @param string $level
   * @param string $rotule
   * @param string $filter
   * @param string $where
   * @param string $group
   * @param string $order
   * @param string $start
   * @param string $end
   * @param boolean $full
   * @param boolean $checkbox
   * @param boolean $readonly
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 28/05/2013 18:42:48
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 28/05/2013 18:42:48
   */
 function jsonListUsuario($target, $level, $rotule, $filter, $where, $group, $order, $start, $end, $full = false, $checkbox = false, $readonly = false){
  ?><?php
  $acesso_controle = -1;
  
  require System::desire('file', '', 'header', 'core', false);
  
  System::desire('m','manager', 'Usuario', 'src', true, '{project.rsc}');
  System::desire('c','manager', 'Usuario', 'src', true, '{project.rsc}');

  $informacao = "";
  $arrayJson = "";
  
  $usuario = new Usuario();
  
  $properties = $usuario->get_usu_properties();
  $reference = $properties['reference'];
  $description = $properties['description'];
  
  $__where = isset($properties["where"]) ? $properties["where"] : "";
  $__group = isset($properties["group"]) ? $properties["group"] : "";
  $__order = isset($properties["order"]) ? $properties["order"] : "";

  $saveonly = isset($properties["saveonly"]) ? $properties["saveonly"] : false;
  $remove = isset($properties["remove"]) ? $properties["remove"] : true;
  $database = isset($properties["database"]) ? $properties["database"] : DEFAULT_DATABASE;
  if(!$readonly){
		$readonly = isset($properties["readonly"]) ? $properties["readonly"] : true;
	}
  
  $search = "";
  if($filter){
    
    $conector = " AND ";
    $__filter = explode(",", Encode::decrypt($filter));
    $__fk = -1;
    
    $items = $usuario->get_usu_items();
  
    foreach($items as $item){
      $key = $item['id'];
      if($item['fk']){
        $__fk++;
        if(isset($__filter[$__fk])){
          $search = $conector.$key." = '".$__filter[$__fk]."'";
        }
      }
    }
    if(substr($search, 0, strlen($conector)) == $conector){
      $search = substr($search, strlen($conector));
    }
    
  }
  
  $query = System::request('query');
  if($query){
    $field = $description;
    if(isset($properties['search'])){
      $field = $properties['search'] ? $properties['search'] : $field;
    }
    $query = $field." LIKE '".$query."%'";
    $search = empty($search) ? $query : "(".$search.") AND (".$query.")";
  }
  
  $code = System::request('code');
  if($code){
    $code = $reference." = '".$code."'";
    $search = empty($search) ? $code : "(".$search.") AND (".$code.")";
  }

  if($where == ""){
    //default order
    if($__where)
      $search = empty($search) ? $__where : "(".$search.") AND (".$__where.")";
  }

  if($group == ""){
    //default group
    if($__group)
      $group = Encode::encrypt($__group);
  }

  if($order == ""){
    //default order
    if($__order)
      $order = Encode::encrypt($__order);
  }
  
  if($acesso_controle >= 0){
    
    $_where = Encode::decrypt($where);
    if($_where == ''){
      $_where = $search;
    }else{
      if($search){
        $search = ' AND ('.$search.')';
      }
      $_where = '('.$_where.')'.$search;
    }
    
    $usuarioCtrl = new UsuarioCtrl(PATH_APP, $database);
    $total = $usuarioCtrl->getCountUsuarioCtrl($_where);
    $usuarios = $usuarioCtrl->getUsuarioCtrl($_where, Encode::decrypt($group), Encode::decrypt($order), $start, $end);
    
    $usuario = new Usuario();
    for($i = 0; $i < count($usuarios); $i++) {

      $usuario = $usuarios[$i];

      $counter = ($start + $i) + 1;

      $id = '';
      $view = '';
      $edit = '';
      $delete = '';
      if($full){
        $id = Encode::encrypt($usuario->get_usu_value($reference));
        $view = Json::actionView.','.$id.','.$target.','.$level;
        if($acesso_controle >= 1 and !$saveonly and !$readonly){
          $edit = Json::actionEdit.','.$id.','.$target.','.$level;
        }
        if($acesso_controle >= 2 and $remove !== false and !$readonly){
          $delete = Json::actionRemove.','.$id.','.$target.','.$level;
        }
      }
      
      $items = $usuario->get_usu_items();
    
      $json = array();
      if($full){
        $json['counter'] = $counter;
        if($checkbox){
          $json['checkbox'] = $id;
        }
        $json['option'] = Json::encodeAction($view.'|'.$edit.'|'.$delete);
      }
      foreach($items as $item){
        $exibir = $item['grid'];
        if($exibir){
          $key = $item['id'];
          if(isset($item['foreign'])){
            $description = $usuarioCtrl->getColumnUsuarioCtrl($item['foreign']['description'], $item['foreign']['key']." = '".$item['value']."'", $item['foreign']['table'], true, false);
            $value = Json::encodeValue($description);
          }else if($item['type'] == 'calculated'){
            $description = $usuarioCtrl->getColumnUsuarioCtrl($item['type_content'], $reference." = '".$usuario->get_usu_value($reference)."'");
            $value = Json::encodeValue($description);
          }else{
            $value = Json::render($usuario, $key, 'usu');
          }
      
          $json[$key] = $value;
        }
      }
      
      $arrayJson[] = $json;
    }
    
  }
  
  print json_encode(
          array("rows" => $arrayJson,
          "total" => $total,
          "begin" => $start,
          "end" => $end,
          "info" => $informacao)
  );
}

