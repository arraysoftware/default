<?php
/**
 * @copyright array software
 *
 * @author WILLIAM MARQUES VICENTE GOMES CORREA - 10/05/2013 15:25:43
 * <br><b>Updated by</b> ADMINISTRADOR - 15/06/2013 18:51:33
 * @category dao
 * @package manager
 *
 * Responsavel.....: ADMINISTRADOR
 * Alteracao.......: 28/07/2013 09:30:03
 */


class AtualizacaoDAO
{
  private  $dao;
  private  $path;
  private  $database;
  private  $table;
  private  $reference;
  private  $join;
  private  $history;

  /**
   * Construtor da classde de acesso a dados da entidade
   * 
   * @param string $path
   * @param string $database
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 10/05/2013 15:25:43
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 10/05/2013 15:25:43
   */
  public function AtualizacaoDAO($path, $database = ""){
    ?><?php

    System::desire('class','base', 'DAO', 'core');
    System::desire('m','manager', 'Atualizacao', 'src', true, '');
      
    $entity = new Atualizacao();
    $properties = $entity->get_atl_properties();

    $this->path = $path;
    $this->table = $properties['table'];
    $this->join = $properties['join'];
    $this->reference = $properties['reference'];
    $this->database = $database;

    $this->history = true;

    $this->dao = new DAO($path, $database);
  }

  /**
   * Recupera dados da entidade do banco de dados
   * 
   * @param string $where
   * @param string $group
   * @param string $order
   * @param string $start
   * @param string $end
   * @param boolean $validate
   * @param boolean $debug
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 10/05/2013 15:25:44
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 10/05/2013 15:25:44
   */
  public function getAtualizacaoDAO($where, $group, $order, $start = "", $end = "", $validate = true, $debug = false){
    ?><?php
    System::desire("m", 'manager', 'Atualizacao', 'src', true, '');
        
    try {
      $limit = "";
      if($where != ""){
        $where = " WHERE " . $where;
      }
      if($group != ""){
        $group = " GROUP BY " . $group;
      }
      if($order != ""){
        $order = " ORDER BY " . $order;
      }
      if($start == ""){
        $start = 0;
      }
      if($end){
        $limit = " LIMIT " . $start . "," . $end;
      }
      
      $object = new Atualizacao();
      $items = $object->get_atl_items();
  
      $f = "";
      foreach($items as $item){
        if($this->dao->isValid($item, 'SELECT')){
          $field = $item['id'];
          if($item['type'] == 'calculated'){
            $field = "(" . $item['type_content'] . ")";
          }
          $f .= "," . $field . " AS " . $item['id'];
        }
      }
      $fields = substr($f, 1);
      
      $sql = "SELECT ".$fields." FROM ".$this->table.$this->join.$where.$group.$order.$limit;

      if($debug){
        print $sql;
      }

      $result = $this->dao->selectSQL($sql, $validate);
      
      $atualizacaos = null;
      $atualizacao = null;
      $i = 0;
      if($result != null){
        while($row = mysql_fetch_array($result)){
          $atualizacao = new Atualizacao();
          foreach($items as $item){
            if($this->dao->isValid($item, 'SELECT')){
              $key = $item['id'];
              $value = $this->dao->renderer($item, $row[$key], 'SELECT');
              $atualizacao->set_atl_value($key, $value);
            }
          }

          $atualizacaos[$i] = $atualizacao;
          $i++;
        }
      }
    }catch (Exception $exception) {
      echo "Caught exception: ",  $exception->getMessage(), "n";
    }
    return $atualizacaos;
  }

  /**
   * Recupera um valor inteiro referente ao n�mero de linhas de determina consulta
   * 
   * @param string $where
   * @param string $group
   * @param boolean $validate
   * @param boolean $debug
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 10/05/2013 15:25:44
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 10/05/2013 15:25:44
   */
  public function getCountAtualizacaoDAO($where, $group = "", $validate = true, $debug = false){
    ?><?php
    System::desire("m", 'manager', 'Atualizacao', 'src', true, '');

    try {
      if($where != ""){
        $where = " WHERE " . $where;
      }
      if($group != ""){
        $group = " GROUP BY " . $group;
      }
      
      $sql = "SELECT COUNT(". $this->reference .") AS total FROM ".$this->table.$this->join.$where.$group;
      
      if($debug){
        print $sql;
      }
      
      $result = $this->dao->selectSQL($sql, $validate);
      $total = 0;
      if($result != null){
        while($row = mysql_fetch_array($result)){
          $total = $row['total'];
        }
      }
    }catch (Exception $exception) {
      echo "Caught exception: ",  $exception->getMessage(), "n";
    }
    return $total;
  }

  /**
   * Adiciona um novo registro desta entidade na base de dados
   * 
   * @param object $object
   * @param boolean $validate
   * @param boolean $debug
   * @param boolean $presave
   * @param int $copy
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 10/05/2013 15:25:44
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 10/05/2013 15:25:44
   */
  public function addAtualizacaoDAO($object, $validate = true, $debug = false, $presave = false, $copy = 0){
    ?><?php
    System::desire("m", 'manager', 'Atualizacao', 'src', true, '');
      
    try {

      $items = $object->get_atl_items();
      $f = "";
      $v = ""; 
      foreach ($items as $item) {
        if ($this->dao->isValid($item, 'INSERT')) {
          $key = $item['id'];
          $value = $this->dao->renderer($item, $object->get_atl_value($key), 'INSERT');
          $f .= ',' . $key;
          $v .= "," . $value . "";
        }
      }
      $fields = substr($f, 1);
      $values = substr($v, 1);

      $sql = "INSERT INTO " . $this->table . " (" . $fields . ") VALUES (" . $values . ")";

      $this->beforeAtualizacaoDAO($object, 'add');
      
      $add = $this->dao->insertSQL($sql, $this->history, $validate, $presave);

      $this->afterAtualizacaoDAO($object, 'add', $add, $copy);
      
      if ($debug) {
        print $sql;
      }
      
    }catch (Exception $exception) {
      echo "Caught exception: ",  $exception->getMessage(), "n";
    }
    
    return $add;
  }

  /**
   * Atualiza uma inst�ncia da entidade na base de dados
   * 
   * @param object $object
   * @param boolean $validate
   * @param boolean $debug
   * @param boolean $trigger
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 10/05/2013 15:25:44
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 10/05/2013 15:25:44
   */
  public function setAtualizacaoDAO($object, $validate = true, $debug = false, $trigger = true){
    ?><?php
    System::desire("m", 'manager', 'Atualizacao', 'src', true, '');
    
    try {
      
      $i = $object->get_atl_items();
      $u = "";
      foreach ($i as $item) {
        if ($this->dao->isValid($item, 'UPDATE')) {
          $key = $item['id'];
          $value = $this->dao->renderer($item, $object->get_atl_value($key), 'UPDATE');
          $u .= "," . $key . " = " . $value . "";
        }
      }
      $updates = substr($u, 1);

      $items = $object->get_atl_items();
      $s = "";
      $conector = " AND ";
      foreach ($items as $item) {
        if ($item['pk']) {
          $key = $item['id'];
          $s = $conector.$key." = '".$object->get_atl_value($key)."'";
        }
      }
      $search = substr($s, strlen($conector));
      
      $sql = "UPDATE " . $this->table . $this->join . " SET " . $updates . " WHERE " . $search;

      $this->beforeAtualizacaoDAO($object, 'set');
      
      $updateds = $this->dao->executeSQL($sql, $this->history, $validate);
      
			if($trigger === true){
				$this->afterAtualizacaoDAO($object, 'set', 0, 0, $trigger);
			}
              
      if ($debug) {
        print $sql;
      }
    }catch (Exception $exception) {
      echo "Caught exception: ",  $exception->getMessage(), "n";
    }
    
    return $updateds;
  }

  /**
   * Remove uma instancia da entidade da base de dados
   * 
   * @param object $object
   * @param boolean $validate
   * @param boolean $debug
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 10/05/2013 15:25:44
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 10/05/2013 15:25:44
   */
  public function removeAtualizacaoDAO($object, $validate = true, $debug = false){
    ?><?php
    System::desire("m", 'manager', 'Atualizacao', 'src', true, '');
    
    try {
      
      $items = $object->get_atl_items();
      $properties = $object->get_atl_properties();
      
      $s = "";
      $conector = " AND ";
      foreach ($items as $item) {
        if ($item['pk']) {
          $key = $item['id'];
          $s = $conector.$key." = '".$object->get_atl_value($key)."'";
        }
      }
      $search = substr($s, strlen($conector));
      
      $sql = "DELETE FROM " . $this->table . " WHERE " . $search;
      if (isset($properties['remove'])) {
        if(is_array($properties['remove'])){

          $field = $properties['remove']['field'];
          $value = $properties['remove']['value'];
          
          $object->set_atl_value($field, $value);
        
          $sql = "UPDATE " . $this->table . " SET ". $field ." = '".$value."', ".$properties['prefix']."_responsavel = '".System::getUser()."', ".$properties['prefix']."_alteracao = NOW() WHERE " . $search;
        }
      }
      
      $this->beforeAtualizacaoDAO($object, 'remove');

      $remove = $this->dao->executeSQL($sql, $this->history, $validate);
      
      $this->afterAtualizacaoDAO($object, 'remove');
      
      if ($debug) {
        print $sql;
      }
    }catch (Exception $exception) {
      echo "Caught exception: ",  $exception->getMessage(), "n";
    }
    
    return $remove;
  }

  /**
   * M�todo de gatilho executado antes de cada atualiza��o e remo��o das inst�ncias da entidade
   * 
   * @param object $object
   * @param string $param
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 10/05/2013 15:25:44
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 10/05/2013 15:25:44
   */
  public function beforeAtualizacaoDAO($object, $param){
    ?><?php
    System::desire("m", 'manager', 'Atualizacao', 'src', true, '');
    
    try {
      
      $executed = false;
      
    }catch (Exception $exception) {
      echo "Caught exception: ",  $exception->getMessage(), "n";
    }
    
    return $executed;
  }

  /**
   * M�todo de gatilho executado depois de cada atualiza��o e remo��o das inst�ncias da entidade
   * 
   * @param object $object
   * @param string $param
   * @param int $value
   * @param int $copy
   * @param boolean $trigger
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 10/05/2013 15:25:44
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 10/05/2013 17:48:49
   */
  public function afterAtualizacaoDAO($object, $param, $value = 0, $copy = 0, $trigger = true){
    ?><?php
    System::desire("m", 'manager', 'Atualizacao', 'src', true, '');
    
    try {
      
      $executed = true;
			
			$items = $object->get_atl_items();
			
      if($param === 'add'){
        $object->set_atl_value($object->get_atl_reference(), $value);
      }

      if($param === 'add' or $param === 'set'){
        if($trigger === true){
					$update = false;

          $atualizacao = new Atualizacao();
          foreach ($items as $item) {
            $key = $item['id'];
            $atualizacao->set_atl_value($key, $items[$key]['value']);
            if($items[$key]['type'] == 'calculated'){
							if($items[$key]['type_content']){
								$atualizacao->set_atl_value($key, $items[$key]['type_content']);
                $atualizacao->set_atl_type($key, 'execute');
								$update = true;
							}
            }else{
              if(!$items[$key]['pk']){
                $atualizacao->set_atl_value($key, null);
              }
            }
          }
					if($update){
						$executed = $this->setAtualizacaoDAO($atualizacao, true, false, 0, true);
					}
        }
      }
      
    }catch (Exception $exception) {
      echo "Caught exception: ",  $exception->getMessage(), "n";
    }
    
    return $executed;
  }

  /**
   * Recupera um dado atrav�s de uma consulta personalizada
   * 
   * @param string $column
   * @param string $where
   * @param boolean $table
   * @param boolean $validate
   * @param boolean $debug
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 10/05/2013 15:25:44
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 10/05/2013 15:25:44
   */
  public function getColumnAtualizacaoDAO($column, $where, $table = "", $validate = true, $debug = false){
    ?><?php
    try {
      if($where != ""){
        $where = "WHERE ".$where;
      }
      if($table == ""){
        $table = $this->table.$this->join;
      }
      
      $sql = "SELECT ".$column." AS custom FROM ".$table." ".$where;
      
      if($debug){
        print $sql;
      }
      
      $result = $this->dao->selectSQL($sql, $validate);
      $custom = null;
      if($result != null){
        while($row = mysql_fetch_array($result)){
          $custom = $row['custom'];
        }
      }
    }catch (Exception $exception) {
      echo "Caught exception: ",  $exception->getMessage(), "n";
    }
    return $custom;
  }

  /**
   * Remove um grupo de items ou atualiza uma quantidade relativamente grande 
   * 
   * @param string $where
   * @param string $update
   * @param boolean $validate
   * @param boolean $debug
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 10/05/2013 15:25:44
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 10/05/2013 15:25:44
   */
  public function executeAtualizacaoDAO($where, $update, $validate = true, $debug = false){
    ?><?php
    System::desire('m', 'manager', 'Atualizacao', 'src', true, '');

    try {

      $sql = "DELETE FROM ".$this->table." WHERE ".$where;
      if($update){
        $update = $update.", atl_responsavel = '".System::getUser(false)."', atl_alteracao = NOW()";
        $sql = "UPDATE ".$this->table." SET ".$update." WHERE ".$where;
      }

      $executed = $this->dao->executeSQL($sql, $this->history, $validate);

      if ($debug) {
        print $sql;
      }

    }catch (Exception $exception) {
      echo "Caught exception: ",  $exception->getMessage(), "n";
    }

    return $executed;
  }


}