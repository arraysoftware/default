<?php
/**
 * @copyright array software
 *
 * @author WILLIAM MARQUES VICENTE GOMES CORREA - 05/05/2013 21:56:15
 * <br><b>Updated by</b> ADMINISTRADOR - 24/07/2013 12:07:37
 * @category dao
 * @package manager
 *
 * Responsavel.....: ADMINISTRADOR
 * Alteracao.......: 28/07/2013 09:30:04
 */


class EmailModeloDAO
{
  private  $dao;
  private  $path;
  private  $database;
  private  $table;
  private  $reference;
  private  $join;
  private  $history;

  /**
   * Construtor da classde de acesso a dados da entidade
   * 
   * @param string $path
   * @param string $database
   *
   * @author ADMINISTRADOR - 24/07/2013 12:07:25
   * <br><b>Updated by</b> ADMINISTRADOR - 24/07/2013 12:07:25
   */
  public function EmailModeloDAO($path, $database = ""){
    ?><?php

    System::desire('class','base', 'DAO', 'core');
    System::desire('m','manager', 'EmailModelo', 'src', true, '{project.rsc}');
      
    $entity = new EmailModelo();
    $properties = $entity->get_emo_properties();

    $this->path = $path;
    $this->table = $properties['table'];
    $this->join = $properties['join'];
    $this->reference = $properties['reference'];
		$this->database = $database ? $database : $properties['database'];

    $this->history = true;

    $this->dao = new DAO($path, $this->database);
  }

  /**
   * Recupera dados da entidade do banco de dados
   * 
   * @param string $where
   * @param string $group
   * @param string $order
   * @param string $start
   * @param string $end
   * @param boolean $validate
   * @param boolean $debug
   *
   * @author ADMINISTRADOR - 24/07/2013 12:07:25
   * <br><b>Updated by</b> ADMINISTRADOR - 24/07/2013 12:07:25
   */
  public function getEmailModeloDAO($where, $group, $order, $start = "", $end = "", $validate = true, $debug = false){
    ?><?php
    System::desire("m", 'manager', 'EmailModelo', 'src', true, '{project.rsc}');
        
    try {
      $limit = "";
      if($where != ""){
        $where = " WHERE " . $where;
      }
      if($group != ""){
        $group = " GROUP BY " . $group;
      }
      if($order != ""){
        $order = " ORDER BY " . $order;
      }
      if($start == ""){
        $start = 0;
      }
      if($end){
        $limit = " LIMIT " . $start . "," . $end;
      }
      
      $object = new EmailModelo();
      $items = $object->get_emo_items();
  
      $f = "";
      foreach($items as $item){
        if($this->dao->isValid($item, 'SELECT')){
          $field = $item['id'];
          if($item['type'] == 'calculated'){
            $field = "(" . $item['type_content'] . ")";
          }
          $f .= "," . $field . " AS " . $item['id'];
        }
      }
      $fields = substr($f, 1);
      
      $sql = "SELECT ".$fields." FROM ".$this->table.$this->join.$where.$group.$order.$limit;

      if($debug){
        print $sql;
      }

      $result = $this->dao->selectSQL($sql, $validate);
      
      $emailModelos = null;
      $emailModelo = null;
      $i = 0;
      if($result != null){
        while($row = mysql_fetch_array($result)){
          $emailModelo = new EmailModelo();
          foreach($items as $item){
            if($this->dao->isValid($item, 'SELECT')){
              $key = $item['id'];
              $value = $this->dao->renderer($item, $row[$key], 'SELECT');
              $emailModelo->set_emo_value($key, $value);
            }
          }

          $emailModelos[$i] = $emailModelo;
          $i++;
        }
      }
    }catch (Exception $exception) {
      echo "Caught exception: ",  $exception->getMessage(), "\n";
    }
    return $emailModelos;
  }

  /**
   * Recupera um valor inteiro referente ao n�mero de linhas de determina consulta
   * 
   * @param string $where
   * @param string $group
   * @param boolean $validate
   * @param boolean $debug
   *
   * @author ADMINISTRADOR - 24/07/2013 12:07:25
   * <br><b>Updated by</b> ADMINISTRADOR - 24/07/2013 12:07:25
   */
  public function getCountEmailModeloDAO($where, $group = "", $validate = true, $debug = false){
    ?><?php
    System::desire('m', 'manager', 'EmailModelo', 'src', true, '{project.rsc}');

    try {
      if($where != ""){
        $where = " WHERE " . $where;
      }
      if($group != ""){
        $group = " GROUP BY " . $group;
      }
      
      $sql = "SELECT COUNT(". $this->reference .") AS total FROM ".$this->table.$this->join.$where.$group;
      
      if($debug){
        print $sql;
      }
      
      $result = $this->dao->selectSQL($sql, $validate);
      $total = 0;
      if($result != null){
        while($row = mysql_fetch_array($result)){
          $total = $row['total'];
        }
      }
    }catch (Exception $exception) {
      echo "Caught exception: ",  $exception->getMessage(), "\n";
    }
    return $total;
  }

  /**
   * Adiciona um novo registro desta entidade na base de dados
   * 
   * @param object $object
   * @param boolean $validate
   * @param boolean $debug
   * @param boolean $presave
   * @param int $copy
   *
   * @author ADMINISTRADOR - 24/07/2013 12:07:25
   * <br><b>Updated by</b> ADMINISTRADOR - 24/07/2013 12:07:25
   */
  public function addEmailModeloDAO($object, $validate = true, $debug = false, $presave = false, $copy = 0){
    ?><?php
    System::desire('m', 'manager', 'EmailModelo', 'src', true, '{project.rsc}');
      
    try {

      $items = $object->get_emo_items();
      $f = "";
      $v = ""; 
      foreach ($items as $item) {
        if ($this->dao->isValid($item, 'INSERT')) {
          $key = $item['id'];
          $value = $this->dao->renderer($item, $object->get_emo_value($key), 'INSERT');
          $f .= ',' . $key;
          $v .= "," . $value . "";
        }
      }
      $fields = substr($f, 1);
      $values = substr($v, 1);

      $sql = "INSERT INTO " . $this->table . " (" . $fields . ") VALUES (" . $values . ")";

      $this->beforeEmailModeloDAO($object, 'add');
      
      $add = $this->dao->insertSQL($sql, $this->history, $validate, $presave);

      $this->afterEmailModeloDAO($object, 'add', $add, $copy);
      
      if ($debug) {
        print $sql;
      }
      
    }catch (Exception $exception) {
      echo "Caught exception: ",  $exception->getMessage(), "\n";
    }
    
    return $add;
  }

  /**
   * Atualiza uma inst�ncia da entidade na base de dados
   * 
   * @param object $object
   * @param boolean $validate
   * @param boolean $debug
   * @param boolean $trigger
   *
   * @author ADMINISTRADOR - 24/07/2013 12:07:25
   * <br><b>Updated by</b> ADMINISTRADOR - 24/07/2013 12:07:25
   */
  public function setEmailModeloDAO($object, $validate = true, $debug = false, $trigger = true){
    ?><?php
	
    System::desire("m", 'manager', 'EmailModelo', 'src', true, '{project.rsc}');
    
    try {
      
      $i = $object->get_emo_items();
      $u = "";
      foreach ($i as $item) {
        if ($this->dao->isValid($item, 'UPDATE')) {
          $key = $item['id'];
          $value = $this->dao->renderer($item, $object->get_emo_value($key), 'UPDATE');
          $u .= "," . $key . " = " . $value . "";
        }
      }
      $updates = substr($u, 1);

      $items = $object->get_emo_items();
      $s = "";
      $conector = " AND ";
      foreach ($items as $item) {
        if ($item['pk']) {
          $key = $item['id'];
          $s = $conector.$key." = '".$object->get_emo_value($key)."'";
        }
      }
      $search = substr($s, strlen($conector));
      
      $sql = "UPDATE " . $this->table . $this->join . " SET " . $updates . " WHERE " . $search;

      $this->beforeEmailModeloDAO($object, 'set');
      
      $updateds = $this->dao->executeSQL($sql, $this->history, $validate);
      
			if($trigger === true){
				$this->afterEmailModeloDAO($object, 'set', 0, 0, $trigger);
			}
              
      if ($debug) {
        print $sql;
      }
    }catch (Exception $exception) {
      echo "Caught exception: ",  $exception->getMessage(), "\n";
    }
    
    return $updateds;
  }

  /**
   * Remove uma instancia da entidade da base de dados
   * 
   * @param object $object
   * @param boolean $validate
   * @param boolean $debug
   *
   * @author ADMINISTRADOR - 24/07/2013 12:07:25
   * <br><b>Updated by</b> ADMINISTRADOR - 24/07/2013 12:07:25
   */
  public function removeEmailModeloDAO($object, $validate = true, $debug = false){
    ?><?php
    System::desire('m', 'manager', 'EmailModelo', 'src', true, '{project.rsc}');
    
    try {
      
      $items = $object->get_emo_items();
      $properties = $object->get_emo_properties();
      
      $s = "";
      $conector = " AND ";
      foreach ($items as $item) {
        if ($item['pk']) {
          $key = $item['id'];
          $s = $conector.$key." = '".$object->get_emo_value($key)."'";
        }
      }
      $search = substr($s, strlen($conector));
      
      $sql = "DELETE FROM " . $this->table . " WHERE " . $search;
      if (isset($properties['remove'])) {
        if(is_array($properties['remove'])){

          $field = $properties['remove']['field'];
          $value = $properties['remove']['value'];
          
          $object->set_emo_value($field, $value);
        
          $sql = "UPDATE " . $this->table . " SET ". $field ." = '".$value."', ".$properties['prefix']."_responsavel = '".System::getUser()."', ".$properties['prefix']."_alteracao = NOW() WHERE " . $search;
        }
      }
      
      $this->beforeEmailModeloDAO($object, 'remove');

      $remove = $this->dao->executeSQL($sql, $this->history, $validate);
      
      $this->afterEmailModeloDAO($object, 'remove');
      
      if ($debug) {
        print $sql;
      }
    }catch (Exception $exception) {
      echo "Caught exception: ",  $exception->getMessage(), "\n";
    }
    
    return $remove;
  }

  /**
   * M�todo de gatilho executado antes de cada atualiza��o e remo��o das inst�ncias da entidade
   * 
   * @param object $object
   * @param string $param
   *
   * @author ADMINISTRADOR - 24/07/2013 12:07:25
   * <br><b>Updated by</b> ADMINISTRADOR - 24/07/2013 12:07:25
   */
  public function beforeEmailModeloDAO($object, $param){
    ?><?php
    System::desire('m', 'manager', 'EmailModelo', 'src', true, '{project.rsc}');
    
    try {
      
      $executed = false;
      
    }catch (Exception $exception) {
      echo "Caught exception: ",  $exception->getMessage(), "\n";
    }
    
    return $executed;
  }

  /**
   * M�todo de gatilho executado depois de cada atualiza��o e remo��o das inst�ncias da entidade
   * 
   * @param object $object
   * @param string $param
   * @param int $value
   * @param int $copy
   * @param boolean $trigger
   *
   * @author ADMINISTRADOR - 24/07/2013 12:07:25
   * <br><b>Updated by</b> ADMINISTRADOR - 24/07/2013 12:07:25
   */
  public function afterEmailModeloDAO($object, $param, $value = 0, $copy = 0, $trigger = true){
    ?><?php

		System::desire('m', 'manager', 'EmailModelo', 'src', true, '{project.rsc}');
    
    try {
      
      $executed = true;
			
      if($param === 'add'){
        $object->set_emo_value($object->get_emo_reference(), $value);
      }
			
			if($param === 'set'){
        $value = $object->get_emo_value($object->get_emo_reference());
      }

      if($param === 'add' or $param === 'set'){
				
				$items = $object->get_emo_items();
				
        if($trigger === true){
					$update = false;

          $emailModelo = new EmailModelo();
          foreach ($items as $item) {
            $key = $item['id'];
						
            $emailModelo->set_emo_value($key, $items[$key]['value']);
						
            if($items[$key]['type'] == 'calculated'){
							
							if($items[$key]['type_content']){
								$emailModelo->set_emo_value($key, $items[$key]['type_content']);
                $emailModelo->set_emo_type($key, 'execute');
								$update = true;
							}
							
            }else if($items[$key]['type'] === 'file'){

              if($items[$key]['value']){
                $file = File::infoContent($item['type_content']);
                
								if($file['path']){
									$id = $object->get_emo_value($object->get_emo_reference());
									$source = $object->get_emo_value($key);
	
									$location = $file['path'].$file['name'].$id.".".File::getExtension($source);
									$destin = PATH_APP.$location;
	
									if(is_writable(PATH_APP.$file['path'])){
										$copy = copy($source, $destin);
										if($copy){
											$emailModelo->set_emo_value($key, $location);
											$update = true;
										}else{
											System::showMessage(MESSAGE_FILE_NOT_COPY." ".$destin);
										}
									}else{
										System::showMessage(MESSAGE_DIR_NOT_WRITABLE." ".$destin);
									}
								}
              }

            }else{
              if(!$items[$key]['pk']){
								
                $emailModelo->set_emo_value($key, null);
								
              }
            }
          }
					if($update){
						$executed = $this->setEmailModeloDAO($emailModelo, true, false, 0, true);
					}
        }
      }
      
    }catch (Exception $exception) {
      echo "Caught exception: ",  $exception->getMessage(), "\n";
    }
    
    return $executed;
  }

  /**
   * Recupera um dado atrav�s de uma consulta personalizada
   * 
   * @param string $column
   * @param string $where
   * @param boolean $table
   * @param boolean $validate
   * @param boolean $debug
   *
   * @author ADMINISTRADOR - 24/07/2013 12:07:25
   * <br><b>Updated by</b> ADMINISTRADOR - 24/07/2013 12:07:25
   */
  public function getColumnEmailModeloDAO($column, $where, $table = "", $validate = true, $debug = false){
    ?><?php
	
    try {
      if($where != ""){
        $where = "WHERE ".$where;
      }
      if($table == ""){
        $table = $this->table.$this->join;
      }
      
      $sql = "SELECT ".$column." AS custom FROM ".$table." ".$where;
      
      if($debug){
        print $sql;
      }
      
      $result = $this->dao->selectSQL($sql, $validate);
      $custom = null;
      if($result != null){
        while($row = mysql_fetch_array($result)){
          $custom = $row['custom'];
        }
      }
    }catch (Exception $exception) {
      echo "Caught exception: ",  $exception->getMessage(), "\n";
    }
    return $custom;
  }

  /**
   * Remove um grupo de items ou atualiza uma quantidade relativamente grande 
   * 
   * @param string $where
   * @param string $update
   * @param boolean $validate
   * @param boolean $debug
   *
   * @author ADMINISTRADOR - 24/07/2013 12:07:25
   * <br><b>Updated by</b> ADMINISTRADOR - 24/07/2013 12:07:25
   */
  public function executeEmailModeloDAO($where, $update, $validate = true, $debug = false){
    ?><?php

    System::desire('m', 'manager', 'EmailModelo', 'src', true, '{project.rsc}');

    try {

      $sql = "DELETE FROM ".$this->table." WHERE ".$where;
      if($update){
        $update = $update.", emo_responsavel = '".System::getUser(false)."', emo_alteracao = NOW()";
        $sql = "UPDATE ".$this->table." SET ".$update." WHERE ".$where;
      }

      $executed = $this->dao->executeSQL($sql, $this->history, $validate);

      if ($debug) {
        print $sql;
      }

    }catch (Exception $exception) {
      echo "Caught exception: ",  $exception->getMessage(), "\n";
    }

    return $executed;
  }


}