<?php
/**
 * @copyright array software
 *
 * @author ADMINISTRADOR - 18/01/2013 11:31:53
 * <br><b>Updated by</b> ADMINISTRADOR - 24/07/2013 10:54:11
 * @category dao
 * @package manager
 *
 * Responsavel.....: ADMINISTRADOR
 * Alteracao.......: 28/07/2013 09:30:04
 */


class ParametrosDAO
{
  private  $dao;
  private  $path;
  private  $database;
  private  $table;
  private  $reference;
  private  $join;
  private  $history;

  /**
   * Construtor da classde de acesso a dados da entidade
   * 
   * @param string $path
   * @param string $database
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 02/05/2013 19:15:17
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 02/05/2013 19:15:17
   */
  public function ParametrosDAO($path, $database = ""){
    ?><?php

    System::desire('class','base', 'DAO', 'core');
    System::desire('m','manager', 'Parametros', 'src', true, '');
      
    $entity = new Parametros();
    $properties = $entity->get_par_properties();

    $this->path = $path;
    $this->table = $properties['table'];
    $this->join = $properties['join'];
    $this->reference = $properties['reference'];
    $this->database = $database;

    $this->history = true;

    $this->dao = new DAO($path, $database);
  }

  /**
   * Recupera um dado atrav�s de uma consulta personalizada
   * 
   * @param string $column
   * @param string $where
   * @param boolean $table
   * @param boolean $validate
   * @param boolean $debug
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 02/05/2013 19:15:17
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 02/05/2013 19:15:17
   */
  public function getColumnParametrosDAO($column, $where, $table = "", $validate = true, $debug = false){
    ?><?php
    try {
      if($where != ""){
        $where = "WHERE ".$where;
      }
      if($table == ""){
        $table = $this->table.$this->join;
      }
      
      $sql = "SELECT ".$column." AS custom FROM ".$table." ".$where;
      
      if($debug){
        print $sql;
      }
      
      $result = $this->dao->selectSQL($sql, $validate);
      $custom = null;
      if($result != null){
        while($row = mysql_fetch_array($result)){
          $custom = $row['custom'];
        }
      }
    }catch (Exception $exception) {
      echo "Caught exception: ",  $exception->getMessage(), "n";
    }
    return $custom;
  }

  /**
   * Recupera dados da entidade do banco de dados
   * 
   * @param string $where
   * @param string $group
   * @param string $order
   * @param string $start
   * @param string $end
   * @param boolean $validate
   * @param boolean $debug
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 02/05/2013 19:15:17
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 02/05/2013 19:15:17
   */
  public function getParametrosDAO($where, $group, $order, $start = "", $end = "", $validate = true, $debug = false){
    ?><?php
    System::desire("m", 'manager', 'Parametros', 'src', true, '');
        
    try {
      $limit = "";
      if($where != ""){
        $where = " WHERE " . $where;
      }
      if($group != ""){
        $group = " GROUP BY " . $group;
      }
      if($order != ""){
        $order = " ORDER BY " . $order;
      }
      if($start == ""){
        $start = 0;
      }
      if($end){
        $limit = " LIMIT " . $start . "," . $end;
      }
      
      $object = new Parametros();
      $items = $object->get_par_items();
  
      $f = "";
      foreach($items as $item){
        if($this->dao->isValid($item, 'SELECT')){
          $field = $item['id'];
          if($item['type'] == 'calculated'){
            $field = "(" . $item['type_content'] . ")";
          }
          $f .= "," . $field . " AS " . $item['id'];
        }
      }
      $fields = substr($f, 1);
      
      $sql = "SELECT ".$fields." FROM ".$this->table.$this->join.$where.$group.$order.$limit;

      if($debug){
        print $sql;
      }

      $result = $this->dao->selectSQL($sql, $validate);
      
      $parametross = null;
      $parametros = null;
      $i = 0;
      if($result != null){
        while($row = mysql_fetch_array($result)){
          $parametros = new Parametros();
          foreach($items as $item){
            if($this->dao->isValid($item, 'SELECT')){
              $key = $item['id'];
              $value = $this->dao->renderer($item, $row[$key], 'SELECT');
              $parametros->set_par_value($key, $value);
            }
          }

          $parametross[$i] = $parametros;
          $i++;
        }
      }
    }catch (Exception $exception) {
      echo "Caught exception: ",  $exception->getMessage(), "\n";
    }
    return $parametross;
  }

  /**
   * Recupera um valor inteiro referente ao n�mero de linhas de determina consulta
   * 
   * @param string $where
   * @param string $group
   * @param boolean $validate
   * @param boolean $debug
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 02/05/2013 19:15:17
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 02/05/2013 19:15:17
   */
  public function getCountParametrosDAO($where, $group = "", $validate = true, $debug = false){
    ?><?php
    System::desire("m", 'manager', 'Parametros', 'src', true, '');

    try {
      if($where != ""){
        $where = " WHERE " . $where;
      }
      if($group != ""){
        $group = " GROUP BY " . $group;
      }
      
      $sql = "SELECT COUNT(". $this->reference .") AS total FROM ".$this->table.$this->join.$where.$group;
      
      if($debug){
        print $sql;
      }
      
      $result = $this->dao->selectSQL($sql, $validate);
      $total = 0;
      if($result != null){
        while($row = mysql_fetch_array($result)){
          $total = $row['total'];
        }
      }
    }catch (Exception $exception) {
      echo "Caught exception: ",  $exception->getMessage(), "\n";
    }
    return $total;
  }

  /**
   * Adiciona um novo registro desta entidade na base de dados
   * 
   * @param object $object
   * @param boolean $validate
   * @param boolean $debug
   * @param boolean $presave
   * @param int $copy
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 02/05/2013 19:15:17
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 02/05/2013 19:15:17
   */
  public function addParametrosDAO($object, $validate = true, $debug = false, $presave = false, $copy = 0){
    ?><?php
    System::desire("m", 'manager', 'Parametros', 'src', true, '');
      
    try {

      $items = $object->get_par_items();
      $f = "";
      $v = ""; 
      foreach ($items as $item) {
        if ($this->dao->isValid($item, 'INSERT')) {
          $key = $item['id'];
          $value = $this->dao->renderer($item, $object->get_par_value($key), 'INSERT');
          $f .= ',' . $key;
          $v .= "," . $value . "";
        }
      }
      $fields = substr($f, 1);
      $values = substr($v, 1);

      $sql = "INSERT INTO " . $this->table . " (" . $fields . ") VALUES (" . $values . ")";

      $this->beforeParametrosDAO($object, 'add');
      
      $add = $this->dao->insertSQL($sql, $this->history, $validate, $presave);

      $this->afterParametrosDAO($object, 'add', $add, $copy);
      
      if ($debug) {
        print $sql;
      }
      
    }catch (Exception $exception) {
      echo "Caught exception: ",  $exception->getMessage(), "\n";
    }
    
    return $add;
  }

  /**
   * Atualiza uma inst�ncia da entidade na base de dados
   * 
   * @param object $object
   * @param boolean $validate
   * @param boolean $debug
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 02/05/2013 19:15:17
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 02/05/2013 19:15:17
   */
  public function setParametrosDAO($object, $validate = true, $debug = false){
    ?><?php
    System::desire("m", 'manager', 'Parametros', 'src', true, '');
    
    try {
      
      $i = $object->get_par_items();
      $u = "";
      foreach ($i as $item) {
        if ($this->dao->isValid($item, 'UPDATE')) {
          $key = $item['id'];
          $value = $this->dao->renderer($item, $object->get_par_value($key), 'UPDATE');
          $u .= "," . $key . " = " . $value . "";
        }
      }
      $updates = substr($u, 1);

      $items = $object->get_par_items();
      $s = "";
      $conector = " AND ";
      foreach ($items as $item) {
        if ($item['pk']) {
          $key = $item['id'];
          $s = $conector.$key." = '".$object->get_par_value($key)."'";
        }
      }
      $search = substr($s, strlen($conector));
      
      $sql = "UPDATE " . $this->table . " SET " . $updates . " WHERE " . $search;

      $this->beforeParametrosDAO($object, 'set');
      
      $updateds = $this->dao->executeSQL($sql, $this->history, $validate);

      $this->afterParametrosDAO($object, 'set');
              
      if ($debug) {
        print $sql;
      }
    }catch (Exception $exception) {
      echo "Caught exception: ",  $exception->getMessage(), "\n";
    }
    
    return $updateds;
  }

  /**
   * Remove uma instancia da entidade da base de dados
   * 
   * @param object $object
   * @param boolean $validate
   * @param boolean $debug
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 02/05/2013 19:15:17
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 02/05/2013 19:15:17
   */
  public function removeParametrosDAO($object, $validate = true, $debug = false){
    ?><?php
    System::desire("m", 'manager', 'Parametros', 'src', true, '');
    
    try {
      
      $items = $object->get_par_items();
      $properties = $object->get_par_properties();
      
      $s = "";
      $conector = " AND ";
      foreach ($items as $item) {
        if ($item['pk']) {
          $key = $item['id'];
          $s = $conector.$key." = '".$object->get_par_value($key)."'";
        }
      }
      $search = substr($s, strlen($conector));
      
      $sql = "DELETE FROM " . $this->table . " WHERE " . $search;
      if (isset($properties['remove'])) {
        if(is_array($properties['remove'])){

          $field = $properties['remove']['field'];
          $value = $properties['remove']['value'];
          
          $object->set_par_value($field, $value);
        
          $sql = "UPDATE " . $this->table . " SET ". $field ." = '".$value."', ".$properties['prefix']."_responsavel = '".System::getUser(false)."', ".$properties['prefix']."_alteracao = NOW() WHERE " . $search;
        }
      }
      
      $this->beforeParametrosDAO($object, 'remove');

      $remove = $this->dao->executeSQL($sql, $this->history, $validate);
      
      $this->afterParametrosDAO($object, 'remove');
      
      if ($debug) {
        print $sql;
      }
    }catch (Exception $exception) {
      echo "Caught exception: ",  $exception->getMessage(), "\n";
    }
    
    return $remove;
  }

  /**
   * M�todo de gatilho executado antes de cada atualiza��o e remo��o das inst�ncias da entidade
   * 
   * @param object $object
   * @param string $param
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 02/05/2013 19:15:17
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 02/05/2013 19:15:17
   */
  public function beforeParametrosDAO($object, $param){
    ?><?php
    System::desire("m", 'manager', 'Parametros', 'src', true, '');
    
    try {
      
      $executed = false;
      
    }catch (Exception $exception) {
      echo "Caught exception: ",  $exception->getMessage(), "\n";
    }
    
    return $executed;
  }

  /**
   * M�todo de gatilho executado depois de cada atualiza��o e remo��o das inst�ncias da entidade
   * 
   * @param object $object
   * @param string $param
   * @param int $value
   * @param int $copy
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 02/05/2013 19:15:17
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 02/05/2013 19:15:17
   */
  public function afterParametrosDAO($object, $param, $value = 0, $copy = 0){
    ?><?php
    System::desire("m", 'manager', 'Parametros', 'src', true, '');
    
    try {
      
      $executed = false;
      
    }catch (Exception $exception) {
      echo "Caught exception: ",  $exception->getMessage(), "\n";
    }
    
    return $executed;
  }


}