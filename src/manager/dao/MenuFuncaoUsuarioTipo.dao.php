<?php

/**
 * @copyright array software
 *
 * @author ADMINISTRADOR - 16/06/2013 10:06:05
 * <br><b>Updated by</b> ADMINISTRADOR - 17/06/2013 20:31:12
 * @category dao
 * @package manager
 *
 * Responsavel.....: ADMINISTRADOR
 * Alteracao.......: 28/07/2013 09:30:03
 */
class MenuFuncaoUsuarioTipoDAO {

  private $dao;
  private $path;
  private $database;
  private $table;
  private $reference;
  private $join;
  private $history;

  /**
   * Construtor da classde de acesso a dados da entidade
   * 
   * @param string $path
   * @param string $database
   *
   * @author ADMINISTRADOR - 16/06/2013 15:42:51
   * <br><b>Updated by</b> ADMINISTRADOR - 16/06/2013 15:42:51
   */
  public function MenuFuncaoUsuarioTipoDAO($path, $database = "") {
?><?php

    System::desire('class', 'base', 'DAO', 'core');
    System::desire('m', 'manager', 'MenuFuncaoUsuarioTipo', 'src', true, '{project.rsc}');

    $entity = new MenuFuncaoUsuarioTipo();
    $properties = $entity->get_mft_properties();

    $this->path = $path;
    $this->table = $properties['table'];
    $this->join = $properties['join'];
    $this->reference = $properties['reference'];
    $this->database = $database;

    $this->history = true;

    $this->dao = new DAO($path, $database);
  }

  /**
   * Recupera dados da entidade do banco de dados
   * 
   * @param string $where
   * @param string $group
   * @param string $order
   * @param string $start
   * @param string $end
   * @param boolean $validate
   * @param boolean $debug
   *
   * @author ADMINISTRADOR - 16/06/2013 15:42:51
   * <br><b>Updated by</b> ADMINISTRADOR - 16/06/2013 15:42:51
   */
  public function getMenuFuncaoUsuarioTipoDAO($where, $group, $order, $start = "", $end = "", $validate = true, $debug = false) {
?><?php

    System::desire("m", 'manager', 'MenuFuncaoUsuarioTipo', 'src', true, '{project.rsc}');

    try {
      $limit = "";
      if ($where != "") {
        $where = " WHERE " . $where;
      }
      if ($group != "") {
        $group = " GROUP BY " . $group;
      }
      if ($order != "") {
        $order = " ORDER BY " . $order;
      }
      if ($start == "") {
        $start = 0;
      }
      if ($end) {
        $limit = " LIMIT " . $start . "," . $end;
      }

      $object = new MenuFuncaoUsuarioTipo();
      $items = $object->get_mft_items();

      $f = "";
      foreach ($items as $item) {
        if ($this->dao->isValid($item, 'SELECT')) {
          $field = $item['id'];
          if ($item['type'] == 'calculated') {
            $field = "(" . $item['type_content'] . ")";
          }
          $f .= "," . $field . " AS " . $item['id'];
        }
      }
      $fields = substr($f, 1);

      $sql = "SELECT " . $fields . " FROM " . $this->table . $this->join . $where . $group . $order . $limit;

      if ($debug) {
        print $sql;
      }

      $result = $this->dao->selectSQL($sql, $validate);

      $menuFuncaoUsuarioTipos = null;
      $menuFuncaoUsuarioTipo = null;
      $i = 0;
      if ($result != null) {
        while ($row = mysql_fetch_array($result)) {
          $menuFuncaoUsuarioTipo = new MenuFuncaoUsuarioTipo();
          foreach ($items as $item) {
            if ($this->dao->isValid($item, 'SELECT')) {
              $key = $item['id'];
              $value = $this->dao->renderer($item, $row[$key], 'SELECT');
              $menuFuncaoUsuarioTipo->set_mft_value($key, $value);
            }
          }

          $menuFuncaoUsuarioTipos[$i] = $menuFuncaoUsuarioTipo;
          $i++;
        }
      }
    } catch (Exception $exception) {
      echo "Caught exception: ", $exception->getMessage(), "
";
    }
    return $menuFuncaoUsuarioTipos;
  }

  /**
   * Recupera um valor inteiro referente ao n�mero de linhas de determina consulta
   * 
   * @param string $where
   * @param string $group
   * @param boolean $validate
   * @param boolean $debug
   *
   * @author ADMINISTRADOR - 16/06/2013 15:42:51
   * <br><b>Updated by</b> ADMINISTRADOR - 16/06/2013 15:42:51
   */
  public function getCountMenuFuncaoUsuarioTipoDAO($where, $group = "", $validate = true, $debug = false) {
?><?php

    System::desire("m", 'manager', 'MenuFuncaoUsuarioTipo', 'src', true, '{project.rsc}');

    try {
      if ($where != "") {
        $where = " WHERE " . $where;
      }
      if ($group != "") {
        $group = " GROUP BY " . $group;
      }

      $sql = "SELECT COUNT(" . $this->reference . ") AS total FROM " . $this->table . $this->join . $where . $group;

      if ($debug) {
        print $sql;
      }

      $result = $this->dao->selectSQL($sql, $validate);
      $total = 0;
      if ($result != null) {
        while ($row = mysql_fetch_array($result)) {
          $total = $row['total'];
        }
      }
    } catch (Exception $exception) {
      echo "Caught exception: ", $exception->getMessage(), "
";
    }
    return $total;
  }

  /**
   * Adiciona um novo registro desta entidade na base de dados
   * 
   * @param object $object
   * @param boolean $validate
   * @param boolean $debug
   * @param boolean $presave
   * @param int $copy
   *
   * @author ADMINISTRADOR - 16/06/2013 15:42:51
   * <br><b>Updated by</b> ADMINISTRADOR - 16/06/2013 15:42:51
   */
  public function addMenuFuncaoUsuarioTipoDAO($object, $validate = true, $debug = false, $presave = false, $copy = 0) {
?><?php

    System::desire("m", 'manager', 'MenuFuncaoUsuarioTipo', 'src', true, '{project.rsc}');

    try {

      $items = $object->get_mft_items();
      $f = "";
      $v = "";
      foreach ($items as $item) {
        if ($this->dao->isValid($item, 'INSERT')) {
          $key = $item['id'];
          $value = $this->dao->renderer($item, $object->get_mft_value($key), 'INSERT');
          $f .= ',' . $key;
          $v .= "," . $value . "";
        }
      }
      $fields = substr($f, 1);
      $values = substr($v, 1);

      $sql = "INSERT INTO " . $this->table . " (" . $fields . ") VALUES (" . $values . ")";

      $this->beforeMenuFuncaoUsuarioTipoDAO($object, 'add');

      $add = $this->dao->insertSQL($sql, $this->history, $validate, $presave);

      $this->afterMenuFuncaoUsuarioTipoDAO($object, 'add', $add, $copy);

      if ($debug) {
        print $sql;
      }
    } catch (Exception $exception) {
      echo "Caught exception: ", $exception->getMessage(), "
";
    }

    return $add;
  }

  /**
   * Atualiza uma inst�ncia da entidade na base de dados
   * 
   * @param object $object
   * @param boolean $validate
   * @param boolean $debug
   * @param boolean $trigger
   *
   * @author ADMINISTRADOR - 16/06/2013 15:42:51
   * <br><b>Updated by</b> ADMINISTRADOR - 16/06/2013 15:42:51
   */
  public function setMenuFuncaoUsuarioTipoDAO($object, $validate = true, $debug = false, $trigger = true) {
?><?php

    System::desire("m", 'manager', 'MenuFuncaoUsuarioTipo', 'src', true, '{project.rsc}');

    try {

      $i = $object->get_mft_items();
      $u = "";
      foreach ($i as $item) {
        if ($this->dao->isValid($item, 'UPDATE')) {
          $key = $item['id'];
          $value = $this->dao->renderer($item, $object->get_mft_value($key), 'UPDATE');
          $u .= "," . $key . " = " . $value . "";
        }
      }
      $updates = substr($u, 1);

      $items = $object->get_mft_items();
      $s = "";
      $conector = " AND ";
      foreach ($items as $item) {
        if ($item['pk']) {
          $key = $item['id'];
          $s = $conector . $key . " = '" . $object->get_mft_value($key) . "'";
        }
      }
      $search = substr($s, strlen($conector));

      $sql = "UPDATE " . $this->table . $this->join . " SET " . $updates . " WHERE " . $search;

      $this->beforeMenuFuncaoUsuarioTipoDAO($object, 'set');

      $updateds = $this->dao->executeSQL($sql, $this->history, $validate);

      if ($trigger === true) {
        $this->afterMenuFuncaoUsuarioTipoDAO($object, 'set', 0, 0, $trigger);
      }

      if ($debug) {
        print $sql;
      }
    } catch (Exception $exception) {
      echo "Caught exception: ", $exception->getMessage(), "n";
    }

    return $updateds;
  }

  /**
   * Remove uma instancia da entidade da base de dados
   * 
   * @param object $object
   * @param boolean $validate
   * @param boolean $debug
   *
   * @author ADMINISTRADOR - 16/06/2013 15:42:51
   * <br><b>Updated by</b> ADMINISTRADOR - 16/06/2013 15:42:51
   */
  public function removeMenuFuncaoUsuarioTipoDAO($object, $validate = true, $debug = false) {
?><?php

    System::desire("m", 'manager', 'MenuFuncaoUsuarioTipo', 'src', true, '{project.rsc}');

    try {

      $items = $object->get_mft_items();
      $properties = $object->get_mft_properties();

      $s = "";
      $conector = " AND ";
      foreach ($items as $item) {
        if ($item['pk']) {
          $key = $item['id'];
          $s = $conector . $key . " = '" . $object->get_mft_value($key) . "'";
        }
      }
      $search = substr($s, strlen($conector));

      $sql = "DELETE FROM " . $this->table . " WHERE " . $search;
      if (isset($properties['remove'])) {
        if (is_array($properties['remove'])) {

          $field = $properties['remove']['field'];
          $value = $properties['remove']['value'];

          $object->set_mft_value($field, $value);

          $sql = "UPDATE " . $this->table . " SET " . $field . " = '" . $value . "', " . $properties['prefix'] . "_responsavel = '" . System::getUser() . "', " . $properties['prefix'] . "_alteracao = NOW() WHERE " . $search;
        }
      }

      $this->beforeMenuFuncaoUsuarioTipoDAO($object, 'remove');

      $remove = $this->dao->executeSQL($sql, $this->history, $validate);

      $this->afterMenuFuncaoUsuarioTipoDAO($object, 'remove');

      if ($debug) {
        print $sql;
      }
    } catch (Exception $exception) {
      echo "Caught exception: ", $exception->getMessage(), "
";
    }

    return $remove;
  }

  /**
   * M�todo de gatilho executado antes de cada atualiza��o e remo��o das inst�ncias da entidade
   * 
   * @param object $object
   * @param string $param
   *
   * @author ADMINISTRADOR - 16/06/2013 15:42:51
   * <br><b>Updated by</b> ADMINISTRADOR - 16/06/2013 15:42:51
   */
  public function beforeMenuFuncaoUsuarioTipoDAO($object, $param) {
?><?php

    System::desire("m", 'manager', 'MenuFuncaoUsuarioTipo', 'src', true, '{project.rsc}');

    try {

      $executed = false;
    } catch (Exception $exception) {
      echo "Caught exception: ", $exception->getMessage(), "
";
    }

    return $executed;
  }

  /**
   * M�todo de gatilho executado depois de cada atualiza��o e remo��o das inst�ncias da entidade
   * 
   * @param object $object
   * @param string $param
   * @param int $value
   * @param int $copy
   * @param boolean $trigger
   *
   * @author ADMINISTRADOR - 16/06/2013 15:42:51
   * <br><b>Updated by</b> ADMINISTRADOR - 16/06/2013 15:42:51
   */
  public function afterMenuFuncaoUsuarioTipoDAO($object, $param, $value = 0, $copy = 0, $trigger = true) {
?><?php

    System::desire("m", 'manager', 'MenuFuncaoUsuarioTipo', 'src', true, '{project.rsc}');

    try {

      $executed = true;

      if ($param === 'add') {
        $object->set_mft_value($object->get_mft_reference(), $value);
      }

      if ($param === 'set') {
        $value = $object->get_mft_value($object->get_mft_reference());
      }

      if ($param === 'add' or $param === 'set') {

        $items = $object->get_mft_items();

        if ($trigger === true) {
          $update = false;

          $menuFuncaoUsuarioTipo = new MenuFuncaoUsuarioTipo();
          foreach ($items as $item) {
            $key = $item['id'];

            $menuFuncaoUsuarioTipo->set_mft_value($key, $items[$key]['value']);

            if ($items[$key]['type'] == 'calculated') {

              if ($items[$key]['type_content']) {
                $menuFuncaoUsuarioTipo->set_mft_value($key, $items[$key]['type_content']);
                $menuFuncaoUsuarioTipo->set_mft_type($key, 'execute');
                $update = true;
              }
            } else if ($items[$key]['type'] === 'file') {

              if ($items[$key]['value']) {
                $file = File::infoContent($item['type_content']);

                if ($file['path']) {
                  $id = $object->get_mft_value($object->get_mft_reference());
                  $source = $object->get_mft_value($key);

                  $location = $file['path'] . $file['name'] . $id . "." . File::getExtension($source);
                  $destin = PATH_APP . $location;

                  if (is_writable(PATH_APP . $file['path'])) {
                    $copy = copy($source, $destin);
                    if ($copy) {
                      $menuFuncaoUsuarioTipo->set_mft_value($key, $location);
                      $update = true;
                    } else {
                      System::showMessage(MESSAGE_FILE_NOT_COPY . " " . $destin);
                    }
                  } else {
                    System::showMessage(MESSAGE_DIR_NOT_WRITABLE . " " . $destin);
                  }
                }
              }
            } else {
              if (!$items[$key]['pk']) {

                $menuFuncaoUsuarioTipo->set_mft_value($key, null);
              }
            }
          }
          if ($update) {
            $executed = $this->setMenuFuncaoUsuarioTipoDAO($menuFuncaoUsuarioTipo, true, false, 0, true);
          }
        }
      }
    } catch (Exception $exception) {
      echo "Caught exception: ", $exception->getMessage(), "
";
    }

    return $executed;
  }

  /**
   * Recupera um dado atrav�s de uma consulta personalizada
   * 
   * @param string $column
   * @param string $where
   * @param boolean $table
   * @param boolean $validate
   * @param boolean $debug
   *
   * @author ADMINISTRADOR - 16/06/2013 15:42:52
   * <br><b>Updated by</b> ADMINISTRADOR - 16/06/2013 15:42:52
   */
  public function getColumnMenuFuncaoUsuarioTipoDAO($column, $where, $table = "", $validate = true, $debug = false) {
?><?php

    try {
      if ($where != "") {
        $where = "WHERE " . $where;
      }
      if ($table == "") {
        $table = $this->table . $this->join;
      }

      $sql = "SELECT " . $column . " AS custom FROM " . $table . " " . $where;

      if ($debug) {
        print $sql;
      }

      $result = $this->dao->selectSQL($sql, $validate);
      $custom = null;
      if ($result != null) {
        while ($row = mysql_fetch_array($result)) {
          $custom = $row['custom'];
        }
      }
    } catch (Exception $exception) {
      echo "Caught exception: ", $exception->getMessage(), "n";
    }
    return $custom;
  }

  /**
   * Remove um grupo de items ou atualiza uma quantidade relativamente grande 
   * 
   * @param string $where
   * @param string $update
   * @param boolean $validate
   * @param boolean $debug
   *
   * @author ADMINISTRADOR - 16/06/2013 15:42:52
   * <br><b>Updated by</b> ADMINISTRADOR - 16/06/2013 15:42:52
   */
  public function executeMenuFuncaoUsuarioTipoDAO($where, $update, $validate = true, $debug = false) {
?><?php

    System::desire('m', 'manager', 'MenuFuncaoUsuarioTipo', 'src', true, '{project.rsc}');

    try {

      $sql = "DELETE FROM " . $this->table . " WHERE " . $where;
      if ($update) {
        $update = $update . ", mft_responsavel = '" . System::getUser(false) . "', mft_alteracao = NOW()";
        $sql = "UPDATE " . $this->table . " SET " . $update . " WHERE " . $where;
      }

      $executed = $this->dao->executeSQL($sql, $this->history, $validate);

      if ($debug) {
        print $sql;
      }
    } catch (Exception $exception) {
      echo "Caught exception: ", $exception->getMessage(), "n";
    }

    return $executed;
  }

  /**
   * Recupera as op��es dispon�veis para cada tipo de acesso
   * 
   * @param int $ust_codigo
   *
   * @author ADMINISTRADOR - 16/06/2013 15:46:40
   * <br><b>Updated by</b> ADMINISTRADOR - 17/06/2013 20:31:10
   */
  public function getOptionMenuFuncaoUsuarioTipoDAO($ust_codigo) {
    ?><?php

    System::desire('m', 'manager', 'MenuFuncaoUsuarioTipo', 'src');
    System::desire('m', 'manager', 'Menu', 'src');
    System::desire('m', 'manager', 'MenuFuncao', 'src');
    System::desire('m', 'manager', 'UsuarioTipo', 'src');

    try {

      $sql = "SELECT men_codigo, men_descricao, mef_codigo, mef_descricao, mef_module, mef_entity, mef_action, mef_separator FROM TBL_MENU JOIN TBL_MENU_FUNCAO ON (mef_cod_MENU = men_codigo) JOIN TBL_MENU_FUNCAO_USUARIO_TIPO ON (mft_cod_MENU_FUNCAO = mef_codigo) WHERE men_ativo = '1' AND mef_ativo = '1' AND mft_cod_USUARIO_TIPO = '" . $ust_codigo . "' ORDER BY men_ordem, mef_order";

      $result = $this->dao->selectSQL($sql, false);

      $menus = array();
      if ($result != null) {
        while ($row = mysql_fetch_array($result)) {
          $menu = new Menu();
          $menu->set_men_value('men_codigo', $row['men_codigo']);
          $menu->set_men_value('men_descricao', $row['men_descricao']);

          $menuFuncao = new MenuFuncao();
          $menuFuncao->set_mef_value('mef_codigo', $row['mef_codigo']);
          $menuFuncao->set_mef_value('mef_descricao', $row['mef_descricao']);
          $menuFuncao->set_mef_value('mef_module', $row['mef_module']);
          $menuFuncao->set_mef_value('mef_entity', $row['mef_entity']);
          $menuFuncao->set_mef_value('mef_action', $row['mef_action']);
          $menuFuncao->set_mef_value('mef_separator', $row['mef_separator']);

          $menus[$row['men_codigo']]['menu'] = $menu;
          $menus[$row['men_codigo']]['option'][$row['mef_codigo']] = $menuFuncao;
        }
      }
    } catch (Exception $exception) {
      echo "Caught exception: ", $exception->getMessage(), "\n";
    }
    return $menus;
  }

}
