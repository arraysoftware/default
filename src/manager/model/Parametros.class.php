<?php
/**
 * @copyright array software
 *
 * @author ADMINISTRADOR - 18/01/2013 11:31:53
 * <br><b>Updated by</b> ADMINISTRADOR - 24/07/2013 10:54:11
 * @category model
 * @package manager
 *
 * Responsavel.....: ADMINISTRADOR
 * Alteracao.......: 28/07/2013 09:30:03
 */


class Parametros
{
  private  $par_items = array();
  private  $par_properties = array();

  /**
   * Construtor do Modelo da Classe
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 02/05/2013 19:15:17
   * <br><b>Updated by</b> ADMINISTRADOR - 16/06/2013 17:06:34
   */
  public function Parametros(){
?><?php

    $this->par_items = array();
    
    // Atributos
    $this->par_items["par_codigo"] = array("pk"=>1, "fk"=>0, "id"=>"par_codigo", "description"=>"C�digo", "title"=>"", "type"=>"pk", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"", "update"=>0, "insert"=>0, "line"=>1, );
    $this->par_items["par_variavel"] = array("pk"=>0, "fk"=>0, "id"=>"par_variavel", "description"=>"Vari�vel", "title"=>"", "type"=>"string", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>1, "grid"=>1, "grid_width"=>"0", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "update"=>1, "insert"=>1, "line"=>2, );
    $this->par_items["par_string"] = array("pk"=>0, "fk"=>0, "id"=>"par_string", "description"=>"String", "title"=>"", "type"=>"yes/no", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>0, "grid"=>1, "grid_width"=>"0", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "update"=>1, "insert"=>1, "line"=>3, );
    $this->par_items["par_descricao"] = array("pk"=>0, "fk"=>0, "id"=>"par_descricao", "description"=>"Descri��o", "title"=>"", "type"=>"string", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>1, "grid"=>1, "grid_width"=>"0", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "update"=>1, "insert"=>1, "line"=>4, );
    $this->par_items["par_valor"] = array("pk"=>0, "fk"=>0, "id"=>"par_valor", "description"=>"Valor", "title"=>"", "type"=>"string", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "update"=>1, "insert"=>1, "line"=>5, );
    $this->par_items["par_procedimento"] = array("pk"=>0, "fk"=>0, "id"=>"par_procedimento", "description"=>"Procedimento", "title"=>"", "type"=>"base64", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "update"=>1, "insert"=>1, "line"=>6, );
    $this->par_items["par_version"] = array("pk"=>0, "fk"=>0, "id"=>"par_version", "description"=>"Vers�o", "title"=>"Vers�o do Aplicativo", "type"=>"string", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>1, "default_view"=>"".VERSION."", "default_sql"=>"".VERSION."", "update"=>1, "insert"=>1, "line"=>7, );


    // Atributos FK


    // Atributos CHILD

    
    // Atributos padrao
    $this->par_items['par_alteracao'] = array('pk'=>false, 'fk'=>false, 'id'=>'par_alteracao', 'description'=>'Altera��o', 'title'=>'', 'type'=>'alteracao', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>true, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>true, 'insert'=>true, 'line'=>0);
    $this->par_items['par_registro'] = array('pk'=>false, 'fk'=>false, 'id'=>'par_registro', 'description'=>'Registro', 'title'=>'', 'type'=>'registro', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>true, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>false, 'insert'=>true, 'line'=>0);
    $this->par_items['par_responsavel'] = array('pk'=>false, 'fk'=>false, 'id'=>'par_responsavel', 'description'=>'Respons�vel', 'title'=>'', 'type'=>'responsavel', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>true, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>true, 'insert'=>true, 'line'=>0);
    $this->par_items['par_criador'] = array('pk'=>false, 'fk'=>false, 'id'=>'par_criador', 'description'=>'Criador', 'title'=>'', 'type'=>'criador', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>true, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>false, 'insert'=>true, 'line'=>0);

    $lines = 0;
    foreach($this->par_items as $item){
      $lines = ($item['line'] > $lines) ? $item['line'] : $lines;
    }
    
    $j = array();
    foreach($this->par_items as $item){
      if($item['fk']){
        if(isset($item['foreign']) or isset($item['parent'])){
          $table = "";
					$key = "";
					if(isset($item['foreign'])){
						$table = $item['foreign']['table'];
						$key = $item['foreign']['key'];
					}else if(isset($item['parent'])){
						$table = $item['parent']['table'];
						$key = $item['parent']['key'];
					}
          $j[$table] = " LEFT JOIN ".$table." ON (".$item['id']." = ".$key.") ";
        }
      }
    }
    $join = " ".join(' ', $j);

    $this->par_properties = array(
      'module'=>'manager',
      'entity'=>'Parametros',
      'table'=>'TBL_PARAMETROS',
			'join'=>$join,
      'tag'=>'parametros',
      'prefix'=>'par',
      'order'=>'',
      'group'=>'',
      'where'=>'',
      'search'=>'',
      'layout'=>'',
			'checkbox'=>false,
      'saveonly'=>false,//desabilita a edi��o de entidade
      'readonly'=>false,//desabilita a cria��o de novos registros
      'remove'=>'',//false or
			 /**
        * array(
        *  "field"=>"prefix_id",
        *  "value"=>"1",
        *  "className"=>"icon-remove",
        *  "title"=>"Excluir",
        *  "message"=>"Deseja realmente remover este registro?",
        *  "success"=>"Registro removido com sucesso"
        * )
        */
			'database'=>null,
      'reference'=>'par_codigo',
      'description'=>'par_descricao',
      'lines'=>$lines
    );
    
    if (!$this->par_properties['reference']) {
      foreach ($this->par_items as $id=>$array) {
        if ($array['pk'] == 1) {
          $this->par_properties['reference'] = $id;
          break;
        }
      }
    }
    if (!$this->par_properties['description']) {
      foreach ($this->par_items as $id=>$array) {
        if ($array['type'] == "string") {
          $this->par_properties['reference'] = $id;
          break;
        }
      }
    }
  }

  /**
   * Recupera as principais propriedades da entidade
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 02/05/2013 19:15:17
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 02/05/2013 19:15:17
   */
  public function get_par_properties(){
    ?><?php
    return $this->par_properties;
  }

  /**
   * Recupera todos os atributos da entidade
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 02/05/2013 19:15:17
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 02/05/2013 19:15:17
   */
  public function get_par_items(){
    ?><?php
    return $this->par_items;
  }

  /**
   * Recupera um atributo da entidade com todas as suas propriedades
   * 
   * @param string $key
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 02/05/2013 19:15:17
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 02/05/2013 19:15:17
   */
  public function get_par_item($key){
    ?><?php
    return $this->par_items[$key];
  }

  /**
   * 
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 02/05/2013 19:15:17
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 02/05/2013 19:15:17
   */
  public function get_par_reference(){
    ?><?php
    $key = $this->par_properties['reference'];
    return $this->paritems[$key];
  }

  /**
   * Recupera o valor de um objeto
   * 
   * @param string $key
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 02/05/2013 19:15:17
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 02/05/2013 19:15:17
   */
  public function get_par_value($key){
    ?><?php
    return $this->par_items[$key]['value'];
  }

  /**
   * Altera o valor de um atributo da inst�ncia da entidade
   * 
   * @param string $key
   * @param object $value
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 02/05/2013 19:15:17
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 02/05/2013 19:15:17
   */
  public function set_par_value($key, $value){
    ?><?php
    $this->par_items[$key]['value'] = $value;
  }


}