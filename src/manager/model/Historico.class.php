<?php
/**
 * @copyright array software
 *
 * @author WILLIAM MARQUES VICENTE GOMES CORREA - 01/05/2013 09:09:56
 * <br><b>Updated by</b> ADMINISTRADOR - 15/06/2013 19:51:55
 * @category model
 * @package manager
 *
 * Responsavel.....: ADMINISTRADOR
 * Alteracao.......: 28/07/2013 09:30:03
 */


class Historico
{
  private  $hst_items = array();
  private  $hst_properties = array();

  /**
   * Construtor do Modelo da Classe
   * 
   *
   * @author ADMINISTRADOR - 15/06/2013 19:39:02
   * <br><b>Updated by</b> ADMINISTRADOR - 15/06/2013 19:56:39
   */
  public function Historico(){
?><?php
    $this->hst_items = array();
    
    // Atributos
    $this->hst_items["hst_codigo"] = array("pk"=>1, "fk"=>0, "id"=>"hst_codigo", "description"=>"C�digo", "title"=>"Chave prim�ria", "type"=>"pk", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"", "update"=>0, "insert"=>0, "line"=>1, );
    $this->hst_items["hst_tipo"] = array("pk"=>0, "fk"=>0, "id"=>"hst_tipo", "description"=>"Tipo", "title"=>"Determina a natureza do comando que ser� armazanado", "type"=>"string", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"", "update"=>1, "insert"=>1, "line"=>2, );
    $this->hst_items["hst_tabela"] = array("pk"=>0, "fk"=>0, "id"=>"hst_tabela", "description"=>"Tabela", "title"=>"Especifica em que tabela foi feita a altera��o", "type"=>"string", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"", "update"=>1, "insert"=>1, "line"=>3, );
    $this->hst_items["hst_comando"] = array("pk"=>0, "fk"=>0, "id"=>"hst_comando", "description"=>"Comando", "title"=>"Comando que foi processado", "type"=>"text", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>1, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"", "update"=>1, "insert"=>1, "line"=>4, );
    $this->hst_items["hst_info"] = array("pk"=>0, "fk"=>0, "id"=>"hst_info", "description"=>"Detalhes", "title"=>"Informa��es resultantes do processamento do comando", "type"=>"string", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"", "update"=>1, "insert"=>1, "line"=>5, );
    $this->hst_items["hst_backup"] = array("pk"=>0, "fk"=>0, "id"=>"hst_backup", "description"=>"Backup", "title"=>"Um backup � feito quando um comando processa no m�ximo 10 registros", "type"=>"text", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>1, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"", "update"=>1, "insert"=>1, "line"=>6, );
    $this->hst_items["hst_ip"] = array("pk"=>0, "fk"=>0, "id"=>"hst_ip", "description"=>"Endere�o IP", "title"=>"Armazena o endere�o IP do usu�rio que est� fazendo a altera��o", "type"=>"string", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>1, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"", "update"=>1, "insert"=>1, "line"=>7, );


    // Atributos FK


    // Atributos CHILD

    
    // Atributos padrao
    $this->hst_items['hst_alteracao'] = array('pk'=>false, 'fk'=>false, 'id'=>'hst_alteracao', 'description'=>'Altera��o', 'title'=>'', 'type'=>'alteracao', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>true, 'insert'=>true, 'line'=>0);
    $this->hst_items['hst_registro'] = array('pk'=>false, 'fk'=>false, 'id'=>'hst_registro', 'description'=>'Registro', 'title'=>'', 'type'=>'registro', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>false, 'insert'=>true, 'line'=>0);
    $this->hst_items['hst_responsavel'] = array('pk'=>false, 'fk'=>false, 'id'=>'hst_responsavel', 'description'=>'Respons�vel', 'title'=>'', 'type'=>'responsavel', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>true, 'insert'=>true, 'line'=>0);
    $this->hst_items['hst_criador'] = array('pk'=>false, 'fk'=>false, 'id'=>'hst_criador', 'description'=>'Criador', 'title'=>'', 'type'=>'criador', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>false, 'insert'=>true, 'line'=>0);

    $lines = 0;
    foreach($this->hst_items as $item){
      $lines = ($item['line'] > $lines) ? $item['line'] : $lines;
    }
     
    $j = array();
    foreach($this->hst_items as $item){
      if($item['fk']){
        if(isset($item['foreign']) or isset($item['parent'])){
          $table = "";
					$key = "";
					if(isset($item['foreign'])){
						$table = $item['foreign']['table'];
						$key = $item['foreign']['key'];
					}else if(isset($item['parent'])){
						$table = $item['parent']['table'];
						$key = $item['parent']['key'];
					}
          $j[$table] = " LEFT JOIN ".$table." ON (".$item['id']." = ".$key.") ";
        }
      }
    }
    $join = " ".join(' ', $j);
    
    $this->hst_properties = array(
      'module'=>'manager',
      'entity'=>'Historico',
      'table'=>'TBL_HISTORICO',
			'join'=>$join,
      'tag'=>'historico',
      'prefix'=>'hst',
      'order'=>'',
      'group'=>'',
      'where'=>'',
      'search'=>'',
      'layout'=>'',
			'checkbox'=>false,
      'saveonly'=>false,//desabilita a edi��o de entidade
      'readonly'=>true,//desabilita a cria��o de novos registros
      'remove'=>false,//false or
			 /**
        * array(
        *  "field"=>"prefix_id",
        *  "value"=>"1",
        *  "className"=>"icon-remove",
        *  "title"=>"Excluir",
        *  "message"=>"Deseja realmente remover este registro?",
        *  "success"=>"Registro removido com sucesso"
        * )
        */
			'database'=>null,
      'reference'=>'hst_codigo',
      'description'=>'hst_tabela',
      'lines'=>$lines
    );
    
    if (!$this->hst_properties['reference']) {
      foreach ($this->hst_items as $id=>$array) {
        if ($array['pk'] == 1) {
          $this->hst_properties['reference'] = $id;
          break;
        }
      }
    }
    if (!$this->hst_properties['description']) {
      foreach ($this->hst_items as $id=>$array) {
        if ($array['type'] == "string") {
          $this->hst_properties['reference'] = $id;
          break;
        }
      }
    }
  }

  /**
   * Recupera as principais propriedades da entidade
   * 
   *
   * @author ADMINISTRADOR - 15/06/2013 19:39:02
   * <br><b>Updated by</b> ADMINISTRADOR - 15/06/2013 19:39:02
   */
  public function get_hst_properties(){
    ?><?php
    return $this->hst_properties;
  }

  /**
   * Recupera todos os atributos da entidade
   * 
   *
   * @author ADMINISTRADOR - 15/06/2013 19:39:02
   * <br><b>Updated by</b> ADMINISTRADOR - 15/06/2013 19:39:02
   */
  public function get_hst_items(){
    ?><?php
    return $this->hst_items;
  }

  /**
   * Recupera um atributo da entidade com todas as suas propriedades
   * 
   * @param string $key
   *
   * @author ADMINISTRADOR - 15/06/2013 19:39:02
   * <br><b>Updated by</b> ADMINISTRADOR - 15/06/2013 19:39:02
   */
  public function get_hst_item($key){
    ?><?php
    return $this->hst_items[$key];
  }

  /**
   * 
   * 
   *
   * @author ADMINISTRADOR - 15/06/2013 19:39:02
   * <br><b>Updated by</b> ADMINISTRADOR - 15/06/2013 19:39:02
   */
  public function get_hst_reference(){
    ?><?php
    $key = $this->hst_properties['reference'];
    return $key;
  }

  /**
   * Recupera o valor de um objeto
   * 
   * @param string $key
   *
   * @author ADMINISTRADOR - 15/06/2013 19:39:02
   * <br><b>Updated by</b> ADMINISTRADOR - 15/06/2013 19:39:02
   */
  public function get_hst_value($key){
    ?><?php
    return $this->hst_items[$key]['value'];
  }

  /**
   * Altera o valor de um atributo da inst�ncia da entidade
   * 
   * @param string $key
   * @param object $value
   *
   * @author ADMINISTRADOR - 15/06/2013 19:39:02
   * <br><b>Updated by</b> ADMINISTRADOR - 15/06/2013 19:39:02
   */
  public function set_hst_value($key, $value){
    ?><?php
    $this->hst_items[$key]['value'] = $value;
  }

  /**
   * Altera o tipo de um atributo da inst�ncia da entidade
   * 
   * @param string $key
   * @param string $type
   *
   * @author ADMINISTRADOR - 15/06/2013 19:39:02
   * <br><b>Updated by</b> ADMINISTRADOR - 15/06/2013 19:39:02
   */
  public function set_hst_type($key, $type){
    ?><?php
    $this->hst_items[$key]['type'] = $type;
  }


}