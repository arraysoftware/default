<?php
/**
 * @copyright array software
 *
 * @author WILLIAM MARQUES VICENTE GOMES CORREA - 21/04/2013 22:45:55
 * <br><b>Updated by</b> ADMINISTRADOR - 15/06/2013 18:51:58
 * @category model
 * @package manager
 *
 * Responsavel.....: ADMINISTRADOR
 * Alteracao.......: 28/07/2013 09:30:02
 */


class UsuarioTipo
{
  private  $ust_items = array();
  private  $ust_properties = array();

  /**
   * Construtor do Modelo da Classe
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 28/05/2013 18:39:13
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 28/05/2013 18:39:13
   */
  public function UsuarioTipo(){
    ?><?php
    $this->ust_items = array();
    
    // Atributos
    $this->ust_items["ust_codigo"] = array("pk"=>1, "fk"=>0, "id"=>"ust_codigo", "description"=>"C�digo", "title"=>"Chave Prim�ria da Entidade", "type"=>"pk", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "update"=>1, "insert"=>1, "line"=>1, );
    $this->ust_items["ust_descricao"] = array("pk"=>0, "fk"=>0, "id"=>"ust_descricao", "description"=>"Descri��o", "title"=>"Nome dado ao tipo de usu�rio cadastrado", "type"=>"string", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "update"=>1, "insert"=>1, "line"=>2, );


    // Atributos FK


    // Atributos CHILD

    
    // Atributos padrao
    $this->ust_items['ust_alteracao'] = array('pk'=>false, 'fk'=>false, 'id'=>'ust_alteracao', 'description'=>'Altera��o', 'title'=>'', 'type'=>'alteracao', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>true, 'insert'=>true, 'line'=>0);
    $this->ust_items['ust_registro'] = array('pk'=>false, 'fk'=>false, 'id'=>'ust_registro', 'description'=>'Registro', 'title'=>'', 'type'=>'registro', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>false, 'insert'=>true, 'line'=>0);
    $this->ust_items['ust_responsavel'] = array('pk'=>false, 'fk'=>false, 'id'=>'ust_responsavel', 'description'=>'Respons�vel', 'title'=>'', 'type'=>'responsavel', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>true, 'insert'=>true, 'line'=>0);
    $this->ust_items['ust_criador'] = array('pk'=>false, 'fk'=>false, 'id'=>'ust_criador', 'description'=>'Criador', 'title'=>'', 'type'=>'criador', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>false, 'insert'=>true, 'line'=>0);

    $lines = 0;
    foreach($this->ust_items as $item){
      $lines = ($item['line'] > $lines) ? $item['line'] : $lines;
    }
     
    $j = array();
    foreach($this->ust_items as $item){
      if($item['fk']){
        if(isset($item['foreign']) or isset($item['parent'])){
          $table = "";
					$key = "";
					if(isset($item['foreign'])){
						$table = $item['foreign']['table'];
						$key = $item['foreign']['key'];
					}else if(isset($item['parent'])){
						$table = $item['parent']['table'];
						$key = $item['parent']['key'];
					}
          $j[$table] = " LEFT JOIN ".$table." ON (".$item['id']." = ".$key.") ";
        }
      }
    }
    $join = " ".join(' ', $j);
    
    $this->ust_properties = array(
      'module'=>'manager',
      'entity'=>'UsuarioTipo',
      'table'=>'TBL_USUARIO_TIPO',
			'join'=>$join,
      'tag'=>'usuariotipo',
      'prefix'=>'ust',
      'order'=>'',
      'group'=>'',
      'where'=>'',
      'search'=>'',
      'layout'=>'',
			'checkbox'=>false,
      'saveonly'=>false,//desabilita a edi��o de entidade
      'readonly'=>false,//desabilita a cria��o de novos registros
      'remove'=>'',//false or
			 /**
        * array(
        *  "field"=>"prefix_id",
        *  "value"=>"1",
        *  "className"=>"icon-remove",
        *  "title"=>"Excluir",
        *  "message"=>"Deseja realmente remover este registro?",
        *  "success"=>"Registro removido com sucesso"
        * )
        */
			'database'=>null,
      'reference'=>'ust_codigo',
      'description'=>'ust_descricao',
      'lines'=>$lines
    );
    
    if (!$this->ust_properties['reference']) {
      foreach ($this->ust_items as $id=>$array) {
        if ($array['pk'] == 1) {
          $this->ust_properties['reference'] = $id;
          break;
        }
      }
    }
    if (!$this->ust_properties['description']) {
      foreach ($this->ust_items as $id=>$array) {
        if ($array['type'] == "string") {
          $this->ust_properties['reference'] = $id;
          break;
        }
      }
    }
  }

  /**
   * Recupera as principais propriedades da entidade
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 28/05/2013 18:39:13
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 28/05/2013 18:39:13
   */
  public function get_ust_properties(){
    ?><?php
    return $this->ust_properties;
  }

  /**
   * Recupera todos os atributos da entidade
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 28/05/2013 18:39:13
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 28/05/2013 18:39:13
   */
  public function get_ust_items(){
    ?><?php
    return $this->ust_items;
  }

  /**
   * Recupera um atributo da entidade com todas as suas propriedades
   * 
   * @param string $key
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 28/05/2013 18:39:13
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 28/05/2013 18:39:13
   */
  public function get_ust_item($key){
    ?><?php
    return $this->ust_items[$key];
  }

  /**
   * 
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 28/05/2013 18:39:13
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 28/05/2013 18:39:13
   */
  public function get_ust_reference(){
    ?><?php
    $key = $this->ust_properties['reference'];
    return $key;
  }

  /**
   * Recupera o valor de um objeto
   * 
   * @param string $key
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 28/05/2013 18:39:13
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 28/05/2013 18:39:13
   */
  public function get_ust_value($key){
    ?><?php
    return $this->ust_items[$key]['value'];
  }

  /**
   * Altera o valor de um atributo da inst�ncia da entidade
   * 
   * @param string $key
   * @param object $value
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 28/05/2013 18:39:13
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 28/05/2013 18:39:13
   */
  public function set_ust_value($key, $value){
    ?><?php
    $this->ust_items[$key]['value'] = $value;
  }

  /**
   * Altera o tipo de um atributo da inst�ncia da entidade
   * 
   * @param string $key
   * @param string $type
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 28/05/2013 18:39:13
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 28/05/2013 18:39:13
   */
  public function set_ust_type($key, $type){
    ?><?php
    $this->ust_items[$key]['type'] = $type;
  }


}