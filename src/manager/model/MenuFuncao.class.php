<?php
/**
 * @copyright array software
 *
 * @author WILLIAM MARQUES VICENTE GOMES CORREA - 22/04/2013 01:52:19
 * <br><b>Updated by</b> ADMINISTRADOR - 15/06/2013 18:52:38
 * @category model
 * @package manager
 *
 * Responsavel.....: ADMINISTRADOR
 * Alteracao.......: 28/07/2013 09:30:02
 */


class MenuFuncao
{
  private  $mef_items = array();
  private  $mef_properties = array();

  /**
   * Construtor do Modelo da Classe
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 28/05/2013 18:35:03
   * <br><b>Updated by</b> ADMINISTRADOR - 12/07/2013 16:04:54
   */
  public function MenuFuncao(){
    ?><?php
    $this->mef_items = array();
    
    // Atributos
    $this->mef_items["mef_codigo"] = array("pk"=>1, "fk"=>0, "id"=>"mef_codigo", "description"=>"C�digo", "title"=>"", "type"=>"pk", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "update"=>0, "insert"=>0, "line"=>1, );
    $this->mef_items["mef_descricao"] = array("pk"=>0, "fk"=>0, "id"=>"mef_descricao", "description"=>"Descri��o", "title"=>"Nome da fun��o a ser exibido no menu", "type"=>"string", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "update"=>1, "insert"=>1, "line"=>3, );
    $this->mef_items["mef_module"] = array("pk"=>0, "fk"=>0, "id"=>"mef_module", "description"=>"M�dulo", "title"=>"M�dulo no qual a fun��o est� alocada", "type"=>"string", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>1, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "update"=>1, "insert"=>1, "line"=>4, );
    $this->mef_items["mef_entity"] = array("pk"=>0, "fk"=>0, "id"=>"mef_entity", "description"=>"Entidade", "title"=>"Nome da entidade que executa a fun��o", "type"=>"string", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>1, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "update"=>1, "insert"=>1, "line"=>5, );
    $this->mef_items["mef_action"] = array("pk"=>0, "fk"=>0, "id"=>"mef_action", "description"=>"A��o", "title"=>"Identificador de a��o da fun��o que ser� executada", "type"=>"string", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "update"=>1, "insert"=>1, "line"=>6, );
    $this->mef_items["mef_separator"] = array("pk"=>0, "fk"=>0, "id"=>"mef_separator", "description"=>"Separador", "title"=>"Especifica se o item do menu ter� ou n�o um separador sobre ele", "type"=>"yes/no", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[S]", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "update"=>1, "insert"=>1, "line"=>7, );
    $this->mef_items["mef_order"] = array("pk"=>0, "fk"=>0, "id"=>"mef_order", "description"=>"Ordem", "title"=>"Ordem de exibi��o dos itens de menu dentro de cada menu", "type"=>"int", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[>0]", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "update"=>1, "insert"=>1, "line"=>8, );
    $this->mef_items["mef_ativo"] = array("pk"=>0, "fk"=>0, "id"=>"mef_ativo", "description"=>"Ativo", "title"=>"", "type"=>"yes/no", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[R]", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "update"=>1, "insert"=>1, "line"=>9, );


    // Atributos FK
    $this->mef_items["mef_cod_MENU"] = array("pk"=>0, "fk"=>1, "id"=>"mef_cod_MENU", "description"=>"Menu", "title"=>"Menu no qual a fun��o ser� exibida", "type"=>"fk", "type_content"=>"", "type_behavior"=>"foreign", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[S]", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "update"=>1, "insert"=>1, "line"=>2, "foreign"=>array("modulo"=>"manager", "entity"=>"Menu", "table"=>"TBL_MENU", "prefix"=>"men", "tag"=>"menu", "key"=>"men_codigo", "description"=>"men_descricao", "form"=>"form", "target"=>"div-mef_cod_MENU-".rand()."-".date("Hisu"), "onchange"=>"", "encode"=>true, "width"=>"200", "where"=>"", "filter"=>""));


    // Atributos CHILD
    $this->mef_items["mef_fk_mft_cod_MENU_FUNCAO_575"] = array("pk"=>0, "fk"=>0, "id"=>"mef_fk_mft_cod_MENU_FUNCAO_575", "description"=>"Permiss�es", "title"=>"", "type"=>"", "type_content"=>"", "type_behavior"=>"popup", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"50", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "update"=>0, "insert"=>0, "line"=>0, "popup"=>array("module"=>"manager", "entity"=>"MenuFuncaoUsuarioTipo", "tag"=>"menufuncaousuariotipo", "prefix"=>"mft", "name"=>"Permiss�es", "filter"=>"mft_codigo", "key"=>"mef_codigo", "params"=>"child=true&width=&height=&rotule=&t=&f=", "target"=>"div-".rand()."-".date("Hisu"), "form"=>"form"));

    
    // Atributos padrao
    $this->mef_items['mef_alteracao'] = array('pk'=>false, 'fk'=>false, 'id'=>'mef_alteracao', 'description'=>'Altera��o', 'title'=>'', 'type'=>'alteracao', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>true, 'insert'=>true, 'line'=>0);
    $this->mef_items['mef_registro'] = array('pk'=>false, 'fk'=>false, 'id'=>'mef_registro', 'description'=>'Registro', 'title'=>'', 'type'=>'registro', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>false, 'insert'=>true, 'line'=>0);
    $this->mef_items['mef_responsavel'] = array('pk'=>false, 'fk'=>false, 'id'=>'mef_responsavel', 'description'=>'Respons�vel', 'title'=>'', 'type'=>'responsavel', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>true, 'insert'=>true, 'line'=>0);
    $this->mef_items['mef_criador'] = array('pk'=>false, 'fk'=>false, 'id'=>'mef_criador', 'description'=>'Criador', 'title'=>'', 'type'=>'criador', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>false, 'insert'=>true, 'line'=>0);

    $lines = 0;
    foreach($this->mef_items as $item){
      $lines = ($item['line'] > $lines) ? $item['line'] : $lines;
    }
     
    $j = array();
    foreach($this->mef_items as $item){
      if($item['fk']){
        if(isset($item['foreign']) or isset($item['parent'])){
          $table = "";
					$key = "";
					if(isset($item['foreign'])){
						$table = $item['foreign']['table'];
						$key = $item['foreign']['key'];
					}else if(isset($item['parent'])){
						$table = $item['parent']['table'];
						$key = $item['parent']['key'];
					}
          $j[$table] = " LEFT JOIN ".$table." ON (".$item['id']." = ".$key.") ";
        }
      }
    }
    $join = " ".join(' ', $j);
    
    $this->mef_properties = array(
      'module'=>'manager',
      'entity'=>'MenuFuncao',
      'table'=>'TBL_MENU_FUNCAO',
			'join'=>$join,
      'tag'=>'menufuncao',
      'prefix'=>'mef',
      'order'=>'men_ordem, mef_order',
      'group'=>'',
      'where'=>'',
      'search'=>'',
      'layout'=>'',
			'checkbox'=>false,
      'saveonly'=>false,//desabilita a edi��o de entidade
      'readonly'=>false,//desabilita a cria��o de novos registros
      'remove'=>'',//false or
			 /**
        * array(
        *  "field"=>"prefix_id",
        *  "value"=>"1",
        *  "className"=>"icon-remove",
        *  "title"=>"Excluir",
        *  "message"=>"Deseja realmente remover este registro?",
        *  "success"=>"Registro removido com sucesso"
        * )
        */
			'database'=>null,
      'reference'=>'mef_codigo',
      'description'=>'mef_descricao',
      'lines'=>$lines
    );
    
    if (!$this->mef_properties['reference']) {
      foreach ($this->mef_items as $id=>$array) {
        if ($array['pk'] == 1) {
          $this->mef_properties['reference'] = $id;
          break;
        }
      }
    }
    if (!$this->mef_properties['description']) {
      foreach ($this->mef_items as $id=>$array) {
        if ($array['type'] == "string") {
          $this->mef_properties['reference'] = $id;
          break;
        }
      }
    }
  }

  /**
   * Recupera as principais propriedades da entidade
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 28/05/2013 18:35:03
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 28/05/2013 18:35:03
   */
  public function get_mef_properties(){
    ?><?php
    return $this->mef_properties;
  }

  /**
   * Recupera todos os atributos da entidade
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 28/05/2013 18:35:03
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 28/05/2013 18:35:03
   */
  public function get_mef_items(){
    ?><?php
    return $this->mef_items;
  }

  /**
   * Recupera um atributo da entidade com todas as suas propriedades
   * 
   * @param string $key
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 28/05/2013 18:35:03
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 28/05/2013 18:35:03
   */
  public function get_mef_item($key){
    ?><?php
    return $this->mef_items[$key];
  }

  /**
   * 
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 28/05/2013 18:35:03
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 28/05/2013 18:35:03
   */
  public function get_mef_reference(){
    ?><?php
    $key = $this->mef_properties['reference'];
    return $key;
  }

  /**
   * Recupera o valor de um objeto
   * 
   * @param string $key
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 28/05/2013 18:35:03
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 28/05/2013 18:35:03
   */
  public function get_mef_value($key){
    ?><?php
    return $this->mef_items[$key]['value'];
  }

  /**
   * Altera o valor de um atributo da inst�ncia da entidade
   * 
   * @param string $key
   * @param object $value
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 28/05/2013 18:35:03
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 28/05/2013 18:35:03
   */
  public function set_mef_value($key, $value){
    ?><?php
    $this->mef_items[$key]['value'] = $value;
  }

  /**
   * Altera o tipo de um atributo da inst�ncia da entidade
   * 
   * @param string $key
   * @param string $type
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 28/05/2013 18:35:03
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 28/05/2013 18:35:03
   */
  public function set_mef_type($key, $type){
    ?><?php
    $this->mef_items[$key]['type'] = $type;
  }


}