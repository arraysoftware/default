<?php
/**
 * @copyright array software
 *
 * @author WILLIAM MARQUES VICENTE GOMES CORREA - 10/05/2013 15:25:43
 * <br><b>Updated by</b> ADMINISTRADOR - 15/06/2013 18:51:33
 * @category model
 * @package manager
 *
 * Responsavel.....: ADMINISTRADOR
 * Alteracao.......: 28/07/2013 09:30:01
 */


class Atualizacao
{
  private  $atl_items = array();
  private  $atl_properties = array();

  /**
   * Construtor do Modelo da Classe
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 10/05/2013 15:25:43
   * <br><b>Updated by</b> ADMINISTRADOR - 16/06/2013 11:38:49
   */
  public function Atualizacao(){
?><?php
    $this->atl_items = array();
    
    // Atributos
    $this->atl_items["atl_codigo"] = array("pk"=>1, "fk"=>0, "id"=>"atl_codigo", "description"=>"C�digo", "title"=>"", "type"=>"pk", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"", "update"=>0, "insert"=>0, "line"=>1, );
    $this->atl_items["atl_versao"] = array("pk"=>0, "fk"=>0, "id"=>"atl_versao", "description"=>"Vers�o", "title"=>"Vers�o produzida pela atualiza��o", "type"=>"string", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>1, "default_view"=>"".VERSION."", "default_sql"=>"", "update"=>1, "insert"=>1, "line"=>2, );
    $this->atl_items["atl_tipo"] = array("pk"=>0, "fk"=>0, "id"=>"atl_tipo", "description"=>"Tipo", "title"=>"Natureza da atualiza��o", "type"=>"option", "type_content"=>"4,M�nima|3,Pequena|2,M�dia|1,Grande", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[R]", "fast"=>0, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"4", "default_sql"=>"", "update"=>1, "insert"=>1, "line"=>3, );
    $this->atl_items["atl_resumo"] = array("pk"=>0, "fk"=>0, "id"=>"atl_resumo", "description"=>"Resumo das Altera��es", "title"=>"", "type"=>"rich-text", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>1, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "update"=>1, "insert"=>1, "line"=>4, );
    $this->atl_items["atl_arquivo"] = array("pk"=>0, "fk"=>0, "id"=>"atl_arquivo", "description"=>"Arquivo de Atualiza��o", "title"=>"", "type"=>"file", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>1, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "update"=>1, "insert"=>1, "line"=>5, );


    // Atributos FK


    // Atributos CHILD

    
    // Atributos padrao
    $this->atl_items['atl_alteracao'] = array('pk'=>false, 'fk'=>false, 'id'=>'atl_alteracao', 'description'=>'Altera��o', 'title'=>'', 'type'=>'alteracao', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>true, 'insert'=>true, 'line'=>0);
    $this->atl_items['atl_registro'] = array('pk'=>false, 'fk'=>false, 'id'=>'atl_registro', 'description'=>'Data', 'title'=>'', 'type'=>'registro', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>true, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>false, 'insert'=>true, 'line'=>99);
    $this->atl_items['atl_responsavel'] = array('pk'=>false, 'fk'=>false, 'id'=>'atl_responsavel', 'description'=>'Respons�vel', 'title'=>'', 'type'=>'responsavel', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>true, 'insert'=>true, 'line'=>0);
    $this->atl_items['atl_criador'] = array('pk'=>false, 'fk'=>false, 'id'=>'atl_criador', 'description'=>'Usu�rio', 'title'=>'', 'type'=>'criador', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>true, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>false, 'insert'=>true, 'line'=>99);

    $lines = 0;
    foreach($this->atl_items as $item){
      $lines = ($item['line'] > $lines) ? $item['line'] : $lines;
    }
     
    $j = array();
    foreach($this->atl_items as $item){
      if($item['fk']){
        if(isset($item['foreign'])){
          $table = $item['foreign']['table'];
          $j[$table] = " LEFT JOIN ".$table." ON (".$item['id']." = ".$item['foreign']['key'].") ";
        }
      }
    }
    $join = " ".join(' ', $j);
    
    $this->atl_properties = array(
      'module'=>'manager',
      'entity'=>'Atualizacao',
      'table'=>'TBL_ATUALIZACAO',
			'join'=>$join,
      'tag'=>'atualizacao',
      'prefix'=>'atl',
      'order'=>'atl_codigo DESC',
      'group'=>'',
      'where'=>'',
      'search'=>'',
      'layout'=>'',
			'checkbox'=>false,
      'saveonly'=>true,//desabilita a edi��o de entidade
      'readonly'=>false,//desabilita a cria��o de novos registros
      'remove'=>false,//false or
			 /**
        * array(
        *  "field"=>"prefix_id",
        *  "value"=>"1",
        *  "className"=>"icon-remove",
        *  "title"=>"Excluir",
        *  "message"=>"Deseja realmente remover este registro?",
        *  "success"=>"Registro removido com sucesso"
        * )
        */
			'database'=>null,
      'reference'=>'atl_codigo',
      'description'=>'atl_versao',
      'lines'=>$lines
    );
    
    if (!$this->atl_properties['reference']) {
      foreach ($this->atl_items as $id=>$array) {
        if ($array['pk'] == 1) {
          $this->atl_properties['reference'] = $id;
          break;
        }
      }
    }
    if (!$this->atl_properties['description']) {
      foreach ($this->atl_items as $id=>$array) {
        if ($array['type'] == "string") {
          $this->atl_properties['reference'] = $id;
          break;
        }
      }
    }
  }

  /**
   * Recupera as principais propriedades da entidade
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 10/05/2013 15:25:43
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 10/05/2013 15:25:43
   */
  public function get_atl_properties(){
    ?><?php
    return $this->atl_properties;
  }

  /**
   * Recupera todos os atributos da entidade
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 10/05/2013 15:25:43
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 10/05/2013 15:25:43
   */
  public function get_atl_items(){
    ?><?php
    return $this->atl_items;
  }

  /**
   * Recupera um atributo da entidade com todas as suas propriedades
   * 
   * @param string $key
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 10/05/2013 15:25:43
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 10/05/2013 15:25:43
   */
  public function get_atl_item($key){
    ?><?php
    return $this->atl_items[$key];
  }

  /**
   * 
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 10/05/2013 15:25:43
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 10/05/2013 15:25:43
   */
  public function get_atl_reference(){
    ?><?php
    $key = $this->atl_properties['reference'];
    return $key;
  }

  /**
   * Recupera o valor de um objeto
   * 
   * @param string $key
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 10/05/2013 15:25:43
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 10/05/2013 15:25:43
   */
  public function get_atl_value($key){
    ?><?php
    return $this->atl_items[$key]['value'];
  }

  /**
   * Altera o valor de um atributo da inst�ncia da entidade
   * 
   * @param string $key
   * @param object $value
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 10/05/2013 15:25:43
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 10/05/2013 15:25:43
   */
  public function set_atl_value($key, $value){
    ?><?php
    $this->atl_items[$key]['value'] = $value;
  }

  /**
   * Altera o tipo de um atributo da inst�ncia da entidade
   * 
   * @param string $key
   * @param string $type
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 10/05/2013 15:25:43
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 10/05/2013 15:25:43
   */
  public function set_atl_type($key, $type){
    ?><?php
    $this->atl_items[$key]['type'] = $type;
  }


}