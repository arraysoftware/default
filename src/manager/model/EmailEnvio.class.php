<?php
/**
 * @copyright array software
 *
 * @author WILLIAM MARQUES VICENTE GOMES CORREA - 06/05/2013 00:53:52
 * <br><b>Updated by</b> ADMINISTRADOR - 24/07/2013 12:07:54
 * @category model
 * @package manager
 *
 * Responsavel.....: ADMINISTRADOR
 * Alteracao.......: 28/07/2013 09:30:03
 */


class EmailEnvio
{
  private  $eme_items = array();
  private  $eme_properties = array();

  /**
   * Construtor do Modelo da Classe
   * 
   *
   * @author ADMINISTRADOR - 24/07/2013 12:07:48
   * <br><b>Updated by</b> ADMINISTRADOR - 24/07/2013 12:10:15
   */
  public function EmailEnvio(){
?><?php
    $this->eme_items = array();
    
    // Atributos
    $this->eme_items["eme_codigo"] = array("pk"=>1, "fk"=>0, "id"=>"eme_codigo", "description"=>"C�digo", "title"=>"", "type"=>"pk", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"", "update"=>0, "insert"=>0, "line"=>1, );
    $this->eme_items["eme_id"] = array("pk"=>0, "fk"=>0, "id"=>"eme_id", "description"=>"Identificador", "title"=>"Identificador do modelo que foi enviado (opcional)", "type"=>"string", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>1, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"", "update"=>1, "insert"=>1, "line"=>2, );
    $this->eme_items["eme_email"] = array("pk"=>0, "fk"=>0, "id"=>"eme_email", "description"=>"Destinat�rio", "title"=>"Endere�o de e-mail para o qual a mensagem foi enviada", "type"=>"string", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"", "update"=>1, "insert"=>1, "line"=>3, );
    $this->eme_items["eme_assunto"] = array("pk"=>0, "fk"=>0, "id"=>"eme_assunto", "description"=>"Assunto", "title"=>"", "type"=>"string", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"", "update"=>1, "insert"=>1, "line"=>4, );
    $this->eme_items["eme_corpo"] = array("pk"=>0, "fk"=>0, "id"=>"eme_corpo", "description"=>"Corpo", "title"=>"Corpo da mensagem que foi enviada", "type"=>"text", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>1, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"", "update"=>1, "insert"=>1, "line"=>5, );
    $this->eme_items["eme_enviado"] = array("pk"=>0, "fk"=>0, "id"=>"eme_enviado", "description"=>"Enviado", "title"=>"", "type"=>"yes/no", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[R]", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"", "update"=>1, "insert"=>1, "line"=>6, );
    $this->eme_items["eme_modelo"] = array("pk"=>0, "fk"=>0, "id"=>"eme_modelo", "description"=>"Modelo", "title"=>"Identificador do modelo que foi usado para enviar a mensagem", "type"=>"string", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"", "update"=>1, "insert"=>1, "line"=>7, );


    // Atributos FK


    // Atributos CHILD

    
    // Atributos padrao
    $this->eme_items['eme_alteracao'] = array('pk'=>false, 'fk'=>false, 'id'=>'eme_alteracao', 'description'=>'Altera��o', 'title'=>'', 'type'=>'alteracao', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>true, 'insert'=>true, 'line'=>0);
    $this->eme_items['eme_registro'] = array('pk'=>false, 'fk'=>false, 'id'=>'eme_registro', 'description'=>'Registro', 'title'=>'', 'type'=>'registro', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>false, 'insert'=>true, 'line'=>0);
    $this->eme_items['eme_responsavel'] = array('pk'=>false, 'fk'=>false, 'id'=>'eme_responsavel', 'description'=>'Respons�vel', 'title'=>'', 'type'=>'responsavel', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>true, 'insert'=>true, 'line'=>0);
    $this->eme_items['eme_criador'] = array('pk'=>false, 'fk'=>false, 'id'=>'eme_criador', 'description'=>'Criador', 'title'=>'', 'type'=>'criador', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>false, 'insert'=>true, 'line'=>0);

    $lines = 0;
    foreach($this->eme_items as $item){
      $lines = ($item['line'] > $lines) ? $item['line'] : $lines;
    }
     
    $j = array();
    foreach($this->eme_items as $item){
      if($item['fk']){
        if(isset($item['foreign']) or isset($item['parent'])){
          $table = "";
					$key = "";
					if(isset($item['foreign'])){
						$table = $item['foreign']['table'];
						$key = $item['foreign']['key'];
					}else if(isset($item['parent'])){
						$table = $item['parent']['table'];
						$key = $item['parent']['key'];
					}
          $j[$table] = " LEFT JOIN ".$table." ON (".$item['id']." = ".$key.") ";
        }
      }
    }
    $join = " ".join(' ', $j);
    
    $this->eme_properties = array(
			'description'=>'Envios de E-mail',
      'module'=>'manager',
      'entity'=>'EmailEnvio',
      'table'=>'TBL_EMAIL_ENVIO',
			'join'=>$join,
      'tag'=>'emailenvio',
      'prefix'=>'eme',
      'order'=>'',
      'group'=>'',
      'where'=>'',
      'search'=>'',
      'layout'=>'',
			'checkbox'=>false,
      'saveonly'=>false,//desabilita a edi��o de entidade
			'editonly'=>false,//desabilita a inser��o de itens de entidade
      'readonly'=>true,//desabilita a cria��o de novos registros
      'remove'=>'',//false or
			 /**
        * array(
        *  "field"=>"prefix_id",
        *  "value"=>"1",
        *  "className"=>"icon-remove",
        *  "title"=>"Excluir",
        *  "message"=>"Deseja realmente remover este registro?",
        *  "success"=>"Registro removido com sucesso"
        * )
        */
			'database'=>null,
      'reference'=>'eme_codigo',
      'description'=>'eme_id',
      'lines'=>$lines
    );
    
    if (!$this->eme_properties['reference']) {
      foreach ($this->eme_items as $id=>$array) {
        if ($array['pk'] == 1) {
          $this->eme_properties['reference'] = $id;
          break;
        }
      }
    }
    if (!$this->eme_properties['description']) {
      foreach ($this->eme_items as $id=>$array) {
        if ($array['type'] == "string") {
          $this->eme_properties['reference'] = $id;
          break;
        }
      }
    }
  }

  /**
   * Recupera as principais propriedades da entidade
   * 
   *
   * @author ADMINISTRADOR - 24/07/2013 12:07:48
   * <br><b>Updated by</b> ADMINISTRADOR - 24/07/2013 12:07:48
   */
  public function get_eme_properties(){
    ?><?php
    return $this->eme_properties;
  }

  /**
   * Recupera todos os atributos da entidade
   * 
   *
   * @author ADMINISTRADOR - 24/07/2013 12:07:48
   * <br><b>Updated by</b> ADMINISTRADOR - 24/07/2013 12:07:48
   */
  public function get_eme_items(){
    ?><?php
    return $this->eme_items;
  }

  /**
   * Recupera um atributo da entidade com todas as suas propriedades
   * 
   * @param string $key
   *
   * @author ADMINISTRADOR - 24/07/2013 12:07:48
   * <br><b>Updated by</b> ADMINISTRADOR - 24/07/2013 12:07:48
   */
  public function get_eme_item($key){
    ?><?php
    return $this->eme_items[$key];
  }

  /**
   * 
   * 
   *
   * @author ADMINISTRADOR - 24/07/2013 12:07:48
   * <br><b>Updated by</b> ADMINISTRADOR - 24/07/2013 12:07:48
   */
  public function get_eme_reference(){
    ?><?php
    $key = $this->eme_properties['reference'];
    return $key;
  }

  /**
   * Recupera o valor de um objeto
   * 
   * @param string $key
   *
   * @author ADMINISTRADOR - 24/07/2013 12:07:48
   * <br><b>Updated by</b> ADMINISTRADOR - 24/07/2013 12:07:48
   */
  public function get_eme_value($key){
    ?><?php
    return $this->eme_items[$key]['value'];
  }

  /**
   * Altera o valor de um atributo da inst�ncia da entidade
   * 
   * @param string $key
   * @param object $value
   *
   * @author ADMINISTRADOR - 24/07/2013 12:07:48
   * <br><b>Updated by</b> ADMINISTRADOR - 24/07/2013 12:07:48
   */
  public function set_eme_value($key, $value){
?><?php
		if (!isset($this->eme_items[$key])) {
      core_err(0, "Atributo $key n�o encontrado em " . get_class($this));
      exit;
    }
    $this->eme_items[$key]['value'] = $value;
  }

  /**
   * Altera o tipo de um atributo da inst�ncia da entidade
   * 
   * @param string $key
   * @param string $type
   *
   * @author ADMINISTRADOR - 24/07/2013 12:07:48
   * <br><b>Updated by</b> ADMINISTRADOR - 24/07/2013 12:07:48
   */
  public function set_eme_type($key, $type){
    ?><?php
    $this->eme_items[$key]['type'] = $type;
  }


}