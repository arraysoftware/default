<?php
/**
 * @copyright array software
 *
 * @author ADMINISTRADOR - 16/06/2013 10:06:05
 * <br><b>Updated by</b> ADMINISTRADOR - 17/06/2013 20:31:12
 * @category model
 * @package manager
 *
 * Responsavel.....: ADMINISTRADOR
 * Alteracao.......: 28/07/2013 09:30:02
 */


class MenuFuncaoUsuarioTipo
{
  private  $mft_items = array();
  private  $mft_properties = array();

  /**
   * Construtor do Modelo da Classe
   * 
   *
   * @author ADMINISTRADOR - 16/06/2013 15:42:51
   * <br><b>Updated by</b> ADMINISTRADOR - 12/07/2013 16:06:40
   */
  public function MenuFuncaoUsuarioTipo(){
?><?php
    $this->mft_items = array();
    
    // Atributos
    $this->mft_items["mft_codigo"] = array("pk"=>1, "fk"=>0, "id"=>"mft_codigo", "description"=>"C�digo", "title"=>"", "type"=>"pk", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"", "update"=>0, "insert"=>0, "line"=>1, );
    $this->mft_items["mft_level"] = array("pk"=>0, "fk"=>0, "id"=>"mft_level", "description"=>"N�vel de Acesso", "title"=>"", "type"=>"option", "type_content"=>"0,Baixo|1,M�dio|2,Alto", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[R]", "fast"=>0, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "update"=>1, "insert"=>1, "line"=>4, );


    // Atributos FK
    $this->mft_items["mft_cod_MENU_FUNCAO"] = array("pk"=>0, "fk"=>1, "id"=>"mft_cod_MENU_FUNCAO", "description"=>"Fun��o de Menu", "title"=>"", "type"=>"fk", "type_content"=>"", "type_behavior"=>"foreign", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>1, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"", "update"=>0, "insert"=>1, "line"=>2, "foreign"=>array("modulo"=>"manager", "entity"=>"MenuFuncao", "table"=>"TBL_MENU_FUNCAO", "prefix"=>"mef", "tag"=>"menufuncao", "key"=>"mef_codigo", "description"=>"mef_descricao", "form"=>"form", "target"=>"div-mft_cod_MENU_FUNCAO-".rand()."-".date("Hisu"), "onchange"=>"", "encode"=>true, "width"=>"400", "where"=>"", "filter"=>""));
    $this->mft_items["mft_cod_USUARIO_TIPO"] = array("pk"=>0, "fk"=>1, "id"=>"mft_cod_USUARIO_TIPO", "description"=>"Tipo de Usu�rio", "title"=>"", "type"=>"fk", "type_content"=>"", "type_behavior"=>"foreign", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "update"=>1, "insert"=>1, "line"=>3, "foreign"=>array("modulo"=>"manager", "entity"=>"UsuarioTipo", "table"=>"TBL_USUARIO_TIPO", "prefix"=>"ust", "tag"=>"usuariotipo", "key"=>"ust_codigo", "description"=>"ust_descricao", "form"=>"form", "target"=>"div-mft_cod_USUARIO_TIPO-".rand()."-".date("Hisu"), "onchange"=>"", "encode"=>true, "width"=>"400", "where"=>"", "filter"=>""));


    // Atributos CHILD

    
    // Atributos padrao
    $this->mft_items['mft_alteracao'] = array('pk'=>false, 'fk'=>false, 'id'=>'mft_alteracao', 'description'=>'Altera��o', 'title'=>'', 'type'=>'alteracao', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>true, 'insert'=>true, 'line'=>0);
    $this->mft_items['mft_registro'] = array('pk'=>false, 'fk'=>false, 'id'=>'mft_registro', 'description'=>'Registro', 'title'=>'', 'type'=>'registro', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>false, 'insert'=>true, 'line'=>0);
    $this->mft_items['mft_responsavel'] = array('pk'=>false, 'fk'=>false, 'id'=>'mft_responsavel', 'description'=>'Respons�vel', 'title'=>'', 'type'=>'responsavel', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>true, 'insert'=>true, 'line'=>0);
    $this->mft_items['mft_criador'] = array('pk'=>false, 'fk'=>false, 'id'=>'mft_criador', 'description'=>'Criador', 'title'=>'', 'type'=>'criador', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>false, 'insert'=>true, 'line'=>0);

    $lines = 0;
    foreach($this->mft_items as $item){
      $lines = ($item['line'] > $lines) ? $item['line'] : $lines;
    }
     
    $j = array();
    foreach($this->mft_items as $item){
      if($item['fk']){
        if(isset($item['foreign']) or isset($item['parent'])){
          $table = "";
					$key = "";
					if(isset($item['foreign'])){
						$table = $item['foreign']['table'];
						$key = $item['foreign']['key'];
					}else if(isset($item['parent'])){
						$table = $item['parent']['table'];
						$key = $item['parent']['key'];
					}
          $j[$table] = " LEFT JOIN ".$table." ON (".$item['id']." = ".$key.") ";
        }
      }
    }
    $join = " ".join(' ', $j);
    
    $this->mft_properties = array(
      'module'=>'manager',
      'entity'=>'MenuFuncaoUsuarioTipo',
      'table'=>'TBL_MENU_FUNCAO_USUARIO_TIPO',
			'join'=>$join,
      'tag'=>'menufuncaousuariotipo',
      'prefix'=>'mft',
      'order'=>'ust_descricao',
      'group'=>'',
      'where'=>'',
      'search'=>'',
      'layout'=>'',
			'checkbox'=>false,
      'saveonly'=>false,//desabilita a edi��o de entidade
			'editonly'=>false,//desabilita a inser��o de itens de entidade
      'readonly'=>false,//desabilita a cria��o de novos registros
      'remove'=>'',//false or
			 /**
        * array(
        *  "field"=>"prefix_id",
        *  "value"=>"1",
        *  "className"=>"icon-remove",
        *  "title"=>"Excluir",
        *  "message"=>"Deseja realmente remover este registro?",
        *  "success"=>"Registro removido com sucesso"
        * )
        */
			'database'=>null,
      'reference'=>'mft_codigo',
      'description'=>'mft_cod_USUARIO_TIPO',
      'lines'=>$lines
    );
    
    if (!$this->mft_properties['reference']) {
      foreach ($this->mft_items as $id=>$array) {
        if ($array['pk'] == 1) {
          $this->mft_properties['reference'] = $id;
          break;
        }
      }
    }
    if (!$this->mft_properties['description']) {
      foreach ($this->mft_items as $id=>$array) {
        if ($array['type'] == "string") {
          $this->mft_properties['reference'] = $id;
          break;
        }
      }
    }
  }

  /**
   * Recupera as principais propriedades da entidade
   * 
   *
   * @author ADMINISTRADOR - 16/06/2013 15:42:51
   * <br><b>Updated by</b> ADMINISTRADOR - 16/06/2013 15:42:51
   */
  public function get_mft_properties(){
    ?><?php
    return $this->mft_properties;
  }

  /**
   * Recupera todos os atributos da entidade
   * 
   *
   * @author ADMINISTRADOR - 16/06/2013 15:42:51
   * <br><b>Updated by</b> ADMINISTRADOR - 16/06/2013 15:42:51
   */
  public function get_mft_items(){
    ?><?php
    return $this->mft_items;
  }

  /**
   * Recupera um atributo da entidade com todas as suas propriedades
   * 
   * @param string $key
   *
   * @author ADMINISTRADOR - 16/06/2013 15:42:51
   * <br><b>Updated by</b> ADMINISTRADOR - 16/06/2013 15:42:51
   */
  public function get_mft_item($key){
    ?><?php
    return $this->mft_items[$key];
  }

  /**
   * 
   * 
   *
   * @author ADMINISTRADOR - 16/06/2013 15:42:51
   * <br><b>Updated by</b> ADMINISTRADOR - 16/06/2013 15:42:51
   */
  public function get_mft_reference(){
    ?><?php
    $key = $this->mft_properties['reference'];
    return $key;
  }

  /**
   * Recupera o valor de um objeto
   * 
   * @param string $key
   *
   * @author ADMINISTRADOR - 16/06/2013 15:42:51
   * <br><b>Updated by</b> ADMINISTRADOR - 16/06/2013 15:42:51
   */
  public function get_mft_value($key){
    ?><?php
    return $this->mft_items[$key]['value'];
  }

  /**
   * Altera o valor de um atributo da inst�ncia da entidade
   * 
   * @param string $key
   * @param object $value
   *
   * @author ADMINISTRADOR - 16/06/2013 15:42:51
   * <br><b>Updated by</b> ADMINISTRADOR - 16/06/2013 15:42:51
   */
  public function set_mft_value($key, $value){
    ?><?php
    $this->mft_items[$key]['value'] = $value;
  }

  /**
   * Altera o tipo de um atributo da inst�ncia da entidade
   * 
   * @param string $key
   * @param string $type
   *
   * @author ADMINISTRADOR - 16/06/2013 15:42:51
   * <br><b>Updated by</b> ADMINISTRADOR - 16/06/2013 15:42:51
   */
  public function set_mft_type($key, $type){
    ?><?php
    $this->mft_items[$key]['type'] = $type;
  }


}