<?php
/**
 * @copyright array software
 *
 * @author WILLIAM MARQUES VICENTE GOMES CORREA - 20/04/2013 12:46:48
 * <br><b>Updated by</b> ADMINISTRADOR - 15/06/2013 18:51:44
 * @category model
 * @package manager
 *
 * Responsavel.....: ADMINISTRADOR
 * Alteracao.......: 28/07/2013 09:30:02
 */


class Usuario
{
  private  $usu_items = array();
  private  $usu_properties = array();

  /**
   * Construtor do Modelo da Classe
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 28/05/2013 18:42:45
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 28/05/2013 18:42:45
   */
  public function Usuario(){
    ?><?php
    $this->usu_items = array();
    
    // Atributos
    $this->usu_items["usu_codigo"] = array("pk"=>1, "fk"=>0, "id"=>"usu_codigo", "description"=>"C�digo", "title"=>"Chave prim�ria da entidade", "type"=>"pk", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"", "update"=>0, "insert"=>0, "line"=>1, );
    $this->usu_items["usu_nome"] = array("pk"=>0, "fk"=>0, "id"=>"usu_nome", "description"=>"Nome", "title"=>"Nome do usu�rio", "type"=>"upper", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "update"=>1, "insert"=>1, "line"=>3, );
    $this->usu_items["usu_login"] = array("pk"=>0, "fk"=>0, "id"=>"usu_login", "description"=>"Login", "title"=>"Nome de usu�rio", "type"=>"string", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "update"=>1, "insert"=>1, "line"=>4, );
    $this->usu_items["usu_password"] = array("pk"=>0, "fk"=>0, "id"=>"usu_password", "description"=>"Senha", "title"=>"Senha de acesso do usu�rio", "type"=>"hash", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[P]", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "update"=>1, "insert"=>1, "line"=>4, );
    $this->usu_items["usu_admin"] = array("pk"=>0, "fk"=>0, "id"=>"usu_admin", "description"=>"Administrador", "title"=>"Define se o usu�rio tem privil�gios de administrador ou n�o", "type"=>"yes/no", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "update"=>1, "insert"=>1, "line"=>5, );
    $this->usu_items["usu_email"] = array("pk"=>0, "fk"=>0, "id"=>"usu_email", "description"=>"E-mail", "title"=>"Endere�o eletr�nico do usu�rio utilizado para notifica��es e recupera��o de senhas", "type"=>"string", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>1, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "update"=>1, "insert"=>1, "line"=>6, );
    $this->usu_items["usu_ativo"] = array("pk"=>0, "fk"=>0, "id"=>"usu_ativo", "description"=>"Ativo", "title"=>"Determina se o usu�rio est� ativo ou n�o", "type"=>"yes/no", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[R]", "fast"=>0, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "update"=>1, "insert"=>1, "line"=>7, );


    // Atributos FK
    $this->usu_items["usu_cod_USUARIO_TIPO"] = array("pk"=>0, "fk"=>1, "id"=>"usu_cod_USUARIO_TIPO", "description"=>"Tipo", "title"=>"Determina qual o tipo de acesso do Usu�rio", "type"=>"fk", "type_content"=>"", "type_behavior"=>"foreign", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[S]", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "update"=>1, "insert"=>1, "line"=>2, "foreign"=>array("modulo"=>"manager", "entity"=>"UsuarioTipo", "table"=>"TBL_USUARIO_TIPO", "prefix"=>"ust", "tag"=>"usuariotipo", "key"=>"ust_codigo", "description"=>"ust_descricao", "form"=>"form", "target"=>"div-usu_cod_USUARIO_TIPO-".rand()."-".date("Hisu"), "onchange"=>"", "encode"=>true, "width"=>"200", "where"=>"", "filter"=>""));


    // Atributos CHILD

    
    // Atributos padrao
    $this->usu_items['usu_alteracao'] = array('pk'=>false, 'fk'=>false, 'id'=>'usu_alteracao', 'description'=>'Altera��o', 'title'=>'', 'type'=>'alteracao', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>true, 'insert'=>true, 'line'=>0);
    $this->usu_items['usu_registro'] = array('pk'=>false, 'fk'=>false, 'id'=>'usu_registro', 'description'=>'Registro', 'title'=>'', 'type'=>'registro', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>false, 'insert'=>true, 'line'=>0);
    $this->usu_items['usu_responsavel'] = array('pk'=>false, 'fk'=>false, 'id'=>'usu_responsavel', 'description'=>'Respons�vel', 'title'=>'', 'type'=>'responsavel', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>true, 'insert'=>true, 'line'=>0);
    $this->usu_items['usu_criador'] = array('pk'=>false, 'fk'=>false, 'id'=>'usu_criador', 'description'=>'Criador', 'title'=>'', 'type'=>'criador', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>false, 'insert'=>true, 'line'=>0);

    $lines = 0;
    foreach($this->usu_items as $item){
      $lines = ($item['line'] > $lines) ? $item['line'] : $lines;
    }
     
    $j = array();
    foreach($this->usu_items as $item){
      if($item['fk']){
        if(isset($item['foreign']) or isset($item['parent'])){
          $table = "";
					$key = "";
					if(isset($item['foreign'])){
						$table = $item['foreign']['table'];
						$key = $item['foreign']['key'];
					}else if(isset($item['parent'])){
						$table = $item['parent']['table'];
						$key = $item['parent']['key'];
					}
          $j[$table] = " LEFT JOIN ".$table." ON (".$item['id']." = ".$key.") ";
        }
      }
    }
    $join = " ".join(' ', $j);
    
    $this->usu_properties = array(
      'module'=>'manager',
      'entity'=>'Usuario',
      'table'=>'TBL_USUARIO',
			'join'=>$join,
      'tag'=>'usuario',
      'prefix'=>'usu',
      'order'=>'',
      'group'=>'',
      'where'=>'',
      'search'=>'',
      'layout'=>'',
			'checkbox'=>false,
      'saveonly'=>false,//desabilita a edi��o de entidade
      'readonly'=>false,//desabilita a cria��o de novos registros
      'remove'=>'',//false or
			 /**
        * array(
        *  "field"=>"prefix_id",
        *  "value"=>"1",
        *  "className"=>"icon-remove",
        *  "title"=>"Excluir",
        *  "message"=>"Deseja realmente remover este registro?",
        *  "success"=>"Registro removido com sucesso"
        * )
        */
			'database'=>null,
      'reference'=>'usu_codigo',
      'description'=>'usu_login',
      'lines'=>$lines
    );
    
    if (!$this->usu_properties['reference']) {
      foreach ($this->usu_items as $id=>$array) {
        if ($array['pk'] == 1) {
          $this->usu_properties['reference'] = $id;
          break;
        }
      }
    }
    if (!$this->usu_properties['description']) {
      foreach ($this->usu_items as $id=>$array) {
        if ($array['type'] == "string") {
          $this->usu_properties['reference'] = $id;
          break;
        }
      }
    }
  }

  /**
   * Recupera as principais propriedades da entidade
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 28/05/2013 18:42:45
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 28/05/2013 18:42:45
   */
  public function get_usu_properties(){
    ?><?php
    return $this->usu_properties;
  }

  /**
   * Recupera todos os atributos da entidade
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 28/05/2013 18:42:45
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 28/05/2013 18:42:45
   */
  public function get_usu_items(){
    ?><?php
    return $this->usu_items;
  }

  /**
   * Recupera um atributo da entidade com todas as suas propriedades
   * 
   * @param string $key
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 28/05/2013 18:42:45
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 28/05/2013 18:42:45
   */
  public function get_usu_item($key){
    ?><?php
    return $this->usu_items[$key];
  }

  /**
   * 
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 28/05/2013 18:42:45
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 28/05/2013 18:42:45
   */
  public function get_usu_reference(){
    ?><?php
    $key = $this->usu_properties['reference'];
    return $key;
  }

  /**
   * Recupera o valor de um objeto
   * 
   * @param string $key
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 28/05/2013 18:42:45
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 28/05/2013 18:42:45
   */
  public function get_usu_value($key){
    ?><?php
    return $this->usu_items[$key]['value'];
  }

  /**
   * Altera o valor de um atributo da inst�ncia da entidade
   * 
   * @param string $key
   * @param object $value
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 28/05/2013 18:42:45
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 28/05/2013 18:42:45
   */
  public function set_usu_value($key, $value){
    ?><?php
    $this->usu_items[$key]['value'] = $value;
  }

  /**
   * Altera o tipo de um atributo da inst�ncia da entidade
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 03/06/2013 14:10:43
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 03/06/2013 14:10:43
   */
  public function set_usu_type(){
    ?><?php
    $this->usu_items[$key]['type'] = $type;
  }

  /**
   * Recupera um objeto da entidade formatado
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 28/05/2013 18:42:45
   * <br><b>Updated by</b> ADMINISTRADOR - 16/06/2013 12:35:54
   */
  public function get_usu_class(){
?><?php
    $usuario = array();
    $usuario['usu_codigo'] = md5($this->get_usu_value('usu_codigo'));
    $usuario['usu_login'] = Encode::encrypt($this->get_usu_value('usu_login'));
    $usuario['usu_tipo'] = Encode::encrypt($this->get_usu_value('usu_cod_USUARIO_TIPO'));
    $usuario['usu_nome'] = Encode::encrypt($this->get_usu_value('usu_nome'));
    $usuario['usu_email'] = Encode::encrypt($this->get_usu_value('usu_email'));
    $usuario['usu_admin'] = md5($this->get_usu_value('usu_admin'));

    return $usuario;
  }


}