<?php
/**
 * @copyright array software
 *
 * @author WILLIAM MARQUES VICENTE GOMES CORREA - 05/05/2013 21:56:15
 * <br><b>Updated by</b> ADMINISTRADOR - 24/07/2013 12:07:37
 * @category screen
 * @package manager
 *
 * Responsavel.....: ADMINISTRADOR
 * Alteracao.......: 28/07/2013 09:30:11
 */


class EmailModeloScreen
{

  /**
   * Escreve um formulário para a entidade com base nos parâmetros informados
   * 
   * @param string $level
   * @param array $contents
   * @param array $interfaces
   * @param array $actions
   * @param array $atributes
   * @param array $recovered
   * @param array $values
   *
   * @author ADMINISTRADOR - 24/07/2013 12:07:27
   * <br><b>Updated by</b> ADMINISTRADOR - 24/07/2013 12:07:27
   */
  public function managerEmailModeloScreen($level, $contents, $interfaces, $actions, $atributes, $recovered, $values){
    ?><?php

	  $acesso_controle = -1;
		
		require System::desire('file', '', 'header',  'core', false);

    $param = isset($contents['param']) ? $contents['param'] : "";
    $items = isset($contents['items']) ? $contents['items'] : "";
    $filter = isset($contents['filter']) ? $contents['filter'] : "";

    $target = isset($interfaces['target']) ? $interfaces['target'] : "";
    $layout = isset($interfaces['layout']) ? $interfaces['layout'] : "";
    $form = isset($interfaces['form']) ? $interfaces['form'] : "";
    $lines = isset($interfaces['lines']) ? $interfaces['lines'] : 0;
    $rotule = isset($interfaces['rotule']) ? $interfaces['rotule'] : "";
    $info = isset($interfaces['info']) ? $interfaces['info'] : "";
    $window_width = isset($interfaces['window_width']) ? $interfaces['window_width'] : WINDOW_WIDTH;
    $window_height = isset($interfaces['window_height']) ? $interfaces['window_height'] : WINDOW_HEIGHT;
    $width_label = isset($interfaces['width_label']) ? $interfaces['width_label'] : 100;
    $width_value = isset($interfaces['width_value']) ? $interfaces['width_value'] : $window_width - $width_label;
    $width = isset($interfaces['width']) ? $interfaces['width'] : WINDOW_WIDTH;
    $height = isset($interfaces['height']) ? $interfaces['height'] : WINDOW_HEIGHT + 30;
    $modal = isset($interfaces['modal']) ? $interfaces['modal'] : false;

    $back = isset($actions['back']) ? $actions['back'] : "";
    $execute = isset($actions['execute']) ? $actions['execute'] : "";
    $load = isset($actions['load']) ? $actions['load'] : "";

    $primary = isset($atributes['primary']) ? $atributes['primary'] : "";
    $foreign = isset($atributes['foreign']) ? $atributes['foreign'] : "";
    $hidden = isset($atributes['hidden']) ? $atributes['hidden'] : "";
    $children = isset($atributes['children']) ? $atributes['children'] : "";
    $popups = isset($atributes['popups']) ? $atributes['popups'] : "";
    $validate = isset($atributes['validate']) ? $atributes['validate'] : "";

    $start = isset($recovered['start']) ? $recovered['start'] : "";
    $extra_child = isset($recovered['child']) ? $recovered['child'] : "";
    $where = isset($recovered['where']) ? $recovered['where'] : "";
    $group = isset($recovered['group']) ? $recovered['group'] : "";
    $order = isset($recovered['order']) ? $recovered['order'] : "";
    $w_description = isset($recovered['w_description']) ? $recovered['w_description'] : "";
    $g_description = isset($recovered['g_description']) ? $recovered['g_description'] : "";
    $o_description = isset($recovered['o_description']) ? $recovered['o_description'] : "";

    $id = isset($values['id']) ? $values['id'] : "";
    $registro = isset($values['registro']) ? $values['registro'] : "";
    $criador = isset($values['criador']) ? $values['criador'] : "";
    $alteracao = isset($values['alteracao']) ? $values['alteracao'] : "";
    $responsavel = isset($values['responsavel']) ? $values['responsavel'] : "";

	  require_once System::desire('class', 'resource', 'Screen', 'core', false);

    System::desire('m', 'manager', 'EmailModelo', 'src', true, '{project.rsc}');

    $screen = new Screen('{project.rsc}');

    if($acesso_controle >= 1){

      try{

        $emailModelo = new EmailModelo();

        $properties = $emailModelo->get_emo_properties();

        $module = $properties["module"];
        $entity = $properties["entity"];
        $tag = $properties["tag"];
        $reference = $properties['reference'];

        $core = false;
        if(!$form){
          $form = 'form-'.$target;
        }

        ?>
        <center>
          <?php
            if($layout === 'manager' or $layout === ''){

              $name = Encode::decrypt($rotule);

              if($layout === 'manager'){
                $info = "";
                $filter = "";
                $height = "";
                $width = "";
                $extra_child = "";
              }

              $screen->manager->printManagerHeader($target, $name, $info, $window_width);

                $screen->manager->printManagerFormHeader($form, $target, Encode::decrypt($rotule), $info, $window_width);

                  $screen->manager->printManagerHiddens($primary, $foreign, $hidden);

                  $screen->manager->printFormValues($target, $level, $where, $group, $order, $filter, $w_description, $g_description, $o_description, $rotule, $start, $height, $width, $extra_child);

                  $screen->manager->printManagerLinesHeader($form, $target, Encode::decrypt($rotule), $info, $window_width);

                    $screen->manager->printManagerLines($param, $items, $lines, $width_label, $width_value, $window_width);

                    if($param != "search"){
                      $screen->line->printPopups($popups);
                    }

                  $screen->manager->printManagerLinesBottom($target, Encode::decrypt($rotule), $info, $window_width);

                $screen->manager->printManagerFormBottom($form, $target, Encode::decrypt($rotule), $info, $window_width);

                if($param != "search"){
                  if($layout === 'children' or $layout === ''){
                    $screen->line->printChildren($children, $window_width, $target, $width_label, $width_value, $param, '', true);
                  }
                }

                if($param != "search"){
                  $screen->manager->printFormHistory($window_width, $registro, $criador, $alteracao, $responsavel, $target, $module, $rotule, $entity, $tag, $id, false);
                }

              $screen->manager->printManagerBottom($target, Encode::decrypt($rotule), $info, $window_width);

            }else{

              $name = "";

              $screen->manager->printManagerHeader($target, $name, $info, $window_width);

                  $screen->manager->printManagerLinesHeader($form, $target, Encode::decrypt($rotule), $info, $window_width, true);

                  if($layout === 'children'){

                    $_name = Encode::decrypt($rotule);
                    $_width = $screen->utilities->getChildWidth($window_width, $modal);
                    $_height = $screen->utilities->getChildHeight($window_height, $layout, $modal, $lines);
                    $params = "&layout=manager&t=".$target."&l=".$level."&f=".$filter."&rotule=".$rotule."&start=".$start."&form=".$form."&w=".$where."&g=".$group."&o=".$order."&wd=".$w_description."&gd=".$g_description."&od=".$o_description;

                    $item = $screen->utilities->generateChild($param, $module, $entity, $tag, $_name, $level, $_width, $_height, $id, $reference, $params);

                    array_unshift($children, $item);
                  }

                  if($param != "search"){
                    if($layout === 'children' or $layout === ''){
                      $screen->line->printChildren($children, $window_width, $target, $width_label, $width_value, $param, '', true);
                    }
                  }

                  $screen->manager->printManagerLinesBottom($target, Encode::decrypt($rotule), $info, $window_width);

              $screen->manager->printManagerBottom($target, Encode::decrypt($rotule), $info, $window_width);

            }
          ?>
        </center>
        <?php

        if($layout === 'children' or $layout === ''){
          $v = "";
          if(count($validate) > 0){
            foreach($validate as $item){
              $details = "";
              if(isset($item['foreign'])){
                $details = ", 'target':'ext-".$item['foreign']['target']."'";
              }
              $v .= ",'".$item['id']."': {'description':'".$item['description']."', 'value':'".$item['validate']."'".$details."}";
            }
          }

          $valid = ($v != "") ? '{'.substr($v, 1).'}' : "";
          $extras = null;

          $html = '';

          $screen->manager->printManagerToolbar($acesso_controle, $param, $target, $level, $module, $entity, $tag, $back, $execute, $valid, $load, $core, $form, $html, $extras, $window_width);
        }

      }catch (Exception $exception){
        print "Caught exception: ".$exception->getMessage()."\n";
      }
    }else{
      $screen->message->printMessageError(MESSAGE_FORBIDDEN);
    }

  }

  /**
   * Escreve uma listagem dos registros da entidade com base nos parâmetros informados
   * 
   * @param string $level
   * @param array $contents
   * @param array $interfaces
   * @param array $actions
   * @param array $recovered
   *
   * @author ADMINISTRADOR - 24/07/2013 12:07:27
   * <br><b>Updated by</b> ADMINISTRADOR - 24/07/2013 12:07:27
   */
  public function listEmailModeloScreen($level, $contents, $interfaces, $actions, $recovered){
    ?><?php

    $acesso_controle = -1;

		require System::desire('file', '', 'header',  'core', false);

    $items = isset($contents['items']) ? $contents['items'] : "";
    $filter = isset($contents['filter']) ? $contents['filter'] : "";

    $target = isset($interfaces['target']) ? $interfaces['target'] : "";
    $rotule = isset($interfaces['rotule']) ? $interfaces['rotule'] : "";
    $window_width = isset($interfaces['window_width']) ? $interfaces['window_width'] : WINDOW_WIDTH;
    $window_height = isset($interfaces['window_height']) ? $interfaces['window_height'] : WINDOW_HEIGHT;
    $width = isset($interfaces['width']) ? $interfaces['width'] : WINDOW_WIDTH;
    $height = isset($interfaces['height']) ? $interfaces['height'] : WINDOW_HEIGHT + 30;

    $checkbox = isset($actions['checkbox']) ? $actions['checkbox'] : false;
    $readonly = isset($actions['readonly']) ? $actions['readonly'] : false;
    $hidetoolbar = isset($actions['hidetoolbar']) ? $actions['hidetoolbar'] : false;

    $start = isset($recovered['start']) ? $recovered['start'] : "";
    $extra_child = isset($recovered['child']) ? $recovered['child'] : "";
    $fast = isset($recovered['fast']) ? $recovered['fast'] : "";
    $where = isset($recovered['where']) ? $recovered['where'] : "";
    $group = isset($recovered['group']) ? $recovered['group'] : "";
    $order = isset($recovered['order']) ? $recovered['order'] : "";
    $w_description = isset($recovered['w_description']) ? $recovered['w_description'] : "";
    $g_description = isset($recovered['g_description']) ? $recovered['g_description'] : "";
    $o_description = isset($recovered['o_description']) ? $recovered['o_description'] : "";

    require_once System::desire('class', 'resource', 'Screen', 'core', false);

    System::desire('m', 'manager', 'EmailModelo', 'src', true, '{project.rsc}');

    $screen = new Screen('{project.rsc}');

    if($acesso_controle >= 0){

      try{

        $emailModelo = new EmailModelo();

        $properties = $emailModelo->get_emo_properties();

        $module = $properties["module"];
        $entity = $properties["entity"];
        $tag = $properties["tag"];
        $reference = $properties['reference'];

        $id = "grid-".$target;
        $end = ITENS;

        $core = false;

        ?>
          <center>
            <form id="form-<?php print $target;?>" name="form-<?php print $target;?>" action="return false;" onsubmit="return false;">
              <?php
                $screen->list->printListHeader($target, Encode::decrypt($rotule), $hidetoolbar, $extra_child);

                $screen->list->printListMarkup($id, $window_width, $window_height);

                $screen->list->printListBottom($target, Encode::decrypt($rotule), $hidetoolbar, $extra_child);

                $screen->manager->printFormValues($target, $level, $where, $group, $order, $filter, $w_description, $g_description, $o_description, $rotule, $start, $height, $width, $extra_child);
              ?>
            </form>
          </center>

        <?php

        $option = $screen->utilities->getViewListOptionWidth($acesso_controle, $properties, false);

        $screen->list->printListToolbar($acesso_controle, $target, $level, $module, $entity, $tag, $fast, $w_description, $core, $readonly, $window_width);

        $screen->list->printListGridAction($module, $rotule, $entity, $tag, $reference, $properties);

        $screen->list->printListGrid($module, $rotule, $entity, $tag, $items, $target, $id, $start, $end, $window_height, $window_width, $level, $filter, $where, $group, $order, $option, $checkbox);

      }catch (Exception $exception){
        print "Caught exception: ".$exception->getMessage()."\n";
      }

    }else{
      $screen->message->printMessageError(MESSAGE_FORBIDDEN);
    }
  }


}