<?php
/**
 * @copyright array software
 *
 * @author ADMINISTRADOR - 24/07/2013 10:53:47
 * <br><b>Updated by</b> ADMINISTRADOR - 24/07/2013 12:07:03
 * @category view
 * @package manager
 *
 * Responsavel.....: ADMINISTRADOR
 * Alteracao.......: 28/07/2013 09:30:08
 */


if (System::request('action')) {
  $action = System::request('action');


 if ($action == 'emailnotificacao-v-edit') {
    $param = System::request('param', "edit");
    $target = System::request('t');
    $level = System::request('l');
    $where = System::request('w');
    $group = System::request('g');
    $order = System::request('o');
    $filter = System::request('f');
    $rotule = System::request('rotule');
    $w_description = System::request('wd');
    $g_description = System::request('gd');
    $o_description = System::request('od');
    $start = System::request('start', "0");

    viewManagerEmailNotificacao($param, $target, $level, $where, $group, $order, $filter, $rotule, $w_description, $g_description, $o_description, $start);
  } else if ($action == 'emailnotificacao-v-view') {
    $param = System::request('param', "view");
    $target = System::request('t');
    $level = System::request('l');
    $where = System::request('w');
    $group = System::request('g');
    $order = System::request('o');
    $filter = System::request('f');
    $rotule = System::request('rotule');
    $w_description = System::request('wd');
    $g_description = System::request('gd');
    $o_description = System::request('od');
    $start = System::request('start', "0");

    viewManagerEmailNotificacao($param, $target, $level, $where, $group, $order, $filter, $rotule, $w_description, $g_description, $o_description, $start);
  } else if ($action == 'emailnotificacao-v-add') {
    $param = System::request('param', "add");
    $target = System::request('t');
    $level = System::request('l');
    $where = System::request('w');
    $group = System::request('g');
    $order = System::request('o');
    $filter = System::request('f');
    $rotule = System::request('rotule');
    $w_description = System::request('wd');
    $g_description = System::request('gd');
    $o_description = System::request('od');
    $start = System::request('start', "0");

    viewManagerEmailNotificacao($param, $target, $level, $where, $group, $order, $filter, $rotule, $w_description, $g_description, $o_description, $start);
  } else if ($action == 'emailnotificacao-v-search') {
    $param = System::request('param', "search");
    $target = System::request('t');
    $level = System::request('l');
    $where = System::request('w');
    $group = System::request('g');
    $order = System::request('o');
    $filter = System::request('f');
    $rotule = System::request('rotule');
    $w_description = System::request('wd');
    $g_description = System::request('gd');
    $o_description = System::request('od');
    $start = System::request('start', "0");

    viewManagerEmailNotificacao($param, $target, $level, $where, $group, $order, $filter, $rotule, $w_description, $g_description, $o_description, $start);
  } else if ($action == 'emailnotificacao-v-list') {
    $target = System::request('t');
    $level = System::request('l');
    $where = System::request('w');
    $group = System::request('g');
    $order = System::request('o');
    $filter = System::request('f');
    $rotule = System::request('rotule');
    $w_description = System::request('wd');
    $g_description = System::request('gd');
    $o_description = System::request('od');
    $start = System::request('start', "0");

    viewListEmailNotificacao($target, $level, $where, $group, $order, $filter, $rotule, $w_description, $g_description, $o_description, $start);
  } else if ($action == 'emailnotificacao-v-select') {
    $id = System::request('id');
    $level = System::request('l');
    $where = System::request('w');
    $group = System::request('g');
    $order = System::request('o');
    $name = System::request('name');
    $selected = System::request('selected');
    $onchange = System::request('onchange');
    $encode = System::request('encode');
    $width = System::request('width');
    $filter = System::request('f');

    viewSelectEmailNotificacao($id, $level, $where, $group, $order, $name, $selected, $onchange, $encode, $width, $filter);
  }
}

  /**
   * Exibe um formul�rio para administrar a Entidade
   * 
   * @param string $param
   * @param string $target
   * @param string $level
   * @param string $where
   * @param string $group
   * @param string $order
   * @param string $filter
   * @param string $rotule
   * @param string $w_description
   * @param string $g_description
   * @param string $o_description
   * @param string $start
   *
   * @author ADMINISTRADOR - 24/07/2013 12:05:59
   * <br><b>Updated by</b> ADMINISTRADOR - 24/07/2013 12:05:59
   */
 function viewManagerEmailNotificacao($param, $target, $level, $where, $group, $order, $filter, $rotule, $w_description, $g_description, $o_description, $start){
    ?><?php

    $acesso_controle = -1;

    require System::desire('file', '', 'header', 'core', false);
    require_once System::desire('class', 'resource', 'Screen', 'core', false);

    System::desire('m', 'manager', 'EmailNotificacao', 'src', true, '{project.rsc}');
    System::desire('c', 'manager', 'EmailNotificacao', 'src', true, '{project.rsc}');
    System::desire('s', 'manager', 'EmailNotificacao', 'src', true, '{project.rsc}');

		$screen = new Screen('{project.rsc}');

		if($acesso_controle >= 1 || $param == "view"){
			
      try{

				$emailNotificacao = new EmailNotificacao();
        $emailNotificacaoScreen = new EmailNotificacaoScreen();

				$items = $emailNotificacao->get_emn_items();
				$primary = array();
				$foreign = array();
				$hidden = array();
				$validate = array();
				$children = array();
				$popups = array();
				$parent = null;
				$load = "";
		
				$properties = $emailNotificacao->get_emn_properties();
				$module = $properties["module"];
				$entity = $properties["entity"];
				$tag = $properties["tag"];
				$lines = $properties["lines"];
				$reference = $properties['reference'];
				$layout = isset($properties['layout']) ? $param !== "search" ? $properties['layout'] : "" : "";
				if(System::request('layout')){
					$layout = System::request('layout');
				}
				$form = System::request('form');
				$modal = Boolean::parse(System::request('modal'));
				$back = System::request('back',true);
				$execute = System::request('execute','');
				$database = isset($properties["database"]) ? $properties["database"] : DEFAULT_DATABASE;
		
				$width_label = 0;
		
				$width = System::request('width', 0);
				$window_width = WINDOW_WIDTH;
				if($width){
					$window_width = $width;
				}
		
				$window_height = WINDOW_HEIGHT;
				$height = System::request('height', 0);
				if($height){
					$window_height = $height;
				}
		
				$extra_child = System::request('child');
		
				$registro = date('d/m/Y H:i:s');
				$criador = System::getUser();
				$alteracao = date('d/m/Y H:i:s');
				$responsavel = System::getUser();
		
				$emailNotificacaoCtrl = new EmailNotificacaoCtrl(PATH_APP, $database);
		
				/* PARENT */
				foreach($items as $item){
					if($item['type_behavior'] == 'parent'){
						if(isset($item['parent'])){
							$p = $item['parent'];
							$class = $p['entity'];
							System::desire('m', $p['modulo'], $class, 'src', true);
							$obj = new $class();
							$get_properties = "get_".$p['prefix']."_properties";
							$get_items = "get_".$p['prefix']."_items";
		
							$pos = $item['type_content'] ? $item['type_content'] : "bottom";
							$prop = $obj->$get_properties();
							$itens = $obj->$get_items();
		
							$parent = array("object"=>$obj,"module"=>$prop["module"],"entity"=>$class,"prefix"=>$p['prefix'],"id"=>$item['id'],"key"=>$p['key'],"position"=>$pos,"lines"=>$prop["lines"],"items"=>$itens);
						}
					}
				}
				/* PARENT */
		
		
				$suported = array("view","edit","add","search");
				if(in_array($param, $suported)){
		
					if($param == "view") {
						$info = "Visualizar ";
					}else if($param == "edit"){
						$info = "Editar ";
					}else if($param == "add"){
						$info = "Novo";
					}else if($param == "search"){
						$info = "Pesquisar";
					}
		
					$before = 0;
					$after = 0;
					if($parent){
						$before = $parent['position'] === 'top' ? $parent['lines'] : $before;
						$after = $parent['position'] === 'bottom' ? $lines : $after;
						$lines = $lines + $parent['lines'];
					}
		
					if($param == "view" or $param == "edit"){
		
						$info = "Editar ";
		
						$s = "";
						$conector = " AND ";
						foreach($items as $item){
							if($item['pk']){
								$key = $item['id'];
								$s .= $conector.$key." = '".System::request($key)."'";
							}
						}
						$search = substr($s, strlen($conector));
		
						$emailNotificacaos = $emailNotificacaoCtrl->getEmailNotificacaoCtrl($search, "", "", "", "");
						if(is_array($emailNotificacaos)){
							$emailNotificacao = $emailNotificacaos[0];
						}
					}
		
					$items = $emailNotificacao->get_emn_items();
		
					foreach($items as $item){
						$key = $item['id'];
						$items[$key]['line'] = $before + $items[$key]['line'];
		
						if($param == "view"){
							$items[$key] = $screen->utilities->configureViewManager($item);
						}else if($param == "edit"){
							$items[$key]['value'] = $emailNotificacao->get_emn_value($key);
							if($item['type'] === 'select-multi'){
								$items[$key]['select-multi']['filter'] = $emailNotificacao->get_emn_value($items[$key]['select-multi']['key']);
							}
							$items[$key] = $screen->utilities->configureEditManager($item);
						}else if($param == "add"){
							$items[$key] = $screen->utilities->configureAddManager($item);
						}else if($param == "search"){
							$items[$key] = $screen->utilities->configureSearchManager($item);
						}
					}
		
					if($parent){
						$itens = $parent['items'];
		
						if($param == "view" or $param == "edit"){
							$relation = $emailNotificacao->get_emn_value($parent['id']);
							if($relation){
								$class = $parent['entity'];
								System::desire('c', $parent['module'], $class, 'src', true);
								$classCtrl = $class."Ctrl";
								$objCtrl = new $classCtrl(PATH_APP);
								$getCtrl = "get".$classCtrl;
								$get_items = "get_".$parent['prefix']."_items";
		
								$objs = $objCtrl->$getCtrl($parent['key']." = '".$relation."'","","");
								if(is_array($objs)){
									$obj = $objs[0];
								}
		
								$itens = $obj->$get_items();
							}
						}
		
						$parent['items'] = null;
		
						foreach($itens as $item){
							$key = $item['id'];
		
							$item['line'] = $after + $item['line'];
		
							if($param == "view"){
								$item = $screen->utilities->configureViewManager($item);
							}else if($param == "add"){
								$item = $screen->utilities->configureAddManager($item);
							}else if($param == "edit"){
								$item = $screen->utilities->configureEditManager($item);
							}else if($param == "search"){
								$item = $screen->utilities->configureSearchManager($item);
							}
							if($item['pk']){
								$hidden[] = array("id"=>$item['id'],"name"=>$item['id'],"value"=>$item['value']);
								$item['line'] = 0;
								$item['form'] = 0;
							}
		
							$first = true;
							if($item['type_behavior'] == 'child'){
								if($item['child']['key'] === $parent['key']){
									$_width = $screen->utilities->getChildWidth($window_width, $modal);
									$_height = $screen->utilities->getChildHeight($window_height, $layout, $modal, $lines);
		
									$_child = $item['child'];
									$action = "list";
									$_child['target'] = 'children'.'-'.$target.'-'.(count($children) + 1);
									$_child['filter'] = Encode::encrypt($emailNotificacao->get_emn_value($parent['id']));
									$params = "t=".$_child['target'];
		
									$child = $screen->utilities->generateChild($action, $_child['module'], $_child['entity'], $_child['tag'], $_child['name'], $level, $_width, $_height, $_child['filter'], $_child['key'], $params, "", $_child['target']);
									$children[] = $child;
		
									if($param == 'add' and $first){
										$load = array("form"=>$_child['form'], "module"=>$_child['module'], "entity"=>$_child['entity'], "tag"=>$_child['tag'], "target"=>$_child['target'], "level"=>$level, "params"=>$params);
										$first = false;
									}
								}
							}
							
							$parent['items'][$item['id']] = $item;
						}
					}
		
					if($param == "view" or $param == "edit"){
		
						$registro = $emailNotificacao->get_emn_value('emn_registro');
						$criador = $emailNotificacao->get_emn_value('emn_criador');
						$alteracao = $emailNotificacao->get_emn_value('emn_alteracao');
						$responsavel = $emailNotificacao->get_emn_value('emn_responsavel');
		
					}
		
				}
		
				if($parent){
					if($parent['position'] === 'top'){
						$items = array_merge($parent['items'], $items);
					}else{
						$items = array_merge($items, $parent['items']);
					}
				}
		
				$one = true;
				$__filter = explode(",", $filter);
				$__fk = -1;
		
				foreach($items as $item){
					$key = $item['id'];
		
					if($item['type_behavior'] == 'child'){
		
						$_width = $screen->utilities->getChildWidth($window_width, $modal);
						$_height = $screen->utilities->getChildHeight($window_height, $layout, $modal, $lines);
		
						$_child = $item['child'];
						$action = "list";/*se precisar que a funcao em children inicie em outro modo custmoize aqui*/
						$_child['target'] = 'children'.'-'.$target.'-'.(count($children) + 1);
						$_child['filter'] = Encode::encrypt($emailNotificacao->get_emn_value($reference));/*se precisar usar outro filtro use o params para passar*/
						$params = "t=".$_child['target'];
		
						$child = $screen->utilities->generateChild($action, $_child['module'], $_child['entity'], $_child['tag'], $_child['name'], $level, $_width, $_height, $_child['filter'], $_child['key'], $params, "", $_child['target']);
						$children[] = $child;
		
						if($param == 'add' and $one){
							$load = array("form"=>$_child['form'],"module"=>$_child['module'],"entity"=>$_child['entity'],"tag"=>$_child['tag'],"target"=>$_child['target'],"level"=>$level,"params"=>$params);
							$one = false;
						}
		
					}
		
					if ($item['type_behavior'] == 'popup') {
						$_height = $window_height - 40;
						$_width = $window_width - 40;
		
						$_name = $item['popup']['name'];
						$_target = 'popup_'.str_replace('-', '_', $item['popup']['target']);
		
						$item['line'] = 0;
						$item['type'] = 'popup';
						$item['popup']['height'] = $_height;
						$item['popup']['width'] = $_width;
						$item['popup']['rotule'] = $_name;
						$item['popup']['level'] = $level;
						$item['popup']['params'] = 'width='.$_width.'&height='.$_height.'&rotule='.Encode::encrypt($_name).'&t='.$_target."&f='+system.encrypt.encode(system.util.getValue('".$item['popup']['key']."'))+'";
		
						$item['popup']['parent']['target'] = $target;
						$item['popup']['parent']['module'] = $module;
						$item['popup']['parent']['entity'] = $entity;
						$item['popup']['parent']['tag'] = $tag;
		
						$popups[] = $item;
					}
		
					if($item['fk'] && $filter){
						$__fk++;
						if($param == 'add'){
							if(isset($__filter[$__fk])){
								$item['value'] = Encode::decrypt($__filter[$__fk]);
								if($item['type_behavior'] == 'foreign' && $filter){
									//usar quando preciso
									//$item['foreign']['where'] = Encode::encrypt($item['foreign']['key']."='".Encode::decrypt($filter)."'");
								}
							}
						}
					}
		
					if($item['pk']){
						$primary[] = $item;
					}
		
					if($item['fk']){
						$foreign[] = $item;
					}
		
					if(isset($item['hidden'])){
						if($item['hidden']){
							$hidden[] = array("id"=>$item['id'],"name"=>$item['id'],"value"=>$item['value']);
						}
					}
		
					if($item['validate']){
						$validate[] = $item;
					}
		
					$items[$key] = $item;
					$width_label = $screen->utilities->getViewManageWidthLabel($item, $width_label);
		
				}
		
				$width_value = $window_width - $width_label;
						
        $contents = array();
        $contents['param'] = $param;
        $contents['items'] = $items;
        $contents['filter'] = $filter;

        $interfaces = array();
        $interfaces['target'] = $target;
        $interfaces['layout'] = $layout;
        $interfaces['form'] = $form;
        $interfaces['lines'] = $lines;
        $interfaces['rotule'] = $rotule;
        $interfaces['info'] = $info;
        $interfaces['window_width'] = $window_width;
        $interfaces['window_height'] = $window_height;
        $interfaces['width_label'] = $width_label;
        $interfaces['width_value'] = $width_value;
        $interfaces['width'] = $width;
        $interfaces['height'] = $height;
        $interfaces['modal'] = $modal;

        $actions = array();
        $actions['back'] = $back;
        $actions['execute'] = $execute;
        $actions['load'] = $load;

        $atributes = array();
        $atributes['primary'] = $primary;
        $atributes['foreign'] = $foreign;
        $atributes['hidden'] = $hidden;
        $atributes['children'] = $children;
        $atributes['popups'] = $popups;
        $atributes['validate'] = $validate;

        $recovered = array();
        $recovered['start'] = $start;
        $recovered['child'] = $extra_child;
        $recovered['where'] = $where;
        $recovered['group'] = $group;
        $recovered['order'] = $order;
        $recovered['w_description'] = $w_description;
        $recovered['g_description'] = $g_description;
        $recovered['o_description'] = $o_description;

        $values = array();
        $values['id'] = $emailNotificacao->get_emn_value($reference);
        $values['registro'] = $registro;
        $values['criador'] = $criador;
        $values['alteracao'] = $alteracao;
        $values['responsavel'] = $responsavel;

        $emailNotificacaoScreen->managerEmailNotificacaoScreen($level, $contents, $interfaces, $actions, $atributes, $recovered, $values);

      }catch (Exception $exception){
        print "Caught exception: ".$exception->getMessage()."\n";
      }
			
		}else{
			$screen->message->printMessageError(MESSAGE_FORBIDDEN);
		}
}

  /**
   * Exibe um formul�rio com uma listagem semi-din�mica para pesquisa e consulta das inst�ncias da entidade na base de dados
   * 
   * @param string $target
   * @param string $level
   * @param string $where
   * @param string $group
   * @param string $order
   * @param string $filter
   * @param string $rotule
   * @param string $w_description
   * @param string $g_description
   * @param string $o_description
   * @param string $start
   *
   * @author ADMINISTRADOR - 24/07/2013 12:06:01
   * <br><b>Updated by</b> ADMINISTRADOR - 24/07/2013 12:06:01
   */
 function viewListEmailNotificacao($target, $level, $where, $group, $order, $filter, $rotule, $w_description, $g_description, $o_description, $start){
  ?><?php

  $acesso_controle = -1;

  require System::desire('file', '', 'header',  'core', false);
  require_once System::desire('class', 'resource', 'Screen', 'core', false);

  System::desire('m', 'manager', 'EmailNotificacao', 'src', true, '{project.rsc}');
  System::desire('c', 'manager', 'EmailNotificacao', 'src', true, '{project.rsc}');
  System::desire('s', 'manager', 'EmailNotificacao', 'src', true, '{project.rsc}');

	$screen = new Screen('{project.rsc}');

  if($acesso_controle >= 0){

    try{

      $emailNotificacao = new EmailNotificacao();
      $emailNotificacaoScreen = new EmailNotificacaoScreen();

      $items = $emailNotificacao->get_emn_items();

      $properties = $emailNotificacao->get_emn_properties();

      $lines = $properties["lines"];
      $readonly = isset($properties["readonly"]) ? $properties["readonly"] : false;
      $checkbox = isset($properties["checkbox"]) ? $properties["checkbox"] : false;

      $child = System::request('child', false);
      $hidetoolbar = '';

      if($child == 'add'){
        $filter = ($filter == "") ? Encode::encrypt('FALSE') : $filter;
        $hidetoolbar = 'display:none;';
      }

      $height = System::request('height', 0);
      $window_height = WINDOW_HEIGHT + 30;
      if($height){
        $window_height = $height;
      }

      $width = System::request('width', 0);
      $window_width = WINDOW_WIDTH;
      if($width){
        $window_width = $width;
      }

      $reload = System::request("reload", false);
      if($reload){
        $recovered = System::recover($items, true, true, true);
        $where = Encode::encrypt($recovered['w']);
        $w_description = Encode::encrypt($recovered['wd']);
        $group = Encode::encrypt($recovered['g']);
        $g_description = Encode::encrypt($recovered['gd']);
        $order = Encode::encrypt($recovered['o']);
        $o_description = Encode::encrypt($recovered['od']);
        $start = 0;
      }

      $fast = System::request("fast");
      if($fast){
        $fast  = System::request('fast-search-'.$target);
        $recovered = System::recover($items, true, false, false, $fast);
        $where = Encode::encrypt($recovered['w']);
        $w_description = Encode::encrypt($recovered['wd']);
        $group = "";
        $g_description = "";
        $order = "";
        $o_description = "";
        $start = 0;
      }

      if($lines){
        $items = $screen->utilities->orderItems($items, $lines);
      }

      $contents = array();
      $contents['items'] = $items;
      $contents['filter'] = $filter;

      $interfaces = array();
      $interfaces['target'] = $target;
      $interfaces['rotule'] = $rotule;
      $interfaces['window_width'] = $window_width;
      $interfaces['window_height'] = $window_height;
      $interfaces['width'] = $width;
      $interfaces['height'] = $height;

      $actions = array();
      $actions['checkbox'] = $checkbox;
      $actions['readonly'] = $readonly;
      $actions['hidetoolbar'] = $hidetoolbar;

      $recovered = array();
      $recovered['start'] = $start;
      $recovered['child'] = $child;
      $recovered['fast'] = $fast;
      $recovered['where'] = $where;
      $recovered['group'] = $group;
      $recovered['order'] = $order;
      $recovered['w_description'] = $w_description;
      $recovered['g_description'] = $g_description;
      $recovered['o_description'] = $o_description;

      $emailNotificacaoScreen->listEmailNotificacaoScreen($level, $contents, $interfaces, $actions, $recovered);

    }catch (Exception $exception){
      print "Caught exception: ".$exception->getMessage()."\n";
    }
	
  }else{
    $screen->printMessageError(MESSAGE_FORBIDDEN);
  }
}

  /**
   * Carrega um combobox para sele��o de uma inst�ncia da entidade dentro de um formul�rio
   * 
   * @param string $id
   * @param string $level
   * @param string $where
   * @param string $group
   * @param string $order
   * @param string $name
   * @param string $selected
   * @param text $onchange
   * @param boolean $encode
   * @param int $width
   * @param string $filter
   *
   * @author ADMINISTRADOR - 24/07/2013 12:06:02
   * <br><b>Updated by</b> ADMINISTRADOR - 24/07/2013 12:06:02
   */
 function viewSelectEmailNotificacao($id, $level, $where, $group, $order, $name, $selected, $onchange, $encode, $width = 400, $filter = ""){
?><?php
  $acesso_controle = -1;
  
  require System::desire('file', '', 'header', 'core', false);  
  require System::desire('class', 'resource', 'Screen', 'core', false);

  System::desire('m','manager', 'EmailNotificacao', 'src', true, '{project.rsc}');
  System::desire('c','manager', 'EmailNotificacao', 'src', true, '{project.rsc}');
  
  $screen = new Screen('{project.rsc}');
  
  try {

    if ($acesso_controle >= 0) {

      $emailNotificacao = new EmailNotificacao();

      $properties = $emailNotificacao->get_emn_properties();
      $module = $properties["module"];
      $entity = $properties["entity"];
      $tag = $properties["tag"];
      $reference = $properties["reference"];
      $description = $properties["description"];

      $render = 'relationship-' . date('dmYHis') . '-' . rand();
      ?>

      <div id="<?php print $render; ?>"></div>

      <script type="text/javascript">
              
        var emn_url = 'src/<?php print $module; ?>/json/<?php print $entity; ?>.json.php?action=<?php print $tag; ?>-j-list&t=<?php print $id; ?>&l=<?php print $level; ?>&w=<?php print $where; ?>&g=<?php print $group; ?>&o=<?php print $order; ?>&f=<?php print $filter; ?>&rotule=&full=0';
        var emn_fields = ['<?php print $reference ?>','<?php print $description ?>'];
        var emn_field_value = '<?php print $reference ?>';
        var emn_field_display = '<?php print $description ?>';
              
        var combo = system.form.createComboBox('<?php print $name; ?>', '<?php print $render; ?>', true, null, emn_url, <?php print $width; ?>, '<?php print $selected; ?>', emn_fields, emn_field_display, emn_field_value, null);
              
        combo.on('select', function(combo, record){
          <?php 
            if($encode){
              $onchange = Encode::decrypt($onchange);
            }
            print $onchange; 
          ?>}
          ,this
        );
              
      </script>
      <?
    } else {
      $screen->printMessageError(MESSAGE_FORBIDDEN);
    }
  } catch (Exception $exception) {
    print "Caught exception: " . $exception->getMessage() . "n";
  }
}

