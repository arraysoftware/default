<?php
/**
 * @copyright array software
 *
 * @author ADMINISTRADOR - 18/01/2013 11:31:53
 * <br><b>Updated by</b> ADMINISTRADOR - 24/07/2013 10:54:11
 * @category view
 * @package manager
 *
 * Responsavel.....: ADMINISTRADOR
 * Alteracao.......: 28/07/2013 09:30:09
 */


if (System::request('action')) {
  $action = System::request('action');


 if ($action == 'parametros-v-edit') {
    $param = System::request('param', "edit");
    $target = System::request('t');
    $level = System::request('l');
    $where = System::request('w');
    $group = System::request('g');
    $order = System::request('o');
    $filter = System::request('f');
    $rotule = System::request('rotule');
    $w_description = System::request('wd');
    $g_description = System::request('gd');
    $o_description = System::request('od');
    $start = System::request('start');

    viewManagerParametros($param, $target, $level, $where, $group, $order, $filter, $rotule, $w_description, $g_description, $o_description, $start);
  } else if ($action == 'parametros-v-view') {
    $param = System::request('param', "view");
    $target = System::request('t');
    $level = System::request('l');
    $where = System::request('w');
    $group = System::request('g');
    $order = System::request('o');
    $filter = System::request('f');
    $rotule = System::request('rotule');
    $w_description = System::request('wd');
    $g_description = System::request('gd');
    $o_description = System::request('od');
    $start = System::request('start');

    viewManagerParametros($param, $target, $level, $where, $group, $order, $filter, $rotule, $w_description, $g_description, $o_description, $start);
  } else if ($action == 'parametros-v-add') {
    $param = System::request('param', "add");
    $target = System::request('t');
    $level = System::request('l');
    $where = System::request('w');
    $group = System::request('g');
    $order = System::request('o');
    $filter = System::request('f');
    $rotule = System::request('rotule');
    $w_description = System::request('wd');
    $g_description = System::request('gd');
    $o_description = System::request('od');
    $start = System::request('start');

    viewManagerParametros($param, $target, $level, $where, $group, $order, $filter, $rotule, $w_description, $g_description, $o_description, $start);
  } else if ($action == 'parametros-v-search') {
    $param = System::request('param', "search");
    $target = System::request('t');
    $level = System::request('l');
    $where = System::request('w');
    $group = System::request('g');
    $order = System::request('o');
    $filter = System::request('f');
    $rotule = System::request('rotule');
    $w_description = System::request('wd');
    $g_description = System::request('gd');
    $o_description = System::request('od');
    $start = System::request('start');

    viewManagerParametros($param, $target, $level, $where, $group, $order, $filter, $rotule, $w_description, $g_description, $o_description, $start);
  } else if ($action == 'parametros-v-list') {
    $target = System::request('t');
    $level = System::request('l');
    $where = System::request('w');
    $group = System::request('g');
    $order = System::request('o');
    $filter = System::request('f');
    $rotule = System::request('rotule');
    $w_description = System::request('wd');
    $g_description = System::request('gd');
    $o_description = System::request('od');
    $start = System::request('start');

    viewListParametros($target, $level, $where, $group, $order, $filter, $rotule, $w_description, $g_description, $o_description, $start);
  } else if ($action == 'parametros-v-select') {
    $id = System::request('id');
    $level = System::request('l');
    $where = System::request('w');
    $group = System::request('g');
    $order = System::request('o');
    $name = System::request('name');
    $selected = System::request('selected');
    $onchange = System::request('onchange');
    $encode = System::request('encode');
    $width = System::request('width');
    $filter = System::request('f');

    viewSelectParametros($id, $level, $where, $group, $order, $name, $selected, $onchange, $encode, $width, $filter);
  }
}

  /**
   * Exibe um formul�rio para administrar a Entidade
   * 
   * @param string $param
   * @param string $target
   * @param string $level
   * @param string $where
   * @param string $group
   * @param string $order
   * @param string $filter
   * @param string $rotule
   * @param string $w_description
   * @param string $g_description
   * @param string $o_description
   * @param string $start
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 02/05/2013 19:15:17
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 09/05/2013 15:25:25
   */
 function viewManagerParametros($param, $target, $level, $where, $group, $order, $filter, $rotule, $w_description, $g_description, $o_description, $start){
  ?><?php
    $acesso_controle = -1;

    require System::desire('file', '', 'header', 'core', false);
    require System::desire('class', 'resource', 'Screen', 'core', false);

    System::desire('m', 'manager', 'Parametros', 'src', true, '');
    System::desire('c', 'manager', 'Parametros', 'src', true, '');

    $screen = new Screen('');

    $parametros = new Parametros();

    $items = $parametros->get_par_items();
    $primary = array();
    $foreign = array();
    $validate = array();
    $children = array();
    $popups = array();
    $load = "";

    $properties = $parametros->get_par_properties();
    $module = $properties["module"];
    $entity = $properties["entity"];
    $tag = $properties["tag"];
    $lines = $properties["lines"];
    $reference = $properties['reference'];
    $description = $properties['description'];
    $layout = isset($properties['layout']) ? $properties['layout'] : "";
    if(System::request('layout')){
      $layout = System::request('layout');
    }
    $form = System::request('form');
    $modal = Boolean::parse(System::request('modal'));
    $back = System::request('back',true);
    $execute = System::request('execute','');
    $core = isset($properties["core"]) ? $properties["core"] : false;
    $database = isset($properties["database"]) ? $properties["database"] : DEFAULT_DATABASE;

    $width_label = 0;

    $width = System::request('width', 0);
    $window_width = WINDOW_WIDTH;
    if($width){
      $window_width = $width;
    }

    $window_height = WINDOW_HEIGHT;
    $height = System::request('height', 0);
    if($height){
      $window_height = $height;
    }

    $extra_child = System::request('child');

    $registro = date('d/m/Y H:i:s');
    $criador = System::getUser();
    $alteracao = date('d/m/Y H:i:s');
    $responsavel = System::getUser();

    $parametrosCtrl = new ParametrosCtrl(PATH_APP, $database);

    if($param == "view" || $param == "edit"){

      $info = "Editar ";

      $s = "";
      $conector = " AND ";
      foreach($items as $item){
        if($item['pk']){
          $key = $item['id'];
          $s .= $conector.$key." = ".System::request($key);
        }
      }
      $search = substr($s, strlen($conector));

      $parametross = $parametrosCtrl->getParametrosCtrl($search, "", "", "", "");
      $parametros = $parametross[0];

      foreach($items as $item){
        $key = $item['id'];
        $items[$key]['value'] = $parametros->get_par_value($key);
				
				if($item['type'] === 'select-multi'){
          $items[$key]['select-multi']['filter'] = $parametros->get_par_value($items[$key]['select-multi']['key']);
        }
      }

      $registro = $parametros->get_par_value('par_registro');
      $criador = $parametros->get_par_value('par_criador');
      $alteracao = $parametros->get_par_value('par_alteracao');
      $responsavel = $parametros->get_par_value('par_responsavel');

      if($param == "view") {
        $info = "Visualizar ";    
        foreach($items as $key => $item){
          $items[$key] = $screen->utilities->configureViewManager($item);
        }
      }

    }else if($param == "add"){

      $info = "Novo";

      foreach($items as $item){
        $key = $item['id'];
        $items[$key] = $screen->utilities->configureAddManager($item);
      }

    }else if($param == "search"){

      $info = "Pesquisar";

      foreach($items as $item){
        $key = $item['id'];
        $items[$key] = $screen->utilities->configureSearchManager($item);
      }

    }

    $one = true;
    $__filter = explode(",", $filter);
    $__fk = -1;

    foreach($items as $item){
      $key = $item['id'];

      if($item['type_behavior'] == 'child'){
        
        $_width = $window_width - 20;
    
        if($layout === 'manager' or $layout === ''){
          $_height = $window_width - (($lines * 28) + 60);
        }else{
          $_height = $window_width - 10;
        }
    
        if($modal){
          $_height = $window_height - 230;
          $_width = $window_width - 20;
        }

        $_key = $item['child']['key'];
        $_name = $item['child']['name'];
        $_filter = $item['child']['filter'];
        $_target = $item['child']['target']."-".$target;

        $item['child']['height'] = $_height;
        $item['child']['width'] = $_width;
        $item['child']['rotule'] = Encode::encrypt($_name);
        $item['child']['level'] = $level;
        $item['child']['params'] = 'child='.$param.'&width='.$_width.'&height='.$_height.'&rotule='.Encode::encrypt($_name).'&t='.$_target.'&f='.Encode::encrypt($parametros->get_par_value($key));

        $_child = $item['child'];
        $children[] = $item;

        $params = "child=list&width=".$_width."&height=".$_height."&rotule=".Encode::encrypt($_name)."&t=".$_target."&f='+system.encrypt.encode(system.util.getValue('".$_key."','".$target."'))+'";
        if($param == 'add' and $one){
          $load = array($_child['form'],$_child['module'],$_child['entity'],$_child['tag'],$_target,$level,$params);
          $one = false;
        }

      }

      if ($item['type_behavior'] == 'popup') {
        $_height = $window_height - 40;
        $_width = $window_width - 40;

        $_key = $item['popup']['key'];
        $_name = $item['popup']['name'];
        $_filter = $item['popup']['filter'];
        $_target = 'popup_'.str_replace('-', '_', $item['popup']['target']);

        $item['line'] = 0;
        $item['type'] = 'popup';
        $item['popup']['height'] = $_height;
        $item['popup']['width'] = $_width;
        $item['popup']['rotule'] = $_name;
        $item['popup']['level'] = $level;
        $item['popup']['params'] = 'width='.$_width.'&height='.$_height.'&rotule='.Encode::encrypt($_name).'&t='.$_target."&f='+system.encrypt.encode(system.util.getValue('".$item['popup']['key']."'))+'";

        $item['popup']['parent']['target'] = $target;
        $item['popup']['parent']['module'] = $module;
        $item['popup']['parent']['entity'] = $entity;
        $item['popup']['parent']['tag'] = $tag;

        $popups[] = $item;
      }

      if($item['fk'] && $filter){
        $__fk++;
        if($param == 'add'){
          if(isset($__filter[$__fk])){
            $item['value'] = Encode::decrypt($__filter[$__fk]);
            if($item['type_behavior'] == 'foreign' && $filter){
              //usar quando preciso
              //$item['foreign']['where'] = Encode::encrypt($item['foreign']['key']."='".Encode::decrypt($filter)."'");
            }
          }
        }
      }

      if($item['pk']){
        $primary[] = $item;
      }

      if($item['fk']){
        $foreign[] = $item;
      }

      if($item['validate']){
        $validate[] = $item;
      }

      $items[$key] = $item;
      $width_label = $screen->utilities->getViewManageWidthLabel($item, $width_label);

    }

    $width_value = $window_width - $width_label;

    try{
      if($acesso_controle >= 1 || $param == "view"){

        $_id = $parametros->get_par_value($reference);
        $form = $form ? $form : 'form-'.$target;

        ?>
        <center>
          <?php 
            if($layout === 'manager' or $layout === ''){
              
              $name = Encode::decrypt($rotule);
              
              if($layout === 'manager'){
                $info = "";
                $filter = "";
                $height = "";
                $width = "";
                $target = System::request('tc');
                $extra_child = "";
              }
              
              $screen->manager->printManagerHeader($target, $name, $info, $window_width);
              
                $screen->manager->printManagerFormHeader($form, $target, Encode::decrypt($rotule), $info, $window_width);

                  $screen->manager->printManagerHiddens($primary, $foreign);

                  $screen->manager->printFormValues($target, $level, $where, $group, $order, $filter, $w_description, $g_description, $o_description, $rotule, $start, $height, $width, $extra_child);

                  $screen->manager->printManagerLinesHeader($form, $target, Encode::decrypt($rotule), $info, $window_width);
                  
                    $screen->manager->printManagerLines($param, $items, $lines, $width_label, $width_value, $window_width);

                    if($param != "search"){
                      $screen->line->printPopups($popups);
                    }
                    

                    if($param != "search"){
                      if($layout === 'children' or $layout === ''){
                        $screen->line->printChildren($children, $window_width, $target, $width_label, $width_value, $param, '', true);
                      }
                    }
                    
                  $screen->manager->printManagerLinesBottom($target, Encode::decrypt($rotule), $info, $window_width);

                $screen->manager->printManagerFormBottom($form, $target, Encode::decrypt($rotule), $info, $window_width);
              
                if($param != "search"){
                  $screen->manager->printFormHistory($window_width, $registro, $criador, $alteracao, $responsavel, $target, $module, $rotule, $entity, $tag, $_id, false);
                }

              $screen->manager->printManagerBottom($target, Encode::decrypt($rotule), $info, $window_width);

            }else{

              $name = Encode::decrypt(System::request('rc'));

              $screen->manager->printManagerHeader($target, $name, $info, $window_width);

              if($layout === 'children'){

                $_key = $reference;
                $_name = Encode::decrypt($rotule);

                $item = $screen->utilities->generateChild($param, $module, $entity, $tag, $_name, $_key, $target, $level, $filter, $form, $_id, $_height, $_width);

                array_unshift($children, $item);
              }

              if($param != "search"){
                if($layout === 'children' or $layout === ''){
                  $screen->line->printChildren($children, $window_width, $target, $width_label, $width_value, $param, '', true);
                }
              }

              $screen->manager->printManagerBottom($target, Encode::decrypt($rotule), $info, $window_width);

            }
          ?>
          <br>
        </center>

        <?php

          if($layout === 'children' or $layout === ''){
            $v = "";
            if(count($validate) > 0){
              foreach($validate as $item){
                $details = "";
                if(isset($item['foreign'])){
                  $details = ", 'target':'ext-".$item['foreign']['target']."'";
                }
                $v .= ",'".$item['id']."': {'description':'".$item['description']."', 'value':'".$item['validate']."'".$details."}";
              }
            }

            $valid = ($v != "") ? '{'.substr($v, 1).'}' : "";

            $value = $_id ? $_id : 'undefined';
            
            $extras = null;

            $html = '<input type="hidden" id="pme_codigo" name="pme_codigo" value="'.$value.'"/>';
            
            $screen->manager->printManagerToolbar($acesso_controle, $param, $target, $level, $module, $entity, $tag, $back, $execute, $valid, $load, $core, $form, $html, $extras, $window_width);
          }

      }else{
        $screen->message->printMessageError(MESSAGE_FORBIDDEN);
      }
    }catch (Exception $exception){
      print "Caught exception: ".$exception->getMessage()."n";
    }
}

  /**
   * Exibe um formul�rio com uma listagem semi-din�mica para pesquisa e consulta das inst�ncias da entidade na base de dados
   * 
   * @param string $target
   * @param string $level
   * @param string $where
   * @param string $group
   * @param string $order
   * @param string $filter
   * @param string $rotule
   * @param string $w_description
   * @param string $g_description
   * @param string $o_description
   * @param string $start
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 02/05/2013 19:15:17
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 09/05/2013 15:25:31
   */
 function viewListParametros($target, $level, $where, $group, $order, $filter, $rotule, $w_description, $g_description, $o_description, $start){
  ?><?php
  $acesso_controle = -1;
  
  require System::desire('file', '', 'header',  'core', false);  
  require System::desire('class', 'resource', 'Screen', 'core', false);
  
  System::desire('m','manager', 'Parametros', 'src', true, '');
  System::desire('c','manager', 'Parametros', 'src', true, '');

  $screen = new Screen('');

  $parametros = new Parametros();
  
  $items = $parametros->get_par_items();
  $fast = '';
  
  $properties = $parametros->get_par_properties();
  $reference = $properties["reference"];
  $module = $properties["module"];
  $entity = $properties["entity"];
  $tag = $properties["tag"];
  $lines = $properties["lines"];
  
  $core = isset($properties["core"]) ? $properties["core"] : false;
  $readonly = isset($properties["readonly"]) ? $properties["readonly"] : false;
  $saveonly = isset($properties["saveonly"]) ? $properties["saveonly"] : false;
  $checkbox = isset($properties["checkbox"]) ? $properties["checkbox"] : false;
  
  $child = System::request('child', false);
  $hidetoolbar = '';
  
  if($child == 'add'){
    $filter = ($filter == "") ? Encode::encrypt('FALSE') : $filter;
    $hidetoolbar = 'display:none;';
  }
  
  $height = System::request('height', 0);
  $window_height = WINDOW_HEIGHT + 30;
  if($height){
    $window_height = $height;
  }
  
  $width = System::request('width', 0);
  $window_width = WINDOW_WIDTH;
  if($width){
    $window_width = $width;
  }
  
  if($acesso_controle >= 0){
    
    try{
      
      $reload = System::request("reload", false);
      if($reload){
        $recovered = System::recover($items, true, true, true);
        $where = Encode::encrypt($recovered['w']);
        $w_description = Encode::encrypt($recovered['wd']);
        $group = Encode::encrypt($recovered['g']);
        $g_description = Encode::encrypt($recovered['gd']);
        $order = Encode::encrypt($recovered['o']);
        $o_description = Encode::encrypt($recovered['od']);
        $start = 0;
      }
      
      $fast = System::request("fast");
      if($fast){
        $fast  = System::request('fast-search-'.$target);
        $recovered = System::recover($items, true, false, false, $fast);
        $where = Encode::encrypt($recovered['w']);
        $w_description = Encode::encrypt($recovered['wd']);
        $group = "";
        $g_description = "";
        $order = "";
        $o_description = "";
        $start = 0;
      }
      
      $items = $screen->utilities->orderItems($items, $lines);
      
      $id = "grid-".$target;
      $end = ITENS;
      
      ?>
        <center>
          <form id="form-<?php print $target;?>" name="form-<?php print $target;?>" action="return false;" onsubmit="return false;">
            <?php
              $screen->list->printListHeader($target, Encode::decrypt($rotule), $hidetoolbar, $child);
      
              $screen->list->printListMarkup($id, $window_width, $window_height);
      
              $screen->list->printListBottom($target, Encode::decrypt($rotule), $hidetoolbar, $child);
      
              $screen->manager->printFormValues($target, $level, $where, $group, $order, $filter, $w_description, $g_description, $o_description, $rotule, $start, $height, $width, $child);
            ?>
          </form>
        </center>

      <?php

			$option = $screen->utilities->getViewListOptionWidth($acesso_controle, $properties, false);
      
      $screen->list->printListToolbar($acesso_controle, $target, $level, $module, $entity, $tag, $fast, $w_description, $core, $readonly, $window_width);
      
      $screen->list->printListGridAction($module, $rotule, $entity, $tag, $reference, $properties);
      
      $screen->list->printListGrid($module, $rotule, $entity, $tag, $items, $target, $id, $start, $end, $window_height, $window_width, $level, $filter, $where, $group, $order, $option, $checkbox);
      
    }catch (Exception $exception){
      print "Caught exception: ".$exception->getMessage()."n";
    }
  }else{
    
    $screen->printMessageError(MESSAGE_FORBIDDEN);
    
  }
}

  /**
   * Carrega um combobox para sele��o de uma inst�ncia da entidade dentro de um formul�rio
   * 
   * @param string $id
   * @param string $level
   * @param string $where
   * @param string $group
   * @param string $order
   * @param string $name
   * @param string $selected
   * @param text $onchange
   * @param boolean $encode
   * @param int $width
   * @param string $filter
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 02/05/2013 19:15:17
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 09/05/2013 15:25:37
   */
 function viewSelectParametros($id, $level, $where, $group, $order, $name, $selected, $onchange, $encode, $width = 400, $filter = ""){
  ?><?php
  $acesso_controle = -1;
  
  require System::desire('file', '', 'header', 'core', false);  
  require System::desire('class', 'resource', 'Screen', 'core', false);

  System::desire('m','manager', 'Parametros', 'src', true, '');
  System::desire('c','manager', 'Parametros', 'src', true, '');
  
  $screen = new Screen('');
  
  try {

    if ($acesso_controle >= 0) {

      $parametros = new Parametros();

      $properties = $parametros->get_par_properties();
      $module = $properties["module"];
      $entity = $properties["entity"];
      $tag = $properties["tag"];
      $reference = $properties["reference"];
      $description = $properties["description"];

      $render = 'relationship-' . date('dmYHis') . '-' . rand();
      ?>

      <div id="<?php print $render; ?>"></div>

      <script type="text/javascript">
              
        var par_url = 'src/<?php print $module; ?>/json/<?php print $entity; ?>.gen.json.php?action=<?php print $tag; ?>-j-list&t=<?php print $id; ?>&l=<?php print $level; ?>&w=<?php print $where; ?>&g=<?php print $group; ?>&o=<?php print $order; ?>&f=<?php print $filter; ?>&rotule=&full=0';
        var par_fields = ['<?php print $reference ?>','<?php print $description ?>'];
        var par_field_value = '<?php print $reference ?>';
        var par_field_display = '<?php print $description ?>';
              
        var combo = system.form.createComboBox('<?php print $name; ?>', '<?php print $render; ?>', true, null, par_url, <?php print $width; ?>, '<?php print $selected; ?>', par_fields, par_field_display, par_field_value, null);
              
        combo.on('select', function(combo, record){
          <?php 
            if($encode){
              $onchange = Encode::decrypt($onchange);
            }
            print $onchange; 
          ?>}
          ,this
        );
              
      </script>
      <?
    } else {
      $screen->printMessageError(MESSAGE_FORBIDDEN);
    }
  } catch (Exception $exception) {
    print "Caught exception: " . $exception->getMessage() . "n";
  }
}

