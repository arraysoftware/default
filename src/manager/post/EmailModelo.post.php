<?php
/**
 * @copyright array software
 *
 * @author WILLIAM MARQUES VICENTE GOMES CORREA - 05/05/2013 21:56:15
 * <br><b>Updated by</b> ADMINISTRADOR - 24/07/2013 12:07:37
 * @category post
 * @package manager
 *
 * Responsavel.....: ADMINISTRADOR
 * Alteracao.......: 28/07/2013 09:30:09
 */


if (System::request('action')) {
  $action = System::request('action');


 if ($action == 'emailmodelo-p-save') {
    $level = System::request('l');
    $copy = System::request('c', false);
    $presave = System::request('p');

    postSaveEmailModelo($level, $copy, $presave);
  } else if ($action == 'emailmodelo-p-copy') {
    $level = System::request('l');
    $copy = System::request('c', true);
    $presave = System::request('p');

    postSaveEmailModelo($level, $copy, $presave);
  } else if ($action == 'emailmodelo-p-remove') {
    $level = System::request('l');

    postRemoveEmailModelo($level);
  }
}

  /**
   * M�todo que permite registrar, editar ou copiar uma inst�ncia da entidade na base de dados
   * 
   * @param string $level
   * @param boolean $copy
   * @param boolean $presave
   *
   * @author ADMINISTRADOR - 24/07/2013 12:07:27
   * <br><b>Updated by</b> ADMINISTRADOR - 24/07/2013 12:07:27
   */
 function postSaveEmailModelo($level, $copy = false, $presave = false){
  ?><?php

  $acesso_controle = -1;

  require System::desire('file','' , 'header', 'core', false);
  require System::desire('class', 'resource', 'Screen', 'core', false);

  System::desire('m', 'manager', 'EmailModelo', 'src', true, '{project.rsc}');
  System::desire('c', 'manager', 'EmailModelo', 'src', true, '{project.rsc}');

  $screen = new Screen();

  if($acesso_controle >= 1){

    $mode = '';
    $message_sucess = MESSAGE_ADD_SUCCESS;
    $message_error = MESSAGE_ADD_ERROR;

    $saved = "";
		$validate = array();
    $emailModelo = new EmailModelo();
    $properties = $emailModelo->get_emo_properties();
    $reference = $properties['reference'];
		$database = isset($properties["database"]) ? $properties["database"] : DEFAULT_DATABASE;

    $pk = System::request($reference);
    if($copy){
      $copy = $pk;
      $pk = '';
      $message_sucess = MESSAGE_COPY_SUCCESS;
      $message_error = MESSAGE_COPY_ERROR;
    }

    if(empty($pk)){
      $mode = 'add';
    }else{
      $mode = 'edit';
      $message_sucess = MESSAGE_SET_SUCCESS;
      $message_error = MESSAGE_SET_ERROR;
    }

    $emailModeloCtrl = new EmailModeloCtrl(PATH_APP, $database);

    $items = $emailModelo->get_emo_items();

    $emailModelo->set_emo_value($reference, $pk);

    foreach($items as $item){
      $key = $item['id'];
      $value = System::request($key, null);

			if($item['type'] === 'select-multi'){
        $entity = $item['select-multi']['entity'];
        $value = System::request('source-'.$entity);
      } else if($item['type_behavior'] === 'parent') {
        if (isset($item['parent'])) {
          $class = $item['parent']['entity'];
          System::desire('m', $item['parent']['modulo'], $class, 'src', true);
          $obj = new $class();
          $set_value = "set_".$item['parent']['prefix']."_value";
          $get_items = "get_".$item['parent']['prefix']."_items";

          $itens = $obj->$get_items();
          foreach ($itens as $iten) {
            $k = $iten['id'];
            $v = System::request($k, null);
            $obj->$set_value($k, $v);
          }
          $value = $obj;
        }
      }

			$message = System::validate($item, $value);
      if($message === ""){
        $emailModelo->set_emo_value($key, $value);
      }else{
        $validate[] = $message;
      }

    }

    $emailModelo->set_emo_value('emo_responsavel', System::getUser());
    $emailModelo->set_emo_value('emo_criador', System::getUser());
		
		$verify = $emailModeloCtrl->verifyEmailModeloCtrl($emailModelo);

		if (!$validate && !$verify) {
		  if ($mode === 'add') {
        $save = $emailModeloCtrl->addEmailModeloCtrl($emailModelo, false, false, $presave, $copy);
        $pk = $save;
      } else if ($mode === 'edit') {
				if(isset($properties['saveonly']) && $properties['saveonly']){
          $message_error = MESSAGE_SAVEONLY;
          $save = false;
        }else{
          $save = $emailModeloCtrl->setEmailModeloCtrl($emailModelo);
        }
      }	
		}else{
			$save = false;
		}
		
    $saved = '<input type="hidden" name="'.$reference.'" id="'.$reference.'" value="'.$pk.'"/>';

    if ($save) {
      $screen->message->printMessageSucess($message_sucess.$saved);
    } else {
			$errors = array_merge($validate, $verify);
      $screen->message->printMessageError($message_error.$saved, true, $errors);
    }

  }else{
    $screen->message->printMessageError(MESSAGE_FORBIDDEN);
  }
}

  /**
   * Inicia o processo de remo��o de uma inst�ncia da entidade da base de dados
   * 
   * @param string $level
   *
   * @author ADMINISTRADOR - 24/07/2013 12:07:27
   * <br><b>Updated by</b> ADMINISTRADOR - 24/07/2013 12:07:27
   */
 function postRemoveEmailModelo($level){
  ?><?php

  $acesso_controle = -1;

  require System::desire('file', '', 'header', 'core', false);
  
  require System::desire('class', 'resource', 'Screen', 'core', false);
  
  System::desire('m','manager', 'EmailModelo', 'src', true, '{project.rsc}');
  System::desire('c','manager', 'EmailModelo', 'src', true, '{project.rsc}');
  
  $screen = new Screen();

  if($acesso_controle >= 2){
    
		$emailModelo = new EmailModelo();
    
    $properties = $emailModelo->get_emo_properties();
    $reference = $properties['reference'];
		$database = isset($properties["database"]) ? $properties["database"] : DEFAULT_DATABASE;

    $key = $reference;
    $value = System::request($key);
		$emailModeloCtrl = new EmailModeloCtrl(PATH_APP, $database);
		$remove = false;
		
		$emailModelos = $emailModeloCtrl->getEmailModeloCtrl($key." = '".$value."'", "", "", "0", "1");
		if(is_array($emailModelos)){
			$emailModelo = $emailModelos[0];
			
			$emailModelo->set_emo_value('emo_responsavel', System::getUser());
			
			$remove = $emailModeloCtrl->removeEmailModeloCtrl($emailModelo);
			
			$message_sucess = MESSAGE_REMOVE_SUCCESS;
			$message_error = MESSAGE_REMOVE_ERROR;
	
			$r = isset($properties["remove"]) ? $properties["remove"] : true;
			if(isset($r["success"])){
				$message_sucess = $r["success"];
			}
		}

    if($remove){
      $screen->message->printMessageSucess($message_sucess, false);
    }else{
      $screen->message->printMessageError($message_error, false);
    }
    
  }else{
    $screen->message->printMessageError(MESSAGE_FORBIDDEN);
  }
}

