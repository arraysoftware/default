<?php
/**
 * @copyright array software
 *
 * @author WILLIAM MARQUES VICENTE GOMES CORREA - 20/04/2013 12:46:48
 * <br><b>Updated by</b> ADMINISTRADOR - 15/06/2013 18:51:44
 * @category post
 * @package manager
 *
 * Responsavel.....: ADMINISTRADOR
 * Alteracao.......: 28/07/2013 09:30:09
 */


if (System::request('action')) {
  $action = System::request('action');


 if ($action == 'usuario-p-save') {
    $level = System::request('l');
    $copy = System::request('c', false);
    $presave = System::request('p');

    postSaveUsuario($level, $copy, $presave);
  } else if ($action == 'usuario-p-copy') {
    $level = System::request('l');
    $copy = System::request('c', true);
    $presave = System::request('p');

    postSaveUsuario($level, $copy, $presave);
  } else if ($action == 'usuario-p-remove') {
    $level = System::request('l');

    postRemoveUsuario($level);
  }
}

  /**
   * M�todo que permite registrar, editar ou copiar uma inst�ncia da entidade na base de dados
   * 
   * @param string $level
   * @param boolean $copy
   * @param boolean $presave
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 28/05/2013 18:42:48
   * <br><b>Updated by</b> ADMINISTRADOR - 23/07/2013 13:54:49
   */
 function postSaveUsuario($level, $copy = false, $presave = false){
  ?><?php

  $acesso_controle = -1;

  require System::desire('file','' , 'header', 'core', false);
  require System::desire('class', 'resource', 'Screen', 'core', false);

  System::desire('m', 'manager', 'Usuario', 'src', true, '{project.rsc}');
  System::desire('c', 'manager', 'Usuario', 'src', true, '{project.rsc}');

  $screen = new Screen();

  if($acesso_controle >= 1){

    $mode = '';
    $message_sucess = MESSAGE_ADD_SUCCESS;
    $message_error = MESSAGE_ADD_ERROR;

    $saved = "";
		$validate = "";
    $usuario = new Usuario();
    $properties = $usuario->get_usu_properties();
    $reference = $properties['reference'];
		$database = isset($properties["database"]) ? $properties["database"] : DEFAULT_DATABASE;

    $pk = System::request($reference);
    if($copy){
      $copy = $pk;
      $pk = '';
      $message_sucess = MESSAGE_COPY_SUCCESS;
      $message_error = MESSAGE_COPY_ERROR;
    }

    if(empty($pk)){
      $mode = 'add';
    }else{
      $mode = 'edit';
      $message_sucess = MESSAGE_SET_SUCCESS;
      $message_error = MESSAGE_SET_ERROR;
    }

    $usuarioCtrl = new UsuarioCtrl(PATH_APP, $database);

    $items = $usuario->get_usu_items();

    $usuario->set_usu_value($reference, $pk);

    foreach($items as $item){
      $key = $item['id'];
      $value = System::request($key, null);

			if($key == 'usu_password' and $value == ''){
				$value = null;
			}
			
			if($item['type'] === 'select-multi'){
        $entity = $item['select-multi']['entity'];
        $value = System::request('source-'.$entity);
      } else if($item['type_behavior'] === 'parent') {
        if (isset($item['parent'])) {
          $class = $item['parent']['entity'];
          System::desire('m', $item['parent']['modulo'], $class, 'src', true);
          $obj = new $class();
          $set_value = "set_".$item['parent']['prefix']."_value";
          $get_items = "get_".$item['parent']['prefix']."_items";

          $itens = $obj->$get_items();
          foreach ($itens as $iten) {
            $k = $iten['id'];
            $v = System::request($k, null);
            $obj->$set_value($k, $v);
          }
          $value = $obj;
        }
      }

			$message = System::validate($item, $value);
      if($message === ""){
        $usuario->set_usu_value($key, $value);
      }else{
        $validate[] = $message;
      }

    }

    $usuario->set_usu_value('usu_responsavel', System::getUser());
    $usuario->set_usu_value('usu_criador', System::getUser());

		if($validate === ""){
		  if ($mode === 'add') {
        $save = $usuarioCtrl->addUsuarioCtrl($usuario, false, false, $presave, $copy);
        $pk = $save;
      } else if ($mode === 'edit') {
				if(isset($properties['saveonly']) && $properties['saveonly']){
          $message_error = MESSAGE_SAVEONLY;
          $save = false;
        }else{
          $save = $usuarioCtrl->setUsuarioCtrl($usuario);
        }
      }	
		}else{
			$save = false;
		}
		
    $saved = '<input type="hidden" name="'.$reference.'" id="'.$reference.'" value="'.$pk.'"/>';

    if ($save) {
      $screen->message->printMessageSucess($message_sucess.$saved);
    } else {
      $screen->message->printMessageError($message_error.$saved, true, $validate);
    }

  }else{
    $screen->message->printMessageError(MESSAGE_FORBIDDEN);
  }
}

  /**
   * Inicia o processo de remo��o de uma inst�ncia da entidade da base de dados
   * 
   * @param string $level
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 28/05/2013 18:42:48
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 28/05/2013 18:42:48
   */
 function postRemoveUsuario($level){
  ?><?php

  $acesso_controle = -1;

  require System::desire('file', '', 'header', 'core', false);
  
  require System::desire('class', 'resource', 'Screen', 'core', false);
  
  System::desire('m','manager', 'Usuario', 'src', true, '{project.rsc}');
  System::desire('c','manager', 'Usuario', 'src', true, '{project.rsc}');
  
  $screen = new Screen();

  if($acesso_controle >= 2){
      
    $usuario = new Usuario();
    
    $properties = $usuario->get_usu_properties();
    $reference = $properties['reference'];
		$database = isset($properties["database"]) ? $properties["database"] : DEFAULT_DATABASE;

    $key = $reference;
    $value = System::request($key);
    $usuario->set_usu_value($key, $value);
    $usuario->set_usu_value('usu_responsavel', System::getUser());
		
    $usuarioCtrl = new UsuarioCtrl(PATH_APP, $database);
    $remove = $usuarioCtrl->removeUsuarioCtrl($usuario);
		
		$message_sucess = MESSAGE_REMOVE_SUCCESS;
    $message_error = MESSAGE_REMOVE_ERROR;

    $r = isset($properties["remove"]) ? $properties["remove"] : true;
    if(isset($r["success"])){
      $message_sucess = $r["success"];
    }

    if($remove){
      $screen->message->printMessageSucess($message_sucess, false);
    }else{
      $screen->message->printMessageError($message_error, false);
    }
    
  }else{
    $screen->message->printMessageError(MESSAGE_FORBIDDEN);
  }
}

