<?php
/**
 * @copyright array software
 *
 * @author WILLIAM MARQUES VICENTE GOMES CORREA - 10/05/2013 15:25:43
 * <br><b>Updated by</b> ADMINISTRADOR - 15/06/2013 18:51:33
 * @category post
 * @package manager
 *
 * Responsavel.....: ADMINISTRADOR
 * Alteracao.......: 28/07/2013 09:30:09
 */


if (System::request('action')) {
  $action = System::request('action');


 if ($action == 'atualizacao-p-save') {
    $level = System::request('l');
    $copy = System::request('c', false);
    $presave = System::request('p');

    postSaveAtualizacao($level, $copy, $presave);
  } else if ($action == 'atualizacao-p-copy') {
    $level = System::request('l');
    $copy = System::request('c', true);
    $presave = System::request('p');

    postSaveAtualizacao($level, $copy, $presave);
  } else if ($action == 'atualizacao-p-remove') {
    $level = System::request('l');

    postRemoveAtualizacao($level);
  }
}

  /**
   * M�todo que permite registrar, editar ou copiar uma inst�ncia da entidade na base de dados
   * 
   * @param string $level
   * @param boolean $copy
   * @param boolean $presave
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 10/05/2013 15:25:46
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 10/05/2013 15:25:46
   */
 function postSaveAtualizacao($level, $copy = false, $presave = false){
?><?php

  $acesso_controle = -1;

  require System::desire('file','' , 'header', 'core', false);
  require System::desire('class', 'resource', 'Screen', 'core', false);

  System::desire('m', 'manager', 'Atualizacao', 'src', true, '');
  System::desire('c', 'manager', 'Atualizacao', 'src', true, '');

  $screen = new Screen();

  if($acesso_controle >= 1){

    $mode = '';
    $message_sucess = MESSAGE_ADD_SUCCESS;
    $message_error = MESSAGE_ADD_ERROR;

    $saved = "";
		$validate = "";
    $atualizacao = new Atualizacao();
    $properties = $atualizacao->get_atl_properties();
    $reference = $properties['reference'];
		$database = isset($properties["database"]) ? $properties["database"] : DEFAULT_DATABASE;

    $pk = System::request($reference);
    if($copy){
      $copy = $pk;
      $pk = '';
      $message_sucess = MESSAGE_COPY_SUCCESS;
      $message_error = MESSAGE_COPY_ERROR;
    }

    if(empty($pk)){
      $mode = 'add';
    }else{
      $mode = 'edit';
      $message_sucess = MESSAGE_SET_SUCCESS;
      $message_error = MESSAGE_SET_ERROR;
    }

    $atualizacaoCtrl = new AtualizacaoCtrl(PATH_APP, $database);

    $items = $atualizacao->get_atl_items();

    $atualizacao->set_atl_value($reference, $pk);

    foreach($items as $item){
      $key = $item['id'];
      $value = System::request($key, null);

			if($item['type'] === 'select-multi'){
        $entity = $item['select-multi']['entity'];
        $value = System::request('source-'.$entity);
      }

			$message = System::validate($item, $value);
      if($message === ""){
        $atualizacao->set_atl_value($key, $value);
      }else{
        $validate[] = $message;
      }

    }

    $atualizacao->set_atl_value('atl_responsavel', System::getUser());
    $atualizacao->set_atl_value('atl_criador', System::getUser());

		if($validate === ""){
		  if ($mode === 'add') {
        $save = $atualizacaoCtrl->addAtualizacaoCtrl($atualizacao, false, false, $presave, $copy);
        $pk = $save;
      } else if ($mode === 'edit') {
				if(isset($properties['saveonly']) && $properties['saveonly']){
          $message_error = MESSAGE_SAVEONLY;
          $save = false;
        }else{
          $save = $atualizacaoCtrl->setAtualizacaoCtrl($atualizacao);
        }
      }	
		}else{
			$save = false;
		}
		
    $saved = '<input type="hidden" name="'.$reference.'" id="'.$reference.'" value="'.$pk.'"/>';

    if ($save) {
      $screen->message->printMessageSucess($message_sucess.$saved);
    } else {
      $screen->message->printMessageError($message_error.$saved, true, $validate);
    }

  }else{
    $screen->message->printMessageError(MESSAGE_FORBIDDEN);
  }
}

  /**
   * Inicia o processo de remo��o de uma inst�ncia da entidade da base de dados
   * 
   * @param string $level
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 10/05/2013 15:25:46
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 10/05/2013 15:25:46
   */
 function postRemoveAtualizacao($level){
  ?><?php

  $acesso_controle = -1;

  require System::desire('file', '', 'header', 'core', false);
  
  require System::desire('class', 'resource', 'Screen', 'core', false);
  
  System::desire('m','manager', 'Atualizacao', 'src', true, '');
  System::desire('c','manager', 'Atualizacao', 'src', true, '');
  
  $screen = new Screen();

  if($acesso_controle >= 2){
      
    $atualizacao = new Atualizacao();
    
    $properties = $atualizacao->get_atl_properties();
    $reference = $properties['reference'];
		$database = isset($properties["database"]) ? $properties["database"] : DEFAULT_DATABASE;

    $key = $reference;
    $value = System::request($key);
    $atualizacao->set_atl_value($key, $value);
    $atualizacao->set_atl_value('atl_responsavel', System::getUser());
		
    $atualizacaoCtrl = new AtualizacaoCtrl(PATH_APP, $database);
    $remove = $atualizacaoCtrl->removeAtualizacaoCtrl($atualizacao);
		
		$message_sucess = MESSAGE_REMOVE_SUCCESS;
    $message_error = MESSAGE_REMOVE_ERROR;

    $r = isset($properties["remove"]) ? $properties["remove"] : true;
    if(isset($r["success"])){
      $message_sucess = $r["success"];
    }

    if($remove){
      $screen->message->printMessageSucess($message_sucess, false);
    }else{
      $screen->message->printMessageError($message_error, false);
    }
    
  }else{
    $screen->message->printMessageError(MESSAGE_FORBIDDEN);
  }
}

