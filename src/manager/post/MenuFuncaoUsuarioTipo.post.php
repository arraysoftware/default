<?php
/**
 * @copyright array software
 *
 * @author ADMINISTRADOR - 16/06/2013 10:06:05
 * <br><b>Updated by</b> ADMINISTRADOR - 17/06/2013 20:31:12
 * @category post
 * @package manager
 *
 * Responsavel.....: ADMINISTRADOR
 * Alteracao.......: 28/07/2013 09:30:09
 */


if (System::request('action')) {
  $action = System::request('action');


 if ($action == 'menufuncaousuariotipo-p-save') {
    $level = System::request('l');
    $copy = System::request('c', false);
    $presave = System::request('p');

    postSaveMenuFuncaoUsuarioTipo($level, $copy, $presave);
  } else if ($action == 'menufuncaousuariotipo-p-copy') {
    $level = System::request('l');
    $copy = System::request('c', true);
    $presave = System::request('p');

    postSaveMenuFuncaoUsuarioTipo($level, $copy, $presave);
  } else if ($action == 'menufuncaousuariotipo-p-remove') {
    $level = System::request('l');

    postRemoveMenuFuncaoUsuarioTipo($level);
  }
}

  /**
   * M�todo que permite registrar, editar ou copiar uma inst�ncia da entidade na base de dados
   * 
   * @param string $level
   * @param boolean $copy
   * @param boolean $presave
   *
   * @author ADMINISTRADOR - 16/06/2013 15:42:54
   * <br><b>Updated by</b> ADMINISTRADOR - 16/06/2013 15:42:54
   */
 function postSaveMenuFuncaoUsuarioTipo($level, $copy = false, $presave = false){
?><?php

  $acesso_controle = -1;

  require System::desire('file','' , 'header', 'core', false);
  require System::desire('class', 'resource', 'Screen', 'core', false);

  System::desire('m', 'manager', 'MenuFuncaoUsuarioTipo', 'src', true, '{project.rsc}');
  System::desire('c', 'manager', 'MenuFuncaoUsuarioTipo', 'src', true, '{project.rsc}');

  $screen = new Screen();

  if($acesso_controle >= 1){

    $mode = '';
    $message_sucess = MESSAGE_ADD_SUCCESS;
    $message_error = MESSAGE_ADD_ERROR;

    $saved = "";
		$validate = array();
    $menuFuncaoUsuarioTipo = new MenuFuncaoUsuarioTipo();
    $properties = $menuFuncaoUsuarioTipo->get_mft_properties();
    $reference = $properties['reference'];
		$database = isset($properties["database"]) ? $properties["database"] : DEFAULT_DATABASE;

    $pk = System::request($reference);
    if($copy){
      $copy = $pk;
      $pk = '';
      $message_sucess = MESSAGE_COPY_SUCCESS;
      $message_error = MESSAGE_COPY_ERROR;
    }

    if(empty($pk)){
      $mode = 'add';
    }else{
      $mode = 'edit';
      $message_sucess = MESSAGE_SET_SUCCESS;
      $message_error = MESSAGE_SET_ERROR;
    }

    $menuFuncaoUsuarioTipoCtrl = new MenuFuncaoUsuarioTipoCtrl(PATH_APP, $database);

    $items = $menuFuncaoUsuarioTipo->get_mft_items();

    $menuFuncaoUsuarioTipo->set_mft_value($reference, $pk);

    foreach($items as $item){
      $key = $item['id'];
      $value = System::request($key, null);

			if($item['type'] === 'select-multi'){
        $entity = $item['select-multi']['entity'];
        $value = System::request('source-'.$entity);
      } else if($item['type_behavior'] === 'parent') {
        if (isset($item['parent'])) {
          $class = $item['parent']['entity'];
          System::desire('m', $item['parent']['modulo'], $class, 'src', true);
          $obj = new $class();
          $set_value = "set_".$item['parent']['prefix']."_value";
          $get_items = "get_".$item['parent']['prefix']."_items";

          $itens = $obj->$get_items();
          foreach ($itens as $iten) {
            $k = $iten['id'];
            $v = System::request($k, null);
            $obj->$set_value($k, $v);
          }
          $value = $obj;
        }
      }

			$message = System::validate($item, $value);
      if($message === ""){
        $menuFuncaoUsuarioTipo->set_mft_value($key, $value);
      }else{
        $validate[] = $message;
      }

    }

    $menuFuncaoUsuarioTipo->set_mft_value('mft_responsavel', System::getUser());
    $menuFuncaoUsuarioTipo->set_mft_value('mft_criador', System::getUser());
		
		$verify = $menuFuncaoUsuarioTipoCtrl->verifyMenuFuncaoUsuarioTipoCtrl($menuFuncaoUsuarioTipo);

		if (!$validate && !$verify) {
		  if ($mode === 'add') {
        $save = $menuFuncaoUsuarioTipoCtrl->addMenuFuncaoUsuarioTipoCtrl($menuFuncaoUsuarioTipo, false, false, $presave, $copy);
        $pk = $save;
      } else if ($mode === 'edit') {
				if(isset($properties['saveonly']) && $properties['saveonly']){
          $message_error = MESSAGE_SAVEONLY;
          $save = false;
        }else{
          $save = $menuFuncaoUsuarioTipoCtrl->setMenuFuncaoUsuarioTipoCtrl($menuFuncaoUsuarioTipo);
        }
      }	
		}else{
			$save = false;
		}
		
    $saved = '<input type="hidden" name="'.$reference.'" id="'.$reference.'" value="'.$pk.'"/>';

    if ($save) {
      $screen->message->printMessageSucess($message_sucess.$saved);
    } else {
			$errors = array_merge($validate, $verify);
      $screen->message->printMessageError($message_error.$saved, true, $errors);
    }

  }else{
    $screen->message->printMessageError(MESSAGE_FORBIDDEN);
  }
}

  /**
   * Inicia o processo de remo��o de uma inst�ncia da entidade da base de dados
   * 
   * @param string $level
   *
   * @author ADMINISTRADOR - 16/06/2013 15:42:54
   * <br><b>Updated by</b> ADMINISTRADOR - 16/06/2013 15:42:54
   */
 function postRemoveMenuFuncaoUsuarioTipo($level){
  ?><?php

  $acesso_controle = -1;

  require System::desire('file', '', 'header', 'core', false);
  
  require System::desire('class', 'resource', 'Screen', 'core', false);
  
  System::desire('m','manager', 'MenuFuncaoUsuarioTipo', 'src', true, '{project.rsc}');
  System::desire('c','manager', 'MenuFuncaoUsuarioTipo', 'src', true, '{project.rsc}');
  
  $screen = new Screen();

  if($acesso_controle >= 2){
    
		$menuFuncaoUsuarioTipo = new MenuFuncaoUsuarioTipo();
    
    $properties = $menuFuncaoUsuarioTipo->get_mft_properties();
    $reference = $properties['reference'];
		$database = isset($properties["database"]) ? $properties["database"] : DEFAULT_DATABASE;

    $key = $reference;
    $value = System::request($key);
		$menuFuncaoUsuarioTipoCtrl = new MenuFuncaoUsuarioTipoCtrl(PATH_APP, $database);
		$remove = false;
		
		$menuFuncaoUsuarioTipos = $menuFuncaoUsuarioTipoCtrl->getMenuFuncaoUsuarioTipoCtrl($key." = '".$value."'", "", "", "0", "1");
		if(is_array($menuFuncaoUsuarioTipos)){
			$menuFuncaoUsuarioTipo = $menuFuncaoUsuarioTipos[0];
			
			$menuFuncaoUsuarioTipo->set_mft_value('mft_responsavel', System::getUser());
			
			$remove = $menuFuncaoUsuarioTipoCtrl->removeMenuFuncaoUsuarioTipoCtrl($menuFuncaoUsuarioTipo);
			
			$message_sucess = MESSAGE_REMOVE_SUCCESS;
			$message_error = MESSAGE_REMOVE_ERROR;
	
			$r = isset($properties["remove"]) ? $properties["remove"] : true;
			if(isset($r["success"])){
				$message_sucess = $r["success"];
			}
		}

    if($remove){
      $screen->message->printMessageSucess($message_sucess, false);
    }else{
      $screen->message->printMessageError($message_error, false);
    }
    
  }else{
    $screen->message->printMessageError(MESSAGE_FORBIDDEN);
  }
}

