<?php
/**
 * @copyright array software
 *
 * @author ADMINISTRADOR - 18/01/2013 11:31:53
 * <br><b>Updated by</b> ADMINISTRADOR - 24/07/2013 10:54:11
 * @category post
 * @package manager
 *
 * Responsavel.....: ADMINISTRADOR
 * Alteracao.......: 28/07/2013 09:30:10
 */


if (System::request('action')) {
  $action = System::request('action');


 if ($action == 'parametros-p-save') {
    $level = System::request('l');
    $copy = System::request('c', false);
    $presave = System::request('p');

    postSaveParametros($level, $copy, $presave);
  } else if ($action == 'parametros-p-copy') {
    $level = System::request('l');
    $copy = System::request('c', true);
    $presave = System::request('p');

    postSaveParametros($level, $copy, $presave);
  } else if ($action == 'parametros-p-remove') {
    $level = System::request('l');

    postRemoveParametros($level);
  }
}

  /**
   * M�todo que permite registrar, editar ou copiar uma inst�ncia da entidade na base de dados
   * 
   * @param string $level
   * @param boolean $copy
   * @param boolean $presave
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 02/05/2013 19:15:17
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 02/05/2013 19:15:17
   */
 function postSaveParametros($level, $copy = false, $presave = false){
  ?><?php

  $acesso_controle = -1;

  require System::desire('file', '', 'header', 'core', false); 
  require System::desire('class', 'resource', 'Screen', 'core', false);
  
  System::desire('m','manager', 'Parametros', 'src', true, '');
  System::desire('c','manager', 'Parametros', 'src', true, '');
 
  $screen = new Screen();

  if($acesso_controle >= 1){
    
    $mode = '';
    $message_sucess = MESSAGE_ADD_SUCCESS;
    $message_error = MESSAGE_ADD_ERROR;

    $saved = "";
    $parametros = new Parametros();
    $properties = $parametros->get_par_properties();
    $reference = $properties['reference'];
		$database = isset($properties["database"]) ? $properties["database"] : DEFAULT_DATABASE;

    $pk = System::request($reference);
    if($copy){
      $pk = '';
      $message_sucess = MESSAGE_COPY_SUCCESS;
      $message_error = MESSAGE_COPY_ERROR;
    }
    
    if(empty($pk)){
      $mode = 'add';
    }else{
      $mode = 'edit';
      $message_sucess = MESSAGE_SET_SUCCESS;
      $message_error = MESSAGE_SET_ERROR;
    }

    $parametrosCtrl = new ParametrosCtrl(PATH_APP, $database);
    

    $items = $parametros->get_par_items();
    
    $parametros->set_par_value($reference, $pk);
    
    foreach($items as $item){
      $key = $item['id'];
      $value = System::request($key, null);
			
			if($item['type'] === 'select-multi'){
        $entity = $item['select-multi']['entity'];
        $value = System::request('source-'.$entity);
      }
			
      $parametros->set_par_value($key, $value);
    }
    
    $parametros->set_par_value('par_responsavel', System::getUser());
    $parametros->set_par_value('par_criador', System::getUser());

    if($mode == 'add'){
      $save = $parametrosCtrl->addParametrosCtrl($parametros, false, false, $presave);
      $pk = $save;
    }else if($mode == 'edit'){
      $save = $parametrosCtrl->setParametrosCtrl($parametros, false, false);
    }

    $saved = '<input type="hidden" name="'.$reference.'" id="'.$reference.'" value="'.$pk.'"/>';

    if($save){
      $screen->message->printMessageSucess($message_sucess.$saved);
    }else{
      $screen->message->printMessageError($message_error);
    }
    
  }else{
    $screen->message->printMessageError(MESSAGE_FORBIDDEN);
  }
}

  /**
   * Inicia o processo de remo��o de uma inst�ncia da entidade da base de dados
   * 
   * @param string $level
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 02/05/2013 19:15:17
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 02/05/2013 19:15:17
   */
 function postRemoveParametros($level){
  ?><?php

  $acesso_controle = -1;

  require System::desire('file', '', 'header', 'core', false);
  
  require System::desire('class', 'resource', 'Screen', 'core', false);
  
  System::desire('m','manager', 'Parametros', 'src', true, '');
  System::desire('c','manager', 'Parametros', 'src', true, '');
  
  $screen = new Screen();

  if($acesso_controle >= 2){
      
    $parametros = new Parametros();
    
    $properties = $parametros->get_par_properties();
    $reference = $properties['reference'];
		$database = isset($properties["database"]) ? $properties["database"] : DEFAULT_DATABASE;

    $key = $reference;
    $value = System::request($key);
    $parametros->set_par_value($key, $value);
    $parametros->set_par_value('par_responsavel', System::getUser());
		
    $parametrosCtrl = new ParametrosCtrl(PATH_APP, $database);
    $remove = $parametrosCtrl->removeParametrosCtrl($parametros);
		
		$message_sucess = MESSAGE_REMOVE_SUCCESS;
    $message_error = MESSAGE_REMOVE_ERROR;

    $r = isset($properties["remove"]) ? $properties["remove"] : true;
    if(isset($r["success"])){
      $message_sucess = $r["success"];
    }

    if($remove){
      $screen->message->printMessageSucess($message_sucess, false);
    }else{
      $screen->message->printMessageError($message_error, false);
    }
    
  }else{
    $screen->message->printMessageError(MESSAGE_FORBIDDEN);
  }
}

