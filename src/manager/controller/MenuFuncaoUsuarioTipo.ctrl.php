<?php
/**
 * @copyright array software
 *
 * @author ADMINISTRADOR - 16/06/2013 10:06:05
 * <br><b>Updated by</b> ADMINISTRADOR - 17/06/2013 20:31:12
 * @category controller
 * @package manager
 *
 * Responsavel.....: ADMINISTRADOR
 * Alteracao.......: 28/07/2013 09:30:04
 */


class MenuFuncaoUsuarioTipoCtrl
{
  private  $path;
  private  $database;

  /**
   * Construtor da classe de processamento e interface de acesso a dados da entidade
   * 
   * @param string $path
   * @param string $database
   *
   * @author ADMINISTRADOR - 16/06/2013 15:42:52
   * <br><b>Updated by</b> ADMINISTRADOR - 16/06/2013 15:42:52
   */
  public function MenuFuncaoUsuarioTipoCtrl($path, $database = ""){
    ?><?php
    $this->path = $path;
    $this->database = $database;

    return $this;
  }

  /**
   * Recupera inst�ncias da entidade na base de dados
   * 
   * @param string $where
   * @param string $group
   * @param string $order
   * @param string $start
   * @param string $end
   * @param boolean $validate
   * @param boolean $debug
   *
   * @author ADMINISTRADOR - 16/06/2013 15:42:52
   * <br><b>Updated by</b> ADMINISTRADOR - 16/06/2013 15:42:52
   */
  public function getMenuFuncaoUsuarioTipoCtrl($where, $group, $order, $start = "", $end = "", $validate = true, $debug = false){
    ?><?php
    System::desire("d", 'manager', 'MenuFuncaoUsuarioTipo', 'src', true, '{project.rsc}');
    
    $menuFuncaoUsuarioTipoDAO = new MenuFuncaoUsuarioTipoDAO($this->path, $this->database);
    $menuFuncaoUsuarioTipos = $menuFuncaoUsuarioTipoDAO->getMenuFuncaoUsuarioTipoDAO($where, $group, $order, $start, $end, $validate, $debug);
    
    return $menuFuncaoUsuarioTipos;
  }

  /**
   * Recupera um valor inteiro referente ao n�mero de linhas de determina consulta
   * 
   * @param string $where
   * @param string $group
   * @param boolean $validate
   * @param boolean $debug
   *
   * @author ADMINISTRADOR - 16/06/2013 15:42:52
   * <br><b>Updated by</b> ADMINISTRADOR - 16/06/2013 15:42:52
   */
  public function getCountMenuFuncaoUsuarioTipoCtrl($where, $group = "", $validate = true, $debug = false){
    ?><?php
    System::desire("d", 'manager', 'MenuFuncaoUsuarioTipo', 'src', true, '{project.rsc}');
    
    $menuFuncaoUsuarioTipoDAO = new MenuFuncaoUsuarioTipoDAO($this->path, $this->database);
    $total = $menuFuncaoUsuarioTipoDAO->getCountMenuFuncaoUsuarioTipoDAO($where, $group, $validate, $debug);
    
    return $total;
  }

  /**
   * Adiciona um novo registro desta entidade na base de dados
   * 
   * @param object $object
   * @param boolean $validate
   * @param boolean $debug
   * @param boolean $presave
   * @param int $copy
   *
   * @author ADMINISTRADOR - 16/06/2013 15:42:52
   * <br><b>Updated by</b> ADMINISTRADOR - 16/06/2013 15:42:52
   */
  public function addMenuFuncaoUsuarioTipoCtrl($object, $validate = true, $debug = false, $presave = false, $copy = 0){
    ?><?php
    System::desire("d", 'manager', 'MenuFuncaoUsuarioTipo', 'src', true, '{project.rsc}');    
    
    $menuFuncaoUsuarioTipoDAO = new MenuFuncaoUsuarioTipoDAO($this->path, $this->database);
    $this->beforeMenuFuncaoUsuarioTipoCtrl($object, "add");
    $add = $menuFuncaoUsuarioTipoDAO->addMenuFuncaoUsuarioTipoDAO($object, $validate, $debug, $presave, $copy);
    $this->afterMenuFuncaoUsuarioTipoCtrl($object, "add", $add, $copy);
    
    return $add;
  }

  /**
   * Remove uma instancia da entidade da base de dados
   * 
   * @param object $object
   * @param boolean $validate
   * @param boolean $debug
   *
   * @author ADMINISTRADOR - 16/06/2013 15:42:52
   * <br><b>Updated by</b> ADMINISTRADOR - 16/06/2013 15:42:52
   */
  public function removeMenuFuncaoUsuarioTipoCtrl($object, $validate = true, $debug = false){
    ?><?php
    System::desire("d", 'manager', 'MenuFuncaoUsuarioTipo', 'src', true, '{project.rsc}');
    
    $menuFuncaoUsuarioTipoDAO = new MenuFuncaoUsuarioTipoDAO($this->path, $this->database);
    $this->beforeMenuFuncaoUsuarioTipoCtrl($object, "remove");
    $remove = $menuFuncaoUsuarioTipoDAO->removeMenuFuncaoUsuarioTipoDAO($object, $validate, $debug);
    $this->afterMenuFuncaoUsuarioTipoCtrl($object, "remove");
    
    return $remove;
  }

  /**
   * Atualiza uma inst�ncia da entidade na base de dados
   * 
   * @param object $object
   * @param boolean $validate
   * @param boolean $debug
   * @param boolean $trigger
   *
   * @author ADMINISTRADOR - 16/06/2013 15:42:52
   * <br><b>Updated by</b> ADMINISTRADOR - 16/06/2013 15:42:52
   */
  public function setMenuFuncaoUsuarioTipoCtrl($object, $validate = true, $debug = false, $trigger = true){
    ?><?php
    System::desire("d", 'manager', 'MenuFuncaoUsuarioTipo', 'src', true, '{project.rsc}');
    
    $menuFuncaoUsuarioTipoDAO = new MenuFuncaoUsuarioTipoDAO($this->path, $this->database);
    $this->beforeMenuFuncaoUsuarioTipoCtrl($object, "set");
    $set = $menuFuncaoUsuarioTipoDAO->setMenuFuncaoUsuarioTipoDAO($object, $validate, $debug, $trigger);
    if($trigger === true){
			$this->afterMenuFuncaoUsuarioTipoCtrl($object, "set");
		}
    
    return $set;
  }

  /**
   * M�todo de gatilho executado antes de cada atualiza��o e remo��o das inst�ncias da entidade
   * 
   * @param object $object
   * @param string $param
   *
   * @author ADMINISTRADOR - 16/06/2013 15:42:52
   * <br><b>Updated by</b> ADMINISTRADOR - 16/06/2013 15:42:52
   */
  public function beforeMenuFuncaoUsuarioTipoCtrl($object, $param){
    ?><?php

    if($param === 'add' or $param === 'set'){

      $items = $object->get_mft_items();
      
      foreach($items as $item){
        if($item['type_behavior'] == 'parent'){
          if(isset($item['parent'])){
            $class = $item['parent']['entity'];
            System::desire('c', $item['parent']['modulo'], $class, 'src', true);
            $classCtrl = $class."Ctrl";
            $obj = new $classCtrl(PATH_APP);
            $entity = $item['value'];
            if($param === 'add'){
              $action = "add".$class."Ctrl";
            }else{
              $action = "set".$class."Ctrl";
            }
            $value = $obj->$action($entity);
            if($param === 'add'){
              $object->set_mft_value($item['id'], $value);
            }else{
              $get_value = "get_".$item['parent']['prefix']."_value";
              $object->set_mft_value($item['id'], $entity->$get_value($item['parent']['key']));
            }
          }
        }
      }


    }

    return $object;
  }

  /**
   * M�todo de gatilho executado depois de cada atualiza��o e remo��o das inst�ncias da entidade
   * 
   * @param object $object
   * @param string $param
   * @param int $value
   * @param int $copy
   *
   * @author ADMINISTRADOR - 16/06/2013 15:42:52
   * <br><b>Updated by</b> ADMINISTRADOR - 16/06/2013 15:42:52
   */
  public function afterMenuFuncaoUsuarioTipoCtrl($object, $param, $value = 0, $copy = 0){
?><?php
    
    $add = false;
    
    if($param === 'set'){
      $value = $object->get_mft_value($object->get_mft_reference());
    }

    if($param === 'add'){
      $object->set_mft_value($object->get_mft_reference(), $value);
    }

    if($param === 'set' or $param === 'add'){
			
      $items = $object->get_mft_items();

      $try = 0;
      $added = 0;
      
      foreach ($items as $item) {
        if($item['type'] === 'select-multi'){
          
          if(isset($item['select-multi'])){
            $select = $item['select-multi'];

            $module = $select['module'];
            $entity = $select['entity'];
            $prefix = $select['prefix'];

            System::desire('c', $module, $entity, 'src');
            System::desire('m', $module, $entity, 'src');

            $remove = "execute".$entity."Ctrl";
            $set_value = 'set_'.$prefix.'_value';
            $add = "add".$entity."Ctrl";

            $entityCtrl = $entity.'Ctrl';
            $objectCtrl = new $entityCtrl(PATH_APP);

            $objectCtrl->$remove($select['foreign']."='".$value."'","");

            $values = $item['value'];
            foreach ($values as $v) {
							if($v){
								$object = new $entity();
								$object->$set_value($select['foreign'],$value);
								$object->$set_value($select['source'],$v);
								$object->$set_value($prefix.'_responsavel',System::getUser());
								$object->$set_value($prefix.'_criador',System::getUser());
	
								$w = $objectCtrl->$add($object);
								$try++;
								if($w){
									$added++;
								}
							}
            }
          }
        }
      }
      
      $add = $try == $added;
      
      if($copy > 0){
        // input here the code to create the complete copy of instance
      }
      
    }

    return $add;
  }

  /**
   * Recupera um dado atrav�s de uma consulta personalizada
   * 
   * @param string $column
   * @param string $where
   * @param string $table
   * @param boolean $validate
   * @param boolean $debug
   *
   * @author ADMINISTRADOR - 16/06/2013 15:42:52
   * <br><b>Updated by</b> ADMINISTRADOR - 16/06/2013 15:42:52
   */
  public function getColumnMenuFuncaoUsuarioTipoCtrl($column, $where, $table = "", $validate = true, $debug = false){
    ?><?php
    System::desire("d", 'manager', 'MenuFuncaoUsuarioTipo', 'src', true, '{project.rsc}');
    
    $menuFuncaoUsuarioTipoDAO = new MenuFuncaoUsuarioTipoDAO($this->path, $this->database);
    $column = $menuFuncaoUsuarioTipoDAO->getColumnMenuFuncaoUsuarioTipoDAO($column, $where, $table, $validate, $debug);
    
    return $column;
  }

  /**
   * Remove um grupo de items ou atualiza uma quantidade relativamente grande 
   * 
   * @param string $where
   * @param string $update
   * @param boolean $validate
   * @param boolean $debug
   *
   * @author ADMINISTRADOR - 16/06/2013 15:42:52
   * <br><b>Updated by</b> ADMINISTRADOR - 16/06/2013 15:42:52
   */
  public function executeMenuFuncaoUsuarioTipoCtrl($where, $update, $validate = true, $debug = false){
  ?><?php
    System::desire('d', 'manager', 'MenuFuncaoUsuarioTipo', 'src', true, '{project.rsc}');

    $menuFuncaoUsuarioTipoDAO = new MenuFuncaoUsuarioTipoDAO($this->path, $this->database);
    $executed = $menuFuncaoUsuarioTipoDAO->executeMenuFuncaoUsuarioTipoDAO($where, $update, $validate, $debug);

    return $executed;
  }

  /**
   * M�todo respons�vel por realizar a valida��o dos dados recebidos pelo post
   * 
   * @param object $object
   *
   * @author ADMINISTRADOR - 16/06/2013 15:42:52
   * <br><b>Updated by</b> ADMINISTRADOR - 16/06/2013 15:42:52
   */
  public function verifyMenuFuncaoUsuarioTipoCtrl($object){
	?><?php

	$items = $object->get_mft_items();
	$error_messages = array();

	foreach ($items as $key => $item) {
		if (!is_null($item['value'])) {
			/*
			 * Validation comes here!
			 * If the validation fails, add an item in the array $error_messages
			 * array("id"=>$item['id'],"description"=>$item['description'],"type"=>$item['type'],"value"=>$item['value'],"message"=>"{MESSAGE}");
			 */
		}
	}

	return $error_messages;
  }

  /**
   * Recupera as op��es dispon�veis para cada tipo de acesso
   * 
   * @param int $ust_codigo
   *
   * @author ADMINISTRADOR - 16/06/2013 15:43:20
   * <br><b>Updated by</b> ADMINISTRADOR - 16/06/2013 15:48:07
   */
  public function getOptionMenuFuncaoUsuarioTipoCtrl($ust_codigo){
?><?php

    System::desire("d", 'manager', 'MenuFuncaoUsuarioTipo', 'src');

    $menuFuncaoUsuarioTipoDAO = new MenuFuncaoUsuarioTipoDAO($this->path, $this->database);
    $option = $menuFuncaoUsuarioTipoDAO->getOptionMenuFuncaoUsuarioTipoDAO($ust_codigo);

    return $option;
  }


}