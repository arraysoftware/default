<?php
/**
 * @copyright array software
 *
 * @author WILLIAM MARQUES VICENTE GOMES CORREA - 01/05/2013 09:09:56
 * <br><b>Updated by</b> ADMINISTRADOR - 15/06/2013 19:51:55
 * @category controller
 * @package manager
 *
 * Responsavel.....: ADMINISTRADOR
 * Alteracao.......: 28/07/2013 09:30:04
 */


class HistoricoCtrl
{
  private  $path;
  private  $database;

  /**
   * Construtor da classe de processamento e interface de acesso a dados da entidade
   * 
   * @param string $path
   * @param string $database
   *
   * @author ADMINISTRADOR - 15/06/2013 19:39:03
   * <br><b>Updated by</b> ADMINISTRADOR - 15/06/2013 19:39:03
   */
  public function HistoricoCtrl($path, $database = ""){
    ?><?php
    $this->path = $path;
    $this->database = $database;

    return $this;
  }

  /**
   * Recupera inst�ncias da entidade na base de dados
   * 
   * @param string $where
   * @param string $group
   * @param string $order
   * @param string $start
   * @param string $end
   * @param boolean $validate
   * @param boolean $debug
   *
   * @author ADMINISTRADOR - 15/06/2013 19:39:03
   * <br><b>Updated by</b> ADMINISTRADOR - 15/06/2013 19:39:03
   */
  public function getHistoricoCtrl($where, $group, $order, $start = "", $end = "", $validate = true, $debug = false){
    ?><?php
    System::desire("d", 'manager', 'Historico', 'src', true, '{project.rsc}');
    
    $historicoDAO = new HistoricoDAO($this->path, $this->database);
    $historicos = $historicoDAO->getHistoricoDAO($where, $group, $order, $start, $end, $validate, $debug);
    
    return $historicos;
  }

  /**
   * Recupera um valor inteiro referente ao n�mero de linhas de determina consulta
   * 
   * @param string $where
   * @param string $group
   * @param boolean $validate
   * @param boolean $debug
   *
   * @author ADMINISTRADOR - 15/06/2013 19:39:03
   * <br><b>Updated by</b> ADMINISTRADOR - 15/06/2013 19:39:03
   */
  public function getCountHistoricoCtrl($where, $group = "", $validate = true, $debug = false){
    ?><?php
    System::desire("d", 'manager', 'Historico', 'src', true, '{project.rsc}');
    
    $historicoDAO = new HistoricoDAO($this->path, $this->database);
    $total = $historicoDAO->getCountHistoricoDAO($where, $group, $validate, $debug);
    
    return $total;
  }

  /**
   * Adiciona um novo registro desta entidade na base de dados
   * 
   * @param object $object
   * @param boolean $validate
   * @param boolean $debug
   * @param boolean $presave
   * @param int $copy
   *
   * @author ADMINISTRADOR - 15/06/2013 19:39:03
   * <br><b>Updated by</b> ADMINISTRADOR - 15/06/2013 19:39:03
   */
  public function addHistoricoCtrl($object, $validate = true, $debug = false, $presave = false, $copy = 0){
    ?><?php
    System::desire("d", 'manager', 'Historico', 'src', true, '{project.rsc}');    
    
    $historicoDAO = new HistoricoDAO($this->path, $this->database);
    $this->beforeHistoricoCtrl($object, "add");
    $add = $historicoDAO->addHistoricoDAO($object, $validate, $debug, $presave, $copy);
    $this->afterHistoricoCtrl($object, "add", $add, $copy);
    
    return $add;
  }

  /**
   * Remove uma instancia da entidade da base de dados
   * 
   * @param object $object
   * @param boolean $validate
   * @param boolean $debug
   *
   * @author ADMINISTRADOR - 15/06/2013 19:39:03
   * <br><b>Updated by</b> ADMINISTRADOR - 15/06/2013 19:39:03
   */
  public function removeHistoricoCtrl($object, $validate = true, $debug = false){
    ?><?php
    System::desire("d", 'manager', 'Historico', 'src', true, '{project.rsc}');
    
    $historicoDAO = new HistoricoDAO($this->path, $this->database);
    $this->beforeHistoricoCtrl($object, "remove");
    $remove = $historicoDAO->removeHistoricoDAO($object, $validate, $debug);
    $this->afterHistoricoCtrl($object, "remove");
    
    return $remove;
  }

  /**
   * Atualiza uma inst�ncia da entidade na base de dados
   * 
   * @param object $object
   * @param boolean $validate
   * @param boolean $debug
   * @param boolean $trigger
   *
   * @author ADMINISTRADOR - 15/06/2013 19:39:03
   * <br><b>Updated by</b> ADMINISTRADOR - 15/06/2013 19:39:03
   */
  public function setHistoricoCtrl($object, $validate = true, $debug = false, $trigger = true){
    ?><?php
    System::desire("d", 'manager', 'Historico', 'src', true, '{project.rsc}');
    
    $historicoDAO = new HistoricoDAO($this->path, $this->database);
    $this->beforeHistoricoCtrl($object, "set");
    $set = $historicoDAO->setHistoricoDAO($object, $validate, $debug, $trigger);
    if($trigger === true){
			$this->afterHistoricoCtrl($object, "set");
		}
    
    return $set;
  }

  /**
   * M�todo de gatilho executado antes de cada atualiza��o e remo��o das inst�ncias da entidade
   * 
   * @param object $object
   * @param string $param
   *
   * @author ADMINISTRADOR - 15/06/2013 19:39:03
   * <br><b>Updated by</b> ADMINISTRADOR - 15/06/2013 19:39:03
   */
  public function beforeHistoricoCtrl($object, $param){
    ?><?php

    if($param === 'add' or $param === 'set'){

      $items = $object->get_hst_items();
      
      foreach($items as $item){
        if($item['type_behavior'] == 'parent'){
          if(isset($item['parent'])){
            $class = $item['parent']['entity'];
            System::desire('c', $item['parent']['modulo'], $class, 'src', true);
            $classCtrl = $class."Ctrl";
            $obj = new $classCtrl(PATH_APP);
            $entity = $item['value'];
            if($param === 'add'){
              $action = "add".$class."Ctrl";
            }else{
              $action = "set".$class."Ctrl";
            }
            $value = $obj->$action($entity);
            if($param === 'add'){
              $object->set_hst_value($item['id'], $value);
            }else{
              $get_value = "get_".$item['parent']['prefix']."_value";
              $object->set_hst_value($item['id'], $entity->$get_value($item['parent']['key']));
            }
          }
        }
      }


    }

    return $object;
  }

  /**
   * M�todo de gatilho executado depois de cada atualiza��o e remo��o das inst�ncias da entidade
   * 
   * @param object $object
   * @param string $param
   * @param int $value
   * @param int $copy
   *
   * @author ADMINISTRADOR - 15/06/2013 19:39:03
   * <br><b>Updated by</b> ADMINISTRADOR - 15/06/2013 19:50:32
   */
  public function afterHistoricoCtrl($object, $param, $value = 0, $copy = 0){
?><?php
    
    $add = false;
    
    if($param === 'set'){
      $value = $object->get_hst_value($object->get_hst_reference());
    }

    if($param === 'add'){
      $object->set_hst_value($object->get_hst_reference(), $value);
    }

    if($param === 'set' or $param === 'add'){
			
      $items = $object->get_hst_items();

      $try = 0;
      $added = 0;
      
      foreach ($items as $item) {
        if($item['type'] === 'select-multi'){
          
          if(isset($item['select-multi'])){
            $select = $item['select-multi'];

            $module = $select['module'];
            $entity = $select['entity'];
            $prefix = $select['prefix'];

            System::desire('c', $module, $entity, 'src');
            System::desire('m', $module, $entity, 'src');

            $remove = "execute".$entity."Ctrl";
            $set_value = 'set_'.$prefix.'_value';
            $add = "add".$entity."Ctrl";

            $entityCtrl = $entity.'Ctrl';
            $objectCtrl = new $entityCtrl(PATH_APP);

            $objectCtrl->$remove($select['foreign']."='".$value."'","");

            $values = $item['value'];
            foreach ($values as $v) {
							if($v){
								$object = new $entity();
								$object->$set_value($select['foreign'],$value);
								$object->$set_value($select['source'],$v);
								$object->$set_value($prefix.'_responsavel',System::getUser());
								$object->$set_value($prefix.'_criador',System::getUser());
	
								$w = $objectCtrl->$add($object);
								$try++;
								if($w){
									$added++;
								}
							}
            }
          }
        }
      }
      
      $add = $try == $added;
      
      if($copy > 0){
        // input here the code to create the complete copy of instance
      }
      
    }

    return $add;
  }

  /**
   * Recupera um dado atrav�s de uma consulta personalizada
   * 
   * @param string $column
   * @param string $where
   * @param string $table
   * @param boolean $validate
   * @param boolean $debug
   *
   * @author ADMINISTRADOR - 15/06/2013 19:39:03
   * <br><b>Updated by</b> ADMINISTRADOR - 15/06/2013 19:39:03
   */
  public function getColumnHistoricoCtrl($column, $where, $table = "", $validate = true, $debug = false){
    ?><?php
    System::desire("d", 'manager', 'Historico', 'src', true, '{project.rsc}');
    
    $historicoDAO = new HistoricoDAO($this->path, $this->database);
    $column = $historicoDAO->getColumnHistoricoDAO($column, $where, $table, $validate, $debug);
    
    return $column;
  }

  /**
   * Remove um grupo de items ou atualiza uma quantidade relativamente grande 
   * 
   * @param string $where
   * @param string $update
   * @param boolean $validate
   * @param boolean $debug
   *
   * @author ADMINISTRADOR - 15/06/2013 19:39:03
   * <br><b>Updated by</b> ADMINISTRADOR - 15/06/2013 19:39:03
   */
  public function executeHistoricoCtrl($where, $update, $validate = true, $debug = false){
  ?><?php
    System::desire('d', 'manager', 'Historico', 'src', true, '{project.rsc}');

    $historicoDAO = new HistoricoDAO($this->path, $this->database);
    $executed = $historicoDAO->executeHistoricoDAO($where, $update, $validate, $debug);

    return $executed;
  }

  /**
   * M�todo respons�vel por realizar a valida��o dos dados recebidos pelo post
   * 
   * @param object $object
   *
   * @author ADMINISTRADOR - 15/06/2013 19:39:03
   * <br><b>Updated by</b> ADMINISTRADOR - 15/06/2013 19:39:03
   */
  public function verifyHistoricoCtrl($object){
	?><?php

	$items = $object->get_hst_items();
	$error_messages = array();

	foreach ($items as $key => $item) {
		if (!is_null($item['value'])) {
			/*
			 * Validation comes here!
			 * If the validation fails, add an item in the array $error_messages
			 * array("id"=>$item['id'],"description"=>$item['description'],"type"=>$item['type'],"value"=>$item['value'],"message"=>"{MESSAGE}");
			 */
		}
	}

	return $error_messages;
  }


}