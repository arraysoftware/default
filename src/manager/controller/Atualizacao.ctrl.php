<?php
/**
 * @copyright array software
 *
 * @author WILLIAM MARQUES VICENTE GOMES CORREA - 10/05/2013 15:25:43
 * <br><b>Updated by</b> ADMINISTRADOR - 15/06/2013 18:51:33
 * @category controller
 * @package manager
 *
 * Responsavel.....: ADMINISTRADOR
 * Alteracao.......: 28/07/2013 09:30:04
 */


class AtualizacaoCtrl
{
  private  $path;
  private  $database;

  /**
   * Construtor da classe de processamento e interface de acesso a dados da entidade
   * 
   * @param string $path
   * @param string $database
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 10/05/2013 15:25:44
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 10/05/2013 15:25:44
   */
  public function AtualizacaoCtrl($path, $database = ""){
    ?><?php
    $this->path = $path;
    $this->database = $database;

    return $this;
  }

  /**
   * Recupera inst�ncias da entidade na base de dados
   * 
   * @param string $where
   * @param string $group
   * @param string $order
   * @param string $start
   * @param string $end
   * @param boolean $validate
   * @param boolean $debug
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 10/05/2013 15:25:44
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 10/05/2013 15:25:44
   */
  public function getAtualizacaoCtrl($where, $group, $order, $start = "", $end = "", $validate = true, $debug = false){
    ?><?php
    System::desire("d", 'manager', 'Atualizacao', 'src', true, '');
    
    $atualizacaoDAO = new AtualizacaoDAO($this->path, $this->database);
    $atualizacaos = $atualizacaoDAO->getAtualizacaoDAO($where, $group, $order, $start, $end, $validate, $debug);
    
    return $atualizacaos;
  }

  /**
   * Recupera um valor inteiro referente ao n�mero de linhas de determina consulta
   * 
   * @param string $where
   * @param string $group
   * @param boolean $validate
   * @param boolean $debug
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 10/05/2013 15:25:44
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 10/05/2013 15:25:44
   */
  public function getCountAtualizacaoCtrl($where, $group = "", $validate = true, $debug = false){
    ?><?php
    System::desire("d", 'manager', 'Atualizacao', 'src', true, '');
    
    $atualizacaoDAO = new AtualizacaoDAO($this->path, $this->database);
    $total = $atualizacaoDAO->getCountAtualizacaoDAO($where, $group, $validate, $debug);
    
    return $total;
  }

  /**
   * Adiciona um novo registro desta entidade na base de dados
   * 
   * @param object $object
   * @param boolean $validate
   * @param boolean $debug
   * @param boolean $presave
   * @param int $copy
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 10/05/2013 15:25:44
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 10/05/2013 15:25:44
   */
  public function addAtualizacaoCtrl($object, $validate = true, $debug = false, $presave = false, $copy = 0){
    ?><?php
    System::desire("d", 'manager', 'Atualizacao', 'src', true, '');    
    
    $atualizacaoDAO = new AtualizacaoDAO($this->path, $this->database);
    $this->beforeAtualizacaoCtrl($object, "add");
    $add = $atualizacaoDAO->addAtualizacaoDAO($object, $validate, $debug, $presave, $copy);
    $this->afterAtualizacaoCtrl($object, "add", $add, $copy);
    
    return $add;
  }

  /**
   * Remove uma instancia da entidade da base de dados
   * 
   * @param object $object
   * @param boolean $validate
   * @param boolean $debug
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 10/05/2013 15:25:44
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 10/05/2013 15:25:44
   */
  public function removeAtualizacaoCtrl($object, $validate = true, $debug = false){
    ?><?php
    System::desire("d", 'manager', 'Atualizacao', 'src', true, '');
    
    $atualizacaoDAO = new AtualizacaoDAO($this->path, $this->database);
    $this->beforeAtualizacaoCtrl($object, "remove");
    $remove = $atualizacaoDAO->removeAtualizacaoDAO($object, $validate, $debug);
    $this->afterAtualizacaoCtrl($object, "remove");
    
    return $remove;
  }

  /**
   * Atualiza uma inst�ncia da entidade na base de dados
   * 
   * @param object $object
   * @param boolean $validate
   * @param boolean $debug
   * @param boolean $trigger
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 10/05/2013 15:25:44
   * <br><b>Updated by</b> ADMINISTRADOR - 15/06/2013 18:22:05
   */
  public function setAtualizacaoCtrl($object, $validate = true, $debug = false, $trigger = true){
?><?php
    System::desire("d", 'manager', 'Atualizacao', 'src', true, '');
    
    $atualizacaoDAO = new AtualizacaoDAO($this->path, $this->database);
    $this->beforeAtualizacaoCtrl($object, "set");
    $set = $atualizacaoDAO->setAtualizacaoDAO($object, $validate, $debug, $trigger);
    if($trigger === true){
			$this->afterAtualizacaoCtrl($object, "set");
		}
    
    return $set;
  }

  /**
   * M�todo de gatilho executado antes de cada atualiza��o e remo��o das inst�ncias da entidade
   * 
   * @param object $object
   * @param string $param
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 10/05/2013 15:25:45
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 10/05/2013 15:25:45
   */
  public function beforeAtualizacaoCtrl($object, $param){
    ?><?php
    return false;
  }

  /**
   * M�todo de gatilho executado depois de cada atualiza��o e remo��o das inst�ncias da entidade
   * 
   * @param object $object
   * @param string $param
   * @param int $value
   * @param int $copy
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 10/05/2013 15:25:45
   * <br><b>Updated by</b> ADMINISTRADOR - 18/07/2013 17:24:12
   */
  public function afterAtualizacaoCtrl($object, $param, $value = 0, $copy = 0){
    ?><?php

    $after = false;

    if($param === 'set'){
      $value = $object->get_atl_value($object->get_atl_reference());
    }

    if($param === 'add'){
      $value = $object->set_atl_value($object->get_atl_reference(), $value);
    }

    if($param === 'set' || $param === 'add'){
      $items = $object->get_atl_items();

      $try = 0;
      $added = 0;

      foreach ($items as $item) {
        if($item['type'] === 'select-multi'){

          if(isset($item['select-multi'])){
            $select = $item['select-multi'];

            $module = $select['module'];
            $entity = $select['entity'];
            $prefix = $select['prefix'];

            System::desire('c', $module, $entity, 'src');
            System::desire('m', $module, $entity, 'src');

            $remove = "execute".$entity."Ctrl";
            $set_value = 'set_'.$prefix.'_value';
            $after = "add".$entity."Ctrl";

            $entityCtrl = $entity.'Ctrl';
            $objectCtrl = new $entityCtrl(PATH_APP);

            $objectCtrl->$remove($select['foreign']."='".$value."'");

            $values = $item['value'];
            foreach ($values as $v) {
              $object = new $entity();
              $object->$set_value($select['foreign'],$value);
              $object->$set_value($select['source'],$v);
              $object->$set_value($prefix.'_responsavel',System::getUser());
              $object->$set_value($prefix.'_criador',System::getUser());

              $w = $objectCtrl->$after($object);
              $try++;
              if($w){
                $added++;
              }
            }
          }

        }
      }

      $after = $try == $added;

      if($copy > 0){
        // input here the code to create the complete copy of instance
      }

    }

    if($param === 'add'){

      $atl_arquivo = $object->get_atl_value('atl_arquivo');
      if(is_file($atl_arquivo)){
        System::desire('class', 'resource', 'Zip', 'core', true);

        $update = new Zip();
        if($update->open($atl_arquivo) === true){
          $updating = PATH_APP."files/update/temp/".uniqid()."/";
          if(!file_exists($updating)){
            mkdir($updating);
          }
          $update->extractTo($updating);
          $update->close();

          $layers = array("view","screen","post","model","json","dao","controller","file","report");

          $log = "";

          $copy = new ZipArchive();
          $unzip = $copy->open(PATH_APP."files/update/restore/".VERSION.".zip", ZipArchive::CREATE);

          if($unzip === true){
            $log .= "<b>Atualiza��o iniciada a partir de</b> $atl_arquivo<br><br>";

            $dir = opendir($updating."src");
            while (false !== ($module = readdir($dir))) {

              if(is_dir($updating."src/".$module) and $module !== "." and $module !== ".."){
                $log .= " - Pronto para ler src/$module<br>";

                foreach($layers as $layer){
                  $location = $updating."src/".$module."/".$layer;
                  $log .= " - Percorrendo $module/$layer<br>";

                  if(is_dir($location)){
                    $files = opendir($location);
                    while (false !== ($file = readdir($files))) {
                      $source = $location."/".$file;
                      if(is_file($source)){
                        $m = true;
                        $l = true;
                        if(!file_exists(PATH_APP."src/".$module."/")){
                          $m = mkdir(PATH_APP."src/".$module."/");
                        }
                        if(!file_exists(PATH_APP."src/".$module."/".$layer."/")){
                          $l = mkdir(PATH_APP."src/".$module."/".$layer."/");
                        }
                        if($m and $l){
                          $log .= " - Pronto para atualizar $file<br>";

                          $dest = PATH_APP."src/".$module."/".$layer."/".$file;
                          if(file_exists($dest)){
                            $copy->addFile($dest);
                            $log .= " &nbsp; C�pia de Seguran�a de $file realizada<br>";

                          }
                          copy($source, $dest);
                          $log .= " &nbsp; O Arquivo src/".$module."/".$layer."/".$file." foi atualizado<br>";

                        }else{
                          $log .= " :: Problemas ao tentar processar src/".$module."/".$layer."/".$file."<br>";
                        }
                      }
                    }
                  }
                }
              }
            }
            $copy->close();
          }else{
            System::showMessage("N�o foi poss�vel descompactar a atualiza��o em: ".PATH_APP."files/update/restore/".VERSION.".zip");
          }

          System::desire('c', 'manager', 'Parametros', 'src', true, '');

          $parametroCtrl = new ParametrosCtrl(PATH_APP);

          $version = $parametroCtrl->updateVersionParametrosCtrl($object->get_atl_value('atl_tipo'));
          $upload = PATH_APP."files/update/upload/".$version.".zip";
          if(copy($atl_arquivo, $upload)){
            unlink($atl_arquivo);
            $log .= " - Arquivos tempor�rios removidos<br>";
          }

          $object->set_atl_value('atl_arquivo', $upload);
          $object->set_atl_value('atl_versao', $version);

          $log .= " - Atualiza��o que inicia a vers�o $version foi salva em $upload<br>";

          $this->setAtualizacaoCtrl($object, true, false, false);
          $after = $after && File::delTree($updating);

          $log .= " - Todos os arquivos tempor�rios foram removidos<br>";
          $log .= "<br><b>Atualiza��o realizada com sucesso</b><br>";

          $log = "'".$log."'";
          $title = "'".TITLE." - "."Detalhes da Atualiza��o"."'";
          System::showMessage('Veja os detalhes da atualiza��o clicando <a href="javascript:void(0);" onclick="system.window.showMessage('.$title.', '.$log.', true, 800, 600, null, true, true)">aqui</a>', SHOW_MESSAGE_SUCCESS);
        }

      }else{
        System::showMessage("Arquivo de atualiza&ccedil;&atilde;o n&atilde;o encontrado: ".$atl_arquivo);
      }

    }

    return $after;
  }

  /**
   * Recupera um dado atrav�s de uma consulta personalizada
   * 
   * @param string $column
   * @param string $where
   * @param string $table
   * @param boolean $validate
   * @param boolean $debug
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 10/05/2013 15:25:45
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 10/05/2013 15:25:45
   */
  public function getColumnAtualizacaoCtrl($column, $where, $table = "", $validate = true, $debug = false){
    ?><?php
    System::desire("d", 'manager', 'Atualizacao', 'src', true, '');
    
    $atualizacaoDAO = new AtualizacaoDAO($this->path, $this->database);
    $column = $atualizacaoDAO->getColumnAtualizacaoDAO($column, $where, $table, $validate, $debug);
    
    return $column;
  }

  /**
   * Remove um grupo de items ou atualiza uma quantidade relativamente grande 
   * 
   * @param string $where
   * @param string $update
   * @param boolean $validate
   * @param boolean $debug
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 10/05/2013 15:25:45
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 10/05/2013 15:25:45
   */
  public function executeAtualizacaoCtrl($where, $update, $validate = true, $debug = false){
  ?><?php
    System::desire('d', 'manager', 'Atualizacao', 'src', true, '');

    $atualizacaoDAO = new AtualizacaoDAO($this->path, $this->database);
    $executed = $atualizacaoDAO->executeAtualizacaoDAO($where, $update, $validate, $debug);

    return $executed;
  }


}