<?php
/**
 * @copyright array software
 *
 * @author ADMINISTRADOR - 18/01/2013 11:31:53
 * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 09/05/2013 15:32:19
 * @category controller
 * @package manager
 *
 * Responsavel.....: WILLIAM MARQUES VICENTE GOMES CORREA
 * Alteracao.......: 09/05/2013 22:25:52
 */


class ParametrosCtrl
{
  private  $path;
  private  $database;

  /**
   * Construtor da classe de processamento e interface de acesso a dados da entidade
   *
   * @param string $path
   * @param string $database
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 02/05/2013 19:15:17
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 02/05/2013 19:15:17
   */
  public function ParametrosCtrl($path, $database = ""){
    ?><?php
    $this->path = $path;
    $this->database = $database;

    return $this;
  }

  /**
   * Remove uma instancia da entidade da base de dados
   *
   * @param object $object
   * @param boolean $validate
   * @param boolean $debug
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 02/05/2013 19:15:17
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 02/05/2013 19:15:17
   */
  public function removeParametrosCtrl($object, $validate = true, $debug = false){
    ?><?php
    System::desire("d", 'manager', 'Parametros', 'src', true, '');

    $parametrosDAO = new ParametrosDAO($this->path, $this->database);
    $this->beforeParametrosCtrl($object, "remove");
    $remove = $parametrosDAO->removeParametrosDAO($object, $validate, $debug);
    $this->afterParametrosCtrl($object, "remove");

    return $remove;
  }

  /**
   * Recupera um dado atrav�s de uma consulta personalizada
   *
   * @param string $column
   * @param string $where
   * @param string $table
   * @param boolean $validate
   * @param boolean $debug
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 02/05/2013 19:15:17
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 02/05/2013 19:15:17
   */
  public function getColumnParametrosCtrl($column, $where, $table = "", $validate = true, $debug = false){
    ?><?php
    System::desire("d", 'manager', 'Parametros', 'src', true, '');

    $parametrosDAO = new ParametrosDAO($this->path, $this->database);
    $column = $parametrosDAO->getColumnParametrosDAO($column, $where, $table, $validate, $debug);

    return $column;
  }

  /**
   * Recupera inst�ncias da entidade na base de dados
   *
   * @param string $where
   * @param string $group
   * @param string $order
   * @param string $start
   * @param string $end
   * @param boolean $validate
   * @param boolean $debug
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 02/05/2013 19:15:17
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 02/05/2013 19:15:17
   */
  public function getParametrosCtrl($where, $group, $order, $start = "", $end = "", $validate = true, $debug = false){
    ?><?php
    System::desire("d", 'manager', 'Parametros', 'src', true, '');

    $parametrosDAO = new ParametrosDAO($this->path, $this->database);
    $parametross = $parametrosDAO->getParametrosDAO($where, $group, $order, $start, $end, $validate, $debug);

    return $parametross;
  }

  /**
   * Atualiza uma inst�ncia da entidade na base de dados
   *
   * @param object $object
   * @param boolean $validate
   * @param boolean $debug
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 02/05/2013 19:15:17
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 02/05/2013 19:15:17
   */
  public function setParametrosCtrl($object, $validate = true, $debug = false){
    ?><?php
    System::desire("d", 'manager', 'Parametros', 'src', true, '');

    $parametrosDAO = new ParametrosDAO($this->path, $this->database);
    $this->beforeParametrosCtrl($object, "set");
    $set = $parametrosDAO->setParametrosDAO($object, $validate, $debug);
    $this->afterParametrosCtrl($object, "set");

    return $set;
  }

  /**
   * Recupera um valor inteiro referente ao n�mero de linhas de determina consulta
   *
   * @param string $where
   * @param string $group
   * @param boolean $validate
   * @param boolean $debug
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 02/05/2013 19:15:17
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 02/05/2013 19:15:17
   */
  public function getCountParametrosCtrl($where, $group = "", $validate = true, $debug = false){
    ?><?php
    System::desire("d", 'manager', 'Parametros', 'src', true, '');

    $parametrosDAO = new ParametrosDAO($this->path, $this->database);
    $total = $parametrosDAO->getCountParametrosDAO($where, $group, $validate, $debug);

    return $total;
  }

  /**
   * M�todo de gatilho executado antes de cada atualiza��o e remo��o das inst�ncias da entidade
   *
   * @param object $object
   * @param string $param
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 02/05/2013 19:15:17
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 02/05/2013 19:15:17
   */
  public function beforeParametrosCtrl($object, $param){
    ?><?php
    return false;
  }

  /**
   * Adiciona um novo registro desta entidade na base de dados
   *
   * @param object $object
   * @param boolean $validate
   * @param boolean $debug
   * @param int $copy
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 02/05/2013 19:15:17
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 02/05/2013 19:15:17
   */
  public function addParametrosCtrl($object, $validate = true, $debug = false, $copy = 0){
    ?><?php
    System::desire("d", 'manager', 'Parametros', 'src', true, '');

    $parametrosDAO = new ParametrosDAO($this->path, $this->database);
    $this->beforeParametrosCtrl($object, "add");
    $add = $parametrosDAO->addParametrosDAO($object, $validate, $debug);
    $this->afterParametrosCtrl($object, "add", $add, $copy);

    return $add;
  }

  /**
   * M�todo de gatilho executado depois de cada atualiza��o e remo��o das inst�ncias da entidade
   *
   * @param object $object
   * @param string $param
   * @param int $value
   * @param int $copy
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 02/05/2013 19:15:17
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 09/05/2013 15:30:46
   */
  public function afterParametrosCtrl($object, $param, $value = 0, $copy = 0){
?><?php

    $add = false;

    if($param === 'set' || $param === 'add'){
      $items = $object->get_par_items();

      $try = 0;
      $added = 0;

      foreach ($items as $item) {
        if($item['type'] === 'select-multi'){
          if($param === 'set'){
            $value = $object->get_par_value($object->get_par_reference());
          }
          if(isset($item['select-multi'])){
            $select = $item['select-multi'];

            $module = $select['module'];
            $entity = $select['entity'];
            $prefix = $select['prefix'];

            System::desire('c', $module, $entity, 'src');
            System::desire('m', $module, $entity, 'src');

            $remove = "execute".$entity."Ctrl";
            $set_value = 'set_'.$prefix.'_value';
            $add = "add".$entity."Ctrl";

            $entityCtrl = $entity.'Ctrl';
            $objectCtrl = new $entityCtrl(PATH_APP);

            $objectCtrl->$remove($select['foreign']."='".$value."'");

            $values = $item['value'];
            foreach ($values as $v) {
              $object = new $entity();
              $object->$set_value($select['foreign'],$value);
              $object->$set_value($select['source'],$v);
              $object->$set_value($prefix.'_responsavel',System::getUser(false));
              $object->$set_value($prefix.'_criador',System::getUser(false));

              $w = $objectCtrl->$add($object);
              $try++;
              if($w){
                $added++;
              }
            }
          }
        }
      }

      $add = $try == $added;

      if($copy > 0){
        // input here the code to create the complete copy of instance
      }

    }

    require System::desire('file', '', 'header', 'core', false);

    System::desire('m', 'manager', 'Parametros', 'src', true, '');

    $parametros = $this->getParametrosCtrl("","","par_variavel","","" );
    $total = count($parametros);
    $source = "";
    $file = "";
    $file =
            "<?php\n".
            "/**\n".
            "* Faculdade Ubaense Ozanam Coelho - Fagoc\n".
            "* Projeto SIGA - 2010\n".
            "* Modulo..........: Principal\n".
            "* Funcionalidade..: Inicializa variaveis utilizadas no sistema\n".
            "* Pre-requisitos..: Nao possui\n".
            "* Data............: ".(date('d/m/Y H:i:s'))."\n".
            "* Responsavel.....: ".(System::getUser(false))."\n".
            "*/\n".
            "\n".
            "if(basename(\$_SERVER[\"PHP_SELF\"])== \"parameters.php\"){\n".
            "  die(\"<h1>Not Found</h1><p>The requested URL was not found on this server.</p><hr><address>Apache/2.2.9 (Debian)</address>\");\n".
            "}\n";

    for($i = 0; $i < $total; $i++) {

      $param_var = "\$". $parametros[$i]->get_par_value('par_variavel');
      $param_type = "String";

      if($parametros[$i]->get_par_value('par_string') == 1) {
        $param_value = '"'.$parametros[$i]->get_par_value('par_valor').'"';
        $source .= "\$".$parametros[$i]->get_par_value('par_variavel')." = \"".$parametros[$i]->get_par_value('par_valor')."\";<br>";
      }else {
        $param_value = $parametros[$i]->get_par_value('par_valor');
        $source .= "\$".$parametros[$i]->get_par_value('par_variavel')." = ".$parametros[$i]->get_par_value('par_valor').";<br>";
      }
      $file .= $param_var." = ". $param_value .";\n";

      $upper = strtoupper($parametros[$i]->get_par_value('par_variavel'));
      $clear = str_replace("[\"", "_", $upper);
      $constant = str_replace("\"]", "", $clear);

      if (is_numeric($param_value)) {
        $param_type = "Numeric";
      } else if ($param_value == 'true' || $param_value == 'false') {
        $param_type = 'Boolean';
      }

      if($parametros[$i]->get_par_value('par_procedimento') != "") {
        $file .= "\n".$parametros[$i]->get_par_value('par_procedimento')."\n\n";
        $source .= "<br>".$parametros[$i]->get_par_value('par_procedimento')."<br><br>";

        $param_value = $param_var;
      }

      if (substr($parametros[$i]->get_par_value('par_valor'), 0, 6) == "array(") {
        $param_value = "serialize(".$param_value.")";
        $param_type = "Array";
      }

      $file .= "if (!defined(\"". $constant ."\")) {\n"
             . "  /**\n"
             . "   * ". htmlentities($parametros[$i]->get_par_value('par_descricao')) ."<br>\n"
             . "   * @name ". $constant ."\n"
             . "   * @var ". $param_type ."\n"
             . "   * @since ". $parametros[$i]->get_par_value('par_version') ."\n"
             . "   * @author ". $parametros[$i]->get_par_value('par_criador') ." (". $parametros[$i]->get_par_value('par_registro') .")\n"
             . "   * <br><b>Updated by</b> ". $parametros[$i]->get_par_value('par_responsavel') ." (". $parametros[$i]->get_par_value('par_alteracao') .")\n"
             . "   */\n"
             . "  define(\"". $constant ."\", ". $param_value .");\n"
             . "}\n\n";
    }
    /*$file .="?>";*/

    $write = File::saveFile(PATH_APP."core/parameters.php", $file, 'w', false);

    return $write;
  }

  /**
   *
   * @param type $tipo
   */
  public function updateVersionParametrosCtrl($tipo){
    ?><?php

    $parametros = $this->getParametrosCtrl("par_variavel = 'version'", "", "");
    $new = "";
    if(is_array($parametros)){
      $parametro = $parametros[0];
      $old = $parametro->get_par_value('par_valor');
      $complex = explode(".", $old);
      $version = array();
      $up = ($tipo - 1);
      for($i = 0; $i < 4; $i++){
        $version[$i] = isset($complex[$i]) ? $complex[$i] : "0";
        if($i === $up){
          $version[$i] = ((int) $version[$i]) + 1;
        }
        if($i > $up){
          $version[$i] = "0";
        }
      }
      $new = join(".", $version);
      $parametro->set_par_value('par_valor', $new);

      $this->setParametrosCtrl($parametro);
    }
    return $new;
  }

}