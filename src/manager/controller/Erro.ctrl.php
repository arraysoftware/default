<?php
/**
 * @copyright array software
 *
 * @author WILLIAM MARQUES VICENTE GOMES CORREA - 27/04/2013 21:29:44
 * <br><b>Updated by</b> ADMINISTRADOR - 15/06/2013 19:24:55
 * @category controller
 * @package manager
 *
 * Responsavel.....: ADMINISTRADOR
 * Alteracao.......: 28/07/2013 09:30:04
 */


class ErroCtrl
{
  private  $path;
  private  $database;

  /**
   * Construtor da classe de processamento e interface de acesso a dados da entidade
   * 
   * @param string $path
   * @param string $database
   *
   * @author ADMINISTRADOR - 15/06/2013 19:17:22
   * <br><b>Updated by</b> ADMINISTRADOR - 15/06/2013 19:17:22
   */
  public function ErroCtrl($path, $database = ""){
    ?><?php
    $this->path = $path;
    $this->database = $database;

    return $this;
  }

  /**
   * Recupera inst�ncias da entidade na base de dados
   * 
   * @param string $where
   * @param string $group
   * @param string $order
   * @param string $start
   * @param string $end
   * @param boolean $validate
   * @param boolean $debug
   *
   * @author ADMINISTRADOR - 15/06/2013 19:17:22
   * <br><b>Updated by</b> ADMINISTRADOR - 15/06/2013 19:17:22
   */
  public function getErroCtrl($where, $group, $order, $start = "", $end = "", $validate = true, $debug = false){
    ?><?php
    System::desire("d", 'manager', 'Erro', 'src', true, '{project.rsc}');
    
    $erroDAO = new ErroDAO($this->path, $this->database);
    $erros = $erroDAO->getErroDAO($where, $group, $order, $start, $end, $validate, $debug);
    
    return $erros;
  }

  /**
   * Recupera um valor inteiro referente ao n�mero de linhas de determina consulta
   * 
   * @param string $where
   * @param string $group
   * @param boolean $validate
   * @param boolean $debug
   *
   * @author ADMINISTRADOR - 15/06/2013 19:17:22
   * <br><b>Updated by</b> ADMINISTRADOR - 15/06/2013 19:17:22
   */
  public function getCountErroCtrl($where, $group = "", $validate = true, $debug = false){
    ?><?php
    System::desire("d", 'manager', 'Erro', 'src', true, '{project.rsc}');
    
    $erroDAO = new ErroDAO($this->path, $this->database);
    $total = $erroDAO->getCountErroDAO($where, $group, $validate, $debug);
    
    return $total;
  }

  /**
   * Adiciona um novo registro desta entidade na base de dados
   * 
   * @param object $object
   * @param boolean $validate
   * @param boolean $debug
   * @param boolean $presave
   * @param int $copy
   *
   * @author ADMINISTRADOR - 15/06/2013 19:17:22
   * <br><b>Updated by</b> ADMINISTRADOR - 15/06/2013 19:17:22
   */
  public function addErroCtrl($object, $validate = true, $debug = false, $presave = false, $copy = 0){
    ?><?php
    System::desire("d", 'manager', 'Erro', 'src', true, '{project.rsc}');    
    
    $erroDAO = new ErroDAO($this->path, $this->database);
    $this->beforeErroCtrl($object, "add");
    $add = $erroDAO->addErroDAO($object, $validate, $debug, $presave, $copy);
    $this->afterErroCtrl($object, "add", $add, $copy);
    
    return $add;
  }

  /**
   * Remove uma instancia da entidade da base de dados
   * 
   * @param object $object
   * @param boolean $validate
   * @param boolean $debug
   *
   * @author ADMINISTRADOR - 15/06/2013 19:17:22
   * <br><b>Updated by</b> ADMINISTRADOR - 15/06/2013 19:17:22
   */
  public function removeErroCtrl($object, $validate = true, $debug = false){
    ?><?php
    System::desire("d", 'manager', 'Erro', 'src', true, '{project.rsc}');
    
    $erroDAO = new ErroDAO($this->path, $this->database);
    $this->beforeErroCtrl($object, "remove");
    $remove = $erroDAO->removeErroDAO($object, $validate, $debug);
    $this->afterErroCtrl($object, "remove");
    
    return $remove;
  }

  /**
   * Atualiza uma inst�ncia da entidade na base de dados
   * 
   * @param object $object
   * @param boolean $validate
   * @param boolean $debug
   * @param boolean $trigger
   *
   * @author ADMINISTRADOR - 15/06/2013 19:17:22
   * <br><b>Updated by</b> ADMINISTRADOR - 15/06/2013 19:17:22
   */
  public function setErroCtrl($object, $validate = true, $debug = false, $trigger = true){
    ?><?php
    System::desire("d", 'manager', 'Erro', 'src', true, '{project.rsc}');
    
    $erroDAO = new ErroDAO($this->path, $this->database);
    $this->beforeErroCtrl($object, "set");
    $set = $erroDAO->setErroDAO($object, $validate, $debug, $trigger);
    if($trigger === true){
			$this->afterErroCtrl($object, "set");
		}
    
    return $set;
  }

  /**
   * M�todo de gatilho executado antes de cada atualiza��o e remo��o das inst�ncias da entidade
   * 
   * @param object $object
   * @param string $param
   *
   * @author ADMINISTRADOR - 15/06/2013 19:17:22
   * <br><b>Updated by</b> ADMINISTRADOR - 15/06/2013 19:17:22
   */
  public function beforeErroCtrl($object, $param){
    ?><?php

    if($param === 'add' or $param === 'set'){

      $items = $object->get_err_items();
      
      foreach($items as $item){
        if($item['type_behavior'] == 'parent'){
          if(isset($item['parent'])){
            $class = $item['parent']['entity'];
            System::desire('c', $item['parent']['modulo'], $class, 'src', true);
            $classCtrl = $class."Ctrl";
            $obj = new $classCtrl(PATH_APP);
            $entity = $item['value'];
            if($param === 'add'){
              $action = "add".$class."Ctrl";
            }else{
              $action = "set".$class."Ctrl";
            }
            $value = $obj->$action($entity);
            if($param === 'add'){
              $object->set_err_value($item['id'], $value);
            }else{
              $get_value = "get_".$item['parent']['prefix']."_value";
              $object->set_err_value($item['id'], $entity->$get_value($item['parent']['key']));
            }
          }
        }
      }


    }

    return $object;
  }

  /**
   * M�todo de gatilho executado depois de cada atualiza��o e remo��o das inst�ncias da entidade
   * 
   * @param object $object
   * @param string $param
   * @param int $value
   * @param int $copy
   *
   * @author ADMINISTRADOR - 15/06/2013 19:17:22
   * <br><b>Updated by</b> ADMINISTRADOR - 15/06/2013 19:46:03
   */
  public function afterErroCtrl($object, $param, $value = 0, $copy = 0){
?><?php
    
    $add = false;
    
    if($param === 'set'){
      $value = $object->get_err_value($object->get_err_reference());
    }

    if($param === 'add'){
      $object->set_err_value($object->get_err_reference(), $value);
    }

		if($object->get_err_value('err_corrigido')){
      $err_descricao = $this->getColumnErroCtrl("err_descricao", "err_codigo = '".$object->get_err_value('err_codigo')."'");
      if($err_descricao){
        $this->executeErroCtrl("err_descricao = '".addslashes($err_descricao)."'", "err_corrigido = 1");
      }
    }

    if($param === 'set' or $param === 'add'){
			
      $items = $object->get_err_items();

      $try = 0;
      $added = 0;
      
      foreach ($items as $item) {
        if($item['type'] === 'select-multi'){
          
          if(isset($item['select-multi'])){
            $select = $item['select-multi'];

            $module = $select['module'];
            $entity = $select['entity'];
            $prefix = $select['prefix'];

            System::desire('c', $module, $entity, 'src');
            System::desire('m', $module, $entity, 'src');

            $remove = "execute".$entity."Ctrl";
            $set_value = 'set_'.$prefix.'_value';
            $add = "add".$entity."Ctrl";

            $entityCtrl = $entity.'Ctrl';
            $objectCtrl = new $entityCtrl(PATH_APP);

            $objectCtrl->$remove($select['foreign']."='".$value."'","");

            $values = $item['value'];
            foreach ($values as $v) {
							if($v){
								$object = new $entity();
								$object->$set_value($select['foreign'],$value);
								$object->$set_value($select['source'],$v);
								$object->$set_value($prefix.'_responsavel',System::getUser());
								$object->$set_value($prefix.'_criador',System::getUser());
	
								$w = $objectCtrl->$add($object);
								$try++;
								if($w){
									$added++;
								}
							}
            }
          }
        }
      }
      
      $add = $try == $added;
      
      if($copy > 0){
        // input here the code to create the complete copy of instance
      }
      
    }

    return $add;
  }

  /**
   * Recupera um dado atrav�s de uma consulta personalizada
   * 
   * @param string $column
   * @param string $where
   * @param string $table
   * @param boolean $validate
   * @param boolean $debug
   *
   * @author ADMINISTRADOR - 15/06/2013 19:17:22
   * <br><b>Updated by</b> ADMINISTRADOR - 15/06/2013 19:17:22
   */
  public function getColumnErroCtrl($column, $where, $table = "", $validate = true, $debug = false){
    ?><?php
    System::desire("d", 'manager', 'Erro', 'src', true, '{project.rsc}');
    
    $erroDAO = new ErroDAO($this->path, $this->database);
    $column = $erroDAO->getColumnErroDAO($column, $where, $table, $validate, $debug);
    
    return $column;
  }

  /**
   * Remove um grupo de items ou atualiza uma quantidade relativamente grande 
   * 
   * @param string $where
   * @param string $update
   * @param boolean $validate
   * @param boolean $debug
   *
   * @author ADMINISTRADOR - 15/06/2013 19:17:22
   * <br><b>Updated by</b> ADMINISTRADOR - 15/06/2013 19:17:22
   */
  public function executeErroCtrl($where, $update, $validate = true, $debug = false){
  ?><?php
    System::desire('d', 'manager', 'Erro', 'src', true, '{project.rsc}');

    $erroDAO = new ErroDAO($this->path, $this->database);
    $executed = $erroDAO->executeErroDAO($where, $update, $validate, $debug);

    return $executed;
  }

  /**
   * M�todo respons�vel por realizar a valida��o dos dados recebidos pelo post
   * 
   * @param object $object
   *
   * @author ADMINISTRADOR - 15/06/2013 19:17:23
   * <br><b>Updated by</b> ADMINISTRADOR - 15/06/2013 19:17:23
   */
  public function verifyErroCtrl($object){
	?><?php

	$items = $object->get_err_items();
	$error_messages = array();

	foreach ($items as $key => $item) {
		if (!is_null($item['value'])) {
			/*
			 * Validation comes here!
			 * If the validation fails, add an item in the array $error_messages
			 * array("id"=>$item['id'],"description"=>$item['description'],"type"=>$item['type'],"value"=>$item['value'],"message"=>"{MESSAGE}");
			 */
		}
	}

	return $error_messages;
  }


}