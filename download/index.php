<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

if(isset($_GET["file"])){

  $file = System::request("file");
  $file = str_replace("../","", $file);
  $file = str_replace(PATH_APP,"", $file);
  $error = true;

  $code = "404";
  $title = "Not Found";
  $message = "The requested URL was not found on this server";

  $valids = array("files", "project");

  $data = explode("/", $file);
  $dir = $data[0];
  $name = $data[count($data)-1];

  if(in_array($dir, $valids, true)){
    $file = PATH_APP.$file;
    if(file_exists($file)){

      header("Content-Type: application/force-download");
      header("Content-Type: application/octet-stream");
      header("Content-Type: application/download");
      header("Content-Type: application/save");
      header("Content-Description: File Transfer");
      header("Content-Transfer-Encoding: binary");
      header("Content-Disposition: attachment; filename=".urlencode($name)."");
      header("Content-Length:".filesize($file));
      header("Expires: 0");
      header("Pragma: no-cache");

      // nesse momento ele le o arquivo e envia
      $fp = fopen($file, "r");
      fpassthru($fp);
      fclose($fp);

      $error = false;
    }else{
      $code = "404";
      $title = "File Not Found";
      $message = "Sorry: There are not any file in this way";
    }
  }else{
    $code = "403";
    $title = "Forbidden";
    $message = "Forbidden: You don't have permission to access this file on this server";
  }

  if($error){
    printMessageDownload($code, $title, $message);
  }
}

/**
 *
 * @param <type> $code
 * @param <type> $title
 * @param <type> $message
 */
function printMessageDownload($code = "", $title = "", $message = ""){
  ?>
    <!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML 2.0//EN">
    <html>
      <head>
        <title><?php print $code;?> <?php print $title;?></title>
      </head>
      <body>
        <h1><?php print $title;?></h1>
        <p><?php print $message;?>.</p>
        <hr>
        <address>Apache/2.2.16 (Debian) Server Port 80</address>
      </body>
    </html>
  <?
}

?>
